<?php
class JConfig {
	public $MetaAuthor = '1';
	public $MetaDesc = '';
	public $MetaKeys = '';
	public $MetaRights = '';
	public $MetaTitle = '1';
	public $MetaVersion = '1';
	public $access = '1';
	public $cache_handler = 'file';
	public $cachetime = '15';
	public $caching = '0';
	public $captcha = '0';
	public $cookie_domain = '';
	public $cookie_path = '';
	public $db = 'magazine';
	public $dbprefix = 'orcgf_';
	public $dbtype = 'mysqli';
	public $debug = '1';
	public $debug_lang = '0';
	public $display_offline_message = '1';
	public $editor = 'jce';
	public $error_reporting = 'development';
	public $feed_email = 'author';
	public $feed_limit = '10';
	public $force_ssl = '0';
	public $fromname = 'Joomla Community Magazine';
	public $ftp_enable = '0';
	public $ftp_host = '127.0.0.1';
	public $ftp_pass = '';
	public $ftp_port = '21';
	public $ftp_root = '';
	public $ftp_user = 'ftplayer@magazine.joomla.org';
	public $gzip = '1';
	public $helpurl = 'https://help.joomla.org/proxy/index.php?option=com_help&keyref=Help{major}{minor}:{keyref}';
	public $host = 'localhost';
	public $lifetime = '60';
	public $list_limit = '50';
	//public $live_site = 'http://magazine.joomla.org/sandbox1';
	public $log_path = '/Users/peterbui/Documents/projects/joomla-community-magazine/log';
	public $mailer = 'mail';
	public $mailfrom = 'magazine@community.joomla.org';
	//public $memcache_compress = '0';
	//public $memcache_persist = '1';
	//public $memcache_server_host = 'localhost';
	//public $memcache_server_port = '11211';
	public $offline = '1';
	public $offline_image = '';
	public $offline_message = 'This site is down for maintenance.<br /> Please check back again soon.';
	public $offset = 'UTC';
	public $offset_user = 'UTC';
	public $password = 'root';
	public $robots = 'noindex, nofollow';
	public $secret = 'qBvDyngNkfaTji6C156lfQssj5lhMhA5';
	public $sef = '1';
	public $sef_rewrite = '0';
	public $sef_suffix = '0';
	public $sendmail = '/usr/sbin/sendmail';
	public $session_handler = 'database';
	public $sitename = 'Joomla! Community Magazine™';
	public $sitename_pagetitles = '0';
	public $smtpauth = '0';
	public $smtphost = 'localhost';
	public $smtppass = '';
	public $smtpport = '25';
	public $smtpsecure = 'none';
	public $smtpuser = '';
	public $tmp_path = '/Users/peterbui/Documents/projects/joomla-community-magazine/tmp';
	public $unicodeslugs = '0';
	public $user = 'root';
	public $memcached_persist = '1';
	public $memcached_compress = '0';
	public $memcached_server_host = 'localhost';
	public $memcached_server_port = '11211';
	//public $redis_persist = '1';
	//public $redis_server_host = 'localhost';
	//public $redis_server_port = '6379';
	//public $redis_server_auth = '';
	//public $redis_server_db = '0';
	public $proxy_enable = '0';
	public $proxy_host = '';
	public $proxy_port = '';
	public $proxy_user = '';
	public $proxy_pass = '';
	public $mailonline = '1';
	public $massmailoff = '1';
	//public $session_memcache_server_host = 'localhost';
	//public $session_memcache_server_port = '11211';
	//public $session_memcached_server_host = 'localhost';
	//public $session_memcached_server_port = '11211';
	public $frontediting = '0';
	public $asset_id = '1';
	public $cache_platformprefix = '0';
}
