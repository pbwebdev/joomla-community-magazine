<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Content.translate
 *
 * @copyright   Copyright (C) 2014 WebSpark, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Translate plugin.
 *
 * @package     Joomla.Plugin
 * @subpackage  Content.translate
 * @since       1.5
 */
class PlgContentTranslate extends JPlugin
{

  /**
   * Load the translations
   *
   * @param   string   $context  The context of the content being passed to the plugin
   * @param   object   &$row     The article object
   * @param   object   &$params  The article params
   * @param   integer  $page     The 'page' number
   *
   * @since   1.6
   */
  public function onContentAfterDisplay($context, &$row, &$params, $page=0)
  {
    if ($context != 'com_k2.relatedByTag') 
    {
       $document = JFactory::getDocument();
        $script   = "window.addEvent('domready', function() {
                      var html = '<ul class=\"unstyled\" style=\"float: left;\" id=\"ajaxLanguage\">";

        $script .= "<li><a href=\"javascript:void(0);\" onclick=\"loadContent(\'" . $row->id . "\');\"><img src=\"http://magazine.joomla.org/media/mod_languages/images/en.gif\" /></a></li>";

        $db = JFactory::getDbo();
        
        $query = $db->getQuery(TRUE);
        $query->select('i.id');
        $query->select('i.metakey');
        $query->from('#__k2_items AS i');
        $query->where('i.extra_fields_search = ' . $row->id);
        $db->setQuery($query);

        $translations = $db->loadObjectList();
        
        foreach ($translations as $translation)
        {
          $script .= "<li><a href=\"javascript:void(0);\" onclick=\"loadContent(\'" . $translation->id . "\');\"><img src=\"http://magazine.joomla.org/media/mod_languages/images/" . $translation->metakey . ".gif\" /></a></li>";
        }

        $script .= "</ul>'; 
                    jQuery('.itemToolbar').prepend(html); });
                    ";

        $loadContentJS = "function loadContent(langId) {
                            jQuery('h1.itemTitle').append('<img src=\"http://magazine.joomla.org/images/ajax.gif\" />');
                            var articleUrl = 'index.php?option=com_k2&view=item&ajax=1&id='+langId;
                            jQuery.ajax({
                                    url: articleUrl,
                                    type: 'POST',
                                    dataType: 'HTML',
                                    success: function (data) {
                                        if (data) {
                                            jQuery('div.itemBody').html(jQuery(data).find('div.itemBody').html());
                                            jQuery('.itemTitle').html(jQuery(data).find('.itemTitle').html());
                                            if(langId != '" . $row->id . "') {
                                              jQuery('span.itemAuthor').html(jQuery(data).find('span.itemAuthor').html().replace('Written by','Translated by'));
                                            } else {
                                              jQuery('span.itemAuthor').html(jQuery(data).find('span.itemAuthor').html());
                                            }
                                        }
                                    }
                                });
                            }";

        $document->addScriptDeclaration($script);
        $document->addScriptDeclaration($loadContentJS);
    }
  }

  /**
   * Remove extra fields from ajax loaded items
   *
   * @param   string   $context  The context of the content being passed to the plugin
   * @param   object   &$row     The article object
   * @param   object   &$params  The article params
   * @param   integer  $page     The 'page' number
   *
   * @since   1.6
   */
  public function onContentBeforeDisplay($context, &$row, &$params, $page=0)
  {
    $row->extra_fields = NULL;
  }
}