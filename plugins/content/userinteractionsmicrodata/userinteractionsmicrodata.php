<?php

	/**
	* @copyright			©2013 B Tasker / Viryasoftware.com
	* @author 			B Tasker <support@viryatechnologies.com>
	* @link				http://www.viryasoftware.com
	* @license 			GNU GPL V2 www.gnu.org/licenses/gpl-2.0.html
	**/

	//--- no direct access
		defined( '_JEXEC' ) or die( 'Restricted access' );
	
	//--- initialize
		jimport( 'joomla.plugin.plugin' );
		jimport( 'joomla.html.parameter' );
		
		

class plgContentuserinteractionsmicrodata extends JPlugin {

	var $pluginName = 'userinteractionsmicrodata';	
	var $pluginNameHumanReadable = 'UserLikes Microdata support';
	var $runonce = false;
	
	function plgContentuserinteractionsmicrodata( &$subject, $params ){
		parent::__construct( $subject, $params );
	}
	

	public function onContentPrepare($context, &$article, &$params, $page = 0)
	{
	//$doc = JFactory::getDocument();
	      // Get the plugin params 
	      $plugin = JPluginHelper::getPlugin('content', $this->pluginName);
	      $pluginParams = new JRegistry($plugin->params);
	      $field='text';

	      if ($context == 'com_k2.item'){
		    // k2 specific bodge - they kindly pass everything through with the context com_k2.item including items in modules
		    if (($article->id == JRequest::getVar('id') && JRequest::getVar('option') == 'com_k2')){
		    $field = $params->get('k2Field',"introtext");
		    }else{
		      return true;
		    }
		}
		// Don't run this plugin more than once perpage
		if ($this->runonce || empty($article->$field))
		{
			return true;
		}

	     
	      //$doc = JFactory::getDocument();
	      $stats = $this->getPageStats($pluginParams);
	      
	      if (!empty($stats->fb[0]->like_count) || !empty($stats->fbshurl[0])){
			$likes = ($stats->fb[0]->like_count)? $stats->fb[0]->like_count : 0;

			    if (!empty($stats->fbshurl[0])){
				$likes = $likes + $stats->fbshurl[0]->like_count;
			    }


			$schematext[] = '<meta itemprop="interactionCount" content="'.$likes.' UserLikes" />';
		  }




	      if (!empty($stats->twitter->count) || !empty($stats->twittershurl)){

			    $likes = ($stats->twitter->count)? $stats->twitter->count : 0;
			    if (!empty($stats->twittershurl)){
				$likes = $likes + $stats->twittershurl->count;
			    }



			 $schematext[] =   '<meta itemprop="interactionCount" content="'.$likes.' UserTweets" />';
		  }



	      if (!empty($stats->google) || !empty($stats->googleshurl)){
			  $likes = ($stats->google)? $stats->google : 0;
			    if (!empty($stats->googleshurl)){
				$likes = $likes + $stats->googleshurl;
			    }


			$schematext[] =   '<meta itemprop="interactionCount" content="'.$likes.' UserPlusOnes" />';
	      }

	      if (!empty($schematext)){
	      $article->$field .= "\n\n<!-- Inserted Microdata -->\n".implode("\n",$schematext);
	      }
	    // We only ever want to run once per page view or tags will be duplicated
	    // Not an issue with com_content, but K2 seem to have decided to pass everything through as an item!
	    $this->runonce = 1;

	}


  function getPageStats($params){

	      $caching= $params->get('caching','1');
	      $url = JUri::current();
	      $page = sha1($url);
	      $timeout = $params->get('timeout','3');
	      	  $doc = JFactory::getDocument();
	      $shurl = '';

if (class_exists(Sh404sefFactory)){
  
$sefConfig = & Sh404sefFactory::getConfig();
if( $sefConfig->Enabled || $sefConfig->enablePageId) {
	  Sh404sefHelperShurl::updateShurls();
         $shPageInfo = & Sh404sefFactory::getPageInfo();
	 $shurl = $shPageInfo->shURL;


    }
}
	    



	      if ($caching){

			     $conf =& JFactory::getConfig();
				// Get the current cachetime value

				  if (method_exists($conf,'getValue')){

				    $oldcachetime = $conf->getValue('config.cachetime');
				    $setfn = 'setValue';
				    }else{
				    $oldcachetime = $conf->get('config.cachetime');
				    $setfn = 'set';
				    }
				      // Set the cache time to 30 mins 
				      $conf->$setfn('config.cachetime', $params->get('cachetime','900'));

				    // Get the Cache object
				    $cache =& JFactory::getCache('plg_userinteractionsmd','output');
				    // Enable caching (if disabled in global configuration)
				    $cache->setCaching( 1 );
				    if ($json = $cache->get('page_'.$page)) {
				      // Use the cached data
				      $conf->$setfn('config.cachetime', $oldcachetime);
				      return json_decode($json);
				  }else{
					 $fdata = json_decode($this->fb_connect($timeout,$url));
					 $tdata = json_decode($this->twitter_connect($timeout,$url));

					$data = new stdClass();
					 $data->fb = $fdata->data;
					 $data->twitter = $tdata;
					 $data->google = $this->getPlusOnes($timeout,$url);


					  // SH404 shorturl support
					  if (!empty($shurl)){

					    $data->fbshurl = json_decode($this->fb_connect($timeout,rtrim(JUri::base(false),"/").$shurl))->data;
					    $data->twittershurl = json_decode($this->twitter_connect($timeout,rtrim(JUri::base(false),"/").$shurl));
					    $data->googleshurl = $this->getPlusOnes($timeout,rtrim(JUri::base(false),"/").$shurl);

					  }


					 $cache->store(json_encode($data), 'page_'.$page);
					 $conf->$setfn('config.cachetime', $oldcachetime);
					 return $data;

					 
				  }

		    }else{
		      $fdata = json_decode($this->fb_connect($timeout,$url));
		      $tdata = json_decode($this->twitter_connect($timeout,$url));

		      $data->fb = $fdata->data;
		      $data->twitter = $tdata;
		      $data->google = $this->getPlusOnes($timeout,$url);

			  if (!empty($shurl)){
					
					    $data->fbshurl = json_decode($this->fb_connect($timeout,rtrim(JUri::base(false),"/").$shurl))->data;
					    $data->twittershurl = json_decode($this->twitter_connect($timeout,rtrim(JUri::base(false),"/").$shurl));
					    $data->googleshurl = $this->getPlusOnes($timeout,rtrim(JUri::base(false),"/").$shurl);

					  }


		      return $data;
		    }


	    }


	    function twitter_connect($timeout,$url){

	      $url = "http://urls.api.twitter.com/1/urls/count.json?url=" . urlencode($url);
	      return $this->connect($timeout,$url);
	    }




	    function fb_connect($timeout,$url){


	      $query = 'SELECT like_count FROM link_stat '.
	      'WHERE url="' .$url . '"';
	      $url = "https://graph.facebook.com/fql?q=".urlencode($query);
	      return $this->connect($timeout,$url);


	      }




	      function connect($timeout,$url){


	      $ch = curl_init($url);
	      curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);
	      curl_setopt($ch,CURLOPT_TIMEOUT,$timeout);
	      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	      $data = curl_exec($ch);
	      curl_close($ch);

	      return $data;



	      }

	      function getPlusOnes($timeout,$url){
	      // Thanks to user 'cardy' saved me a lot of work! - http://stackoverflow.com/questions/8853342/how-to-get-google-1-count-for-current-page-in-php

$html = file_get_contents( "https://plusone.google.com/_/+1/fastbutton?url=".urlencode($url));
$html = explode('<div id="aggregateCount" class="Oy">', $html)[1];
return explode('</div>', $html)[0];
	      }


}

?>
