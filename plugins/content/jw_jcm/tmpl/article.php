<?php
/**
 * @version		1.0
 * @package		JCM Utilities Plugin
 * @author    JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

?>

<div class="jcmPlgSocialContainer">
	<h3><?php echo JText::_("Social sharing is caring ;)"); ?></h3>
	<div class="jcmPlgSocialTwitter">
		<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-via="joomla"><?php echo JText::_("Tweet"); ?></a>
		<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
	</div>
	<div class="jcmPlgSocialGplusone">
		<g:plusone size="medium" annotation="inline" width="120"></g:plusone>
		<script type="text/javascript">
		  (function() {
		    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		    po.src = 'https://apis.google.com/js/plusone.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		  })();
		</script>
	</div>
	<div class="jcmPlgSocialFacebook">
		<div id="fb-root"></div>
		<script type="text/javascript">
		(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) {return;}
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		</script>
		<div class="fb-like" data-href="<?php echo $output->pageUrl; ?>" data-send="false" data-width="200" data-show-faces="true"></div>
	</div>
	<div class="clr"></div>
</div>
