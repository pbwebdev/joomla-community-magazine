<?php
/**
 * @version		1.0
 * @package		JCM Utilities Plugin
 * @author    JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.html.parameter.element');

// Create a menu item selector
class JElementMenus extends JElement {

	var	$_name = 'menus';
	
	function fetchElement($name, $value, &$node, $control_name){
		
		$document =& JFactory::getDocument();
		$menus = array();
		
		// Create the 'all menus' listing
		$temp->value = '';
		$temp->text = JText::_("Select all menus");
		
		// Grab all the menus, grouped
		$menus = JHTML::_('menu.linkoptions');

		// Merge the above
		array_unshift($menus,$temp);

		// Output
		$output = JHTML::_('select.genericlist',  $menus, ''.$control_name.'['.$name.'][]', 'class="inputbox" style="width:90%;" multiple="multiple" size="12"', 'value', 'text', $value );
		
		return $output;
		
	}
	
} // end class
