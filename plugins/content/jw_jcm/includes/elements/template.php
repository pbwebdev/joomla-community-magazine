<?php 
/**
 * @version		1.0
 * @package		JCM Utilities Plugin
 * @author    JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.html.parameter.element');

class JElementTemplate extends JElement {

	// Set these parameters to suit your extensions name
	var $extensionName = 'jw_jcm'; // e.g. mod_name or plg_name
	var $extensionType = 'plugin'; // module or plugin
	var $extensionPluginFolder = 'content'; // plugin group - see all plugin groups as folders listed in /plugins/
	
	var $_name = 'template';

	function fetchElement($name, $value, & $node, $control_name) {
		
		jimport('joomla.filesystem.folder');
		
		if($this->extensionType=='module'){
			$sourceTemplatesPath = JPATH_SITE.DS.'modules'.DS.$this->extensionName.DS.'tmpl';
		} else {
			$sourceTemplatesPath = JPATH_SITE.DS.'plugins'.DS.$this->extensionPluginFolder.DS.$this->extensionName.'.php'.DS.$this->extensionName.DS.'tmpl';
		}
		
		$sourceTemplatesFolders = JFolder::folders($sourceTemplatesPath);
		
		$db =& JFactory::getDBO();
		$query = "SELECT template FROM #__templates_menu WHERE client_id = 0 AND menuid = 0";
		$db->setQuery($query);
		$defaultemplate = $db->loadResult();
		
		$templatePath = JPATH_SITE.DS.'templates'.DS.$defaultemplate.DS.'html'.DS.$this->extensionName;
		
		if (JFolder::exists($templatePath)){
			$templateFolders = JFolder::folders($templatePath);
			$folders = @array_merge($templateFolders, $sourceTemplatesFolders);
			$folders = @array_unique($folders);
		} else {
			$folders = $sourceTemplatesFolders;
		}
		
		sort($folders);
		
		$options = array();
		foreach($folders as $folder) {
			$options[] = JHTML::_('select.option', $folder, $folder);
		}
		
		array_unshift($options, JHTML::_('select.option','','-- '.JText::_('Select template').' --'));
		
		return JHTML::_('select.genericlist', $options, ''.$control_name.'['.$name.']', 'class="inputbox"', 'value', 'text', $value, $control_name.$name);
	
	}

}
