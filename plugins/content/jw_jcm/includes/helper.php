<?php
/**
 * @version		1.0
 * @package		JCM Utilities Plugin
 * @author    JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Utilities Class - tip: replace 'JCM' with plugin name to avoid same name class conflicts
class JCMHelper {

	// Load Includes
	function loadHeadIncludes($headIncludes){
		global $JCMPluginIncludes;
		$document = & JFactory::getDocument();
		if(!$JCMPluginIncludes){
			$JCMPluginIncludes=1;
			$document->addCustomTag($headIncludes);
		}
	}
	
	// Load Module Position
	function loadModulePosition($position,$style=''){
		$document	= &JFactory::getDocument();
		$renderer	= $document->loadRenderer('module');
		$params		= array('style'=>$style);
	
		$contents = '';
		foreach (JModuleHelper::getModules($position) as $mod){
			$contents .= $renderer->render($mod, $params);
		}
		return $contents;
	}
	
	// Word Limiter
	function wordLimiter($str,$limit=100,$end_char='&#8230;'){
		if (trim($str) == '') return $str;
		preg_match('/\s*(?:\S*\s*){'. (int) $limit .'}/', $str, $matches);
		if (strlen($matches[0]) == strlen($str)) $end_char = '';
		return rtrim($matches[0]).$end_char;
	}

	// Text Cleanup
	function cleanupText($string,$allowed_tags){
		//e.g. $allowed_tags = "span,a,b,p,br,img,hr,h1,h2,h3,h4";
		$allowed_tags_array = explode(",",$allowed_tags);
		$allowed_htmltags = array();
		foreach($allowed_tags_array as $tag){
			$allowed_htmltags[] .= "<".$tag.">";
		}
		$allowed_htmltags = implode("",$allowed_htmltags);
		$string = strip_tags($string, $allowed_htmltags);
		return $string;
	}
	
	// Clean HTML Tag Attributes - e.g. cleanupAttributes($string,"img,hr,h1,h2,h3,h4","style,width,height,hspace,vspace,border,class,id");
	function cleanupAttributes($string,$tag_array,$attr_array){ 
		$tag_array = explode(",",$tag_array);
		$attr_array =  explode(",",$attr_array);
		$attr = implode("|",$attr_array);
		foreach($tag_array as $tag){
			preg_match_all("#<($tag) .+?>#", $string, $matches, PREG_PATTERN_ORDER);
			foreach($matches[0] as $match){
				preg_match_all('/('.$attr.')=([\\"\\\']).+?([\\"\\\'])/', $match, $matchesAttr, PREG_PATTERN_ORDER);
				foreach($matchesAttr[0] as $attrToClean){
					$string = str_replace($attrToClean,'',$string);
					$string = preg_replace('|  +|', ' ', $string);
					$string = str_replace(' >','>',$string);
				}
			}
		}
		return $string;
	}
		
	// Plugin overrides for single template
	function getTemplatePath($pluginName,$file){
	
		$mainframe= &JFactory::getApplication();
		$p = new JObject;
		
		if(file_exists(JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.$pluginName.DS.str_replace('/',DS,$file))){
			$p->file = JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.$pluginName.DS.$file;
			$p->http = JURI::base()."templates/".$mainframe->getTemplate()."/html/{$pluginName}/{$file}";
		} else {
			$p->file = JPATH_SITE.DS.'plugins'.DS.'content'.DS.$pluginName.DS.'tmpl'.DS.$file;
			$p->http = JURI::base()."plugins/content/{$pluginName}/tmpl/{$file}";
		}
		return $p;
	}
	
	// Plugin overrides for folder templates
	function getTemplateFolderPath($pluginName,$file,$tmpl){

		$mainframe= &JFactory::getApplication();
		$p = new JObject;
		
		if(file_exists(JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.$pluginName.DS.$tmpl.DS.str_replace('/',DS,$file))){
			$p->file = JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.$pluginName.DS.$tmpl.DS.$file;
			$p->http = JURI::base()."templates/".$mainframe->getTemplate()."/html/{$pluginName}/{$tmpl}/{$file}";
		} else {
			$p->file = JPATH_SITE.DS.'plugins'.DS.'content'.DS.$pluginName.DS.'tmpl'.DS.$tmpl.DS.$file;
			$p->http = JURI::base()."plugins/content/{$pluginName}/tmpl/{$tmpl}/{$file}";
		}
		return $p;
	}

} // end class
