<?php
/**
 * @version		1.0
 * @package		JCM Utilities Plugin
 * @author    JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

class plgContentJw_jcm extends JPlugin {

  // JoomlaWorks reference parameters
  var $plgName              = "jw_jcm";
  var $plgCopyrightsStart   = "\n\n<!-- JoomlaWorks \"JCM Utilities\" Plugin (v1.0) starts here -->\n";
  var $plgCopyrightsEnd     = "\n<!-- JoomlaWorks \"JCM Utilities\" Plugin (v1.0) ends here -->\n\n";

	public function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}
  
	function plgContentJw_jcm( &$subject, $params ){
		parent::__construct( $subject, $params );
	}

	//function onPrepareContent( &$item, &$params, $limitstart ){
	public function onContentAfterDisplay($context, &$row, &$params, $page = 0) {

		// API
    $mainframe= &JFactory::getApplication();
		$document = &JFactory::getDocument();
		//$db 			= &JFactory::getDBO();
		//$user 		= &JFactory::getUser();
		//$aid 			= $user->get('aid');

		// API: Import helper utilities
		//jimport('joomla.filesystem.file');

		// Assign paths
    $sitePath = JPATH_SITE;
    $siteUrl  = substr(JURI::base(), 0, -1);

	
    // Check if plugin is enabled
    if(JPluginHelper::isEnabled('content',$this->plgName)==false) return '';

	
		// Load the plugin language file the proper way
		JPlugin::loadLanguage('plg_content_'.$this->plgName, JPATH_ADMINISTRATOR);

		jimport( 'joomla.html.parameter' );
		// ----------------------------------- Get plugin parameters -----------------------------------
		$plugin =& JPluginHelper::getPlugin('content',$this->plgName);
		$pluginParams = new JParameter( $plugin->params );

		/* Block Title */
		$variable				= $pluginParams->get('variable','');

		//$pluginTemplate = $pluginParams->get('pluginTemplate','Default'); // If override templates are in use
		//$selectedMenus = $pluginParams->get('selectedMenus'); // If menu selection is enabled

		/* Advanced */
		$debugMode			= $pluginParams->get('debugMode',0);
		if($debugMode==0) error_reporting(0); // Turn off all error reporting

		// External parameter for controlling plugin layout within modules
		if(!$params) $params = new JParameter(null);
		$parsedInModule = $params->get('parsedInModule');



		// ----------------------------------- Before plugin render -----------------------------------

		// Simple check before parsing the plugin
		if(!$row->id) return '';


		
		// Requests
		$option 		= JRequest::getCmd('option');
		$view 			= JRequest::getCmd('view');
		$layout 		= JRequest::getCmd('layout');
		$page 			= JRequest::getCmd('page');
		$secid 			= JRequest::getInt('secid');
		$catid 			= JRequest::getInt('catid');
		$itemid 		= JRequest::getInt('Itemid');
		if(!$itemid) $itemid = 999999;

		// Define plugin menu restrictions
		/*
		if (is_array($selectedMenus)){
			$menus = $selectedMenus;
		} elseif (is_string($selectedMenus) && $selectedMenus!=''){
			$menus[] = $selectedMenus;
		} elseif ($selectedMenus==''){
			$menus[] = $itemid;
		}
		*/


		// ----------------------------------- Prepare the output -----------------------------------
		// Includes
		//require_once(JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');
		require_once(dirname(__FILE__).DS.'includes'.DS.'helper.php');

		// $output = new JObject;

		// Get the page URL
		// $uri =& JURI::getInstance();
		// $output->pageUrl = $uri->_uri;


		// ----------------------------------- Render the output -----------------------------------

		// Do the replace
		if($option=='com_k2' && $view=='item'){
		
			// Render the head includes
			$pluginCSS 		= JCMHelper::getTemplatePath($this->plgName,'css/template.css');
			$pluginCSS 		= $pluginCSS->http;
			$document->addStyleSheet($pluginCSS);

			// Fetch the template
			ob_start();
			$getTemplatePath = JCMHelper::getTemplatePath($this->plgName,'article.php');
			$getTemplatePath = $getTemplatePath->file;
			include($getTemplatePath);
			$getTemplate = $this->plgCopyrightsStart.ob_get_contents().$this->plgCopyrightsEnd;
			ob_end_clean();

			// Item modifications
			return $getTemplate;
		}
		
		return "";

	} // END FUNCTION

} // END CLASS
