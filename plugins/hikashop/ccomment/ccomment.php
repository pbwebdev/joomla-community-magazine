<?php
/**
 * @package    - com_comment
 * @author     : DanielDimitrov - compojoom.com
 * @date: 06.05.13
 *
 * @copyright  Copyright (C) 2008 - 2013 compojoom.com . All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */

defined('_JEXEC') or die('Direct Access to this location is not allowed.');

class plgHikashopCcomment extends JPlugin
{

	public function onHikashopBeforeDisplayView(&$view)
	{
		$appl = JFactory::getApplication();
		// show readmore for listings
		if ($appl->isSite() && $view->getLayout() == 'listing')
		{
			JLoader::discover('ccommentHelper', JPATH_SITE . '/components/com_comment/helpers');
			foreach ($view->rows as $k => $row)
			{
				// add the category id as it is normally missing from the $row object
				$row->category_id = $view->element->category_id;
				$comments = ccommentHelperUtils::commentInit('com_hikashop', $row);
				$view->rows[$k]->extraData->afterProductName[] = $comments;
			}
		}

		// show comment form for products
		if ($appl->isSite() && $view->getLayout() == 'show')
		{
			JLoader::discover('ccommentHelper', JPATH_SITE . '/components/com_comment/helpers');
			$comments = ccommentHelperUtils::commentInit('com_hikashop', $view->element);
			$view->element->extraData->bottomEnd[] = $comments;
		}
	}

}