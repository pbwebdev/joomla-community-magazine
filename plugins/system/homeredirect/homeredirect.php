<?php
/**
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
JLoader::register('MenusHelper', JPATH_ADMINISTRATOR . '/components/com_menus/helpers/menus.php');

JLoader::register('MultilangstatusHelper', JPATH_ADMINISTRATOR.'/components/com_languages/helpers/multilangstatus.php');
/**
 * Joomla! Language Filter Plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	System.languagefilter
 * @since		1.6
 */
class plgSystemHomeredirect extends JPlugin
{

	public function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}

	public function onAfterInitialise()
	{
		$app = JFactory::getApplication();
        
		if ($app->isSite()) {
            $uri = JURI::getInstance();
            $path = $uri->getPath();
            $parts = explode('/', $path);
            
            $db = JFactory::getDBO();
            /*
            // http://magazine.joomla.org/home/item/1569-raising-the-bar
            if($parts[0] == "home" && $parts[1] == "item") {
                $id = (int) $parts[2];
                if($id > 1) {
                    $query = 'SELECT c.alias FROM #__k2_items as i join #__k2_categories as c on i.catid = c.id WHERE i.id = "'.$id.'"';
                    $db->setQuery($query);
                    $result = $db->loadResult();
                    $targetURL = '/issues/'.$result.'/item/'.$parts[2];
                    $app->redirect($targetURL, '', 'message', true);
                } 
            }
            // http://magazine.joomla.org/es/portada/item/1570-editorial-octubre
            if($parts[1] == "portada" && $parts[2] == "item") {
                $id = (int) $parts[3];
                if($id > 1) {
                    $query = 'SELECT c.alias FROM #__k2_items as i join #__k2_categories as c on i.catid = c.id WHERE i.id = "'.$id.'"';
                    $db->setQuery($query);
                    $result = $db->loadResult();
                    $targetURL = '/es/ediciones-anteriores/'.$result.'/item/'.$parts[3];
                    $app->redirect($targetURL, '', 'message', true);
                }
            }
            */
		}
	}
}
