<?php
// no direct access
defined('_JEXEC') or die ('Restricted access');
JLoader::register('K2Plugin', JPATH_ADMINISTRATOR.DS.'components'.DS.'com_k2'.DS.'lib'.DS.'k2plugin.php');

class plgK2Extra_info_fields extends K2Plugin {

	var $pluginName = 'extra_info_fields';
	var $pluginNameHumanReadable = 'Extra Information';

	function plgK2Extra_info_fields( & $subject, $params) {
		parent::__construct($subject, $params);
	}

	function onK2PrepareContent( & $item, & $params, $limitstart) {
	
		jimport( 'joomla.html.parameter' );
		$plugin = & JPluginHelper::getPlugin('k2', 'extra_info_fields');
		$pluginParams = new JParameter($plugin->params);
	
		$plugins = new K2Parameter($item->plugins, '', $this->pluginName);
		
		$extra_info = $plugins->get('extra_info');
		$item->level = $extra_info;
		
		$creditsUrl = $plugins->get('imageCreditsUrl');
		$item->creditsUrl = $creditsUrl;
		
	}
	
	

}