<?php
/* K2 Canonical plugin
*/

// no direct access
defined('_JEXEC') or die ('Restricted access');

// Load the K2 Plugin API
JLoader::register('K2Plugin', JPATH_ADMINISTRATOR.DS.'components'.DS.'com_k2'.DS.'lib'.DS.'k2plugin.php');

// Initiate class to hold plugin events
class plgK2Canonical extends K2Plugin {

	// Some params
	var $pluginName = 'canonical';
	var $pluginNameHumanReadable = 'Canonical';

	function plgK2Canonical( & $subject, $params) {
		parent::__construct($subject, $params);
	}


	function onK2PrepareContent( &$item, &$params, $limitstart) {

		//do not add tag if called from module
		if ( $params->get('parsedInModule') )
			return true;
		
		//add tag only if item is displayed (do not add for category, search resutls, etc..)
		if ( JRequest::getWord('view') != 'item')
			return true;
		
		$scheme = $_SERVER['SERVER_PORT'] == 443 ? 'https://' : 'http://';

		//generate absolute path
		$articlecanonicalurl=
			$scheme
			. $_SERVER['HTTP_HOST']
			. JRoute::_(K2HelperRoute::getItemRoute($item->id.':'.$item->alias, $item->catid.':'.$item->categoryAlias));
            
            
		$canonical= '<link rel="canonical" href="'.$articlecanonicalurl.'" />';
		
		$document =& JFactory::getDocument();
		$document->addCustomTag($canonical);
	
	}

} // END CLASS

