<?php
/**
 * @version		1.0
 * @package		K2 Joomla! Magazine Plugin
 * @author    JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

JLoader::register('K2Plugin', JPATH_ADMINISTRATOR.DS.'components'.DS.'com_k2'.DS.'lib'.DS.'k2plugin.php');

class plgK2JMagazine extends K2Plugin {

	var $pluginName = 'jmagazine';
	var $pluginNameHumanReadable = 'Joomla! Magazine';

	function plgK2JMagazine(& $subject, $config) {
		parent::__construct($subject, $config);
	}

	
	function onAfterK2Save(&$item, $isNew) {
	
		//Get editorial team groups
		$editorialGroups = (array)$this->params->get('editors');
		JArrayHelper::toInteger($editorialGroups);
		$authorGroups = (array)$this->params->get('authors');
		JArrayHelper::toInteger($authorGroups);
		
		//Notify editorial team about new submission
		if($isNew && count($editorialGroups)){
			$db = &JFactory::getDBO();
			$query = "SELECT juser.email FROM #__users as juser 
			LEFT JOIN #__k2_users as k2user ON juser.id=k2user.userID
			WHERE k2user.`group` IN (".implode(',', $editorialGroups).") AND juser.block = 0 AND juser.sendEmail = 1";
			$db->setQuery($query);
			$editors = $db->loadResultArray();
			if(count($editors)){
				$mainframe = &JApplication::getInstance('site');
				JLoader::register('K2HelperUtilities', JPATH_SITE.DS.'components'.DS.'com_k2'.DS.'helpers'.DS.'utilities.php');
				JLoader::register('K2HelperRoute', JPATH_SITE.DS.'components'.DS.'com_k2'.DS.'helpers'.DS.'route.php');
				$user = &JFactory::getUser();
				$sitename = $mainframe->getCfg('sitename');
				$mailfrom = $mainframe->getCfg('mailfrom');
				$fromname = $mainframe->getCfg('fromname');
				$subject = JText::sprintf('User %s submitted a new item at %s', $user->name, $sitename);
				$subject = html_entity_decode($subject, ENT_QUOTES);
				
				$userInfo = '<a href="'.JRoute::_(JURI::root().K2HelperRoute::getUserRoute($user->id)).'">'.$user->name.'</a>';
				$itemInfo = '<a href="'.JURI::root().'administrator/index.php?option=com_k2&amp;view=item&amp;cid='.$item->id.'">'.$item->title.'</a>';
				$message = JText::sprintf('User %s submitted item %s at %s', $userInfo, $itemInfo, $sitename);
				$message = html_entity_decode($message, ENT_QUOTES);
				JUtility::sendMail($mailfrom, $fromname, $editors, $subject, $message, true);
			}
		}
		

		//Notify author about editorial changes
		if(!$isNew){
			$author = &JFactory::getUser($item->created_by);
			$editor = &JFactory::getUser($item->modified_by);
			
			//Send email only if creator belongs at Author's team and moderator belongs to Editorial team
			$db = &JFactory::getDBO();
			$query = "SELECT COUNT(*) FROM #__k2_users 
			WHERE `group` IN (".implode(',', array_merge($editorialGroups, $authorGroups)).") AND (userID = {$author->id} OR userID = {$editor->id})";
			$db->setQuery($query);
			$check = $db->loadResult();
			
			//If check is equal to 2 and author is not blocked then continue
			if($check==2 && $author->block==0 && $this->params->get( 'sendeditnotificationtoauthor', 1 )){
				$mainframe = &JApplication::getInstance('site');
				$sitename = $mainframe->getCfg('sitename');
				$mailfrom = $mainframe->getCfg('mailfrom');
				$fromname = $mainframe->getCfg('fromname');
				$subject = JText::sprintf('Your item at %s has been modified', $sitename);
				$subject = html_entity_decode($subject, ENT_QUOTES);
				$message = JText::sprintf('Your item %s has been modified by the editorial team at %s', $item->title, $sitename);
				$message = html_entity_decode($message, ENT_QUOTES);
				JUtility::sendMail($mailfrom, $fromname, $author->email, $subject, $message);
			}
		}		
		
		return;
	}
	
	function onBeforeK2Save(&$item, $isNew){
	
		if(!$isNew){
		
			//We have to load the item again to find the previous moderator
			JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_k2'.DS.'tables');
			$K2Item = &JTable::getInstance('K2Item', 'Table');
			$K2Item->load($item->id);

		
			//Go further only if moderator is different than current user
			$user = &JFactory::getUser();
			
			if($K2Item->modified_by && $user->id!= $K2Item->modified_by){
			
				$authorGroups = (array)$this->params->get('authors');
				JArrayHelper::toInteger($authorGroups);
			
				$db = &JFactory::getDBO();
				$query = "SELECT COUNT(*) FROM #__k2_users 
				WHERE `group` IN (".implode(',', $authorGroups).") AND userID = {$user->id}";
				$db->setQuery($query);
				$check = $db->loadResult();

			
				//Go further if user is an author team member
				if($check > 0){
					
					$editorialGroups = (array)$this->params->get('editors');
					JArrayHelper::toInteger($editorialGroups);
				
					$query = "SELECT juser.email FROM #__users as juser 
					LEFT JOIN #__k2_users as k2user ON juser.id=k2user.userID
					WHERE k2user.`group` IN (".implode(',', $editorialGroups).") AND juser.block = 0 AND juser.sendEmail = 1";
					$db->setQuery($query);
					$editors = $db->loadResultArray();
					if(count($editors)){
						$mainframe = &JApplication::getInstance('site');
						JLoader::register('K2HelperUtilities', JPATH_SITE.DS.'components'.DS.'com_k2'.DS.'helpers'.DS.'utilities.php');
						JLoader::register('K2HelperRoute', JPATH_SITE.DS.'components'.DS.'com_k2'.DS.'helpers'.DS.'route.php');
						$user = &JFactory::getUser();
						$sitename = $mainframe->getCfg('sitename');
						$mailfrom = $mainframe->getCfg('mailfrom');
						$fromname = $mainframe->getCfg('fromname');
						$subject = JText::sprintf('User %s updated his item at %s', $user->name, $sitename);
						$subject = html_entity_decode($subject, ENT_QUOTES);
						
						$userInfo = '<a href="'.JRoute::_(JURI::root().K2HelperRoute::getUserRoute($user->id)).'">'.$user->name.'</a>';
						$itemInfo = '<a href="'.JURI::root().'administrator/index.php?option=com_k2&amp;view=item&amp;cid='.$item->id.'">'.$item->title.'</a>';
						$message = JText::sprintf('User %s updated item %s at %s', $userInfo, $itemInfo, $sitename);
						$message = html_entity_decode($message, ENT_QUOTES);
						JUtility::sendMail($mailfrom, $fromname, $editors, $subject, $message, true);
					}

				}

			}
		
		}	
		return;
	}
}
?>