<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

defined('_JEXEC') or die('');

require_once JPATH_ADMINISTRATOR . '/components/com_menus/helpers/menus.php';

/**
 * Joomla! Contact translation Plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	josetta.contactitem
 */

class plgJosettaMenuitem extends JosettaadminClass_Extensionplugin
{
	protected $_context = 'com_menus_item';

	/**
	 * Load language files that may be need by the extension being
	 * translated
	 */
	public function loadLanguages()
	{
		parent::loadLanguages();

		$language = JFactory::getLanguage();
		//load the administrator english language of the component if language translation is not available
		$language->load('com_menus', JPATH_ADMINISTRATOR, 'en-GB', true);
		//load the administrator default language of the component if language translation is not available
		$language->load('com_menus', JPATH_ADMINISTRATOR, null, true);

		$language->load('com_finder.sys', JPATH_ADMINISTRATOR, 'en-GB', true);
		$language->load('com_finder.sys', JPATH_ADMINISTRATOR, null, true);

		$language->load('com_search', JPATH_ADMINISTRATOR, 'en-GB', true);
		$language->load('com_search', JPATH_ADMINISTRATOR, null, true);

		$language->load('com_newsfeeds', JPATH_ADMINISTRATOR, 'en-GB', true);
		$language->load('com_newsfeeds', JPATH_ADMINISTRATOR, null, true);

		$language->load('com_weblinks', JPATH_ADMINISTRATOR, 'en-GB', true);
		$language->load('com_weblinks', JPATH_ADMINISTRATOR, null, true);
	}

	/**
	 * Method to build the dropdown of josetta translator screen
	 *
	 * @return array
	 *
	 */
	public function onJosettaGetTypes()
	{
		//return the protected variable $_context and label of component
		$item = array($this->_context => JText::_('COM_JOSETTA_PLUGIN_MENU_ITEM_TITLE'));
		$items[] = $item;
		return $items;
	}

	/**
	 *
	 * Finds the sub type of a provided item
	 * usually by looking up the definition file
	 * Need to override as menu items subtype can be
	 * based on a combination of fields, normally component view and layout
	 *
	 * @param string $context
	 * @param mixed $item
	 */
	public function onJosettaGetSubType($context, $item)
	{
		$subType = parent::onJosettaGetSubType($context, $item);

		switch ($subType)
		{
			case 'component':
				$model = new JosettaadminmenusModel_Item();

				$menuItem = $model->getItem($item->id);
				// and build the subtype, by using option, view and layout
				if (empty($menuItem->request['option']))
				{
					break;
				}
				else
				{
					// drop the 'component' part of the name
					$subType = $menuItem->request['option'];
					// Load the language file for the component.
					$lang = JFactory::getLanguage();
					$lang->load($menuItem->request['option'], JPATH_ADMINISTRATOR, null, false, false)
						|| $lang
							->load($menuItem->request['option'], JPATH_ADMINISTRATOR . '/components/' . $menuItem->request['option'], null, false,
								false) || $lang->load($menuItem->request['option'], JPATH_ADMINISTRATOR, $lang->getDefault(), false, false)
						|| $lang
							->load($menuItem->request['option'], JPATH_ADMINISTRATOR . '/components/' . $menuItem->request['option'],
								$lang->getDefault(), false, false);
				}
				if (empty($menuItem->request['view']))
				{
					break;
				}
				else
				{
					$subType .= '_' . $menuItem->request['view'];
				}
				if (empty($menuItem->request['layout']))
				{
					break;
				}
				else
				{
					$subType .= '_' . $menuItem->request['layout'];
				}

				break;

			// all other subtypes are handled directly
			case 'separator':
			case 'alias':
			case 'url':
			default:
				break;
		}

		return $subType;
	}

	/**
	 *
	 * Compute the subtitle of a provided item
	 * Subtitle is used for display to the user of an item
	 * to provide more context
	 * By default, we use an item category
	 *
	 * @param string $context
	 * @param mixed $item
	 */
	public function onJosettaGetSubtitle($context, $item)
	{
		if (!empty($context) && ($context != $this->_context))
		{
			return;
		}

		$subTitle = '';
		if (empty($item))
		{
			return $subTitle;
		}

		if (!empty($item->menutype) && !is_array($item->menutype))
		{
			try
			{
				$subTitle = ShlDbHelper::selectResult('#__menu_types', 'title', array('menutype' => $item->menutype));
			}
			catch (Exception $e)
			{
				ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
				$subTitle = '';
			}
		}

		return $subTitle;
	}

	/**
	 *
	 * @see JosettaadminClass_Extensionplugin::onJosettaLoadItem()
	 */
	public function onJosettaLoadItem($context, $id = '')
	{
		if ((!empty($context) && ($context != $this->_context)) || (empty($id)))
		{
			return null;
		}

		$item = parent::onJosettaLoadItem($context, $id);

		// attach data coming from subType
		switch ($item->type)
		{

			case 'component':
				$model = new JosettaadminmenusModel_Item();

				$menuItem = $model->getItem($item->id);
				$item->request = $menuItem->request;

				break;
		}

		return $item;
	}

	/**
	 * Save an item after it has been translated
	 * This will be called by Josetta when a user clicks
	 * the Save button. The context is passed so
	 * that each plugin knows if it must process the data or not
	 *
	 * if $item->reference_id is empty, this is
	 * a new item, otherwise we are updating the item
	 *
	 * $item->data contains the fields entered by the user
	 * that needs to be saved
	 *
	 *@param context type
	 *@param data in form of array
	 *
	 *return table id if data is inserted
	 *
	 *return false if error occurs
	 *
	 */

	public function onJosettaSaveItem($context, $item, &$errors)
	{
		if (($context != $this->_context))
		{
			return;
		}

		$errors = array();

		// sanity checks
		$item['menuordering'] = isset($item['menuordering']) ? $item['menuordering'] : -2;

		// pre-process params field, encoding its content to json
		$item = $this->_saveItemProcessParams($item, $context);

		// Check for the special 'request' entry.
		// this will build the 'link' field based on user data entry for themenu target
		if ($item['type'] == 'component' && isset($item['request']) && is_array($item['request']) && !empty($item['request']))
		{
			// Parse the submitted link arguments.
			$args = array();
			if (!empty($item['link']))
			{
				parse_str(parse_url($item['link'], PHP_URL_QUERY), $args);
			}

			// Merge in the user supplied request arguments.
			$args = array_merge($args, $item['request']);
			$item['link'] = JosettaadminHelper_General::buildurl($args);
			unset($item['request']);
		}

		// we need to be sure the component id is attached to the menu item,if appropriate.
		// Fetch installed Josetta extension id, as menu item needs that
		if (!empty($item['type']) && $item['type'] == 'component' && empty($item['component_id']))
		{
			// first identify the component name
			if (preg_match("/^index.php\?option=([a-zA-Z\-0-9_]*)/", $item['link'], $option))
			{
				$componentName = empty($option[1]) ? '' : $option[1];
			}
			else
			{
				// no option in link, unable to identify component, fail
				$this->setError('Unable to identify component while saving translated menu item');
				return false;
			}

			// then find about the component id in the extensions table
			$db = ShlDbHelper::getDb();
			$query = $db->getQuery(true);
			$query->select('extension_id')->from('#__extensions')->where($db->quoteName('type') . '=' . $db->Quote('component'))
				->where($db->quoteName('element') . '=' . $db->Quote($componentName));
			try
			{
				$componentId = $db->setQuery($query)->shlLoadResult();
				// set the component id in the menu item record
				$item['component_id'] = empty($componentId) ? 0 : $componentId;
			}
			catch (Exception $e)
			{
				ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
				// TODO: this will be lost, need to be enqueued or otherwise displayed
				$errors[] = $e->getMessage();
				return false;
			}
		}

		// load languages for form and error messages
		$this->loadLanguages();

		// use Joomla model to save the item
		// now we can create the menu item. We can't use directly Joomla item model
		// has it has an hardcoded JPATH_COMPONENT require_once that fails if used
		// from another extension than com_menus
		$model = new JosettaadminmenusModel_Item();
		$saved = $model->save($item);
		$state = $model->get('state');
		$menuItemId = $state->get('item.id');

		if (!$saved)
		{
			$errors = $model->getErrors();
			return false;
		}

		// return id of the saved item
		return $menuItemId;
	}

	/**
	 *
	 * Process generic filters definitions as read from plugin
	 * xml file, and turn them into a (part of a) query
	 *
	 * @param object $state
	 */
	protected function _buildFilterQuery($state)
	{
		$db = ShlDbHelper::getDb();

		// let base plugin do all the hard work
		parent::_buildFilterQuery($state);

		// add a specific condition: we don't want the "All" language home page
		// link to be translated
		$connector = strpos($this->_queryNonTranslated, 'where') !== false ? ' and ' : ' where ';
		$this->_queryNonTranslated .= $connector . '(' . $db->quoteName('a.language') . ' <> ' . $db->quote('*') . ' or ' . $db->quoteName('a.home')
			. ' <> ' . 1 . ')';
		$connector = strpos($this->_queryTranslated, 'where') !== false ? ' and ' : ' where ';
		$this->_queryTranslated .= $connector . '(' . $db->quoteName('a.language') . ' <> ' . $db->quote('*') . ' or ' . $db->quoteName('a.home')
			. ' <> ' . 1 . ')';
	}

	/**
	 *
	 * Find which table column to sort by
	 *
	 * @param object $state
	 */
	protected function _getOrderingColumn()
	{
		return 'lft';

	}

}
