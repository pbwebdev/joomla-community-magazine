<?php

// no direct access
defined('_JEXEC') or die('Restricted access');

/* -------------------- API Calls -------------------- */
$document 	= JFactory::getDocument();
$app 				= JFactory::getApplication();

// Requests
$option 		= JRequest::getCmd('option');
$view 			= JRequest::getCmd('view');
$layout 		= JRequest::getCmd('layout');
$page 			= JRequest::getCmd('page');
$task 			= JRequest::getCmd('task');
$itemid 		= JRequest::getInt('Itemid');
$tmpl 			= JRequest::getCmd('tmpl');
$siteTitle 	= $app->getCfg('sitename');
$lang = $this->language;


/* -------------------- Template Specific -------------------- */
// Detecting Home
$frontpageItemIDs = array(571,101,570); // Set any "frontpage" menu items ids here

$menu = JSite::getMenu();
if (($menu->getActive() == $menu->getDefault() || in_array(JRequest::getInt('Itemid'), $frontpageItemIDs)) && (JRequest::getCmd('option') != "com_search")){
	$siteHome = 1;
} else {
	$siteHome = 0;
}

// Get template parameters
$sidecolumnwidth  	= "3";
$leftcolgrid    		= "3";
$rightcolgrid    		= "3";
$maingrid      			= "6";
$layoutstyle     		= $this->params->get("layoutstyle");
$sidecolumnwidth  	= $this->params->get("sidecolumnwidth");

// Include grid.php
require_once(JPATH_SITE.'/templates/'.$this->template.'/grid.php');

// Detecting Active Variables
if($task == "edit" || $layout == "form" ){
	$fullWidth = 1;
} else {
	$fullWidth = 0;
}

// Set the body CSS class
$bodyClass = '';
if($option) 	$bodyClass .= ' '.$option;
if($view) 		$bodyClass .= ' view-'.$view;
if($layout) 	$bodyClass .= ' layout-'.$layout;
if($task) 		$bodyClass .= ' task-'.$task;
if($itemid) 	$bodyClass .= ' itemid-'.$itemid;
if($siteHome) $bodyClass .= ' homepage frontpage';
if($lang) $bodyClass .= ' lang-'.$lang;
$bodyClass = 'site '.trim($bodyClass);

// Frontpage RSS feed
if($siteHome) unset($this->_links);
$this->addHeadLink('http://feeds.joomla.org/JoomlaMagazine', 'alternate', 'rel', array('type'=>'application/rss+xml','title'=>'Joomla! Community Magazine | Issues'));

// Other Stuff
$pageTitle = "The Joomla! Community Magazine&trade;";



/* -------------------- CSS & JS Handling -------------------- */
$document->addStyleSheet(JURI::base(true).'/templates/system/css/system.css');
$document->addStyleSheet(JURI::base(true).'/templates/system/css/general.css');

if($tmpl!='component'){
	$document->addStyleSheet(JURI::base(true).'/templates/'.$this->template.'/css/1_grid.css');
	$document->addStyleSheet(JURI::base(true).'/templates/'.$this->template.'/css/2_joomla.css');
	$document->addStyleSheet(JURI::base(true).'/templates/'.$this->template.'/css/3_layout.css');
	$document->addStyleSheet(JURI::base(true).'/templates/'.$this->template.'/css/4_menus.css');
	$document->addStyleSheet(JURI::base(true).'/templates/'.$this->template.'/css/5_magazine.css');
	if($fullWidth){
		$document->addStyleDeclaration('
			#mainbody{width:100%;background:none;}
			#content{width:100%;}
			#sidebar, #sidebar2{display:none;}
		');
	}
}

// Alternate Language Style Sheets
//$document->addStyleSheet(JURI::base(true).'/templates/'.$this->template.'/css/font_hanuman.css');
//$document->addStyleSheet('http://fonts.googleapis.com/css?family=Tangerine');
//$document->addStyleSheet('http://fonts.googleapis.com/css?family=Hanuman');

// JS
//$document->addScript(JURI::base(true).'/templates/'.$this->template.'/js/behaviour.js');
