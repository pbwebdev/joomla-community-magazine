<?php
/*
// "Joomla! Community Magazine" by Nuevvo for Joomla! 1.5.x - Version 1.0
// Copyright (c) 2010-2011 Nuevvo Webware Ltd. All rights reserved.
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Tools {

	// Custom module renderer
	function renderModules($position, $style) {
		$user = & JUser::getInstance();
		foreach (JModuleHelper::getModules($position) as $module) {
			$cache = &JFactory::getCache($module->module);

			$html = "<div class=\"module\">\n";

			$html .= "\t<h3>".$module->title."</h3>";

			$html .= $cache->get( array('JModuleHelper', 'renderModule'), array( $module, array('style' => $style) ), $module->id. $user->get('aid', 0) );

			$html .= '</div>';

		return $html;
		}
	}

	// Custom module renderer (tabs)
	function renderModulesTabs($position, $style, $class) {
		$user = & JUser::getInstance();

		$html = '<div class="mooTabs '.$class.'">';
		$html .= '<ul class="mooTabsNavigation">';

		foreach (JModuleHelper::getModules($position) as $key=>$module) {
			$cache = &JFactory::getCache($module->module);
			$active = ($key==0)?' class="active"':'';
			$html .= '<li><a '.$active.' href="#">'.$module->title.'</a></li>';

		}
		$html .= '</ul>';
		$html .='<div class="mooTabsContent">';

		foreach (JModuleHelper::getModules($position) as $key=>$module) {
			$cache = &JFactory::getCache($module->module);
			$active = ($key==0)?' active':'';
			$html .= '<div class="mooTab'.$active.'">';
			$html .= $cache->get( array('JModuleHelper', 'renderModule'), array( $module, array('style' => '') ), $module->id. $user->get('aid', 0) );
			$html .= '</div>';
		}
		$html .= '</div>';

		$html .= '</div>';

	return $html;
	}

}
