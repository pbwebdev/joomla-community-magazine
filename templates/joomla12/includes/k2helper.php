<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class K2Tools {
	function getUserTypeClass($userID){
		if(!$userID) return '';
		$user = &JFactory::getUser($userID);
		return strtolower(str_replace(' ','',$user->usertype));
	}
}
