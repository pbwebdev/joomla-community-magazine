<?php

// Modified on March 11th, 2013 by Fotis Evangelou @fevangelou

// no direct access
defined('_JEXEC') or die;

include_once(JPATH_SITE.DS."templates".DS.$this->template.DS."includes".DS."helper.php");

?>
<!DOCTYPE html>
<!--[if lte IE 6]><html prefix="og: http://ogp.me/ns#" class="isIe isIE6" lang="en-gb"><![endif]-->
<!--[if IE 7]>    <html prefix="og: http://ogp.me/ns#" class="isIe isIE7" lang="en-gb"><![endif]-->
<!--[if IE 8]>    <html prefix="og: http://ogp.me/ns#" class="isIe isIE8" lang="en-gb"><![endif]-->
<!--[if IE 9]>    <html prefix="og: http://ogp.me/ns#" class="isIe isIE9" lang="en-gb"><![endif]-->
<!--[if gt IE 9]><!--><html prefix="og: http://ogp.me/ns#" lang="<?php echo $this->language; ?>" dir="ltr"><!--<![endif]-->
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <meta http-equiv="imagetoolbar" content="false" />
    <jdoc:include type="head" />
    <link href="https://plus.google.com/112939345266070287086" rel="publisher" />
	<!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Google Analytics -->
    <script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-544070-3']);
      _gaq.push(['_setDomainName', '.joomla.org']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>

    <script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-544070-25']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>
    <?php /*
    <link href='http://fonts.googleapis.com/css?family=Gabriela' rel='stylesheet' type='text/css'>
    <style type="text/css">h1,h2,h3,h4,h5,p,a,catItemIntroText, body * {font-family: 'Gabriela' !important;} h1,h2,h3,h4,h5,a.moduleItemTitle {color: #ec008c !important; }</style>
  */ ?>
  </head>
<body class="<?php echo $bodyClass; ?>" itemscope itemtype="http://schema.org/WebPage" >
    <?php echo(file_get_contents('http://cdn.joomla.org/template/menu/menu.php'));?>
    <div id="titleWrap">
      <div id="title" class="container_12 clearfix">
        <div class="grid_7">
          <h1 title="<?php print $document->getTitle();?>"><?php echo $pageTitle; ?></h1>
        </div>
        <div class="grid_5 right">
          <div class="downdemo">
            <p class="download-16"><a href="http://www.joomla.org/download.html">Download</a></p>
            <p class="demo"><a href="http://demo.joomla.org">Demo</a></p>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <div class="clear"></div>
    <?php if($this->countModules('main-menu')): ?>
    <div id="insetWrap">
      <div id="inset" class="container_12 clearfix">
        <jdoc:include type="modules" name="main-menu" style="" />
        <div class="clear"></div>
      </div>
    </div>
    <div class="clear"></div>
    <?php endif; ?>
    <div id="containerWrap">
      <div id="container" class="container_12 clearfix">
        <div id="mainbody" class="<?php echo $maingrid;?> maincontent">
          <?php if($this->countModules('top or topwithstyle')): ?>
          <div id="top">
            <jdoc:include type="modules" name="topwithstyle" style="xhtml" />
            <jdoc:include type="modules" name="top" style="" />
            <div class="clr"></div>
          </div>
          <?php endif; ?>
          <div class="clr"></div>
          <jdoc:include type="message" />
          <jdoc:include type="modules" name="before" style="xhtml" />
          <?php if($siteHome): ?>
          <jdoc:include type="modules" name="index-view" style="" />
          <?php else: ?>
          <jdoc:include type="component" />
          <?php endif; ?>
          <jdoc:include type="modules" name="after" style="xhtml" />
        </div>
        <?php if($this->countModules('leftmenu')): ?>
        <div id="sidebar" class="<?php echo $leftcolgrid;?> left">
          <jdoc:include type="modules" name="leftmenu" style="xhtml" />
        </div>
        <?php endif; ?>
        <?php if($this->countModules('right or lang or user4')): ?>
        <div id="sidebar2" class="<?php echo $rightcolgrid;?> right">
          <jdoc:include type="modules" name="lang" style="xhtml" />
          <?php if($currentModulePosition!=""): // we get this from the k2 item ?>
          <jdoc:include type="modules" name="<?php echo "Topic_".$currentModulePosition; ?>" style="tw" />
          <?php endif; ?>          
          <jdoc:include type="modules" name="right" style="tw" />
          <?php if(!$siteHome): ?>
          <jdoc:include type="modules" name="user4" style="tw" />
          <?php endif; ?>
        </div>
        <?php endif; ?>
        <div class="clear"></div>
      </div>
    </div>
    <div class="clear"></div>
    <div id="closingWrap">
      <div id="closing" class="container_12 clearfix"></div>
    </div>
    <div class="clear"></div>
    <div id="footerWrap">
      <div id="footer" class="container_12 clearfix">
        <div class="inside">
          <div id="copy">
            <ul class="menu">
              <li class="item97">
                <a href="http://www.joomla.org"><span>Home</span></a>
              </li>
              <li class="item110">
                <a href="http://www.joomla.org/about-joomla.html"><span>About</span></a>
              </li>
              <li class="item98">
                <a href="http://community.joomla.org"><span>Community</span></a>
              </li>
              <li class="item99">
                <a href="http://forum.joomla.org"><span>Forum</span></a>
              </li>
              <li class="item100">
                <a href="http://extensions.joomla.org"><span>Extensions</span></a>
              </li>
              <li class="item206">
                <a href="http://resources.joomla.org"><span>Resources</span></a>
              </li>
              <li class="item102">
                <a href="http://docs.joomla.org"><span>Docs</span></a>
              </li>
              <li class="item101">
                <a href="http://developer.joomla.org"><span>Developer</span></a>
              </li>
              <li class="item103">
                <a href="http://shop.joomla.org"><span>Shop</span></a>
              </li>
            </ul>
            <div class="clr"></div>
          </div>
          <div id="link">
            <div id="footerInfo">&copy; 2005 - <?php echo date('Y'); ?> <a href="http://www.opensourcematters.org">Open Source Matters, Inc.</a> All rights reserved.&nbsp;&nbsp;&nbsp; <a href="http://www.joomla.org/accessibility-statement.html">Accessibility</a>&nbsp;&nbsp;&nbsp; <a href="http://www.joomla.org/privacy-policy.html">Privacy</a>&nbsp;&nbsp;&nbsp; <a href="http://getk2.org" target="_blank">Content by K2</a>&nbsp;&nbsp;&nbsp; <a href="http://anything-digital.com/josetta" title="Joomla translations made easy" target="_blank">Translations by Josetta</a>&nbsp;&nbsp;&nbsp;  <a href="/login" title="Author Login" target="_blank">Login</a>&nbsp;&nbsp;&nbsp; <a href="http://www.rochenhost.com/joomla-hosting" target="_blank">Joomla Hosting by</a>&nbsp;&nbsp; <a href="http://www.rochenhost.com/joomla-hosting" target="_blank" class="rochen">Rochen Ltd.</a>
            </div>
            <div class="clr"></div>
          </div>
          <div class="clr"></div>
        </div>
      </div>
    </div>
    <?php if($this->countModules('debug')): ?>
    <div id="debug">
      <div class="width">
        <div class="inside">
          <jdoc:include type="modules" name="debug" style="xhtml" />
        </div>
      </div>
    </div>
    <?php endif; ?>
    <!-- test -->
  </body>
</html>
