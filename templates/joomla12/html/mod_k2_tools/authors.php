<?php
/**
 * @version		$Id: authors.php 1492 2012-02-22 17:40:09Z joomlaworks@gmail.com $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2012 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

$document = JFactory::getDocument();

// Detecting Home
$frontpageItemIDs = array(571,101,570); // Set any "frontpage" menu items ids here

$menu = JSite::getMenu();
if (($menu->getActive() == $menu->getDefault()) || in_array(JRequest::getInt('Itemid'), $frontpageItemIDs)){
	$siteHome = 1;
} else {
	$siteHome = 0;
}

$splitter = ceil(count($authors)/2)-1;

?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="k2AuthorsListBlock k2UsersBlock <?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?>">
	<ul class="split<?php echo $splitter; ?><?php if($siteHome) echo " frontpage"; ?>">
		<?php foreach ($authors as $key=>$author): ?>
		<li class="a<?php echo $key; ?>">
			<?php if ($params->get('authorAvatar')): ?>
			<a class="k2Avatar abAuthorAvatar" href="<?php echo $author->link; ?>" title="<?php echo K2HelperUtilities::cleanHtml($author->name); ?>"> <img src="<?php echo $author->avatar; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($author->name); ?>" style="width:<?php echo $avatarWidth; ?>px;height:auto;" /> </a>
			<?php endif; ?>
			<a class="abAuthorName" href="<?php echo $author->link; ?>"> <?php echo $author->name; ?>
			<?php if ($params->get('authorItemsCounter')): ?>
			<span>(<?php echo $author->items; ?>)</span>
			<?php endif; ?>
			</a>
			<p><?php echo K2HelperUtilities::wordLimit($author->profile->description, '4'); ?></p>
			<!--<a class="abAuthorsWebsite" href="<?php echo $author->profile->url; ?>" title="<?php echo JText::_("Authors Website"); ?>"><?php echo $author->profile->url; ?></a>-->
			<?php if ($params->get('authorLatestItem')): ?>
			<a class="abAuthorLatestItem" href="<?php echo $author->latest->link; ?>" title="<?php echo K2HelperUtilities::cleanHtml($author->latest->title); ?>">
				<?php echo $author->latest->title; ?> <span class="abAuthorCommentsCount"> (<?php echo $author->latest->numOfComments; ?> <?php if($author->latest->numOfComments=='1') echo JText::_('K2_MODK2TOOLS_COMMENT'); else echo JText::_('K2_MODK2TOOLS_COMMENTS'); ?>)</span>
			</a>
			<?php endif; ?>
		</li>
		<?php if(($siteHome) && ($key==$splitter)):?>
	</ul>
	<ul class="rightList">
		<?php endif; ?>
		<?php endforeach; ?>
	</ul>
</div>