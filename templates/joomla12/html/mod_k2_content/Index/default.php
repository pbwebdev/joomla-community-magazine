<?php
/**
 * @version		$Id: default.php 361 2010-02-01 14:29:10Z lefteris.kavadas $
 * @package		K2
 * @author    JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

//Inherits all values from Category_item.php
// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<div id="k2ModuleBox<?php echo $module->id; ?>" class="k2ItemsBlock mg">
	<?php // The First Item Begins Here => the Pseudo Leading ?>
	
	<div id="k2Container">
	
    <?php foreach ($items as $key=>$item):	?>
	
	<?php if ($key==0) : //The Pseudo Leading Element ?>
	
		<div class="catItemView" id="itemListLeading" itemscope itemtype="Article" >
			<div class="catItemView groupLeading">
			    <div class="catItemViewWrap">
			      <!-- Plugins: BeforeDisplay -->
			      <?php echo $item->event->BeforeDisplay; ?>
			
			      <!-- K2 Plugins: K2BeforeDisplay -->
			      <?php echo $item->event->K2BeforeDisplay; ?>
			      
				  <div class="catItemHeader">    
			      
			      <?php if($params->get('itemDateCreated')): ?>
			      <span class="catItemDateCreated">
			      	<?php echo JHTML::_('date', $item->created, JText::_('M Y')); ?>
			      </span>
				  <meta itemprop="dateCreated" content="<?php echo date('Ymd',strtotime($item->created));?>T<?php echo date('His',strtotime($item->created)); ?>"/>
			      <?php endif; ?>
			
			      <?php if($params->get('itemAuthor')): ?>
			      	<span class="catItemAuthor">
			      		<?php echo K2HelperUtilities::writtenBy($item->authorGender); ?>
							<?php if(isset($item->authorLink)): ?>
								<a  href="<?php echo $item->authorLink; ?>" itemprop="author"><?php echo $item->author; ?></a>
							<?php else: ?>
								<span itemprop="author"><?php echo $item->author; ?></span>
							<?php endif; ?>
					</span>
				  <?php endif; ?>	
									
			<?php if($params->get('itemCommentsCounter') && $componentParams->get('comments')): ?>		
			| 
				<?php if(!empty($item->event->K2CommentsCounter)): ?>
					<!-- K2 Plugins: K2CommentsCounter -->
					<?php echo $item->event->K2CommentsCounter; ?>
				<?php else: ?>
					<?php if($item->numOfComments>0): ?>
					<a class="linkToComment" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
						<?php echo $item->numOfComments; ?> <?php if($item->numOfComments>1) echo JText::_('K2_COMMENTS'); else echo JText::_('K2_COMMENT'); ?>
					</a>
					<?php else: ?>
					<a class="linkToComment" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
						<?php echo JText::_('K2_BE_THE_FIRST_TO_COMMENT'); ?>
					</a>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>
				
					<?php if($params->get('itemHits')): ?>
					<span class="moduleItemHits">
						<?php echo JText::_('Read'); ?> <?php echo $item->hits; ?> <?php echo JText::_('times'); ?>
					</span>
					<?php endif; ?>
				
				<div class="clr"></div>
				
				</div> <!-- Item Header -->
				
			     
		      	<h3 class="catItemTitle">
		      		<a itemprop="name" class="moduleItemTitle" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
		      	</h3>
			      
				
				  <!-- Plugins: AfterDisplayTitle -->
			      <?php echo $item->event->AfterDisplayTitle; ?>
			
			      <!-- K2 Plugins: K2AfterDisplayTitle -->
			      <?php echo $item->event->K2AfterDisplayTitle; ?>
			
			      <!-- Plugins: BeforeDisplayContent -->
			      <?php echo $item->event->BeforeDisplayContent; ?>
			
			      <!-- K2 Plugins: K2BeforeDisplayContent -->
			      <?php echo $item->event->K2BeforeDisplayContent; ?>

				      <?php if($params->get('itemImage') || $params->get('itemIntroText')): ?>
			      <div class="catItemBody">
			
				      <?php if($params->get('itemImage') && isset($item->image)): ?>
				      <span class="catItemImage">
				     	 <a href="<?php echo $item->link; ?>" title="<?php echo JText::_('Continue reading'); ?> &quot;<?php echo htmlentities($item->title, ENT_QUOTES, 'UTF-8'); ?>&quot;">
				      		<img itemprop="image" src="<?php echo $item->image; ?>" alt="<?php echo $item->title; ?>"/>
				      	</a>
				      </span>
				      <?php endif; ?>
					
			      	<?php if($params->get('itemIntroText')): ?>
			      		<div itemprop="description" class="catItemIntroText">
			      			<?php echo $item->introtext; ?>
			      		</div>
			      	<?php endif; ?>
			      	
			      </div>
			      <?php endif; ?>
			
			      <?php if($params->get('itemExtraFields') && count($item->extra_fields)): ?>
			      <b><?php echo JText::_('Additional Info'); ?></b>
			      <ul class="moduleItemExtraFields">
			        <?php foreach ($item->extra_fields as $extraField): ?>
							<li class="type<?php echo ucfirst($extraField->type); ?> group<?php echo $extraField->group; ?>">
								<span class="moduleItemExtraFieldsLabel"><?php echo $extraField->name; ?></span>
								<span class="moduleItemExtraFieldsValue"><?php echo $extraField->value; ?></span>
								<br class="clr" />
							</li>
			        <?php endforeach; ?>
			      </ul>
			      <?php endif; ?>
			
				  <?php if($params->get('itemTags') && count($item->tags)>0): ?>
				  <span class="catItemTags">
					  <span class="tagLabel"><?php echo JText::_("Tagged under"); ?>:</span>
				        <?php foreach ($item->tags as $tag): ?>
				        	<a itemprop="keywords" href="<?php echo $tag->link; ?>"><?php echo $tag->name; ?></a>
				 	        <?php endforeach; ?>
			      </span>
			      <?php endif; ?>
			
				<?php if($params->get('itemReadMore')): ?>
					<div class="catItemReadMore">
						<a class="k2ReadMore" href="<?php echo $item->link; ?>">
							<?php echo JText::_('Read more...'); ?>
						</a>
					</div>
				<?php endif; ?>
			
				<div class="clr"></div>
				
				<div class="catItemLinks">
			
			      <?php if($params->get('itemVideo')): ?>
			      <p class="moduleItemVideo">
			      	<?php echo $item->video ;?>
			      	<br class="clr" />
			      	<span class="moduleItemVideoCaption"><?php echo $item->video_caption ;?></span>
			      	<span class="moduleItemVideoCredits"><?php echo $item->video_credits ;?></span>
			      	<br class="clr" />
			      </p>
			      <?php endif; ?>
			
			      <span class="clr"></span>
			
			      <!-- Plugins: AfterDisplayContent -->
			      <?php echo $item->event->AfterDisplayContent; ?>
			
			      <!-- K2 Plugins: K2AfterDisplayContent -->
			      <?php echo $item->event->K2AfterDisplayContent; ?>
			
			
			      <?php if($params->get('itemCategory')): ?>
			      <?php //echo JText::_('in') ;?> <a class="moduleItemCategory" href="<?php echo $item->categoryLink; ?>"><span><?php echo $item->categoryname; ?></span></a>
			      <?php endif; ?>
			
			      <span class="clr"></span>
			
			
			      <?php if($params->get('itemAttachments') && count($item->attachments)): ?>
				  <p class="moduleAttachements">
				  <?php foreach ($item->attachments as $attachment): ?>
					<a title="<?php echo $attachment->titleAttribute; ?>" href="<?php echo JRoute::_('index.php?option=com_k2&view=item&task=download&id='.$attachment->id); ?>">
						<?php echo $attachment->title ; ?>
					</a>
				  <?php endforeach;?>
				  </p>
			      <?php endif; ?>
			
			      <!-- Plugins: AfterDisplay -->
			      <?php echo $item->event->AfterDisplay; ?>
			
			      <!-- K2 Plugins: K2AfterDisplay -->
			      <?php echo $item->event->K2AfterDisplay; ?>
					</div><!-- links -->
			      <span class="clr"></span>
			   </div> <!-- wrap -->			
			</div><!-- groupLeading -->
		</div><!-- Leading-->

		
	<?php else: //The Rest of the posts ?>
	
	<?php if ($key==1) echo('<div class="catItemView" id="itemListPrimary">'); ?>
	
	<div class="catItemView" itemscope itemtype="Article" >
      <div class="catItemViewWrap">
      <!-- Plugins: BeforeDisplay -->
      <?php echo $item->event->BeforeDisplay; ?>

      <!-- K2 Plugins: K2BeforeDisplay -->
      <?php echo $item->event->K2BeforeDisplay; ?>
      
	  <div class="catItemHeader">    
      
      <?php if($params->get('itemDateCreated')): ?>
      <span class="catItemDateCreated">
      	<?php echo JHTML::_('date', $item->created, JText::_('M Y')); ?>
		<meta itemprop="dateCreated" content="<?php echo date('Ymd',strtotime($item->created));?>T<?php echo date('His',strtotime($item->created)); ?>"/>
      </span>
      <?php endif; ?>

      <?php if($params->get('itemAuthor')): ?>
      	<span class="catItemAuthor">
      		<?php echo K2HelperUtilities::writtenBy($item->authorGender); ?>
				<?php if(isset($item->authorLink)): ?>
					<a itemprop="author" href="<?php echo $item->authorLink; ?>"><?php echo $item->author; ?></a>
				<?php else: ?>
					<span itemprop="author"><?php echo $item->author; ?></span>
				<?php endif; ?>
		</span>
	  <?php endif; ?>

	<?php if($params->get('itemCommentsCounter') && $componentParams->get('comments')): ?>
	| 
		<?php if(!empty($item->event->K2CommentsCounter)): ?>
		<!-- K2 Plugins: K2CommentsCounter -->
			<?php echo $item->event->K2CommentsCounter; ?>
			<?php else : ?>
				<?php if($item->numOfComments>0): ?>
					<a class="linkToComment" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
						<?php echo $item->numOfComments; ?> <?php if($item->numOfComments>1) echo JText::_('comments'); else echo JText::_('comment'); ?>
					</a>
				<?php else: ?>
					<a class="linkToComment" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
						<?php echo JText::_('Be the first to comment!'); ?>
					</a>
			<?php endif; ?>
		<?php endif; ?>
	<?php endif; ?>
		
		<?php if($params->get('itemHits')): ?>
		<span class="moduleItemHits">
			<?php echo JText::_('Read'); ?> <?php echo $item->hits; ?> <?php echo JText::_('times'); ?>
		</span>
		<?php endif; ?>
	
	<div class="clr"></div>
	
	</div> <!-- Item Header -->
	
     <?php if($params->get('itemTitle')): ?>
      	<h3 class="catItemTitle">
      		<a itemprop="name" class="moduleItemTitle" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
      	</h3>
      <?php endif; ?>

      <!-- Plugins: AfterDisplayTitle -->
      <?php echo $item->event->AfterDisplayTitle; ?>

      <!-- K2 Plugins: K2AfterDisplayTitle -->
      <?php echo $item->event->K2AfterDisplayTitle; ?>

      <!-- Plugins: BeforeDisplayContent -->
      <?php echo $item->event->BeforeDisplayContent; ?>

      <!-- K2 Plugins: K2BeforeDisplayContent -->
      <?php echo $item->event->K2BeforeDisplayContent; ?>

      <?php if($params->get('itemImage') || $params->get('itemIntroText')): ?>
      <div class="catItemBody">

	      <?php if($params->get('itemImage') && isset($item->image)): ?>
	      <span class="catItemImage">
	     	 <a href="<?php echo $item->link; ?>" title="<?php echo JText::_('Continue reading'); ?> &quot;<?php echo htmlentities($item->title, ENT_QUOTES, 'UTF-8'); ?>&quot;">
	      		<img itemprop="image" src="<?php echo $item->image; ?>" alt="<?php echo $item->title; ?>"/>
	      	</a>
	      </span>
	      <?php endif; ?>
		
      	<?php if($params->get('itemIntroText')): ?>
      		<div itemprop="description" class="catItemIntroText">
      			<?php echo $item->introtext; ?>
      		</div>
      	<?php endif; ?>
      	
      </div>
      <?php endif; ?>

      <?php if($params->get('itemExtraFields') && count($item->extra_fields)): ?>
      <b><?php echo JText::_('Additional Info'); ?></b>
      <ul class="moduleItemExtraFields">
        <?php foreach ($item->extra_fields as $extraField): ?>
				<li class="type<?php echo ucfirst($extraField->type); ?> group<?php echo $extraField->group; ?>">
					<span class="moduleItemExtraFieldsLabel"><?php echo $extraField->name; ?></span>
					<span class="moduleItemExtraFieldsValue"><?php echo $extraField->value; ?></span>
					<br class="clr" />
				</li>
        <?php endforeach; ?>
      </ul>
      <?php endif; ?>

	  <?php if($params->get('itemTags') && count($item->tags)>0): ?>
	  <span class="catItemTags">
		  <span class="tagLabel"><?php echo JText::_("Tagged under"); ?>:</span>
	        <?php foreach ($item->tags as $tag): ?>
	        	<a itemprop="keywords" href="<?php echo $tag->link; ?>"><?php echo $tag->name; ?></a>
	 	        <?php endforeach; ?>
      </span>
      <?php endif; ?>

	<?php if($params->get('itemReadMore')): ?>
		<div class="catItemReadMore">
			<a class="k2ReadMore" href="<?php echo $item->link; ?>">
				<?php echo JText::_('Read more...'); ?>
			</a>
		</div>
	<?php endif; ?>

	<div class="clr"></div>
	
	<div class="catItemLinks">

      <?php if($params->get('itemVideo')): ?>
      <p class="moduleItemVideo">
      	<?php echo $item->video ;?>
      	<br class="clr" />
      	<span class="moduleItemVideoCaption"><?php echo $item->video_caption ;?></span>
      	<span class="moduleItemVideoCredits"><?php echo $item->video_credits ;?></span>
      	<br class="clr" />
      </p>
      <?php endif; ?>

      <span class="clr"></span>

      <!-- Plugins: AfterDisplayContent -->
      <?php echo $item->event->AfterDisplayContent; ?>

      <!-- K2 Plugins: K2AfterDisplayContent -->
      <?php echo $item->event->K2AfterDisplayContent; ?>


      <?php if($params->get('itemCategory')): ?>
      <?php //echo JText::_('in') ;?> <a class="moduleItemCategory" href="<?php echo $item->categoryLink; ?>"><span><?php echo $item->categoryname; ?></span></a>
      <?php endif; ?>

      <span class="clr"></span>


      <?php if($params->get('itemAttachments') && count($item->attachments)): ?>
	  <p class="moduleAttachements">
	  <?php foreach ($item->attachments as $attachment): ?>
		<a title="<?php echo $attachment->titleAttribute; ?>" href="<?php echo JRoute::_('index.php?option=com_k2&view=item&task=download&id='.$attachment->id); ?>">
			<?php echo $attachment->title ; ?>
		</a>
	  <?php endforeach;?>
	  </p>
      <?php endif; ?>


      <!-- Plugins: AfterDisplay -->
      <?php echo $item->event->AfterDisplay; ?>

      <!-- K2 Plugins: K2AfterDisplay -->
      <?php echo $item->event->K2AfterDisplay; ?>
		</div><!-- links -->
      <span class="clr"></span>
      </div> <!-- wrap -->
    </div><!-- view -->
    <?php endif; ?>
    <?php endforeach; ?>

	<?php if($params->get('itemCustomLink')): ?>
	<div class="customLink">
    <a class="moduleCustomLink" href="<?php echo $params->get('itemCustomLinkURL'); ?>" title="<?php echo htmlentities($params->get('itemCustomLinkTitle'), ENT_QUOTES, 'UTF-8'); ?>">
		<span><?php echo $params->get('itemCustomLinkTitle'); ?></span>
	</a>
    </div>
	<?php endif; ?>

	<?php if($params->get('feed')): ?>
	<div class="k2FeedIcon">
		<a href="<?php echo JRoute::_('index.php?option=com_k2&view=itemlist&format=feed&moduleID='.$module->id);?>" title="<?php echo JText::_('Subscribe to this RSS feed'); ?>">
			<span><?php echo JText::_('Subscribe to this RSS feed'); ?></span>
		</a>
		<div class="clr"></div>
	</div>
	<?php endif; ?>
<div class="clr"></div>

 </div><!-- .catItemView -->
</div><!-- #K2container-->
</div><!-- #K2Module -->