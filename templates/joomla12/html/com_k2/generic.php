<?php
/**
 * @version		$Id: generic.php 329 2010-01-15 19:39:21Z joomlaworks $
 * @package		K2
 * @author    JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
define('DATE_FORMAT_TPL','<big>%d</big><small>%b %Y</small>');

// need to know the tag name to load the module position fot this topic
if(JRequest::getVar('task')=='tag'){
		$firstTag =  str_replace(" ","",trim(JRequest::getVar('tag')));
		$firstTag = str_replace("&","",$firstTag);
		JRequest::setVar('currentTag',$firstTag);
		
		}
?>

<?php // Start K2 Generic Layout ?>
<div itemscope itemtype="searchResults" class="mg">
<div id="k2ContainerGeneric" class="itemListView<?php if($this->params->get('pageclass_sfx')) echo ' '.$this->params->get('pageclass_sfx'); ?><?php if(count($this->pagination->getPagesCounter())) echo ' hasPagination';?>">

	<?php if($this->params->get('show_page_title')): ?>
	<?php // Page title ?>
	<div class="componentheading<?php echo $this->params->get('pageclass_sfx')?>">
		<?php echo $this->escape($this->params->get('page_title')); ?>
	</div>
	<?php endif; ?>
	

	<?php if(count($this->items)): ?>
	<div id="itemListPrimary">
		
		<?php foreach($this->items as $key=>$item): ?>
		<div class="itemContainer <?php if(!$key)echo ' itemContainerFirst'; ?><?php if($key+1==count($this->items)) echo ' itemContainerLast';?>" style="width:100%;">

		<?php // Start K2 Item Layout ?>
		<div class="catItemView">        
        	<div class="catItemViewWrap">
				
                <div class="catItemHeader">
					<?php if($item->params->get('genericItemDateCreated')): ?>
					<?php // Date created ?>
						<span class="catItemDateCreated">
							<?php echo JHTML::_('date', $item->created, JText::_('DATE_FORMAT_JCMCAT')); ?>
						</span>
					<?php endif; ?>               
                	  	
            		<div class="clr"></div>
            	</div>
                            
             	<?php if($item->params->get('genericItemTitle')): ?>
			  		<?php // Item title ?>
			  			<h3 class="catItemTitle">
						<meta itemprop="name" content="<?php echo $item->title; ?>" />
			  				<?php if ($item->params->get('genericItemTitleLinked')): ?>
								<a href="<?php echo $item->link; ?>">
			  						<?php echo $item->title; ?>
			  					</a>
			  				<?php else: ?>
			  					<?php echo $item->title; ?>
			  				<?php endif; ?>
			  			</h3>
			  	<?php endif; ?>

		  	 	<div class="catItemBody">
			  		<?php if($item->params->get('genericItemImage') && !empty($item->imageGeneric)): ?>
			  			<?php // Item Image ?>		 
				  			<span class="catItemImage">
				    			<a href="<?php echo $item->link; ?>" title="<?php if(!empty($item->image_caption)) echo $item->image_caption; else echo $item->title; ?>">
				    				<img itemprop="image" src="<?php echo $item->imageMedium; ?>" alt="<?php if(!empty($item->image_caption)) echo $item->image_caption; else echo $item->title; ?>" />
				    			</a>
				  			</span>				 
					<?php endif; ?>
			  
			  		<?php if($item->params->get('genericItemIntroText')): ?>
			  			<?php // Item introtext ?>
			  				<div itemprop="description" class="catItemIntroText">
			  					<?php echo $item->introtext; ?>
				  			</div>
			  		<?php endif; ?>
              		<div class="clr"></div>
            		<?php if ($item->params->get('genericItemReadMore')): ?>
						<?php // Item "read more..." link ?>
							<div class="catItemReadMore">
								<a class="k2ReadMore" href="<?php echo $item->link; ?>">
									<?php echo JText::_('Read more...'); ?>
								</a>
							</div>
					<?php endif; ?>
            
            	<div class="clr"></div>
                
                <?php if($item->params->get('genericItemExtraFields') && count($item->extra_fields)): ?>
		  		<?php // Item extra fields ?>
			  		<div class="genericItemExtraFields">
			  			<h4><?php echo JText::_('Additional Info'); ?></h4>
			  			<ul>
							<?php foreach ($item->extra_fields as $key=>$extraField): ?>
								<li class="<?php echo ($key%2) ? "odd" : "even"; ?> type<?php echo ucfirst($extraField->type); ?> group<?php echo $extraField->group; ?>">
									<span class="genericItemExtraFieldsLabel"><?php echo $extraField->name; ?></span>
									<span class="genericItemExtraFieldsValue"><?php echo $extraField->value; ?></span>
									<span class="clr"></span>		
								</li>
							<?php endforeach; ?>
						</ul>
		    			<div class="clr"></div>
		  			</div>
		  	<?php endif; ?>	
                
		  	</div>
            
        	<div class="clr"></div>
        	</div>
		</div>
		<?php // End K2 Item Layout ?>
        </div>
        <div class="clr"></div>
		<?php endforeach; ?>
    </div>

	<?php // Pagination ?>
	<?php if($this->pagination->getPagesLinks()): ?>
	<dl class="pageNavigationContainer">
    	<dd class="pageCounter">
			<span>
				<?php echo $this->pagination->getPagesCounter(); ?>
        	</span>
		</dd>
        <dd class="pagesLinks">
			<?php echo $this->pagination->getPagesLinks(); ?>
        </dd>
	</dl>
	<?php endif; ?>

<?php if($this->params->get('userFeed')): ?>
	<?php // RSS feed icon ?>
	<div class="k2FeedIcon">
		<a href="<?php echo $this->feed; ?>" title="<?php echo JText::_('Subscribe to this RSS feed'); ?>">
			<span><?php echo JText::_('Subscribe to this RSS feed'); ?></span>
		</a>
		<div class="clr"></div>
	</div>
<?php endif; ?>	

<?php endif; ?>
</div>
</div>
<?php // End K2 Generic Layout ?>
