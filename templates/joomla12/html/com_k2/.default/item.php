<?php
/**
 * @version		$Id: item.php 303 2010-01-07 02:56:33Z joomlaworks $
 * @package		K2
 * @author    JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

// K2 helper
require_once(JPATH_SITE.DS."templates".DS.$mainframe->getTemplate().DS."includes".DS."k2helper.php");

$k2tools = new K2Tools();

?>

<?php // Start K2 Item Layout ?>
<span id="startOfPageId<?php echo JRequest::getInt('id'); ?>"></span>

<div id="k2Container" class="itemView<?php if($this->item->params->get('pageclass_sfx')) echo ' '.$this->item->params->get('pageclass_sfx'); ?>">

	<?php // Plugins: BeforeDisplay ?>
	<?php echo $this->item->event->BeforeDisplay; ?>
	
	<?php // K2 Plugins: K2BeforeDisplay ?>
	<?php echo $this->item->event->K2BeforeDisplay; ?>
	

	

	<div class="itemHeader">
	  <?php if($this->item->params->get('itemTitle')): ?>
	  <?php // Item title ?>
	  <h1 class="itemTitle">
	  	<?php echo $this->item->title; ?>
	  	
	  	<?php if($this->item->params->get('itemFeaturedNotice') && $this->item->featured): ?>
	  	<?php // Featured flag ?>
	  	<span>
		  	<sup>
		  		<?php echo JText::_('Featured'); ?>
		  	</sup>
	  	</span>
	  	<?php endif; ?>
	  </h1>
	  <?php endif; ?>
	  

		<?php if($this->item->params->get('itemAuthor')): ?>
		<?php // Item Author ?>
		<span class="itemAuthor">
			<?php echo K2HelperUtilities::writtenBy($this->item->author->profile->gender); ?> <a href="<?php echo $this->item->author->link; ?>"><?php echo $this->item->author->name; ?></a>
		</span>
		|
		<?php endif; ?>
		
		<?php if($this->item->params->get('itemDateCreated')): ?>
		<?php // Date created ?>
		<span class="itemDateCreated">
			<?php echo JHTML::_('date', $this->item->created , JText::_('DATE_FORMAT_LC2')); ?>
		</span>
		|
		<?php endif; ?>
		
		<?php if($this->item->params->get('itemCategory')): ?>
		<?php //  Item category name  ?>
		<span class="itemCategory">
			<span><?php echo JText::_('Published in'); ?></span>
			<a href="<?php echo $this->item->category->link; ?>"><?php echo $this->item->category->name; ?></a>
		</span>
		<?php endif; ?>
  </div>

  <?php // Plugins: AfterDisplayTitle ?>
  <?php echo $this->item->event->AfterDisplayTitle; ?>
  
  <?php // K2 Plugins: K2AfterDisplayTitle ?>
  <?php echo $this->item->event->K2AfterDisplayTitle; ?>

	<?php if(
		isset($this->item->editLink) ||
		$this->item->params->get('itemFontResizer') || 
		$this->item->params->get('itemPrintButton') || 
		$this->item->params->get('itemEmailButton') || 
		$this->item->params->get('itemSocialButton') || 
		$this->item->params->get('itemVideoAnchor') || 
		$this->item->params->get('itemImageGalleryAnchor') || 
		$this->item->params->get('itemCommentsAnchor')
	): ?>
	
  <div class="itemToolbar">
		<ul>
			<?php if($this->item->params->get('itemFontResizer')): ?>
			<?php //  Font Resizer  ?>
			<li>
				<span class="itemTextResizerTitle"><?php echo JText::_('font size'); ?></span>
				<a href="#" id="fontS">
					<img src="/images/M_images/system/font_decrease.gif" title="decrease font size" >
				</a>
				<a href="#" id="fontN">
                    <img src="/images/M_images/system/font_normal.gif" title="reset font size" >
				</a>
				<a href="#" id="fontB">
                    <img src="/images/M_images/system/font_increase.gif" title="increase font size" >
				</a>
			</li>
			<?php endif; ?>
			
			<?php if(isset($this->item->editLink)): ?>
				<?php // Item edit link ?>
				<li class="itemEditLink">
					<a class="modal" rel="{handler:'iframe',size:{x:990,y:650}}" href="<?php echo $this->item->editLink; ?>">
                        <img src="/images/M_images/system/font_increase.gif" title="increase font size" >
					</a>
				</li>
			<?php endif; ?>
			
			<?php if($this->item->params->get('itemPrintButton')): ?>
			<?php //  Print Button  ?>
			<li>
				<?php if(JRequest::getCmd('print')==1): ?>
				<a class="itemPrintLink" href="<?php echo $this->item->printLink; ?>" onclick="window.print();return false;">
                    <span><img src="/images/M_images/printButton.png" title="Print" ></span>
				</a>		
				<?php else: ?>
				<a class="modal itemPrintLink" href="<?php echo $this->item->printLink; ?>" rel="{handler:'iframe',size:{x:900,y:500}}">
                    <span><img src="/images/M_images/printButton.png" title="Print" ></span>
				</a>
				<?php endif; ?>
			</li>
			<?php endif; ?>

			<?php if($this->item->params->get('itemEmailButton') && (!JRequest::getInt('print')) ): ?>
			<?php //  Email Button  ?>
			<li>
				<a class="itemEmailLink" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;" href="<?php echo $this->item->emailLink; ?>">
                    <span><img src="/images/M_images/emailButton.png" title="email" ></span>
				</a>
			</li>
			<?php endif; ?>

			<?php if($this->item->params->get('itemSocialButton') && !is_null($this->item->params->get('socialButtonCode', NULL))): ?>
			<?php //  Item Social Button  ?>
			<li>
				<?php echo $this->item->params->get('socialButtonCode'); ?>
			</li>
			<?php endif; ?>
			
			<?php if($this->item->params->get('itemVideoAnchor') && !empty($this->item->video)): ?>
			<?php //  Anchor link to item video below - if it exists  ?>
			<li>
				<a class="itemVideoLink" href="<?php echo $this->item->link; ?>#itemVideoAnchor"><?php echo JText::_('Video'); ?></a>
			</li>
			<?php endif; ?>
			
			<?php if($this->item->params->get('itemImageGalleryAnchor') && !empty($this->item->gallery)): ?>
			<?php //  Anchor link to item image gallery below - if it exists  ?>
			<li>
				<a class="itemImageGalleryLink" href="<?php echo $this->item->link; ?>#itemImageGalleryAnchor"><?php echo JText::_('Image Gallery'); ?></a>
			</li>
			<?php endif; ?>
			
			<?php if($this->item->params->get('itemCommentsAnchor') && $this->item->params->get('itemComments') && ( ($this->item->params->get('comments') == '2' && !$this->user->guest) || ($this->item->params->get('comments') == '1')) ): ?>
			<?php //  Anchor link to comments below - if enabled  ?>
			<li>
				<?php //  K2 Plugins: K2CommentsCounter  ?>
				<?php echo $this->item->event->K2CommentsCounter; ?>
				<?php if(empty($this->item->event->K2CommentsCounter)):?>					
					<?php if($this->item->numOfComments > 0): ?>
					<a class="itemCommentsLink" href="<?php echo $this->item->link; ?>#itemCommentsAnchor">
						<span><?php echo $this->item->numOfComments; ?></span> <?php echo ($this->item->numOfComments>1) ? JText::_('comments') : JText::_('comment'); ?>
					</a>
					<?php else: ?>
					<a class="itemCommentsLink" href="<?php echo $this->item->link; ?>#itemCommentsAnchor">
						<?php echo JText::_('Be the first to comment!'); ?>
					</a>
					<?php endif; ?>
				<?php endif; ?>
			</li>
			<?php endif; ?>
		</ul>
		<div class="clr"></div>
  </div>
	<?php endif; ?>

	

  <div class="itemBody">

	  <?php //  Plugins: BeforeDisplayContent  ?>
	  <?php echo $this->item->event->BeforeDisplayContent; ?>
	  
	  <?php //  K2 Plugins: K2BeforeDisplayContent  ?>
	  <?php echo $this->item->event->K2BeforeDisplayContent; ?>
	  
	  <?php if($this->item->params->get('itemIntroText')): ?>
		  <?php //  Item introtext  ?>
		  <div class="itemIntroText">
		  	<?php echo $this->item->introtext; ?>
		  </div>
		<?php endif; ?>

	  <?php if($this->item->params->get('itemImage') && !empty($this->item->image)): ?>
	  <?php //  Item Image  ?>
	  <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" href="<?php echo $this->item->imageXLarge; ?>" title="<?php echo JText::_('Click to preview image'); ?>">
		  		<img src="<?php echo $this->item->image; ?>" alt="<?php if(!empty($this->item->image_caption)) echo $this->item->image_caption; else echo $this->item->title; ?>" style="width:<?php echo $this->item->imageWidth; ?>px; height:auto;" />
		  	</a>
		  </span>
		  
		  <?php if($this->item->params->get('itemImageMainCaption') && !empty($this->item->image_caption)): ?>
		  <?php //  Image caption  ?>
		  <span class="itemImageCaption"><?php echo $this->item->image_caption; ?></span>
		  <?php endif; ?>
		  
		  <?php if($this->item->params->get('itemImageMainCredits') && !empty($this->item->image_credits)): ?>
		  <?php //  Image credits  ?>
		  <span class="itemImageCredits">
		  <?php if($this->item->creditsUrl): ?>
		  		<a target="_blank" href="http://<?php echo $this->item->creditsUrl; ?>"><?php echo $this->item->image_credits; ?></a>
		  	<?php else: ?>
		  		<?php echo $this->item->image_credits; ?>
		  	<?php endif; ?>
		  
		  </span>
		  <?php endif; ?>
		  
		  <div class="clr"></div>
	  </div>
	  <?php endif; ?>
	  
	  <?php if($this->item->params->get('itemFullText')): ?>
	  <?php //  Item fulltext  ?>
	  <div class="itemFullText">
	  	<?php echo $this->item->fulltext; ?>
	  </div>
	  <?php endif; ?>
		
		<div class="clr"></div>

	  <?php if($this->item->params->get('itemExtraFields') && count($this->item->extra_fields)): ?>
	  <?php //  Item extra fields  ?>  
	  <div class="itemExtraFields">
	  	<h3><?php echo JText::_('Additional Info'); ?></h3>
	  	<ul>
			<?php foreach ($this->item->extra_fields as $key=>$extraField):?>
			<li class="<?php echo ($key%2) ? "odd" : "even"; ?> type<?php echo ucfirst($extraField->type); ?> group<?php echo $extraField->group; ?>">
				<span class="itemExtraFieldsLabel"><?php echo $extraField->name; ?>:</span>
				<span class="itemExtraFieldsValue"><?php echo $extraField->value; ?></span>
				<br class="clr" />			
			</li>
			<?php endforeach; ?>
			</ul>
	    <div class="clr"></div>
	  </div>
	  <?php endif; ?>
	  
		<?php if($this->item->params->get('itemDateModified') && intval($this->item->modified)!=0):?>
		<?php //  Item date modified  ?>
		<?php if($this->item->created != $this->item->modified): ?>
		<span class="itemDateModified">
			<?php echo JText::_('Last modified on'); ?> <?php echo JHTML::_('date', $this->item->modified, JText::_('DATE_FORMAT_LC2')); ?> 
		</span>
		<?php endif; ?>
		<?php endif; ?>

	  <div class="clr"></div>
  </div>

  <?php if(
  $this->item->params->get('itemHits') || 
  $this->item->params->get('itemTwitterLink') || 
  $this->item->params->get('itemTags') || 
  $this->item->params->get('itemShareLinks') || 
  $this->item->params->get('itemAttachments')
  ): ?>
  <div class="itemLinks">

		<?php if($this->item->params->get('itemHits') || $this->item->params->get('itemTwitterLink')): ?>
		<div class="itemHitsTwitter">
			<?php if($this->item->params->get('itemHits')): ?>
			<?php //  Item Hits  ?>
			<span class="itemHits">
				<?php echo JText::_('Read'); ?> <b><?php echo $this->item->hits; ?></b> <?php echo JText::_('times'); ?>
			</span>
			<?php endif; ?>
			
			<?php if($this->item->params->get('itemHits') && $this->item->params->get('itemTwitterLink')): ?>
			<span class="itemHitsTwitterSep">|</span>
			<?php endif; ?>
		
			<?php if($this->item->params->get('itemTwitterLink') && $this->item->params->get('twitterUsername')): ?>
			<?php //  Twitter Link  ?>
			<span class="itemTwitterLink">
				<a title="<?php echo JText::_('Like this? Tweet it to your followers!'); ?>" href="<?php echo $this->item->twitterURL; ?>" target="_blank">
					<?php echo JText::_('Like this? Tweet it to your followers!'); ?>
				</a>
			</span>
			<?php endif; ?>
			
			<div class="clr"></div>
		</div>
		<?php endif; ?>

		
		
	  <?php if($this->item->params->get('itemTags') && count($this->item->tags)): ?>
	  <?php //  Item tags  ?>
	  <span class="itemTagsBlock">
		  <span><?php echo JText::_("Tagged under"); ?></span>
		  
		    <?php foreach ($this->item->tags as $key=>$tag): ?>
		    <a href="<?php echo $tag->link; ?>"><?php echo trim($tag->name); ?><?php if (($key+1)<count($this->item->tags))echo ',';?></a>
		    <?php endforeach; ?>
	  </span>
	  <?php endif; ?>

	  <?php //  Plugins: AfterDisplayContent  ?>
	  <?php echo $this->item->event->AfterDisplayContent; ?>
	  
	  <?php //  K2 Plugins: K2AfterDisplayContent  ?>
	  <?php echo $this->item->event->K2AfterDisplayContent; ?>

	  <?php if($this->item->params->get('itemAttachments') && count($this->item->attachments)): ?>
	  <?php //  Item attachments  ?>
	  <div class="itemAttachmentsBlock">
		  <span><?php echo JText::_("Download attachments:"); ?></span>
		  <ul class="itemAttachments">
		    <?php foreach ($this->item->attachments as $attachment): ?>
		    <li>
			    <a title="<?php echo K2HelperUtilities::cleanHtml($attachment->titleAttribute); ?>" href="<?php echo $attachment->link; ?>"><?php echo $attachment->title; ?></a>
			    <?php if($this->item->params->get('itemAttachmentsCounter')): ?>
			    <span>(<?php echo $attachment->hits; ?> <?php echo ($attachment->hits==1) ? JText::_('K2_DOWNLOAD') : JText::_('K2_DOWNLOADS'); ?>)</span>
			    <?php endif; ?>
		    </li>
		    <?php endforeach; ?>
		  </ul>
	  </div>
	  <?php endif; ?>

		<div class="clr"></div>
  </div>
  <?php endif; ?>
  
  
  
  

	<div class="clr"></div>

  <?php if($this->item->params->get('itemVideo') && !empty($this->item->video)): ?>
  <?php //  Item video  ?>
  <a name="itemVideoAnchor" id="itemVideoAnchor"></a>

  <div class="itemVideoBlock">
  	<h3><?php echo JText::_('Related Video'); ?></h3>
  	
	  <span class="itemVideo<?php if($this->item->videoType=='embedded'): ?> embedded<?php endif; ?>"><?php echo $this->item->video; ?></span>
	  
	  <?php if($this->item->params->get('itemVideoCaption') && !empty($this->item->video_caption)): ?>
	  <span class="itemVideoCaption"><?php echo $this->item->video_caption; ?></span>
	  <?php endif; ?>
	  
	  <?php if($this->item->params->get('itemVideoCredits') && !empty($this->item->video_credits)): ?>
	  <span class="itemVideoCredits"><?php echo $this->item->video_credits; ?></span>
	  <?php endif; ?>
	  
	  <div class="clr"></div>
  </div>
  <?php endif; ?>
  
  <?php if($this->item->params->get('itemImageGallery') && !empty($this->item->gallery)): ?>
  <?php //  Item image gallery  ?>
  <a name="itemImageGalleryAnchor" id="itemImageGalleryAnchor"></a>
  <div class="itemImageGallery">
	  <h3><?php echo JText::_('Image Gallery'); ?></h3>
	  <?php echo $this->item->gallery; ?>
  </div>
  <?php endif; ?>

  <?php if($this->item->params->get('itemNavigation') && !JRequest::getCmd('print') && (isset($this->item->nextLink) || isset($this->item->previousLink))): ?>
  <?php //  Item navigation  ?>
  <div class="itemNavigation">
  	<span class="itemNavigationTitle"><?php echo JText::_('More in this category:'); ?></span>
	
		<?php if(isset($this->item->previousLink)): ?>
		<a class="itemPrevious" href="<?php echo $this->item->previousLink; ?>">
			&laquo; <?php echo $this->item->previousTitle; ?>
		</a>
		<?php endif; ?>
		
		<?php if(isset($this->item->nextLink)): ?>
		<a class="itemNext" href="<?php echo $this->item->nextLink; ?>">
			<?php echo $this->item->nextTitle; ?> &raquo;
		</a>
		<?php endif; ?>
		
  </div>
  <?php endif; ?>
  
  <?php if($this->item->params->get('itemRating')): ?>
	<?php //  Item Rating  ?>
	<div class="itemRatingBlock">
		<span><?php echo JText::_('Rate this item'); ?></span> 
		<div class="itemRatingForm">
			<ul class="itemRatingList">
				<li class="itemCurrentRating" id="itemCurrentRating<?php echo $this->item->id; ?>" style="width:<?php echo $this->item->votingPercentage; ?>%;"></li>
				<li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('1 star out of 5'); ?>" class="one-star">1</a></li>
				<li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('2 stars out of 5'); ?>" class="two-stars">2</a></li>
				<li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('3 stars out of 5'); ?>" class="three-stars">3</a></li>
				<li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('4 stars out of 5'); ?>" class="four-stars">4</a></li>
				<li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('5 stars out of 5'); ?>" class="five-stars">5</a></li>
			</ul>
			<div id="itemRatingLog<?php echo $this->item->id; ?>" class="itemRatingLog"><?php echo $this->item->numOfvotes; ?></div>
			<div class="clr"></div>
		</div>
		<div class="clr"></div>
	</div>
	<?php endif; ?>
	
	<?php if($this->item->params->get('itemAuthorBlock') && empty($this->item->created_by_alias)):?>
  <?php //  Author Block  ?>
  <div class="itemAuthorBlock">

  	<?php if($this->item->params->get('itemAuthorImage') && !empty($this->item->author->avatar)):?>
  	<img class="itemAuthorAvatar" src="<?php echo $this->item->author->avatar; ?>" alt="<?php echo $this->item->author->name; ?>" />
  	<?php endif; ?>
  	
    <div class="itemAuthorDetails">
      <h3 class="itemAuthorName">
      	<a href="<?php echo $this->item->author->link; ?>"><?php echo $this->item->author->name; ?></a>
      </h3>
      
      <?php if($this->item->params->get('itemAuthorDescription') && !empty($this->item->author->profile->description)):?>
      <p><?php echo $this->item->author->profile->description; ?></p>
      <?php endif; ?>
      
      <?php if($this->item->params->get('itemAuthorURL') && !empty($this->item->author->profile->url)):?>
      <span class="itemAuthorUrl"><?php echo JText::_("Website:"); ?> <a href="<?php echo $this->item->author->profile->url; ?>" target="_blank"><?php echo str_replace('http://','',$this->item->author->profile->url); ?></a></span>
      <?php endif; ?>
      
      <?php if($this->item->params->get('itemAuthorEmail')):?>
      <span class="itemAuthorEmail"><?php echo JText::_("E-mail:"); ?> <?php echo JHTML::_('Email.cloak', $this->item->author->email); ?></span>
      <?php endif; ?>
            
    </div>
    <div class="clr"></div>
  </div>  
  <?php endif; ?>

  <?php if($this->item->params->get('itemAuthorLatest') && empty($this->item->created_by_alias) && isset($this->authorLatestItems)): ?>
  <?php //  Latest items from author  ?>
	<div class="itemAuthorLatest">
		<h3><?php echo JText::_("Latest from"); ?> <?php echo $this->item->author->name; ?></h3>
		<ul>
			<?php foreach($this->authorLatestItems as $key=>$item): ?>
			<li class="<?php echo ($key%2) ? "odd" : "even"; ?>">
				<a href="<?php echo $item->link ?>"><?php echo $item->title; ?></a>
			</li>
			<?php endforeach; ?>
		</ul>
		<div class="clr"></div>
	</div>
	<?php endif; ?>

  <?php if($this->item->params->get('itemRelated') && isset($this->relatedItems)): ?>
  <?php //  Related items by tag  ?>
	<div class="itemRelated">
		<h3><?php echo JText::_("Related items (by tag)"); ?></h3>
		<ul>
			<?php foreach($this->relatedItems as $key=>$item): ?>
			<li class="<?php echo ($key%2) ? "odd" : "even"; ?>">
				<a href="<?php echo $item->link ?>"><?php echo $item->title; ?></a>
			</li>
			<?php endforeach; ?>
		</ul>
		<div class="clr"></div>
	</div>
	<?php endif; ?>

  <?php //  Plugins: AfterDisplay  ?>
  <?php echo $this->item->event->AfterDisplay; ?>
  
  <?php //  K2 Plugins: K2AfterDisplay  ?>
  <?php echo $this->item->event->K2AfterDisplay; ?>

  <?php if($this->item->params->get('itemComments') && ( ($this->item->params->get('comments') == '2' && !$this->user->guest) || ($this->item->params->get('comments') == '1'))):?>
  	<?php //  K2 Plugins: K2CommentsBlock  ?>
  	<?php echo $this->item->event->K2CommentsBlock; ?>
  <?php endif;?>
  
 <?php if(
	 $this->item->params->get('itemComments') 
	 && !JRequest::getInt('print') 
	 && ($this->item->params->get('comments') == '1' || ($this->item->params->get('comments') == '2'))
	 && empty($this->item->event->K2CommentsBlock)
  ):?>
  <?php //  Item comments  ?>
  <a name="itemCommentsAnchor" id="itemCommentsAnchor"></a>
  
  <div class="itemComments">
  
	  <?php if($this->item->params->get('commentsFormPosition')=='above' && $this->item->params->get('itemComments') && !JRequest::getInt('print') && ($this->item->params->get('comments') == '1' || ($this->item->params->get('comments') == '2' && K2HelperPermissions::canAddComment($this->item->catid)))): ?>
	  <?php //  Item comments form  ?>
	  <div class="itemCommentsForm">
	  	<?php echo $this->loadTemplate('comments_form'); ?>
	  </div>
	  <?php endif; ?>
	  
	  <?php if($this->item->numOfComments>0 && $this->item->params->get('itemComments') && !JRequest::getInt('print') && ($this->item->params->get('comments') == '1' || ($this->item->params->get('comments') == '2'))): ?>
	  <?php //  Item user comments  ?>
	  <h3 class="itemCommentsCounter">
	  	<span><?php echo $this->item->numOfComments; ?></span> <?php echo ($this->item->numOfComments>1) ? JText::_('comments') : JText::_('comment'); ?>
	  </h3>
	  
	  <ol class="itemCommentsList">
	  
	  <?php
	 
	  
	   ?>
	    <?php foreach ($this->item->comments as $key=>$comment): ?>
	    <li class="<?php echo $k2tools->getUserTypeClass($comment->userID); ?> <?php echo ($key%2) ? "odd" : "even"; ?>">
	    	
	    	<div class="commentLeft">
				<span class="commentNumber">
			    	<?php echo $key+1;?>
			    </span>
		    </div>
	    
	    	<?php /*<span class="commentLink">
		    	<a href="<?php echo $this->item->link; ?>#comment<?php echo $comment->id; ?>" name="comment<?php echo $comment->id; ?>" id="comment<?php echo $comment->id; ?>">
		    		<?php echo JText::_('Comment Link'); ?>
		    	</a>
		    </span>
		    */ ?>
		    
			
	    	<div class="commentRight">
	    	<?php if($comment->userImage):?>
				<img src="<?php echo $comment->userImage; ?>" alt="<?php echo $comment->userName; ?>" width="<?php echo $this->item->params->get('commenterImgWidth'); ?>" />
				<?php endif; ?>
				
			
		    
		    <span class="commentAuthorName">
			    <?php //echo JText::_("posted by"); ?>
			    <?php if(!empty($comment->userLink)): ?>
			    <a href="<?php echo $comment->userLink; ?>" title="<?php echo $comment->userName; ?>" rel="nofollow">
			    	<?php echo $comment->userName; ?>
			    </a>
			    <?php else: ?>
			    <?php echo $comment->userName; ?>
			    <?php endif; ?>
		    </span>
		    <br/>
		    <span class="commentDate">
		    	<?php echo JHTML::_('date', $comment->commentDate, JText::_('DATE_FORMAT_LC2')); ?>
		    </span>
		    <div class="clr"></div>
		    <p><?php echo $comment->commentText; ?></p>
		    <?php /*
		    <span class="commentAuthorEmail">
		    	<?php echo JHTML::_('Email.cloak', $comment->commentEmail, 0); ?>
		    </span>
		    
		    */ ?>
		    
				</div>
				<div class="clr"></div>
	    </li>
	    <?php endforeach; ?>
	  </ol>
	  
	  <div class="itemCommentsPagination">
	  	<?php echo $this->pagination->getPagesLinks(); ?>
	  	<div class="clr"></div>
	  </div>
		<?php endif; ?>

		<?php if($this->item->params->get('commentsFormPosition')=='below' && $this->item->params->get('itemComments') && !JRequest::getInt('print') && ($this->item->params->get('comments') == '1' || ($this->item->params->get('comments') == '2' && K2HelperPermissions::canAddComment($this->item->catid)))): ?>
	  <?php //  Item comments form  ?>
	  <div class="itemCommentsForm">
	  	<?php echo $this->loadTemplate('comments_form'); ?>
	  </div>
	  <?php endif; ?>
	  
	  <?php $user = &JFactory::getUser(); if ($this->item->params->get('comments') == '2' && $user->guest):?>
	  		<div><?php echo JText::_('Login to post comments');?></div>
	  <?php endif; ?>
	  
  </div>
  <?php endif; ?>

	<div class="itemBackToTop">
		<a href="<?php echo $this->item->link; ?>#startOfPageId<?php echo JRequest::getInt('id'); ?>"><?php echo JText::_("back to top"); ?></a>
	</div>
	
	<div class="clr"></div>
</div>
<?php //  End K2 Item Layout  ?>
