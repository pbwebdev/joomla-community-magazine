<?php
/**
 * @version		$Id: default.php 354 2010-01-30 18:18:40Z joomlaworks $
 * @package		K2
 * @author    	JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

$document = JFactory::getDocument();

// Detecting Home
$frontpageItemIDs = array(571,101,570); // Set any "frontpage" menu items ids here

$menu = JSite::getMenu();
if (($menu->getActive() == $menu->getDefault()) || in_array(JRequest::getInt('Itemid'), $frontpageItemIDs)){
	$siteHome = 1;
} else {
	$siteHome = 0;
}

?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="k2UsersBlock <?php echo $params->get('moduleclass_sfx'); ?>">
	<ul>
		<?php $splitter = round(count($users)/2); ?>
		<?php foreach($users as $key=>$user): ?>
		<li<?php if(count($users)==$key+1) echo ' class="lastItem"'; ?>>
			<?php if($userAvatar && !empty($user->avatar)): ?>
			<a class=" ubUserAvatar" href="<?php echo $user->link; ?>" title="<?php echo K2HelperUtilities::cleanHtml($user->name); ?>"> <img src="<?php echo $user->avatar; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($user->name); ?>" style="width:<?php echo $avatarWidth; ?>px;height:auto;" /> </a>
			<?php endif; ?>
			<?php if($params->get('userFeed')): ?>
			<!-- RSS feed icon -->
			<div class="k2FeedIcon">
				<a href="<?php echo $user->feed; ?>" title="<?php echo JText::_('Subscribe to this RSS feed'); ?>">
					<span><?php echo JText::_('Subscribe to this RSS feed'); ?></span>
				</a>
				<div class="clr"></div>
			</div>
			<?php endif; ?>
			<?php if($params->get('userName')): ?>
			<h2><a href="<?php echo $user->link; ?>"><?php echo $user->name; ?></a></h2>
			<?php endif; ?>
			<?php if ($params->get('userDescription') && $user->description): ?>
			<p class="ubUserDescription"><?php echo K2HelperUtilities::wordLimit($user->description, '4'); ?></p>
			<?php endif; ?>
			<?php if ($params->get('userURL') || $params->get('userEmail')): ?>
			<p class="ubUserAdditionalInfo">
				<?php if ($params->get('userURL') && isset($user->url)): ?>
				<span class="ubUserURL">
				<?php //echo JText::_("Website URL"); ?>
				<a href="<?php echo $user->url; ?>" target="_blank"><?php echo $user->url; ?></a> </span>
				<?php endif; ?>
				<?php if ($params->get('userEmail')): ?>
				<span class="ubUserEmail"> <?php echo JText::_("E-mail"); ?>: <?php echo JHTML::_('Email.cloak', $user->email); ?> </span>
				<?php endif; ?>
			</p>
			<?php endif; ?>
			<?php if(count($user->items)): ?>
			<ul class="ubUserItems">
				<?php foreach ($user->items as $item): ?>
				<li>
					<a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
				</li>
				<?php endforeach ; ?>
			</ul>
			<?php endif; ?>
			<div class="clr"></div>
		</li>
		<?php if($siteHome && ($key+1==$splitter)):?>
	</ul>
	<ul class="rightList">
		<?php endif; ?>
		<?php endforeach; ?>
	</ul>
</div>
<div class="clr"></div>

<?php
/*
<div class="joinUsLink">
	<?php $user = & JUser::getInstance();
			foreach (JModuleHelper::getModules("join") as $mymodule) {
				$cache = &JFactory::getCache($mymodule->module);
				echo $cache->get( array('JModuleHelper', 'renderModule'), array( $mymodule, array('style' => "kmr") ), $mymodule->id. $user->get('aid', 0) );
			}
	?>
</div>
*/
?>
