<?php
/*** @copyright Copyright (C) 2011. All rights reserved. */
// no direct access
defined('_JEXEC') or die;
$app = JFactory::getApplication();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<?php 
// $this->addStyleSheet(JURI::base(true).'/templates/'.$this->template.'/css/font_hanuman.css');
// $this->addStyleSheet('http://fonts.googleapis.com/css?family=Tangerine');
// $this->addStyleSheet('http://fonts.googleapis.com/css?family=Hanuman');

// Detecting Home
$menu = & JSite::getMenu();
if ($menu->getActive() == $menu->getDefault()) :
$siteHome = 1;
else:
$siteHome = 0;
endif;
// get template parameters
$sidecolumnwidth  = "3";
$leftcolgrid    = "3";
$rightcolgrid    = "3";
$maingrid      = "6";
$layoutstyle     = $this->params->get("layoutstyle");
$sidecolumnwidth  = $this->params->get("sidecolumnwidth");

// add grid.php
define( 'YOURBASEPATH', dirname(__FILE__) );
require( YOURBASEPATH.DS."grid.php");

// Detecting Active Variables
$option = JRequest::getCmd('option', '');
$view = JRequest::getCmd('view', '');
$layout = JRequest::getCmd('layout', '');
$task = JRequest::getCmd('task', '');
$itemid = JRequest::getCmd('Itemid', '');
$siteTitle = $app->getCfg('sitename');
if($task == "edit" || $layout == "form" ) :
$fullWidth = 1;
else:
$fullWidth = 0;
endif;
$mydoc =& JFactory::getDocument();

$bodyClass = '';
if($option) $bodyClass .= ' '.$option;
if($view) $bodyClass .= ' view-'.$view;
if($layout) $bodyClass .= ' layout-'.$layout;
if($task) $bodyClass .= ' task-'.$task;
if($itemid) $bodyClass .= ' itemid-'.$itemid;
if($siteHome) $bodyClass .= ' homepage frontpage';
$bodyClass = 'site '.trim($bodyClass);


$pageTitle = "The Joomla! <sup>&reg;</sup> Community Magazine";

if($siteHome) unset($this->_links);
$this->addHeadLink('http://feeds.joomla.org/JoomlaMagazine', 'alternate', 'rel', array('type'=>'application/rss+xml','title'=>'Joomla! Community Magazine | Issues'));
  
?>
<jdoc:include type="head" />
<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template ?>/css/1_grid.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template ?>/css/2_joomla.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template ?>/css/3_layout.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template ?>/css/4_menus.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template ?>/css/5_magazine.css" type="text/css" />

<!--[if IE]>
<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template ?>/css/<?php echo $this->params->get('templateTheme'); ?>/ie.css" type="text/css" />
<![endif]-->
<!--[if IE 6]>
<style>
body {behavior: url("<?php echo $this->baseurl; ?>/templates/<?php echo $this->template ?>/css/csshover3.htc");} #menu li .drop {background:url("images/css/drop.gif") no-repeat right 8px; 
</style>
<![endif]-->
<style type="text/css">
<?php if($fullWidth): ?>
#mainbody{width:100%; background:none;} #content{width:100%;} #sidebar{display:none;} #sidebar2{display:none;}
<?php endif; ?>
</style>
</head>
<body class="<?php echo $bodyClass; ?>">

<?php 
//add menu.php
require( YOURBASEPATH.DS."menu.php"); 
?>


    <div id="titleWrap">
        <div id="title" class="container_12 clearfix">
          <div class="grid_7">
            <h1 title="<?php print $mydoc->getTitle();?>"><?php echo $pageTitle; ?></h1>
          </div>
          <div class="grid_5 right">
            <div class="downdemo">
                <p class="download-16">
                  <a href="http://www.joomla.org/download.html">Download</a>
                </p>
                <p class="demo">
                  <a href="http://demo.joomla.org">Demo</a>
                </p>
            </div>
          </div>
          <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>

<?php if($this->countModules('main-menu')) : ?>
<div id="insetWrap">
  <div id="inset" class="container_12 clearfix">
      <jdoc:include type="modules" name="main-menu" style="" />
        <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>
<?php endif; ?>
<div id="containerWrap">
    <div id="container" class="container_12 clearfix">

        <div id="mainbody" class="<?php echo $maingrid;?> maincontent">
            <?php if ($this->countModules('top or topwithstyle')) : ?>
            <div id="top">
				<jdoc:include type="modules" name="topwithstyle" style="xhtml" />
                <jdoc:include type="modules" name="top" style="" />
                <div class="clr"></div>
            </div>
            <?php endif; ?>
            <div class="clr"></div>
            <jdoc:include type="message" />
            <jdoc:include type="modules" name="before" style="xhtml" />
							<?php $menu = & JSite::getMenu();
								if ($menu->getActive() == $menu->getDefault()) {?>
									<jdoc:include type="modules" name="index-view" style="" />
								<?php }else{?>
								<jdoc:include type="component" />
							<?php } ?>
          <jdoc:include type="modules" name="after" style="xhtml" />
        </div>
        <?php if($this->countModules('leftmenu')) : ?>
        <div id="sidebar" class="<?php echo $leftcolgrid;?> left">
            <jdoc:include type="modules" name="leftmenu" style="xhtml" />
        </div>
        <?php endif; ?>
        <?php if($this->countModules('right or lang or user4')) : ?>
        <div id="sidebar2" class="<?php echo $rightcolgrid;?> right">
            		<jdoc:include type="modules" name="lang" style="xhtml" />
					<?php if($currentModulePosition!=""): // we get this from the k2 item ?>
					<jdoc:include type="modules" name="<?php echo "Topic_".$currentModulePosition; ?>" style="tw" />
					<?php endif; ?>
					<jdoc:include type="modules" name="right" style="tw" />
					<?php if(!$siteHome): ?>
					<jdoc:include type="modules" name="user4" style="tw" />
					<?php endif; ?>
        </div>
        <?php endif; ?>
        <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>
<div id="closingWrap"><div id="closing" class="container_12 clearfix"></div></div>
<div class="clear"></div>
<div id="footerWrap">
  <div id="footer" class="container_12 clearfix">
    <div class="inside">
      <div id="copy">
        <ul class="menu">
          <li class="item97">
            <a href="http://www.joomla.org"><span>Home</span></a>
          </li>
          <li class="item110"><a href="http://www.joomla.org/about-joomla.html"><span>About</span></a>
          </li>
          <li class="item98"><a href="http://community.joomla.org"><span>Community</span></a></li><li class="item99"><a href="http://forum.joomla.org"><span>Forum</span></a>
          </li>
          <li class="item100"><a href="http://extensions.joomla.org"><span>Extensions</span></a></li><li class="item206"><a href="http://resources.joomla.org"><span>Resources</span></a>
          </li>
          <li class="item102"><a href="http://docs.joomla.org"><span>Docs</span></a></li><li class="item101"><a href="http://developer.joomla.org"><span>Developer</span></a>
          </li>
          <li class="item103"><a href="http://shop.joomla.org"><span>Shop</span></a>
          </li>
        </ul>
    <div class="clr"></div>
      </div>
      <div id="link">
        <div id="footerInfo">© 2005 - <?php echo date('Y'); ?> <a href="http://www.opensourcematters.org">Open Source Matters, Inc.</a> All rights reserved.&nbsp;&nbsp;&nbsp;
                     
                     <a href="http://www.joomla.org/accessibility-statement.html">Accessibility Statement</a>&nbsp;&nbsp;&nbsp;
                     <a href="http://www.joomla.org/privacy-policy.html">Privacy Policy</a>&nbsp;&nbsp;&nbsp;
                     <a href="/authorlogin">Log in</a>&nbsp;&nbsp;&nbsp;
                     <a href="http://www.rochenhost.com/joomla-hosting" target="_blank">Joomla Hosting by</a>&nbsp;&nbsp;
                     <a href="http://www.rochenhost.com/joomla-hosting" target="_blank" class="rochen">Rochen Ltd.</a>
                     <!-- end #footerInfo -->
        </div>
        <div class="clr"></div>
      </div>
      <div class="clr"></div>
    </div>
  </div>
</div>
<?php if ($this->countModules('debug')) : ?>
<div id="debug">
  <div class="width">
    <div class="inside">
      <jdoc:include type="modules" name="debug" style="xhtml" />
    </div>
  </div>
</div>
<?php endif; ?>
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-544070-3']);
		  _gaq.push(['_setDomainName', '.joomla.org']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		</script>

		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-544070-25']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		</script>
</body>
</html>
