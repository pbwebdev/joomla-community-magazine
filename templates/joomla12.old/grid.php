<?php
// no direct access
defined('_JEXEC') or die;
switch ($this->countModules('topfeaturehome')) {
	case 1:
		$user1gridcount = "12";
		break;
	case 2:
		$user1gridcount = "6";
		break;	
	case 3:
		$user1gridcount = "4";
		break;		
	case 4:
		$user1gridcount = "3";
		break;		
	case 6:
		$user1gridcount = "2";
		break;	
	default:
		$user1gridcount = "2";
		break;
}
switch ($this->countModules('user2')) {
	case 1:
		$user2gridcount = "12";
		break;	
	case 2:
		$user2gridcount = "6";
		break;		
	case 3:
		$user2gridcount = "4";
		break;		
	case 4:
		$user2gridcount = "3";
		break;		
	case 6:
		$user2gridcount = "2";
		break;	
	default:
		$user2gridcount = "2";
		break;
}
switch ($this->countModules('user3')) {
	case 1:
		$user3gridcount = "12";
		break;	
	case 2:
		$user3gridcount = "6";
		break;		
	case 3:
		$user3gridcount = "4";
		break;		
	case 4:
		$user3gridcount = "3";
		break;		
	case 6:
		$user3gridcount = "2";
		break;	
	default:
		$user3gridcount = "2";
		break;
}
switch ($this->countModules('user4')) {
	case 1:
		$user4gridcount = "12";
		break;	
	case 2:
		$user4gridcount = "6";
		break;		
	case 3:
		$user4gridcount = "4";
		break;		
	case 4:
		$user4gridcount = "3";
		break;		
	case 6:
		$user4gridcount = "2";
		break;	
	default:
		$user4gridcount = "2";
		break;
}
switch ($this->countModules('user5')) {
	case 1:
		$user5gridcount = "12";
		break;	
	case 2:
		$user5gridcount = "6";
		break;		
	case 3:
		$user5gridcount = "4";
		break;		
	case 4:
		$user5gridcount = "3";
		break;		
	case 6:
		$user5gridcount = "2";
		break;	
	default:
		$user5gridcount = "2";
		break;
}
switch ($this->countModules('user6')) {
	case 1:
		$user6gridcount = "12";
		break;	
	case 2:
		$user6gridcount = "6";
		break;		
	case 3:
		$user6gridcount = "4";
		break;		
	case 4:
		$user6gridcount = "3";
		break;		
	case 6:
		$user6gridcount = "2";
		break;	
	default:
		$user6gridcount = "2";
		break;
}
switch ($this->countModules('user7')) {
	case 1:
		$user7gridcount = "12";
		break;	
	case 2:
		$user7gridcount = "6";
		break;		
	case 3:
		$user7gridcount = "4";
		break;		
	case 4:
		$user7gridcount = "3";
		break;		
	case 6:
		$user7gridcount = "2";
		break;	
	default:
		$user7gridcount = "2";
		break;
}
switch ($this->countModules('user8')) {
	case 1:
		$user8gridcount = "12";
		break;	
	case 2:
		$user8gridcount = "6";
		break;		
	case 3:
		$user8gridcount = "4";
		break;		
	case 4:
		$user8gridcount = "3";
		break;		
	case 6:
		$user8gridcount = "2";
		break;	
	default:
		$user8gridcount = "2";
		break;
}
switch ($this->countModules('user9')) {
	case 1:
		$user9gridcount = "12";
		break;	
	case 2:
		$user9gridcount = "6";
		break;		
	case 3:
		$user9gridcount = "4";
		break;		
	case 4:
		$user9gridcount = "3";
		break;		
	case 6:
		$user9gridcount = "2";
		break;	
	default:
		$user9gridcount = "2";
		break;
}
switch ($this->countModules('user10')) {
	case 1:
		$user10gridcount = "12";
		break;	
	case 2:
		$user10gridcount = "6";
		break;		
	case 3:
		$user10gridcount = "4";
		break;		
	case 4:
		$user10gridcount = "3";
		break;		
	case 6:
		$user10gridcount = "2";
		break;	
	default:
		$user10gridcount = "2";
		break;
}
?>

<?php
// Main columns
if ($this->countModules('leftmenu') and $layoutstyle=="lmr") :?>
<?php $leftcolgrid = "grid_".$sidecolumnwidth." pull_".(12-$sidecolumnwidth);?>
<?php $maingrid = "grid_".(12-$sidecolumnwidth)." push_".$sidecolumnwidth;?>
<?php endif; ?>
<?php
if ($this->countModules('right') and $layoutstyle=="lmr") :?>
<?php $rightcolgrid = "grid_".$sidecolumnwidth;?>
<?php $maingrid = "grid_".(12-$sidecolumnwidth);?>
<?php endif; ?>
<?php
if ($this->countModules('leftmenu and right') == 1 and $layoutstyle=="lmr") :?>
<?php $leftcolgrid = "grid_".$sidecolumnwidth." pull_".(12-($sidecolumnwidth)*2);?>
<?php $rightcolgrid = "grid_".$sidecolumnwidth;?>
<?php $maingrid = "grid_".(12-($sidecolumnwidth)*2)." push_".$sidecolumnwidth;?>
<?php endif; ?>

<?php
if ($this->countModules('leftmenu') and $layoutstyle=="mlr") :?>
<?php $leftcolgrid = "grid_".$sidecolumnwidth;?>
<?php $maingrid = "grid_".(12-$sidecolumnwidth);?>
<?php endif; ?>
<?php
if ($this->countModules('right') and $layoutstyle=="mlr") :?>
<?php $rightcolgrid = "grid_".$sidecolumnwidth;?>
<?php $maingrid = "grid_".(12-$sidecolumnwidth);?>
<?php endif; ?>
<?php
if ($this->countModules('leftmenu and right') == 1 and $layoutstyle=="mlr") :?>
<?php $leftcolgrid = "grid_".$sidecolumnwidth;?>
<?php $rightcolgrid = "grid_".$sidecolumnwidth;?>
<?php $maingrid = "grid_".(12-($sidecolumnwidth)*2);?>
<?php endif; ?>

<?php
if ($this->countModules('leftmenu') and $layoutstyle=="lrm") :?>
<?php $leftcolgrid = "grid_".$sidecolumnwidth." pull_".(12-$sidecolumnwidth);?>
<?php $maingrid = "grid_".(12-$sidecolumnwidth)." push_".$sidecolumnwidth;?>
<?php endif; ?>
<?php
if ($this->countModules('right') and $layoutstyle=="lrm") :?>
<?php $rightcolgrid = "grid_".$sidecolumnwidth." pull_".(12-$sidecolumnwidth);?>
<?php $maingrid = "grid_".(12-$sidecolumnwidth)." push_".$sidecolumnwidth;?>
<?php endif; ?>
<?php
if ($this->countModules('leftmenu and right') == 1 and $layoutstyle=="lrm") :?>
<?php $leftcolgrid = "grid_".$sidecolumnwidth." pull_".(12-($sidecolumnwidth)*2);?>
<?php $rightcolgrid = "grid_".$sidecolumnwidth." pull_".(12-($sidecolumnwidth)*2);?>
<?php $maingrid = "grid_".(12-($sidecolumnwidth)*2)." push_".$sidecolumnwidth*2;?>
<?php endif; ?>