<?php
/**
 * @version		1.1
 * @package		Venom (template)
 * @author    JoomlaWorks - http://joomlaworks.net
 * @copyright	Copyright (c) 2006-2011 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/* -------------------- API Calls -------------------- */
$document 	= & JFactory::getDocument();

// Requests
$option 		= JRequest::getCmd('option');
$view 			= JRequest::getCmd('view');
$layout 		= JRequest::getCmd('layout');
$page 			= JRequest::getCmd('page');
$task 			= JRequest::getCmd('task');
$itemid 		= JRequest::getInt('Itemid');
$tmpl 			= JRequest::getCmd('tmpl');



/* -------------------- CSS & JS Handling -------------------- */
$document->addStyleSheet(JURI::base(true).'/templates/system/css/system.css');
$document->addStyleSheet(JURI::base(true).'/templates/system/css/general.css');
if($tmpl!='component'){
	$document->addStyleSheet(JURI::base(true).'/templates/'.$this->template.'/css/template.php?v=20120314_2100');
}

// Alternate Language Style Sheets
$document->addStyleSheet(JURI::base(true).'/templates/'.$this->template.'/css/font_hanuman.css');
$document->addStyleSheet('http://fonts.googleapis.com/css?family=Tangerine');
$document->addStyleSheet('http://fonts.googleapis.com/css?family=Hanuman');

// JS
//$document->addScript(JURI::base(true).'/templates/'.$this->template.'/js/behaviour.js');



/* ---------------------------- Kyle's stuff ---------------------------- */

// Detecting Home
$menu = & JSite::getMenu();
if ($menu->getActive() == $menu->getDefault()) :
	$siteHome = 1;
else:
	$siteHome = 0;
endif;

// Get template parameters
$sidecolumnwidth	= "3";
$leftcolgrid			= "3";
$rightcolgrid			= "3";
$maingrid					= "6";
$layoutstyle 			= $this->params->get("layoutstyle");
$sidecolumnwidth	= $this->params->get("sidecolumnwidth");

// Add grid.php
require(JPATH_SITE.DS.'templates'.DS.$this->template.DS.'grid.php');

// Detecting Active Variables
$siteTitle = $mainframe->getCfg('sitename');
if($task == "edit" || $layout == "form" ) :
	$fullWidth = 1;
else:
	$fullWidth = 0;
endif;

// K2
$currentModulePosition = JRequest::getVar('currentTag');

// Site title
$pageTitle = "The Joomla!<sup>&reg;</sup> Community Magazine";

// Body Class
$bodyClass = '';
if($option) $bodyClass .= ' '.$option;
if($view) $bodyClass .= ' view-'.$view;
if($layout) $bodyClass .= ' layout-'.$layout;
if($task) $bodyClass .= ' task-'.$task;
if($itemid) $bodyClass .= ' itemid-'.$itemid;
if($siteHome) $bodyClass .= ' homepage frontpage';
$bodyClass = 'site '.trim($bodyClass);

// Feedburner RSS
if($siteHome) unset($document->_links);
$document->addHeadLink('http://feeds.joomla.org/JoomlaMagazine', 'alternate', 'rel', array('type'=>'application/rss+xml','title'=>'Joomla! Community Magazine | Issues'));
