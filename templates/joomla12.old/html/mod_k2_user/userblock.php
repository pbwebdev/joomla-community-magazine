<?php
/**
 * @version		$Id$
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2012 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="k2UserBlock">

<p class="k2ModuleBoxUser">
	  <?php if($params->get('userAvatar')): ?>
	  <a class="k2Avatar ubAvatar" href="<?php echo JRoute::_(K2HelperRoute::getUserRoute($user->id)); ?>" title="<?php echo JText::_('K2_MY_PAGE'); ?>">
	  	<img src="<?php echo K2HelperUtilities::getAvatar($user->id, $user->email); ?>" alt="<?php echo K2HelperUtilities::cleanHtml($user->name); ?>" style="width:<?php echo $avatarWidth; ?>px;height:auto;" />
	  </a>
	  <?php endif; ?>
<p>
	  <span class="ubName">
		<?php if($userGreetingText): ?><?php echo $userGreetingText; ?><?php endif; ?>
		<b><?php echo $user->name; ?></b>
	  </span>
		<!--<span class="ubCommentsCount"><?php echo JText::_('K2_YOU_HAVE'); ?> <b><?php echo $user->numOfComments; ?></b> <?php if($user->numOfComments==1) echo JText::_('K2_PUBLISHED_COMMENT'); else echo JText::_('K2_PUBLISHED_COMMENTS'); ?></span>-->
</p>
<p>
	<span class="ubMenu">
	  <ul class="k2UserBlockActions">
			<li>
				<a href="<?php echo JRoute::_(K2HelperRoute::getUserRoute($user->id)); ?>"><?php echo JText::_('K2_MY_PAGE'); ?></a>
			</li>
			<li>
				<a href="<?php echo $profileLink; ?>"><?php echo JText::_('K2_MY_ACCOUNT'); ?></a>
			</li>
	  </ul>
	  <ul>
			<?php if(is_object($user->profile) && isset($user->profile->addLink)): ?>
			<li>
				<a class="modal" rel="{handler:'iframe',size:{x:990,y:550}}" href="<?php echo $user->profile->addLink; ?>"><?php echo JText::_('K2_ADD_NEW_ITEM'); ?></a>
			</li>
			<?php endif; ?>
			<li><a href="<?php echo JRoute::_('index.php?option=com_k2&view=itemlist&task=category&id=12'); ?>"><?php echo JText::_('Article Submission Bucket'); ?></a>
	  	  	</li>
	  </ul>
	  <ul>
			<li>
			<a href="<?php echo JRoute::_('index.php?option=com_k2&view=itemlist&task=category&id=23&Itemid=62'); ?>"><?php echo JText::_('Author Resources'); ?></a>
			</li>
	  </ul>	
	</span>
	</p>
</p>

  <form action="<?php echo JURI::root(true); ?>/index.php" method="post">
    <input type="submit" name="Submit" class="button ubLogout" value="<?php echo JText::_('K2_LOGOUT'); ?>" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="return" value="<?php echo $return; ?>" />
    <?php echo JHTML::_( 'form.token' ); ?>
  </form>
  <div class="clr"></div>

</div>
