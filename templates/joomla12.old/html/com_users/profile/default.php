<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.6
 */

defined('_JEXEC') or die;
JHtml::_('behavior.tooltip');
JLoader::register('K2HelperUtilities', 'components'.DS.'com_k2'.DS.'helpers'.DS.'utilities.php');
JLoader::register('K2HelperRoute', 'components'.DS.'com_k2'.DS.'helpers'.DS.'route.php');



$userObject = &JFactory::getUser();

$link = K2HelperRoute::getUserRoute($userObject->id);
$app = JFactory::getApplication();
$app->redirect(JRoute::_($link));

?>
<div class="profile<?php echo $this->pageclass_sfx?>">
<?php if ($this->params->get('show_page_heading')) : ?>
<h1>
	<?php echo $this->escape($this->params->get('page_heading')); ?>
</h1>
<?php endif; ?>

<?php //echo $this->loadTemplate('core'); ?>

<?php //echo $this->loadTemplate('params'); ?>

<?php //echo $this->loadTemplate('custom'); ?>

<?php if (JFactory::getUser()->id == $this->data->id) : ?>
<a href="<?php echo JRoute::_('index.php?option=com_users&task=profile.edit&user_id='.(int) $this->data->id);?>">
	<?php // echo JText::_('COM_USERS_Edit_Profile'); ?></a>
<?php endif; ?>
</div>