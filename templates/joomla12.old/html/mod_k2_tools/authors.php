<?php
/**
 * @version		$Id: authors.php 1492 2012-02-22 17:40:09Z joomlaworks@gmail.com $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2012 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
$menu = &JSite::getMenu();
if($menu->getActive() == $menu->getDefault()) $frontpage = true; else $frontpage = false;
?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="k2AuthorsListBlock k2UsersBlock <?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?>">
	<?php
	//O. NOLBERT 2012-12-02 Added the ceil() function to be sure to have an integer number
	$splitter =  ceil(count($authors)/2)-1;
	?>
	
	<ul class="split<?php echo $splitter; ?><?php if($frontpage) echo " frontpage"; ?>">
    <?php foreach ($authors as $key=>$author): ?>
    <li class="a<?php echo $key; ?>">
      <?php if ($params->get('authorAvatar')): ?>
      <a class="k2Avatar abAuthorAvatar" rel="author" href="<?php echo $author->link; ?>" title="<?php echo K2HelperUtilities::cleanHtml($author->name); ?>">
      	<img src="<?php echo $author->avatar; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($author->name); ?>" style="width:<?php echo $avatarWidth; ?>px;height:auto;" />
      </a>
      <?php endif; ?>

      <a class="abAuthorName" rel="author" href="<?php echo $author->link; ?>">
      	<?php echo $author->name; ?>

      	<?php if ($params->get('authorItemsCounter')): ?>
      	<span>(<?php echo $author->items; ?>)</span>
      	<?php endif; ?>
      </a>
	 <p> <?php echo K2HelperUtilities::wordLimit($author->profile->description, '4'); ?></p>
	 <!--<a class="abAuthorsWebsite" href="<?php echo $author->profile->url; ?>" title="<?php echo JText::_("Authors Website"); ?>"><?php echo $author->profile->url; ?></a>-->
	  
      <?php if ($params->get('authorLatestItem')): ?>
      <a class="abAuthorLatestItem" href="<?php echo $author->latest->link; ?>" title="<?php echo K2HelperUtilities::cleanHtml($author->latest->title); ?>">
      	<?php echo $author->latest->title; ?>
	      <span class="abAuthorCommentsCount">
	      	(<?php echo $author->latest->numOfComments; ?> <?php if($author->latest->numOfComments=='1') echo JText::_('K2_MODK2TOOLS_COMMENT'); else echo JText::_('K2_MODK2TOOLS_COMMENTS'); ?>)
	      </span>
      </a>
      <?php endif; ?>
    </li>
    <?php if(($frontpage) && ($key==$splitter)):?>
    </ul>
    
    <ul class="rightList">
        <?php endif; ?>

    <?php endforeach; ?>
  </ul>
</div>
