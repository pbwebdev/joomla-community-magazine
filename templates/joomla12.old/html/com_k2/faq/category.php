<?php
/**
 * @version		$Id: category.php 303 2010-01-07 02:56:33Z joomlaworks $
 * @package		K2
 * @author    JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
define('DATE_FORMAT_TPL','<big>%d</big><small>%b %Y</small>');
?>

<div class="faq">
<?php // Start K2 Category Layout ?>
	<div id="k2Container" class="itemListView<?php if($this->params->get('pageclass_sfx')) echo ' '.$this->params->get('pageclass_sfx'); ?><?php if(count($this->pagination->getPagesCounter())) echo ' hasPagination';?>">
		
		<?php if($this->params->get('show_page_title')): ?>
		<?php // Page title ?>
		<div class="componentheading<?php echo $this->params->get('pageclass_sfx')?>">
			<?php echo $this->escape($this->params->get('page_title')); ?>
		</div>
		<?php endif; ?>
		
		
		
		<?php if(isset($this->category) || ( $this->params->get('subCategories') && isset($this->subCategories) && count($this->subCategories) )): ?>
		<?php // Blocks for current category and subcategories ?>
		<div class="itemListCategoriesBlock">
		
			<?php if(isset($this->category) && ( $this->params->get('catImage') || $this->params->get('catTitle') || $this->params->get('catDescription') || $this->category->event->K2CategoryDisplay )): ?>
			<?php // Category block ?>
			<div class="itemListCategory">
			
				<?php if(isset($this->addLink)): ?>
				<?php // Item add link ?>
				<span class="catItemAddLink">
					<a class="modal" rel="{handler:'iframe',size:{x:990,y:650}}" href="<?php echo $this->addLink; ?>">
						<?php echo JText::_('Add a new item in this category'); ?>
					</a>
				</span>
				<?php endif; ?>
				
				<?php if($this->params->get('catImage')): ?>
				<?php // Category image ?>
				<img alt="<?php echo $this->category->name; ?>" src="<?php echo $this->category->image; ?>" style="width:<?php echo $this->params->get('catImageWidth'); ?>px; height:auto;" />
				<?php endif; ?>
				
				<?php if($this->params->get('catTitle')): ?>
				<?php // Category title ?>
				<h2><?php echo $this->category->name; ?><?php if($this->params->get('catTitleItemCounter')) echo ' ('.$this->pagination->total.')'; ?></h2>
				<?php endif; ?>
				
				<?php if($this->params->get('catDescription')): ?>
				<?php // Category description ?>
				<p><?php echo $this->category->description; ?></p>
				<?php endif; ?>
				
				<?php // K2 Plugins: K2CategoryDisplay ?>
				<?php echo $this->category->event->K2CategoryDisplay; ?>
				
				<div class="clr"></div>
			</div>
			<?php endif; ?>
			
			<?php if($this->params->get('subCategories') && isset($this->subCategories) && count($this->subCategories)): ?>
			<?php // Subcategories ?>
			<div class="itemListSubCategories">
	        						
				<?php foreach($this->subCategories as $key=>$subCategory): ?>
				<div class="subCategoryContainer" style="width:<?php echo number_format(100/$this->params->get('subCatColumns'), 1); ?>%;">              
        			<div class="subCategory">                                        
						<?php if($this->params->get('subCatImage')): ?>
						<?php // Subcategory image ?>
						<a class="subCategoryImage" href="<?php echo $subCategory->link; ?>">
							<img alt="<?php echo $subCategory->name; ?>" src="<?php echo $subCategory->image; ?>" />
						</a>
						<?php endif; ?>
		
						<?php if($this->params->get('subCatTitle')): ?>
						<?php // Subcategory title ?>
						<h2>
							<a href="<?php echo $subCategory->link; ?>">
							<?php echo $subCategory->name; ?>
							</a>                        			
						</h2>
                        <h3>
						<?php if($this->params->get('subCatTitleItemCounter')): echo $subCategory->numOfItems.'&nbsp;'.JText::_('Articles'); endif;?>
                        </h3>
						<?php endif; ?>
		
						<?php if($this->params->get('subCatDescription')): ?>
						<?php // Subcategory description ?>
						<p><?php echo $subCategory->description; ?></p>
						<?php endif; ?>
			
						<?php // Subcategory more... ?>
						<?php /* <a class="subCategoryMore" href="<?php echo $subCategory->link; ?>">
									<?php echo JText::_('View items...'); ?>
								</a> */ ?>
			
					<div class="clr"></div>  
                  </div>
				</div>
				<?php if(($key+1)%($this->params->get('subCatColumns'))==0): ?>
				<div class="clr"></div>
				<?php endif; ?>
				<?php endforeach; ?>
				
				<div class="clr"></div>
			</div>
			<?php endif; ?>
	
		</div>
		<?php endif; ?>
		
		
		
		<?php if((isset($this->leading) || isset($this->primary) || isset($this->secondary) || isset($this->links)) && (count($this->leading) || count($this->primary) || count($this->secondary) || count($this->links))): ?>
		<?php // Item list ?>
		<div class="itemList">
			
			<?php if(isset($this->leading) && count($this->leading)): ?>
			<?php // Leading items ?>
			<div id="itemListLeading">
				<?php foreach($this->leading as $key=>$item): ?>
				<div class="itemContainer" style="width:<?php echo number_format(100/$this->params->get('num_leading_columns'), 1); ?>%;">
				<?php
					// Load category_item.php by default
					$this->item=$item;
					echo $this->loadTemplate('item');
				?>
				</div>
				<?php if(($key+1)%($this->params->get('num_leading_columns'))==0): ?>
				<div class="clr"></div>
				<?php endif; ?>
				<?php endforeach; ?>
				<div class="clr"></div>
			</div>
			<?php endif; ?>
			
			<?php if(isset($this->primary) && count($this->primary)): ?>
			<?php // Primary items ?>
			<div id="itemListPrimary">
				<?php foreach($this->primary as $key=>$item): ?>
				<div class="itemContainer<?php if(!$key)echo ' itemContainerFirst'; ?><?php if($key+1==count($this->primary)) echo ' itemContainerLast';?>" style="width:<?php echo number_format(100/$this->params->get('num_primary_columns'), 1); ?>%;">
				<?php
					// Load category_item.php by default
					$this->item=$item;
					echo $this->loadTemplate('item');
				?>
				</div>
				<?php if(($key+1)%($this->params->get('num_primary_columns'))==0): ?>
				<div class="clr"></div>
				<?php endif; ?>
				<?php endforeach; ?>
				<div class="clr"></div>
			</div>
			<?php endif; ?>
	
			<?php if(isset($this->secondary) && count($this->secondary)): ?>
			<?php // Secondary items ?>
			<div id="itemsListSecondary">
				<?php foreach($this->secondary as $key=>$item): ?>
				<div class="itemContainer" style="width:<?php echo number_format(100/$this->params->get('num_secondary_columns'), 1); ?>%;">
				<?php
					// Load category_item.php by default
					$this->item=$item;
					echo $this->loadTemplate('item');
				?>
				</div>
				<?php if(($key+1)%($this->params->get('num_secondary_columns'))==0): ?>
				<div class="clr"></div>
				<?php endif; ?>
				<?php endforeach; ?>
				<div class="clr"></div>
			</div>
			<?php endif; ?>
	
			<?php if(isset($this->links) && count($this->links)): ?>
			<?php // Link items ?>
			<div id="itemListLinks">
				<h4><?php echo JText::_('More...'); ?></h4>
				<?php foreach($this->links as $key=>$item): ?>
				<div class="itemContainer" style="width:<?php echo number_format(100/$this->params->get('num_links_columns'), 1); ?>%;">
				<?php
					// Load category_item_links.php by default
					$this->item=$item;
					echo $this->loadTemplate('item_links');
				?>
				</div>
				<?php if(($key+1)%($this->params->get('num_links_columns'))==0): ?>
				<div class="clr"></div>
				<?php endif; ?>
				<?php endforeach; ?>
				<div class="clr"></div>
			</div>
			<?php endif; ?>
			
		</div>
		
		<?php // Pagination ?>
		<?php if(count($this->pagination->getPagesCounter())): ?>
		<?php if($this->params->get('catPagination')): ?>
		<dl class="pageNavigationContainer">
			
			<?php if($this->params->get('catPagination')) :?>
				<?php if($this->pagination->getPagesCounter()): ?>
				<dd class="pageCounter">
					<span>
						<?php echo $this->pagination->getPagesCounter(); ?>
					</span>
				</dd>
				<?php endif; ?>
			<?php endif; ?>
			
			<?php if($this->params->get('catPaginationResults')):?>
				<dd class="pagesLinks">
					<?php echo $this->pagination->getPagesLinks(); ?>
				</dd>
			<?php endif; ?>
			
		</dl>
		<?php endif; ?>
		<?php endif; ?>
		
		<?php if($this->params->get('catFeedLink')): ?>
		<?php // RSS feed icon ?>
		<div class="k2FeedIcon">
			<a href="<?php echo $this->feed; ?>" title="<?php echo JText::_('Subscribe to this RSS feed'); ?>">
				<span><?php echo JText::_('Subscribe to this RSS feed'); ?></span>
			</a>
			<div class="clr"></div>
		</div>
		<?php endif; ?>
		
		<?php endif; ?>
		
	</div>
</div>
<?php // End K2 Category Layout ?>
