<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php /** @todo Should this be routed */ ?>

<div class="userLogOut <?php echo $this->escape($this->params->get('pageclass_sfx')); ?>">
  <?php if ( $this->params->get( 'show_logout_title' ) ) : ?>
  <h2 class="componentheading"> <?php echo $this->escape($this->params->get( 'header_logout' )); ?> </h2>
  <?php endif; ?>
  <div class="descText"> <?php echo $this->image; ?>
    <?php
			if ($this->params->get('description_logout')) :
				echo $this->escape($this->params->get('description_logout_text'));
			endif;
		?>
    <div class="clr"></div>
  </div>
  <form action="<?php echo JRoute::_( 'index.php' ); ?>" method="post" name="login" id="login" class="userForms">
    <fieldset>
    <input type="submit" name="Submit" class="button" value="<?php echo JText::_( 'Logout' ); ?>" />
    </fieldset>
    <input type="hidden" name="option" value="com_user" />
    <input type="hidden" name="task" value="logout" />
    <input type="hidden" name="return" value="<?php echo $this->return; ?>" />
  </form>
</div>
