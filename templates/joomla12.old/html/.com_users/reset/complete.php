<?php defined('_JEXEC') or die; ?>

<h2 class="componentheading"> <?php echo JText::_('Reset your Password'); ?> </h2>
<form action="<?php echo JRoute::_( 'index.php?option=com_user&task=completereset' ); ?>" method="post" class="userForms josForm form-validate">
  <fieldset>
  <p><?php echo JText::_('RESET_PASSWORD_COMPLETE_DESCRIPTION'); ?></p>
  </fieldset>
  <fieldset>
  <label for="password1" class="hasTip" title="<?php echo JText::_('RESET_PASSWORD_PASSWORD1_TIP_TITLE'); ?>::<?php echo JText::_('RESET_PASSWORD_PASSWORD1_TIP_TEXT'); ?>"><?php echo JText::_('Password'); ?>:</label>
  <input id="password1" name="password1" type="password" class="required validate-password" />
  </fieldset>
  <fieldset>
  <label for="password2" class="hasTip" title="<?php echo JText::_('RESET_PASSWORD_PASSWORD2_TIP_TITLE'); ?>::<?php echo JText::_('RESET_PASSWORD_PASSWORD2_TIP_TEXT'); ?>"><?php echo JText::_('Verify Password'); ?>:</label>
  <input id="password2" name="password2" type="password" class="required validate-password" />
  </fieldset>
  <fieldset>
  <button type="submit" class="validate"><?php echo JText::_('Submit'); ?></button>
  </fieldset>
  <?php echo JHTML::_( 'form.token' ); ?>
</form>
