/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date    2015-12-23
 */

var Josetta = {

  saved : {},

  inputsList : [],

  initCount : 0,

  docReady : false,

  messages : {},

  formSetup : {},

  /* set in PHP */
  translatorViewUrl : '',
  languages : {},
  itemType : '',
  srcLanguage : '',
  originalItemId : 0,
  josettaItemid : 0,
  formToken : '',
  saveEditorContent : [],

  init : function() {

    try {
      Josetta.inputsList = document.getElementsByTagName('input');
      Josetta.docReady = Josetta.inputsList && Josetta.inputsList.length;
      if (Josetta.docReady) {
        Josetta.languages.each(function(language) {
          Josetta.initEditorsHash(language.code);
          if (!Josetta.saved[language.code]) {
            document.id('jta_lang_' + language.code).addClass('jta-modified');
          }
        });
      }
    } catch (e) {
    }
    Josetta.initCount++;
    if (!Josetta.docReady && (Josetta.initCount < 10)) {
      setTimeout(Josetta.init, 500);
    }
    if (Josetta.initCount >= 20) {
      alert('Cannot Initialize Josetta javascript');
    }
  },

  initEditorsHash : function(languageCode) {
    for ( var i = 0; i < Josetta.inputsList.length; i++) {
      if ((Josetta.inputsList[i].type == 'hidden') && (Josetta.inputsList[i].name.substr(0, 9) == 'jta_sha1_')
          && (Josetta.inputsList[i].name.substr(9, languageCode.length) == languageCode)) {
        editorId = Josetta.inputsList[i].name.substr(9, 200);
        try {
          editorSha1 = jta_sha1(Josetta.getEditorContent(editorId));
        } catch (e) {
          Josetta.docReady = false;
          return;
        }
        // store sha1, for later comparison
        Josetta.inputsList[i].value = editorSha1;
      }
    }
  },

  itemChanged : function(element) {
    try {
      var reg = new RegExp("_");
      var element_id = typeof element == 'string' ? element : element.id;
      var bits = element_id.split(reg);
      var language = bits[0] + '_' + bits[1];
      Josetta.saved[language] = false;
      document.id('jta_lang_' + language).addClass('jta-modified');
    } catch (e) {
      alert(e.toString());
    }
  },

  close : function(timeout) {
    if (timeout) {
      setTimeout( function(){window.location = Josetta.translatorViewUrl;}, timeout);
    } else {
      window.location = Josetta.translatorViewUrl;
    }
  },

  hasUnsavedContent : function(languageCode) {
    var isSaved = true;
    var editorId = '';
    var editorSha1 = '';
    isSaved = Josetta.saved[languageCode];

    // special case: editors: no standard way to monitor change,
    // so using a Sha1: first lookup all editor instances, then check them
    for ( var i = 0; i < Josetta.inputsList.length; i++) {
      if ((Josetta.inputsList[i].type == 'hidden') && (Josetta.inputsList[i].name.substr(0, 9) == 'jta_sha1_')
          && (Josetta.inputsList[i].name.substr(9, languageCode.length) == languageCode)) {
        editorId = Josetta.inputsList[i].name.substr(9, 200);
        editorSha1 = jta_sha1(Josetta.getEditorContent(editorId));
        isSaved = isSaved && (editorSha1 == Josetta.inputsList[i].value);
      }
    }

    return !isSaved;
  },

  hasUnsavedContentInAnyLanguage : function() {
    var hasUnsaved = false;
    Josetta.languages.each(function(language) {
      hasUnsaved = hasUnsaved || Josetta.hasUnsavedContent(language.code);
    });
    return hasUnsaved;
  },

  cancel : function() {
    var shouldClose = !Josetta.hasUnsavedContentInAnyLanguage()
        || confirm(Joomla.JText._('COM_JOSETTA_CONFIRM_TRANSLATE_CANCEL'));
    if (shouldClose) {
      Josetta.close();
    }
    ;
  },

  save : function(task) {
    Josetta.removeMessages();
    Josetta.languages.each(function(language) {
      switch (task) {
      case 'translate.apply':
      case 'translate.save':
        if (!Josetta.hasUnsavedContent(language.code)) {
          Josetta.addMessage('Item ' + language.name + ' not modified. No need to save', 'success');
          if (task == 'translate.save' && !Josetta.hasUnsavedContentInAnyLanguage()) {
            Josetta.close(2000);
          }
          break;
        }
        if (document.formvalidator.isValid(document.id('translate-form-' + language.code))) {
          // ajax save
          Josetta.saveForm(task, document.id('translate-form-' + language.code), language);
        } else {
          alert(Joomla.JText._('COM_JOSETTA_TRANSLATE_INVALID'));
        }
        break;
      default:
        break;
      }
    });
    Josetta.displayMessages();
  },

  saveForm : function(task, form, language) {
    var _handler = {
      onSuccess : function(response) {
        try {
          var r = JSON.decode(response);
          if (r.error && r.message) {
            Josetta.addMessage(r.message, 'error');
            Josetta.displayMessages();
          }
          if (r.messages) {
            r.messages.message.each(function(m) {
              Josetta.addMessage(language.name + ': ' + m, r.error ? 'error' : 'success');
            });
            Josetta.displayMessages();
          }
          if (r.success) {
            // reset modified state
            Josetta.initEditorsHash(language.code);
            Josetta.saved[language.code] = true;
            document.id('jta_lang_' + language.code).removeClass('jta-modified');
            // return to list view, if all saving has completed
            if (task == 'translate.save' && !Josetta.hasUnsavedContentInAnyLanguage()) {
              Josetta.close(2000);
            }
            // store id of saved item
            document.id(language.code + '_josetta_form_id').value = r.data;
          }
        } catch (e) {
          alert('saveForm ' + language + ' / ' + e.toString() + "\n[Response:" + response + "\n]");
        }
      },
      onFailure : function(xhr) {
        if (xhr.response) {
          Josetta.addMessage(JSON.decode(xhr.response).msg, 'error');
          Josetta.displayMessages();
        } else {
          Josetta.addMessage(Joomla.JText._('COM_JOSETTA_ERROR_SERVER_NOT_RESPONDING'), 'error');
          Josetta.displayMessages();
          ;
        }
      },
      onError : function(text, error) {
        alert(error + "\n\n" + text);
      }
    };

    // collect editors content
    Josetta.saveEditorContent[language.code]();
    // Send the form.
    form.task.value = task;
    if (!Josetta.formSetup[language.code]) {
      Josetta.formSetup[language.code] = true;
      form.set('send', {
        url : 'index.php?option=com_josetta&task=translate.save&format=json',
        method : 'post',
        onSuccess : _handler.onSuccess,
        onFailure : _handler.onFailure,
        onError : _handler.onError
      });
    }
    if (typeof form.onsubmit == 'function') {
      form.onsubmit();
    }
    if (typeof form.fireEvent == "function") {
      form.fireEvent('submit');
    }
    form.send();
  },

  dissociate : function(languageCode) {
    // ask for confirmation if any content is unsaved
    if (Josetta.hasUnsavedContentInAnyLanguage()) {
      if (!confirm(Joomla.JText._('COM_JOSETTA_CONFIRM_LOOSE_TRANSLATION'))) {
        SqueezeBox.close();
        return;
      }
    }

    if (confirm(Joomla.JText._('COM_JOSETTA_CONFIRM_DISSOCIATE'))) {
      var form = document.id('translate-form-' + languageCode);
      Joomla.submitform('translate.dissociate', form);
    }
  },

  useExistingContent : function(id, language) {

    // ask for confirmation if any content is unsaved
    if (Josetta.hasUnsavedContentInAnyLanguage()) {
      if (!confirm(Joomla.JText._('COM_JOSETTA_CONFIRM_LOOSE_TRANSLATION'))) {
        SqueezeBox.close();
        return;
      }
    }

    // good to go
    window.location = 'index.php?option=com_josetta&task=translate.selectExistingContent&view=combo&type=' + Josetta.itemType
        + '&jlang=' + language + '&source_language=' + Josetta.srcLanguage + '&' + language + '_exist_content_id=' + id
        + '&cid[]=' + Josetta.originalItemId + '&Itemid=' + Josetta.josettaItemid + '&' + Josetta.formToken + '=1' + '&dirty['
        + language + ']=1';
  },

  copyValue : function(fieldName, language) {
    try {
      document.id(language + '_josetta_form_' + fieldName).value = document.id(language + '_refdata_' + fieldName).get('html');
      Josetta.itemChanged(language + '_josetta_form_' + fieldName);
    } catch (e) {
      alert("Error copying original content." + language + '_refdata_' + fieldName + e.toString());
    }
  },

  copyEditor : function(fieldName, language) {
    try {
      var txt = document.id(language + '_refdata_' + fieldName).get('html');
      Josetta.setEditorContent(language + '_josetta_form_' + fieldName, txt);
      Josetta.itemChanged(language + '_josetta_form_' + fieldName);
    } catch (e) {
      alert("Error copying editor original content." + e.toString());
    }
  },

  copyHiddenField : function(fieldName, language) {
    try {
      var sourceField = document.id(language + '_' + fieldName);
      var targetField = document.id(language + '_josetta_form_' + fieldName);

      if (sourceField.checked) {
        targetField.value = "1";
      } else {
        targetField.value = "0";
      }

    } catch (e) {
      alert("Error copying hidden field." + e.toString());
    }
  },

  addMessage : function(text, type) {
    switch (type) {
    case 'success':
      Josetta.messages.success = Josetta.messages.message || [];
      Josetta.messages.success.push(text);
      break;
    case 'error':
      Josetta.messages.error = Josetta.messages.error || [];
      Josetta.messages.error.push(text);
      break;
    }
  },

  displayMessages : function(timeout) {
    Joomla.renderMessages(Josetta.messages);
    if (timeout) {
      setTimeout(function() {
        Josetta.removeMessages();
      }, timeout);
    }
  },

  removeMessages : function() {
    Josetta.messages = {};
    var children = $$('#system-message-container > *');
    children.dissolve();
  }

};