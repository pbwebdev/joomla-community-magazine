/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   weeblr, llc (c) 2014
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.2.1.567
 * @date    2014-01-16
 */

var Josetta = Josetta || {};

Josetta.suggestTranslation = function(fieldName, language, type, token) {

  try {

    var sourceText = document.id(language + '_refdata_' + fieldName).get('html');
    var tgtLanguage = language.replace('_', '-');
    var request = new Request.JSON({

      url : Josetta.suggestUrl + '&' + token + '=1',

      onSuccess : function(responseJSON, responseText) {
        switch (type) {
        case 'text': {
          document.id(language + '_josetta_form_' + fieldName).value = responseJSON.text;
          Josetta.itemChanged(language + '_josetta_form_' + fieldName);
        }
          break;
        case 'editor': {
          Josetta.setEditorContent(language + '_josetta_form_' + fieldName, responseJSON.text);
          Josetta.itemChanged(language + '_josetta_form_' + fieldName);
        }
          break;
        }
      },

      onError : function(text, error) {
        alert('Automated suggestion error: ' + text + ', msg: ' + error);
      },

      onFailure : function(xhr) {
        if (xhr.response) {
          alert(JSON.decode(xhr.response).msg);
        } else {
          alert('Empty response!');
        }
      }

    }).post({
      src : Josetta.srcLanguage,
      tgt : tgtLanguage,
      txt : (type == 'text') ? sourceText : sourceText,
      type : (type == 'text') ? 'text/plain' : 'text/html'
    });

  } catch (e) {
    alert("Could not connect to translation service: " + e.toString());
  }
};
