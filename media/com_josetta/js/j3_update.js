/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author       Yannick Gaultier
 * @copyright    (c) Yannick Gaultier - Weeblr llc - 2015
 * @package      josetta
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version      2.4.3.723
 *
 * 2015-12-23
 */

// insert link to script at bottom of page
(function () {
    var _josetta_version_server = 'u1.weeblr.com/public/direct/josetta/legacy/josetta_updates_j3.js';
    var jta_update = document.createElement('script');
    jta_update.type = 'text/javascript';
    jta_update.async = true;
    jta_update.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + _josetta_version_server;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(jta_update, s);
})();

/**
 * compatiblity with online josetta_update.js file. May need to adjust it, so it
 * doesn't depends on JSON object!
 */
var JSON = {
    encode: function (content) {
        return content;
    }
}

// callback function to inject update data into the document
var JosettaUpdate = {
    togglers: null,
    sliders: null,
    currentUpdate: null,
    _json: null,
    _attempts: 0,
    update_content: function (json) {

        // store json from update server
        JosettaUpdate._json = json;

        // find about update, and display
        JosettaUpdate.doUpdate();
    },

    doUpdate: function () {

        if (typeof _josetta_version == 'undefined' || !jQuery('josetta-update-dontknow')) {
            JosettaUpdate._attempts++;
            // allow some time for Mootols and joomla sliders js to execute
            if (JosettaUpdate._attempts < 10) {
                setTimeout(JosettaUpdate.doUpdate, 800);
            }
            return;
        }

        try {
            // retrieve raw object
            updates = JosettaUpdate._json;

            // make decision
            switch (JosettaUpdate.should_update(updates)) {
                case 'dontknow':
                {
                    jQuery('#josetta-update-dontknow').show();
                    jQuery('#josetta-update-yes').hide();
                    jQuery('#josetta-update-no').hide();
                }
                    break;
                case 'yes':
                {
                    jQuery('#josetta-update-dontknow').hide();
                    jQuery('#josetta-update-yes').show();
                    jQuery('#josetta-update-no').hide();
                    JosettaUpdate.togglers = jQuery('div#josetta-panel-sliders a.accordion-toggle');
                    // replace tags in document, to display new version details
                    var element = jQuery('#josetta-update-yes')[0];
                    element.innerHTML = element.innerHTML.replace(new RegExp('_josetta_update_installed_version_'), _josetta_version);
                    element.innerHTML = element.innerHTML
                        .replace(new RegExp('_josetta_update_current_'), JosettaUpdate.currentUpdate.current);
                    element.innerHTML = element.innerHTML.replace(new RegExp('_josetta_update_changelog_link_'),
                        JosettaUpdate.currentUpdate.changelog_link);
                    element.innerHTML = element.innerHTML.replace(new RegExp('_josetta_update_download_link_'),
                        JosettaUpdate.currentUpdate.download_link);
                    element.innerHTML = element.innerHTML.replace(new RegExp('_josetta_update_note_'), JosettaUpdate.currentUpdate.note);

                    // open, then close the slider
                    setTimeout(JosettaUpdate.open_update_panel, 2000);
                    setTimeout(JosettaUpdate.open_stats_panel, 5000);
                }
                    break;
                case 'no':
                {
                    jQuery('#josetta-update-dontknow').hide();
                    jQuery('#josetta-update-yes').hide();
                    jQuery('#josetta-update-no').show();
                    jQuery('#josetta-update-no').removeClass('josetta_hide');
                }
            }
        } catch (e) {
            alert(e);
        }
        ;
    },

    should_update: function (updates) {

        var _shouldUpdate = false;
        try {
            jQuery.each(updates, function (index, update) {
                if (_shouldUpdate) {
                    return;
                }
                ;
                var tmp = JosettaUpdate.versionCompare(_josetta_version, update.current) == -1;
                tmp = tmp && JosettaUpdate.versionCompare(_josetta_version, update.min_version_to_upgrade) == 1;
                tmp = tmp || JosettaUpdate.versionCompare(_josetta_version, update.min_version_to_upgrade) == 0;
                tmp = tmp
                    && (!update.max_version_to_upgrade
                        || JosettaUpdate.versionCompare(_josetta_version, update.max_version_to_upgrade) == -1 || JosettaUpdate
                        .versionCompare(_josetta_version, update.max_version_to_upgrade) == 0);
                if (tmp) {
                    _shouldUpdate = true;
                    JosettaUpdate.currentUpdate = update;
                }
            });
        } catch (e) {
            return 'dontknow';
        }
        return _shouldUpdate ? 'yes' : 'no';
    },

    open_update_panel: function () {
        var l = JosettaUpdate.togglers.first();
        l.css('color', 'red');
        l.click();
    },
    open_stats_panel: function () {
        JosettaUpdate.togglers[1].click();
    },

    versionCompare: function (str1, str2) {

        var a = str1.split('.');
        var b = str2.split('.');

        for (var i = 0; i < a.length; ++i) {
            a[i] = Number(a[i]);
        }
        if (a.length == 1) {
            a[1] = 0;
        }
        if (a.length == 2) {
            a[2] = 0;
        }
        if (a.length == 3) {
            a[3] = 0;
        }
        for (var i = 0; i < b.length; ++i) {
            b[i] = Number(b[i]);
        }
        if (b.length == 1) {
            b[1] = 0;
        }
        if (b.length == 2) {
            b[2] = 0;
        }
        if (b.length == 3) {
            b[3] = 0;
        }

        if (a[0] > b[0])
            return 1;
        if (a[0] < b[0])
            return -1;

        if (a[1] > b[1])
            return 1;
        if (a[1] < b[1])
            return -1;

        if (a[2] > b[2])
            return 1;
        if (a[2] < b[2])
            return -1;

        if (a[3] > b[3])
            return 1;
        if (a[3] < b[3])
            return -1;

        return 0;

    }
};
