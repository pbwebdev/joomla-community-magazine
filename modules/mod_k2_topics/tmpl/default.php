<?php
/**
 * @version		1.0
 * @package		K2 Topics
 * @author		JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="k2TopicsBlock <?php echo $params->get('moduleclass_sfx'); ?>">
	<ul>
	
	<?php 
	$tags = array();
	foreach ($items as $item){
		$tags[]=$item->tagID;
	}
	$result = array_unique($tags);
	$numOfTags = count($result);
	$splitIndex = round($numOfTags/2);
	$splitTag = $result[$splitIndex];
	
	
	$tags = array();
	
	foreach($result as $tag){
	 $tags[]=$tag;
	}
	$splitTag = $tags[$splitIndex];

	?>
	
	<?php $total = count($items); $split = round($total/2);	?>
	<?php foreach ($items as $key=>$item): ?>
	<?php if($key==0 || $items[$key]->tagID!=$items[$key-1]->tagID):?>
			<li class="topic<?php if(($key==0)||($key+1==(round($total/2)+1))) echo ' first';?>"><a href="<?php echo JRoute::_(K2HelperRoute::getTagRoute($item->name));?>"><?php echo $item->name;?></a>
				<dl>
	<?php endif;?>
					<dd class="issue"><a href="<?php echo JRoute::_(K2HelperRoute::getItemRoute($item->id.':'.$item->alias, $item->catid.':'.$item->categoryAlias));?>"><?php echo $item->title;?></a></dd>
	<?php if($key+1==$total || $items[$key]->tagID!=$items[$key+1]->tagID):?>
				</dl>
			</li>
	<?php endif;?>
	<?php if($key>0 && $items[$key]->tagID!=$items[$key+1]->tagID && $items[$key+1]->tagID==$splitTag ):?>
		</ul>
		<ul class="rightList">
	<?php endif;?>
	<?php endforeach; ?>
	</ul>
</div>
