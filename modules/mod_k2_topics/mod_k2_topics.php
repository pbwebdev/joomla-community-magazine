<?php
/**
 * @version		1.0
 * @package		K2 Topics
 * @author		JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
require_once (dirname(__FILE__).DS.'helper.php');

// Params
$items = modK2TopicsHelper::getItems($params);
require JModuleHelper::getLayoutPath('mod_k2_topics');
