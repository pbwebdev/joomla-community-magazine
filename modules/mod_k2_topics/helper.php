<?php
/**
 * @version		1.0
 * @package		K2 Topics
 * @author		JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

require_once (JPATH_SITE.DS.'components'.DS.'com_k2'.DS.'helpers'.DS.'route.php');

class modK2TopicsHelper {

	function getItems(&$params) {

		$user = &JFactory::getUser();
		$aid = (int) $user->get('aid');

		$db = &JFactory::getDBO();
		$jnow = &JFactory::getDate();
		$now = $jnow->toSQL();
		$nullDate = $db->getNullDate();
		$category = $params->get('category');
		$ordering = $params->get('ordering');
		$exclude = $params->get('exclude');

		$query = "SELECT i.title,i.alias,i.catid,i.id,i.ordering,c.alias AS categoryAlias,tags_xref.tagID, tags.name FROM #__k2_items AS i
		RIGHT JOIN #__k2_tags_xref tags_xref ON tags_xref.itemID = i.id
		RIGHT JOIN #__k2_tags tags ON tags_xref.tagID = tags.id
		LEFT JOIN #__k2_categories c ON c.id = i.catid
		WHERE tags_xref.itemID > 0
		AND tags.published = 1
		AND i.published=1
		AND ( i.publish_up = ".$db->Quote($nullDate)." OR i.publish_up <= ".$db->Quote($now)." ) 
		AND ( i.publish_down = ".$db->Quote($nullDate)." OR i.publish_down >= ".$db->Quote($now)." )
		AND i.trash=0";
		if (K2_JVERSION != '15')
		{
			$query .= " AND i.access IN(".implode(',', $user->getAuthorisedViewLevels()).") ";
		}
		else
		{
			$query .= " AND i.access<={$aid} ";
		}
		$query .= " AND i.trash = 0 AND c.published = 1 ";
		if (K2_JVERSION != '15')
		{
			$query .= " AND c.access IN(".implode(',', $user->getAuthorisedViewLevels()).") ";
		}
		else
		{
			$query .= " AND c.access<={$aid} ";
		}
		$query .= " AND c.trash = 0";
		
		if ($category > 0) {
			if($params->get('category_recursive')){
				$categories = modK2TopicsHelper::getCategoryChildren($category);
				$categories[] = $category;
				$query .= " AND i.catid IN(".implode(',', $categories).")";
			}
			else {
				$query .= " AND i.catid = {$category}";
			}
		}
		
		$query .= " ORDER BY i.ordering DESC";
		
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		$items = array();
		
		if($exclude){
			$exclude = explode(',', $exclude);
			foreach($rows as $key=>$row){
				if(in_array($row->tagID, $exclude)){
					unset($rows[$key]);
				}
			}
		}
		
		if($ordering){
			$ordering = explode(',', $ordering);
			if(is_array($ordering)){
				foreach($ordering as $tagID){
					foreach($rows as $key=>$row){
						if($tagID==$row->tagID){
							$items[]=$row;
							unset($rows[$key]);
						}
					}
				}
			}
		}
		foreach($rows as $row){
			$items[]=$row;
		}
		return $items;
		
	}
	
	function getCategoryChildren($catid) {

	    static $array = array();
		$user = &JFactory::getUser();
	    $aid = (int) $user->get('aid');
	    $catid = (int) $catid;
	    $db = &JFactory::getDBO();
	    $query = "SELECT * FROM #__k2_categories WHERE parent={$catid} AND published=1 AND trash=0" ;
		if (K2_JVERSION != '15')
		{
			$query .= " AND access IN(".implode(',', $user->getAuthorisedViewLevels()).") ";
		}
		else
		{
			$query .= " AND access<={$aid} ";
		}
		$query .= "ORDER BY ordering ";
		
	    $db->setQuery($query);
	    $rows = $db->loadObjectList();
	    if ($db->getErrorNum()) {
			echo $db->stderr();
			return false;
	    }
	    foreach ($rows as $row) {
			array_push($array, $row->id);
			if (modK2TopicsHelper::hasChildren($row->id)) {
				modK2TopicsHelper::getCategoryChildren($row->id);
			}
		}
		return $array;
	}
	
	function hasChildren($id) {

	    $user = &JFactory::getUser();
	    $aid = (int) $user->get('aid');
	    $id = (int) $id;
	    $db = &JFactory::getDBO();
	    $query = "SELECT * FROM #__k2_categories  WHERE parent={$id} AND published=1 AND trash=0";
		if (K2_JVERSION != '15')
		{
			$query .= " AND access IN(".implode(',', $user->getAuthorisedViewLevels()).") ";
		}
		else
		{
			$query .= " AND access<={$aid} ";
		}		
	    $db->setQuery($query);
	    $rows = $db->loadObjectList();
	    if ($db->getErrorNum()) {
			echo $db->stderr();
			return false;
		}
		if (count($rows)) {
			return true;
		} 
		else {
			return false;
		}
	}
}
