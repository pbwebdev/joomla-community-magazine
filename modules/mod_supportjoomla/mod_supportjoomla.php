<?php
/**
 * @version		$Id: mod_supportjoomla.php 12 2010-06-15 07:33:30Z louis $
 * @package		Joomla.Modules
 * @subpackage	mod_supportjoomla
 * @copyright	Copyright (C) 2010 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Build the URL for the image.
$image = JURI::base().'modules/mod_supportjoomla/media/'.$params->get('display_type', 'right-corner').'/'.$params->get('display_color', 'black').'.png';

// Build the CSS style definition for the banner.
$style[] = '.support-joomla-'.$params->get('display_type', 'right-corner').$params->get('moduleclass_sfx').' {';
$style[] = 'display: block;';
switch ($params->get('display_type', 'right-corner'))
{
	case 'left-corner':
		$style[] = 'position: absolute;';
		$style[] = 'left: 0;';
		$style[] = 'top: 0;';
		$style[] = 'height: 125px;';
		$style[] = 'width: 125px;';
		break;

	case 'rectangle':
		$style[] = 'height: 60px;';
		$style[] = 'width: 234px;';
		break;

	case 'right-corner':
		$style[] = 'position: absolute;';
		$style[] = 'right: 0;';
		$style[] = 'top: 0;';
		$style[] = 'height: 125px;';
		$style[] = 'width: 125px;';
		break;

	case 'square':
		$style[] = 'height: 125px;';
		$style[] = 'width: 125px;';
		break;
}
$style[] = 'background: url('.$image.') no-repeat;';
$style[] = 'text-indent: -999em;';
$style[] = 'text-decoration: none;';
$style[] = '}';

// Add the style to the page.
JFactory::getDocument()->addStyleDeclaration(implode($style));

// Display the layout.
require JModuleHelper::getLayoutPath('mod_supportjoomla');
