<?php
/**
 * @version		$Id: default.php 14 2010-06-17 21:43:30Z louis $
 * @package		Joomla.Modules
 * @subpackage	mod_supportjoomla
 * @copyright	Copyright (C) 2010 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<a class="support-joomla-<?php echo $params->get('display_type','right-corner').$params->get('moduleclass_sfx'); ?>" href="http://contribute.joomla.org" title="<?php echo htmlspecialchars(JText::_('Support_Joomla_Title')); ?>">
	<?php echo JText::_('Support_Joomla'); ?>
</a>
