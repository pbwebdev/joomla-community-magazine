<?php
/*---------------------------------------------------------------
# default.php - Mashable Style Social Widget
# ----------------------------------------------------------------
# author Sujith Kumar
# copyright Copyright (C) 2011-12 techlineinfo.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.techlineinfo.com
# Technical Support: http://www.techlineinfo.com/mashable-style-social-widget-for-joomla/
--------------------------------------------------------------*/
// no direct access
defined('_JEXEC') or die('Restricted access'); ?>

<div style="text-align: <?php echo $image_align ?>;">
<div id="techlineinfo-mashable-bar" style="width:<?php echo $widgetwidth_id; ?>px;"> <!-- Begin Widget -->
<div class="fb-likebox" style="background: <?php echo $fbboxcolor_id; ?>;"> <!-- Facebook -->
		<iframe src="//www.facebook.com/plugins/like.php?href=<?php echo $facebook_id; ?>&amp;send=false&amp;layout=standard&amp;width=<?php echo $facebookwidth_id; ?>&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=<?php echo $fbheight_id; ?>&amp;appId=234513819928295" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:<?php echo $facebookwidth_id; ?>px; height:<?php echo $fbheight_id; ?>px;"></iframe>
	</div>
<div class="googleplus" style="background: <?php echo $gpluscolor_id; ?>;"> <!-- Google -->
		<span><?php echo $recommend_id; ?></span>
<div class="g-plusone" data-size="medium"></div>  
		<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script> 
	</div>
<div class="twitter" style="background: <?php echo $twitcolor_id; ?>;"> <!-- Twitter -->
        	<iframe title="" style="width: 300px; height: 20px;" class="twitter-follow-button" src="http://platform.twitter.com/widgets/follow_button.html#_=1319978796351&amp;align=&amp;button=blue&amp;id=twitter_tweet_button_0&amp;lang=en&amp;link_color=&amp;screen_name=<?php echo $twitter_id; ?>&amp;show_count=&amp;show_screen_name=&amp;text_color=" frameborder="0" scrolling="no"></iframe>
	</div>
<div id="email-news-subscribe" style="background: <?php echo $emailcolor_id; ?>;"> <!-- Email Subscribe -->
		<div class="email-box">
			<form action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open('http://feedburner.google.com/fb/a/mailverify?uri=<?php echo $feedburner_id; ?>', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">	
				<input class="email" type="text" style="width: <?php echo $emailwidth_id; ?>px; font-size: 12px;" id="email" name="email" value="<?php echo $emailtext_id; ?>" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">		
				<input type="hidden" value="<?php echo $feedburner_id; ?>" name="uri">
				<input type="hidden" name="loc" value="en_US">
				<input class="subscribe" name="commit" type="submit" value="Subscribe">	
			</form>
		</div>
	</div>
<div id="other-social-bar" style="background: <?php echo $othercolor_id; ?>;"> <!-- Other Social Bar -->
	<ul class="other-follow">
		<li class="my-rss">
			<a rel="nofollow" title="RSS" href="http://feeds.feedburner.com/<?php echo $feedburner_id; ?>" target="_blank">RSS Feed</a>
		</li>
		<li class="my-linkedin">
			<a rel="nofollow external" title="LinkedIn" href="http://my.linkedin.com/in/<?php echo $linkedin_id; ?>" target="_blank">LinkedIn</a>
		</li>
		<li class="my-gplus">
			<a rel="nofollow" title="Google Plus" rel="author" href="http://plus.google.com/<?php echo $googleplus_id; ?>" target="_blank">Google Plus</a>
		</li>
	</ul>
</div>
<div id="get-techlineinfo" style="background: #EBEBEB;border: 1px solid #CCC;border-top: 1px solid white;padding: 1px 8px 1px 3px;text-align: right;border-image: initial;font-size:10px;font-family: "Arial","Helvetica",sans-serif;">
      <span class="author-credit" style="font-family: Arial, Helvetica, sans-serif;"><a href="<?php echo $footerurl_id; ?>" target="_blank" title="<?php echo $footertext_id; ?>"><?php echo $footertext_id; ?> </a></span></div></div> <!-- End Widget -->
</div>
<!--end of social widget--> 