<?php
/*---------------------------------------------------------------
# mod_easymash.php - Mashable Style Social Widget
# ----------------------------------------------------------------
# author Sujith Kumar
# copyright Copyright (C) 2011-12 techlineinfo.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.techlineinfo.com
# Technical Support: http://www.techlineinfo.com/mashable-style-social-widget-for-joomla/
--------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
$doc = &JFactory::getDocument();
$doc->addStyleSheet('modules/mod_easymash/styles.css');
$widgetwidth_id		= $params->get ('widgetwidth_id','280');
$fbboxcolor_id		= $params->get ('fbboxcolor_id','#FFFFFF');
$fbheight_id		= $params->get ('fbheight_id', '80');
$facebook_id		= $params->get ('facebook_id', 'techlineinfo');
$facebookwidth_id	= $params->get ('facebookwidth_id', '270');
$facebookheight_id		= $params->get ('facebookheight_id', '80');
$gpluscolor_id			= $params->get ('gpluscolor_id', '#F5FCFE');
$recommend_id		=	$params->get ('recommend_id','Recomend On Google');
$twitcolor_id		= $params->get ('twitcolor_id','#EEF9FD');
$twitter_id			= $params->get('twitter_id', 'techlineinfo');
$emailcolor_id		= $params->get ('emailcolor_id', '#E3EDF4');
$feedburner_id		= $params->get ('feedburner_id', 'TechlineInfo');
$emailwidth_id		= $params->get ('emailwidth_id','140');
$emailtext_id		= $params->get ('emailtext_id','Enter your email');
$othercolor_id		= $params->get ('othercolor_id', '#D8E6EB');
$linkedin_id		= $params->get ('linkedin_id', 'techlineinfo');
$googleplus_id		= $params->get ('googleplus_id','108956285753620219730');
$footerurl_id		= $params->get ('footerurl_id', 'http://www.techlineinfo.com');
$footertext_id	=$params->get ('footertext_id', 'Get this module from');
require(JModuleHelper::getLayoutPath('mod_easymash'));
