<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// No direct access.
defined('_JEXEC') or die;

if (version_compare(JVERSION, '3.0', 'ge'))
{
	$joomlaVersionPrefix = 'j3';
}
else
{
	$joomlaVersionPrefix = 'j2';
}
require JModuleHelper::getLayoutPath('mod_josetta_jump', $params->get('layout', 'popup') . '_' . $joomlaVersionPrefix);
