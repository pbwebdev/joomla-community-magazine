<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// No direct access.
defined('_JEXEC') or die;

require_once JPATH_ROOT . '/components/com_josetta/helpers/helper.php';
$targetUrl = JURI::root() . 'index.php?option=com_josetta&view=translator&Itemid=' . JosettaHelper::getJosettaItemid();

?>

<span class="multilanguage"><a class="modal" href="<?php echo $targetUrl;?>" rel="{handler:'iframe', size:{x:window.getSize().x*0.95,y:window.getSize().y*0.90}}"><?php echo JText::_('MOD_JOSETTA_JUMP');?></a></span>

