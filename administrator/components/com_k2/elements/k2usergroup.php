<?php
/**
 * @version             1..0.4 2011-08-05
 * @package             K2 - Users view
 * @author              Olivier Nolbert http://www.jiliko.net
 * @copyright           Copyright (c) 2009 - 2011 jiliko.net.
 * @license             GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

if(K2_JVERSION!= '15'){
	jimport('joomla.form.formfield');
	class JFormFieldK2UserGroup extends JFormField {

		var	$type = 'k2usergroup';

		function getInput(){
			return JElementK2UserGroup::fetchElement($this->name, $this->value, $this->element, $this->options['control']);
		}
	}
}

jimport('joomla.html.parameter.element');


class JElementK2UserGroup extends JElement
{

	var	$_name = 'k2usergroup';
	
	function fetchElement($name, $value, &$node, $control_name)
	{
	
		$fieldName = (K2_JVERSION!= '15')? $name : $control_name.'['.$name.']';
		$db = &JFactory::getDBO();
		$query = 'SELECT id as value, name as text FROM #__k2_user_groups g ORDER BY name';
		$db->setQuery( $query );
		$groups = $db->loadObjectList();

		return JHTML::_('select.genericlist',  $groups, ''.$fieldName.'[]', 'class="inputbox" style="width:90%;" multiple="multiple" size="15"', 'value', 'text', $value );

	}

}
