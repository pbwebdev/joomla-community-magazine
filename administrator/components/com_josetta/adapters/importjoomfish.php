<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   weeblr, llc (c) 2014
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.2.1.567
 * @date		2014-01-16
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_JEXEC'))
	die('Direct Access to this location is not allowed.');

/**
 * Implement wizard based importation of joomfish content
 *
 * @author shumisha
 *
 */
class JosettaadminAdapter_Importjoomfish extends JObject
{

	const TERMINATE = -2;
	const CANCEL = -1;
	const START = 0;
	const UPLOAD_FILE = 1;
	const UNZIP_UPLOADED_FILE = 2;
	const STORE_RAW_DATA = 3;
	const IMPORT_MENU_TYPES = 4;
	const IMPORT_MENUS = 5;
	const IMPORT_SECTIONS = 6;
	const IMPORT_CATEGORIES = 7;
	const IMPORT_ARTICLES = 8;
	const IMPORT_FRONTPAGE = 9;
	const POST_PROCESS = 10;

	/**
	 * An array holding each step details
	 * A step is defined as a task, a view and a layout
	 * By default, task can be 'display', but still need
	 * to be defined in array
	 * @var array
	 */
	public $_stepsMap = array(self::TERMINATE => array('task' => 'doTerminate', 'view' => 'wizard', 'layout' => 'default'),
		self::CANCEL => array('task' => 'doCancel', 'view' => 'wizard', 'layout' => 'default'),
		self::START => array('task' => 'doStart', 'view' => 'wizard', 'layout' => 'default'),
		self::UPLOAD_FILE => array('task' => 'doUploadFile', 'view' => 'wizard', 'layout' => 'default'),
		self::UNZIP_UPLOADED_FILE => array('task' => 'doUnzipUploadedFile', 'view' => 'wizard', 'layout' => 'default'),
		self::STORE_RAW_DATA => array('task' => 'doStoreRawData', 'view' => 'wizard', 'layout' => 'default'),
		self::IMPORT_MENU_TYPES => array('task' => 'doImportMenuTypes', 'view' => 'wizard', 'layout' => 'default'),
		self::IMPORT_MENUS => array('task' => 'doImportMenus', 'view' => 'wizard', 'layout' => 'default'),
		self::IMPORT_SECTIONS => array('task' => 'doImportSections', 'view' => 'wizard', 'layout' => 'default'),
		self::IMPORT_CATEGORIES => array('task' => 'doImportCategories', 'view' => 'wizard', 'layout' => 'default'),
		self::IMPORT_ARTICLES => array('task' => 'doImportContent', 'view' => 'wizard', 'layout' => 'default'),
		self::IMPORT_FRONTPAGE => array('task' => 'doImportFrontpage', 'view' => 'wizard', 'layout' => 'default'),
		self::POST_PROCESS => array('task' => 'doPostProcess', 'view' => 'wizard', 'layout' => 'default'));

	public $_stepsCount = 0;
	public $_steps = array('next' => self::START, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);
	public $_button = '';
	public $_buttonsList = array('next', 'previous', 'terminate', 'cancel');
	// visible buttons are displayed as toolbar pressbutton
	// buttons not on that list are passed as 'hidden' post data
	public $_visibleButtonsList = array('previous', 'next', 'terminate', 'cancel');

	protected $_context = 'joomfish';
	protected $_total = 0;
	protected $_parentController = null;
	protected $_filename = '';

	const MAX_ITEMS_PER_STEP = 10;

	/**
	 * Constructor, keep reference to controller
	 * which called the adapter
	 * @param unknown_type $parentController
	 */
	public function __construct($parentController)
	{

		$this->_parentController = $parentController;

	}

	/**
	 * Parameters for current adapter, to be used by parent controller
	 *
	 */
	public function setup()
	{

		$this->_stepsCount = count($this->_stepsMap);

		// prepare data for controller
		$properties = array();

		$properties['_defaultController'] = 'wizard';
		$properties['_defaultTask'] = '';
		$properties['_defaultModel'] = '';
		$properties['_defaultView'] = 'wizard';
		$properties['_defaultLayout'] = 'default';

		$properties['_returnController'] = 'default';
		$properties['_returnTask'] = '';
		$properties['_returnView'] = 'imports';
		$properties['_returnLayout'] = 'default';
		$properties['_pageTitle'] = JText::_('COM_JOSETTA_IMPORTING_TITLE');

		return $properties;

	}

	/**
	 * First step, by default a message
	 * and a Terminate button
	 *
	 */
	public function doStart()
	{

		// clean up previous files, so as to not create a big mess
		jimport('joomla.filesystem.folder');
		if (JFolder::exists(JosettaadminHelper_Files::getTempPath() . '/com_josetta'))
		{
			JFolder::delete(JosettaadminHelper_Files::getTempPath() . '/com_josetta');
		}

		// which button should be displayed ?
		$this->_visibleButtonsList = array('next', 'cancel');

		// A customiser :
		$this->_steps = array('next' => self::UPLOAD_FILE, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);

		// return results
		$result = array();

		$result['mainText'] = JText::_('COM_JOSETTA_IMPORT_JOOMFISH_START');

		return $result;

	}

	/**
	 * Disable missing_translation plugin
	 *
	 */
	public function doUploadFile()
	{

		// check for request forgery
		JRequest::checkToken() or jexit('Invalid Token');

		// which button should be displayed ?
		$this->_visibleButtonsList = array('previous', 'next', 'cancel');

		// next steps definition
		$this->_steps = array('next' => self::UNZIP_UPLOADED_FILE, 'previous' => self::START, 'cancel' => self::CANCEL,
			'terminate' => self::TERMINATE);

		// return results
		$result = array();

		// make sure we can upload, ie set the correct encoding type for the form
		$result['setFormEncType'] = 'multipart/form-data';

		// prepare display
		$result['mainText'] = JText::sprintf('COM_JOSETTA_IMPORT_UPLOAD_FILE', JosettaadminHelper_Files::getMaxUploadSize());

		// add a file browse button
		$result['mainText'] .= '<div style="text-align:center;width:100%;" ><input type="file" name="wizardfile" size="70" /></div>';

		return $result;

	}

	/**
	 * Joomla content sections
	 *
	 */
	public function doUnzipUploadedFile()
	{

		// check for request forgery
		JRequest::checkToken() or jexit('Invalid Token');

		// which button should be displayed ?
		$this->_visibleButtonsList = array();

		// next steps definition
		$this->_steps = array('previous' => self::START, 'next' => self::STORE_RAW_DATA, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);

		// return results
		$result = array();

		// get uploaded file from request
		$uploadedFileRecord = JRequest::getVar('wizardfile', null, 'files');

		// get items from model
		$josettaImportModel = ShlMvcModel_Base::getInstance('Jfimporter', 'JosettaadminModel_');
		$setup = $josettaImportModel->setup();
		if (!$setup)
		{
			$result['mainText'] = $josettaImportModel->getError();
			// which button should be displayed ?
			$this->_visibleButtonsList = array('terminate');
			// next step so to trigger download, as file is ready now
			$this->_steps = array('next' => self::TERMINATE, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);

			return $result;
		}

		// ask model to unzip uploaded file
		$unzipped = $josettaImportModel->unzipUploadedFile($uploadedFileRecord);

		// do we have anything to export ?
		if (!$unzipped)
		{
			$result['mainText'] = $this->_styleErrorMessage($josettaImportModel->getError());
			$josettaImportModel->setImportState(JosettaadminModel_Jfimporter::STATE_FAILED, $result['mainText']);
			// which button should be displayed ?
			$this->_visibleButtonsList = array('terminate');
			// next step so to trigger download, as file is ready now
			$this->_steps = array('next' => self::TERMINATE, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);

			return $result;
		}

		$result['mainText'] = JText::_('COM_JOSETTA_UPLOADED_FILE_UNZIPPED');

		// now move to categories
		$result['continue'] = array('task' => 'next', 'import_id' => $josettaImportModel->getState('import_id'), JSession::getFormToken() => '1');
		$result['continue'] = array_merge($result['continue'], $this->_steps);

		return $result;

	}

	/**
	 * Read unzipped import files, and store their content
	 * in a series of temporary database tables, as raw data
	 * Later on, we will create actual Joomla objects
	 *
	 * We store one file at a time, to be sure
	 * there won't be any time out
	 *
	 */
	public function doStoreRawData()
	{

		// check for request forgery
		JRequest::checkToken('get') or jexit('Invalid Token');

		// which button should be displayed ?
		$this->_visibleButtonsList = array();

		// next steps definition
		$this->_steps = array('next' => self::STORE_RAW_DATA, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);

		// return results
		$result = array();

		$josettaImportModel = ShlMvcModel_Base::getInstance('Jfimporter', 'JosettaadminModel_');
		$importId = (int) JRequest::getInt('import_id', 0);
		$setup = $josettaImportModel->setup($importId);
		if (!$setup)
		{
			$result['mainText'] = $this->_styleErrorMessage($josettaImportModel->getError());
			$josettaImportModel->setImportState(JosettaadminModel_Jfimporter::STATE_FAILED, $josettaImportModel->getError(), $importId);
			// which button should be displayed ?
			$this->_visibleButtonsList = array('terminate');
			// next step so to trigger download, as file is ready now
			$this->_steps = array('next' => self::TERMINATE, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);

			return $result;
		}

		// read list of files
		$rawDataFilesList = $josettaImportModel->getRawDataInputFiles();
		if (empty($rawDataFilesList))
		{
			$this->setError(JText::sprintf('COM_JOSETTA_ERROR_NO_IMPORT_INPUT_FILE', $sourceDir));
			return false;
		}

		// importing one item type at a time, starting with sections, if first time around
		$josettaImportModel = ShlMvcModel_Base::getInstance('Jfimporter', 'JosettaadminModel_');
		$itemType = JRequest::getCmd('item_type');
		// and one file at a time, within an item type
		$start = (int) JRequest::getInt('start', 0);
		do
		{

			// if starting, request next item type
			$itemType = empty($itemType) ? $josettaImportModel->getNextItemType($itemType) : $itemType;

			// do we have an import file for those conditions?
			if (!empty($rawDataFilesList[$itemType][$start]))
			{
				$this->_filename = $rawDataFilesList[$itemType][$start];
			}
			else
			{
				// reset counters
				$this->_filename = '';
				$start = 0;
				// and get next Item type in line
				$itemType = $josettaImportModel->getNextItemType($itemType);
			}

		}
		while (!empty($itemType) && empty($this->_filename));

		// do we have anything to export ?
		if (empty($itemType))
		{

			// mark data as stored
			$josettaImportModel->setImportState(JosettaadminModel_Jfimporter::STATE_RAW_DATA_IMPORTED);

			// move on to actually creating Joomla! objects
			$result['mainText'] = JText::_('COM_JOSETTA_IMPORT_RAW_DATA_STORED');
			// which button should be displayed ?
			$this->_visibleButtonsList = array();
			// next step so to re-enable missing_translation plugin
			$this->_steps = array('next' => self::IMPORT_MENU_TYPES, 'previous' => self::START, 'cancel' => self::CANCEL,
				'terminate' => self::TERMINATE);
			// now move to next step
			$result['continue'] = array('task' => 'next', 'import_id' => $importId, JSession::getFormToken() => '1');
			$result['continue'] = array_merge($result['continue'], $this->_steps);
			return $result;
		}

		// actually import raw data in temp table
		$stored = (int) $josettaImportModel->importRawDataToDb($itemType, $this->_filename);
		if ($stored < 0)
		{
			// error while reading or importing
			$result['mainText'] = $this->_styleErrorMessage($josettaImportModel->getError());
			$josettaImportModel->setImportState(JosettaadminModel_Jfimporter::STATE_FAILED, $josettaImportModel->getError());
			// which button should be displayed ?
			$this->_visibleButtonsList = array('terminate');
			// next step so to trigger download, as file is ready now
			$this->_steps = array('next' => self::TERMINATE, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);

			return $result;
		}

		// prepare for importing next file
		$start++;

		// are we done ? if so, move to next step
		$result['hiddenText'] = '';

		// continuing for another round
		$result['mainText'] = JText::sprintf('COM_JOSETTA_IMPORT_STORING_RAW_DATA', $stored, $itemType,
			str_replace(JPATH_ROOT, '', $this->_filename));

		$result['continue'] = array('task' => 'next', 'start' => $start, 'item_type' => $itemType, 'import_id' => $importId,
			JSession::getFormToken() => '1');
		$result['continue'] = array_merge($result['continue'], $this->_steps);

		return $result;

	}

	/**
	 * Joomla menutypes
	 *
	 */
	public function doImportMenuTypes()
	{

		// check for request forgery
		JRequest::checkToken('get') or jexit('Invalid Token');

		// which button should be displayed ?
		$this->_visibleButtonsList = array();

		// next steps definition
		$this->_steps = array('next' => self::IMPORT_MENUS, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);

		// return results
		$result = array();

		// initialize a model
		$josettaImportModel = ShlMvcModel_Base::getInstance('Jfimporter', 'JosettaadminModel_');
		$importId = (int) JRequest::getInt('import_id', 0);
		$setup = $josettaImportModel->setup($importId);

		// ask it to do the import
		$imported = $josettaImportModel->importItems(JosettaadminModel_Jfimporter::MENU_TYPES);

		// did it go well?
		if ($imported < 0)
		{
			$result['mainText'] = JText::sprintf('COM_JOSETTA_IMPORT_ERROR_IMPORTING_MENU_TYPES', $josettaImportModel->getError());
			$josettaImportModel->setImportState(JosettaadminModel_Jfimporter::STATE_FAILED, $result['mainText']);
			$result['mainText'] = $this->_styleErrorMessage($result['mainText']);
			// which button should be displayed ?
			$this->_visibleButtonsList = array('terminate');
			// next step so to trigger download, as file is ready now
			$this->_steps = array('next' => self::TERMINATE, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);
			return $result;
		}

		$result['mainText'] = JText::sprintf('COM_JOSETTA_IMPORTED_MENU_TYPES', $imported);

		// now move to categories
		$result['continue'] = array('task' => 'next', 'import_id' => $importId, JSession::getFormToken() => '1');
		$result['continue'] = array_merge($result['continue'], $this->_steps);

		return $result;

	}

	/**
	 * Joomla menus
	 *
	 */
	public function doImportMenus()
	{

		// check for request forgery
		JRequest::checkToken('get') or jexit('Invalid Token');

		// which button should be displayed ?
		$this->_visibleButtonsList = array();

		// next steps definition
		$this->_steps = array('next' => self::IMPORT_SECTIONS, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);

		// return results
		$result = array();

		// initialize a model
		$josettaImportModel = ShlMvcModel_Base::getInstance('Jfimporter', 'JosettaadminModel_');
		$importId = (int) JRequest::getInt('import_id', 0);
		$setup = $josettaImportModel->setup($importId);

		// ask it to do the import
		$imported = $josettaImportModel->importItems(JosettaadminModel_Jfimporter::MENU_ITEMS);

		// did it go well?
		if ($imported < 0)
		{
			$result['mainText'] = JText::sprintf('COM_JOSETTA_IMPORT_ERROR_IMPORTING_MENU_ITEMS', $josettaImportModel->getError());
			$josettaImportModel->setImportState(JosettaadminModel_Jfimporter::STATE_FAILED, $result['mainText']);
			$result['mainText'] = $this->_styleErrorMessage($result['mainText']);
			// which button should be displayed ?
			$this->_visibleButtonsList = array('terminate');
			// next step so to trigger download, as file is ready now
			$this->_steps = array('next' => self::TERMINATE, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);
			return $result;
		}

		$result['mainText'] = JText::sprintf('COM_JOSETTA_IMPORTED_MENU_ITEMS', $imported);

		// now move to categories
		$result['continue'] = array('task' => 'next', 'import_id' => $importId, JSession::getFormToken() => '1');
		$result['continue'] = array_merge($result['continue'], $this->_steps);

		return $result;

	}

	/**
	 * Joomla sections
	 *
	 */
	public function doImportSections()
	{

		// check for request forgery
		JRequest::checkToken('get') or jexit('Invalid Token');

		// which button should be displayed ?
		$this->_visibleButtonsList = array();

		// next steps definition
		$this->_steps = array('next' => self::IMPORT_CATEGORIES, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);

		// return results
		$result = array();

		// initialize a model
		$josettaImportModel = ShlMvcModel_Base::getInstance('Jfimporter', 'JosettaadminModel_');
		$importId = (int) JRequest::getInt('import_id', 0);
		$setup = $josettaImportModel->setup($importId);

		// ask it to do the import
		$imported = $josettaImportModel->importItems(JosettaadminModel_Jfimporter::SECTIONS);

		// did it go well?
		if ($imported < 0)
		{
			$result['mainText'] = JText::sprintf('COM_JOSETTA_IMPORT_ERROR_IMPORTING_SECTIONS', $josettaImportModel->getError());
			$josettaImportModel->setImportState(JosettaadminModel_Jfimporter::STATE_FAILED, $result['mainText']);
			$result['mainText'] = $this->_styleErrorMessage($result['mainText']);
			// which button should be displayed ?
			$this->_visibleButtonsList = array('terminate');
			// next step so to trigger download, as file is ready now
			$this->_steps = array('next' => self::TERMINATE, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);
			return $result;
		}

		$result['mainText'] = JText::sprintf('COM_JOSETTA_IMPORTED_SECTIONS', $imported);

		// now move to categories
		$result['continue'] = array('task' => 'next', 'import_id' => $importId, JSession::getFormToken() => '1');
		$result['continue'] = array_merge($result['continue'], $this->_steps);

		return $result;

	}

	/**
	 * Joomla categories
	 *
	 */
	public function doImportCategories()
	{

		// check for request forgery
		JRequest::checkToken('get') or jexit('Invalid Token');

		// which button should be displayed ?
		$this->_visibleButtonsList = array();

		// next steps definition
		$this->_steps = array('next' => self::IMPORT_ARTICLES, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);

		// return results
		$result = array();

		// initialize a model
		$josettaImportModel = ShlMvcModel_Base::getInstance('Jfimporter', 'JosettaadminModel_');
		$importId = (int) JRequest::getInt('import_id', 0);
		$setup = $josettaImportModel->setup($importId);

		// ask it to do the import
		$imported = $josettaImportModel->importItems(JosettaadminModel_Jfimporter::CATEGORIES);

		// did it go well?
		if ($imported < 0)
		{
			$result['mainText'] = JText::sprintf('COM_JOSETTA_IMPORT_ERROR_IMPORTING_CATEGORIES', $josettaImportModel->getError());
			$josettaImportModel->setImportState(JosettaadminModel_Jfimporter::STATE_FAILED, $result['mainText']);
			$result['mainText'] = $this->_styleErrorMessage($result['mainText']);
			// which button should be displayed ?
			$this->_visibleButtonsList = array('terminate');
			// next step so to trigger download, as file is ready now
			$this->_steps = array('next' => self::TERMINATE, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);
			return $result;
		}

		$result['mainText'] = JText::sprintf('COM_JOSETTA_IMPORTED_CATEGORIES', $imported);

		// now move to categories
		$result['continue'] = array('task' => 'next', 'import_id' => $importId, JSession::getFormToken() => '1');
		$result['continue'] = array_merge($result['continue'], $this->_steps);

		return $result;

	}

	/**
	 * Import articles into the DB, one batch at a time
	 *
	 */
	public function doImportContent()
	{

		// check for request forgery
		JRequest::checkToken('get') or jexit('Invalid Token');

		// which button should be displayed ?
		$this->_visibleButtonsList = array();

		// next steps definition
		$this->_steps = array('next' => self::IMPORT_ARTICLES, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);

		// return results
		$result = array();

		$josettaImportModel = ShlMvcModel_Base::getInstance('Jfimporter', 'JosettaadminModel_');
		$importId = (int) JRequest::getInt('import_id', 0);
		$setup = $josettaImportModel->setup($importId, self::MAX_ITEMS_PER_STEP);
		if (!$setup)
		{
			$result['mainText'] = $josettaImportModel->getError();
			$josettaImportModel->setImportState(JosettaadminModel_Jfimporter::STATE_FAILED, $result['mainText'], $importId);
			$result['mainText'] = $this->_styleErrorMessage($result['mainText']);
			// which button should be displayed ?
			$this->_visibleButtonsList = array('terminate');
			// next step so to trigger download, as file is ready now
			$this->_steps = array('next' => self::TERMINATE, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);

			return $result;
		}

		try
		{

			$this->_total = ShlDbHelper::count('#__josetta_jf_content', 'id', array('jta_import_id' => $importId));

		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
		}

		// do we have anything to export ?
		if (empty($this->_total))
		{
			$result['mainText'] = JText::_('COM_JOSETTA_IMPORT_NO_ARTICLE');
			// which button should be displayed ?
			$this->_visibleButtonsList = array();
			// next is to import front page items
			$this->_steps = array('next' => self::IMPORT_FRONTPAGE, 'previous' => self::START, 'cancel' => self::CANCEL,
				'terminate' => self::TERMINATE);
			// now move to next step
			$result['continue'] = array('task' => 'next', 'import_id' => $importId, JSession::getFormToken() => '1');
			$result['continue'] = array_merge($result['continue'], $this->_steps);
			return $result;
		}

		// get new start item
		$nextStart = (int) JRequest::getInt('nextstart', 0);
		if (empty($nextStart))
		{
			// this is first pass, starting from 0
			$start = 0;
			$nextStart = $start + self::MAX_ITEMS_PER_STEP;
			if ($nextStart >= $this->_total)
			{
				// reached the end
				$nextStart = $this->_total;
			}
		}
		else
		{
			$start = $nextStart;
			$nextStart = $start + self::MAX_ITEMS_PER_STEP;
		}

		// are we done ? if so, move to next step
		$result['hiddenText'] = '';

		if ($start >= $this->_total)
		{
			$result['mainText'] = JText::sprintf('COM_JOSETTA_IMPORTED_ARTICLES', $this->_total);
			// which button should be displayed ?
			$this->_visibleButtonsList = array();
			// next step: import front page records
			$this->_steps = array('next' => self::IMPORT_FRONTPAGE, 'previous' => self::START, 'cancel' => self::CANCEL,
				'terminate' => self::TERMINATE);
			// now move to next step
			$result['continue'] = array('task' => 'next', 'import_id' => $importId, JSession::getFormToken() => '1');
			$result['continue'] = array_merge($result['continue'], $this->_steps);
			return $result;

		}

		// ask model to do the import
		$josettaImportModel->setStart($start);
		$imported = $josettaImportModel->importItems(JosettaadminModel_Jfimporter::ARTICLES);
		// did it go well?
		if ($imported < 0)
		{
			$result['mainText'] = JText::sprintf('COM_JOSETTA_IMPORT_ERROR_IMPORTING_CONTENT', $josettaImportModel->getError());
			$josettaImportModel->setImportState(JosettaadminModel_Jfimporter::STATE_FAILED, $result['mainText']);
			$result['mainText'] = $this->_styleErrorMessage($result['mainText']);
			// which button should be displayed ?
			$this->_visibleButtonsList = array('terminate');
			// the end
			$this->_steps = array('next' => self::TERMINATE, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);
			return $result;
		}

		// continuing for another round
		$result['mainText'] = JText::sprintf('COM_JOSETTA_IMPORTING_ARTICLES', $start + 1, $this->_total);

		$result['continue'] = array('task' => 'next', 'nextstart' => $nextStart, 'import_id' => $importId, JSession::getFormToken() => '1');
		$result['continue'] = array_merge($result['continue'], $this->_steps);

		return $result;

	}

	/**
	 * Joomla frontpage records
	 *
	 */
	public function doImportFrontpage()
	{

		// check for request forgery
		JRequest::checkToken('get') or jexit('Invalid Token');

		// which button should be displayed ?
		$this->_visibleButtonsList = array();

		// next steps definition
		$this->_steps = array('next' => self::POST_PROCESS, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);

		// return results
		$result = array();

		// initialize a model
		$josettaImportModel = ShlMvcModel_Base::getInstance('Jfimporter', 'JosettaadminModel_');
		$importId = (int) JRequest::getInt('import_id', 0);
		$setup = $josettaImportModel->setup($importId);

		// ask it to do the import
		$imported = $josettaImportModel->importItems(JosettaadminModel_Jfimporter::FRONTPAGE);

		// did it go well?
		if ($imported < 0)
		{
			$result['mainText'] = JText::sprintf('COM_JOSETTA_IMPORT_ERROR_IMPORTING_FRONTPAGE', $josettaImportModel->getError());
			$josettaImportModel->setImportState(JosettaadminModel_Jfimporter::STATE_FAILED, $result['mainText']);
			$result['mainText'] = $this->_styleErrorMessage($result['mainText']);
			// which button should be displayed ?
			$this->_visibleButtonsList = array('terminate');
			// The end
			$this->_steps = array('next' => self::TERMINATE, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);
			return $result;
		}

		$result['mainText'] = JText::sprintf('COM_JOSETTA_IMPORTED_FRONTPAGE', $imported);

		// now move to next
		$result['continue'] = array('task' => 'next', 'import_id' => $importId, JSession::getFormToken() => '1');
		$result['continue'] = array_merge($result['continue'], $this->_steps);

		return $result;

	}

	/**
	 * Post process all imported translations
	 * Essentially fixing links, to adjust to new ids
	 *
	 */
	public function doPostProcess()
	{

		// check for request forgery
		JRequest::checkToken('get') or jexit('Invalid Token');

		// which button should be displayed ?
		$this->_visibleButtonsList = array();

		// next steps definition
		$this->_steps = array('next' => self::POST_PROCESS, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);

		// return results
		$result = array();

		$josettaImportModel = ShlMvcModel_Base::getInstance('Jfimporter', 'JosettaadminModel_');
		$importId = (int) JRequest::getInt('import_id', 0);
		$setup = $josettaImportModel->setup($importId, self::MAX_ITEMS_PER_STEP);
		if (!$setup)
		{
			$result['mainText'] = $josettaImportModel->getError();
			$josettaImportModel->setImportState(JosettaadminModel_Jfimporter::STATE_FAILED, $result['mainText'], $importId);
			$result['mainText'] = $this->_styleErrorMessage($result['mainText']);
			// which button should be displayed ?
			$this->_visibleButtonsList = array('terminate');
			// next step so to trigger download, as file is ready now
			$this->_steps = array('next' => self::TERMINATE, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);

			return $result;
		}

		// count how many items need reprocessing: menu items and articles
		try
		{

			$nameQuoted = array('id', '#__josetta_jf_journal', 'import_id', 'imported_context');
			$quoted = array($importId, 'menu_items', 'content');
			$this->_total = ShlDbHelper::quoteQuery('select count(??) from ?? where ?? = ? and ?? in (?,?)', $nameQuoted, $quoted)->shlLoadResult();

		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
		}

		// do we have anything to postprocess ?
		if (empty($this->_total))
		{
			$result['mainText'] = JText::_('COM_JOSETTA_IMPORT_NO_POST_PROCESSING');
			$result['mainText'] .= $this->_getTerminateOptions();
			// which button should be displayed ?
			$this->_visibleButtonsList = array('terminate');
			// we should be totally done!
			$josettaImportModel->setImportState(JosettaadminModel_Jfimporter::STATE_COMPLETED);
			$this->_steps = array('next' => self::TERMINATE, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);
			return $result;
		}

		// get new start item
		$nextStart = (int) JRequest::getInt('nextstart', 0);
		if (empty($nextStart))
		{
			// this is first pass, starting from 0
			$start = 0;
			$nextStart = $start + self::MAX_ITEMS_PER_STEP;
			if ($nextStart >= $this->_total)
			{
				// reached the end
				$nextStart = $this->_total;
			}
		}
		else
		{
			$start = $nextStart;
			$nextStart = $start + self::MAX_ITEMS_PER_STEP;
		}

		// are we done ? if so, move to next step
		$result['hiddenText'] = '';

		if ($start >= $this->_total)
		{
			$result['mainText'] = JText::sprintf('COM_JOSETTA_POST_PROCESSED', $this->_total);
			$result['mainText'] .= $this->_getTerminateOptions();

			// which button should be displayed ?
			$this->_visibleButtonsList = array('terminate');
			// we should be totally done!
			$josettaImportModel->setImportState(JosettaadminModel_Jfimporter::STATE_COMPLETED);
			$this->_steps = array('next' => self::TERMINATE, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);
			return $result;

		}

		// ask model to do the processing
		$josettaImportModel->setStart($start);
		$processed = $josettaImportModel->postProcessImportedItems();
		// did it go well?
		if ($processed < 0)
		{
			$result['mainText'] = JText::sprintf('COM_JOSETTA_IMPORT_ERROR_IMPORTING_CONTENT', $josettaImportModel->getError());
			$josettaImportModel->setImportState(JosettaadminModel_Jfimporter::STATE_FAILED, $result['mainText']);
			$result['mainText'] = $this->_styleErrorMessage($result['mainText']);
			$result['mainText'] .= $this->_getTerminateOptions();
			// which button should be displayed ?
			$this->_visibleButtonsList = array('terminate');
			// the end
			$this->_steps = array('next' => self::TERMINATE, 'previous' => self::START, 'cancel' => self::CANCEL, 'terminate' => self::TERMINATE);
			return $result;
		}

		// continuing for another round
		$result['mainText'] = JText::sprintf('COM_JOSETTA_POST_PROCESSING', $start + 1, $this->_total);

		$result['continue'] = array('task' => 'next', 'nextstart' => $nextStart, 'import_id' => $importId, JSession::getFormToken() => '1');
		$result['continue'] = array_merge($result['continue'], $this->_steps);

		return $result;

	}

	/**
	 * Close the wizard window and redirect to default page
	 *
	 */
	public function doTerminate()
	{

		// are we set to purge temporary files ?
		$purgeTempFiles = (int) JRequest::getInt('purge_temp_files', 0);
		if (!empty($purgeTempFiles))
		{
			// always purge temporary files ?
			jimport('joomla.filesystem.folder');
			JFolder::delete(JosettaadminHelper_Files::getTempPath() . '/com_josetta');

			// and temp tables
			$josettaImportModel = ShlMvcModel_Base::getInstance('Jfimporter', 'JosettaadminModel_');
			$josettaImportModel->deleteTemporaryTable($importId = 0);
		}

		// now go back to main page
		$result = array('redirectTo' => true);

		return $result;

	}

	/**
	 * Close the wizard window and redirect to default page
	 *
	 */
	public function doCancel()
	{

		$result = array();
		$result['redirectTo'] = true;
		$result['redirectOptions'] = array('josettaMsg' => 'COM_JOSETTA_WIZARD_CANCELLED');

		return $result;

	}

	protected function _getTerminateOptions()
	{

		$options = '<br /><br />';
		$options .= '<input type="checkbox" name="purge_temp_files" value="1" checked="checked">';
		$options .= JText::_('COM_JOSETTA_PURGE_TEMP_FILES');
		$options .= '<br />';

		return $options;
	}

	/**
	 * Add some html to an error message
	 * for display on wizard page
	 *
	 * @param string $message
	 */
	protected function _styleErrorMessage($message)
	{

		return '<font color="red">' . $message . '</font>';
	}

}
