<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   weeblr, llc (c) 2014
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.2.1.567
 * @date		2014-01-16
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_JEXEC'))
	die('Direct Access to this location is not allowed.');

jimport('joomla.application.component.view');

class JosettaadminViewWizard extends ShlMvcView_Base
{

	// we are in 'editurl' view
	protected $_context = 'wizard';

	public function display($tpl = null)
	{
		// version prefix
		$this->joomlaVersionPrefix = JosettaadminHelper_General::getJoomlaVersionPrefix();

		if (version_compare(JVERSION, '3.0', 'ge'))
		{
			$document = JFactory::getDocument();

			// add modal css and js
			ShlHtmlBs_helper::addBootstrapCss(JFactory::getDocument());
			ShlHtmlBs_helper::addBootstrapModalFixCss(JFactory::getDocument());
			ShlHtmlBs_helper::addBootstrapJs(JFactory::getDocument());

			// build the toolbar
			$this->toolbar = $this->_makeToolbarJ3();
		}
		else
		{ // J2
		// if redirecting to another page, we need to simply send some javascript
		// to : a / close the popup, b / redirect the parent page to where we
		// want to go
		// push a title
			$this->assign('stepTitle', $this->pageTitle);

			if (!empty($this->redirectTo))
			{
				$document = JFactory::getDocument();
				if (!empty($this->redirectTo))
				{
					$js = 'window.addEvent( \'domready\', function () {
        setTimeout( \'shRedirectTo()\', 100);
      });
      function shRedirectTo() {
      parent.window.location="' . $this->redirectTo . '";
      }
      ';
					$document->addScriptDeclaration($js);
				}
			}
			else
			{
				// build the toolbar
				$toolbar = $this->_makeToolbarJ2();
				$this->assignRef('toolbar', $toolbar);

				// add our own css
				$this->_addCss();
			}

			// collect any error
			$this->errors = $this->getErrors();
		}

		// now display normally
		parent::display($this->joomlaVersionPrefix);

	}

	/**
	 * Create toolbar for current view
	 *
	 * @param midxed $params
	 */
	private function _makeToolbarJ2($params = null)
	{
		// if redirect is set, no toolbar
		if (!empty($this->redirectTo))
		{
			return;
		}

		// Get the JComponent instance of JToolBar
		$bar = JToolBar::getInstance('toolbar');

		// add path to our custom buttons
		$bar->addButtonPath(SHLIB_ROOT_PATH . 'toolbarbutton');

		// display all buttons we are supposed to display
		foreach ($this->visibleButtonsList as $button)
		{
			$bar
				->appendButton('Shpopupstandardbutton', $button, JText::_('COM_JOSETTA_WIZARD_' . strtoupper($button)), $task = $button,
					$list = false);
		}

		return $bar;
	}

	/**
	 * Create toolbar for current view
	 *
	 * @param midxed $params
	 */
	private function _makeToolbarJ3($params = null)
	{
		// if redirect is set, no toolbar
		if (!empty($this->redirectTo))
		{
			return;
		}

		// Get the JComponent instance of JToolBar
		$bar = JToolBar::getInstance('toolbar');

		// prepare configuration button
		$bar->addButtonPath(SHLIB_ROOT_PATH . 'toolbarbutton');

		// display all buttons we are supposed to display
		$buttonsGroup = array();
		$this->visibleButtonsList = array_reverse($this->visibleButtonsList);
		foreach ($this->visibleButtonsList as $buttonType)
		{
			$button = array();
			switch ($buttonType)
			{
				case 'next':
					$button['type'] = 'primary';
					$button['onclick'] = "Joomla.submitbutton('next');";
					break;
				case 'previous':
					$button['type'] = '';
					$button['onclick'] = "Joomla.submitbutton('previous');";
					break;
				case 'terminate':
					$button['type'] = 'success';
					$button['onclick'] = "Joomla.submitbutton('terminate');";
					break;
				case 'cancel':
					$button['type'] = '';
					$button['onclick'] = "Joomla.submitbutton('cancel');";
					break;
			}
			$button['size'] = 'large';
			$button['text'] = JText::_('COM_JOSETTA_WIZARD_' . strtoupper($buttonType));
			// build a list of buttons
			// store button in button group
			$buttonsGroup[] = array($button);

		}
		$bar = ShlHtmlBs_Helper::buttonsToolbar($buttonsGroup);
		return $bar;
	}

	private function _addCss()
	{
		// link to Joomla! javascript
		$document = JFactory::getDocument();
		$document->addScript(JUri::root(true) . '/media/system/js/core.js');

		// add our own css / shared with the confirmation box
		JHtml::styleSheet(JosettaadminHelper_General::getComponentUrl() . '/assets/css/wizard.css');
		ShlHtmlBs_helper::addBootstrapCss($document);
	}
}
