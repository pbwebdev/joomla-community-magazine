<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */


// No direct access

defined('_JEXEC') or die('Restricted access');

echo JHtml::_('sliders.start', 'josetta-panel-sliders', array('startOffset' => $this->startOffset));

foreach( $this->sliders as $slider) {

  echo JHtml::_('sliders.panel', JText::_( $slider['title']), 'cpanel-slider-' . $slider['title']);
  echo $slider['content'];

}

echo JHtml::_('sliders.end');
