<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */


// No direct access

defined('_JEXEC') or die('Restricted access');

?>

<!-- start updates panel markup -->

<table class=" josetta_update">

	<thead>
		<tr>
			<td colspan="2">
			<?php echo JText::_( 'COM_JOSETTA_UPDATE_NEW_VERSION_AVAILABLE')?>
      </td>
		</tr>
	</thead>
	
	<tr>
	   <td class="celltitle" >
	     <?php echo JText::_( 'COM_JOSETTA_UPDATE_INSTALLED_VERSION')?>
	   </td>
	   <td class="cellvalue">
	   _josetta_update_installed_version_
     </td>
	</tr>
	
	<tr>
	   <td class="celltitle">
	     <?php echo JText::_( 'COM_JOSETTA_UPDATE_AVAILABLE_VERSION')?>
	   </td>
	   <td class="cellvalue">
	   <?php 
	       echo '_josetta_update_current_ [' 
	       . '<a target="_blank" href="_josetta_update_changelog_link_" >'
	       . JText::_('COM_JOSETTA_UPDATE_VIEW_CHANGELOG') 
	       . '</a>]'
	       . '&nbsp['
	       . '<a target="_blank" href="_josetta_update_download_link_" >'
         . JText::_('COM_JOSETTA_UPDATE_GET_IT') 
         . '</a>]';
	   ?>
     </td>
	</tr>
	
	<tr>
     <td class="celltitle">
       <?php echo JText::_( 'COM_JOSETTA_UPDATE_NOTES')?>
     </td>
     <td class="cellvalue">
     _josetta_update_note_
     </td>
  </tr>
  
</table>

<!-- end updates panel markup -->

