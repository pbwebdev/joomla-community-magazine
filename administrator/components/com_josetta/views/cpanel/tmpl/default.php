<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */


// No direct access
defined('_JEXEC') or die('Restricted access');

$htmlUpdateAvailable = $this->loadTemplate( 'update_available');
$html = '
<fieldset class="adminform">
	<div id="update-info-panel" class="cpanel-container">

		<div id="josetta-update-dontknow" class="cpanel-block josetta_show">
			' . JText::_('COM_JOSETTA_CPANEL_NO_UPDATE_INFORMATION') . '
		</div>

		<div id="josetta-update-no" class="cpanel-block josetta_hide">
			' . JText::_('COM_JOSETTA_CPANEL_NO_UPDATE_AVAILABLE') . '
		</div>

		<div id="josetta-update-yes" class="cpanel-block josetta_hide">
			' . $htmlUpdateAvailable . '
		</div>

	</div>
</fieldset>';

// getting started slider panel
$this->sliders = array();
$this->sliders['update-info'] = array(
    'title' => 'COM_JOSETTA_CPANEL_UPDATE_INFO_TITLE',
    'content' => $html
);

$html = '
<fieldset class="adminform">
	<div class="cpanel-container">';

if(JosettaadminHelper_General::getVersionType() == 'free')
{
	$html .=
'        <div class="cpanel-block">
			' . JText::sprintf('COM_JOSETTA_CPANEL_USING_FREE', JosettaadminHelper_General::getLandingPageUrl()) . '
		 </div>
';
}

$html .=
'		<div id="joomla-ml-sites" class="cpanel-block">
			' . JText::_('COM_JOSETTA_CPANEL_ABOUT_MULTILINGUALSTATUS_MODULE') . '
		</div>
		<div class="cpanel-block">
			' . JText::_('COM_JOSETTA_CPANEL_JOOMLA_ML_SETUP_HELP') . '
		</div>
	</div>
</fieldset>
';
// Joomla multilingual help slider panel
$this->sliders['Joomla-ML-help'] = array(
    'title' => 'COM_JOSETTA_CPANEL_JOOMLA_ML_HELP_TITLE',
    'content' => $html
);

$html = '
<fieldset class="adminform">
<div id="getting-started" class="cpanel-container">
';

if(JosettaadminHelper_General::getVersionType() == 'free')
{
	$html .=
	'        <div class="cpanel-block">
			' . JText::sprintf('COM_JOSETTA_CPANEL_USING_FREE', JosettaadminHelper_General::getLandingPageUrl()) . '
		 </div>
';
} else {
	$html .=

'    <div class="cpanel-block">
     ' . JText::_('COM_JOSETTA_CPANEL_WARNING_AUTHORIZATION') . '
     </div>
';

}

$html .=
'<div class="cpanel-block">
' . JText::_('COM_JOSETTA_ONLINE_USER_MANUAL') . '
</div>
<div class="cpanel-block">
' . JText::_('COM_JOSETTA_ONLINE_GUIDE_BING') . '
</div>
</div>
</fieldset>';

// getting started slider panel
$this->sliders['getting-started'] = array(
    'title' => 'COM_JOSETTA_CPANEL_GETTING_STARTED',
    'content' => $html
);

if(version_compare(JVERSION, '3.0', 'ge')):
	if(!empty( $this->sidebar)): ?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php
  endif;
endif;
?>

<div class="josetta-cpanel-left-block">

  <div class="josetta-stats-container">

    <h3 class="josetta-cpanel-block-title"><?php echo JText::_('COM_JOSETTA_CPANEL_STATS_TITLE'); ?></h3>
    <hr class="josetta-cpanel-block-title" />

    <table class="josetta-stats-table">
      <tr>
        <td class="josetta-stats-table-content-title"><?php echo JText::_('COM_JOSETTA_CONTENT_STATS_TYPE'); ?></td>
        <td class="josetta-stats-table-count-title"><?php echo JText::_('COM_JOSETTA_STATS_COUNT'); ?></td>
      </tr>

      <?php
      foreach( $this->contentTypes as $contentType => $contentTypeName) {
        $count = empty($this->translationStats->associationsRecords[$contentType]) ? 0 : $this->translationStats->associationsRecords[$contentType]->count;
        echo '<tr>'
        . '<td class="josetta-stats-table-content">' . JText::_($contentTypeName) . '</td>'
        . '<td class="josetta-stats-table-count">' . $count . '</td>'
        . '</tr>'
        ;
      }
      ?>

      <tr>
        <td class="josetta-stats-table-total-title"><?php echo JText::_('COM_JOSETTA_STATS_TOTAL'); ?></td>
        <td class="josetta-stats-table-total"><?php echo $this->translationStats->total; ?></td>
      </tr>

    </table>

  </div>
</div>

<div class="josetta-cpanel-right-block">

<?php

  echo $this->loadTemplate($this->joomlaVersionPrefix . '_accordion');

?>

</div>

<div class="josetta-cpanel-bottom-block">
  <div class="josetta-log-container">

    <h3 class="josetta-cpanel-block-title"><?php echo JText::_('COM_JOSETTA_CPANEL_LOGS_TITLE'); ?></h3>
    <hr class="josetta-cpanel-block-title" />

    <div class="josetta-log-lines">
    <?php echo $this->logs; ?>
    </div>
  </div>
</div>

<div class="josetta-footer-container">
	<?php echo $this->footerText; ?>
</div>

<?php

if(version_compare(JVERSION, '3.0', 'ge'))
{
  echo '</div>';
}
