<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// No direct access

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class JosettaadminViewCpanel extends ShlMvcView_Base
{

	const UPDATE_INFO = 0;
	const STATISTICS = 1;
	const GETTNG_STARTED = 2;
	const ML_HELP = 3;

	public function display($tpl = null)
	{
		// read stats
		$this->translationStats = JosettaHelper::getTranslationStats();
		$this->contentTypes = JosettaHelper::getJosettaContentTypes();

		// compute which slider panel to open on start
		if (!empty($this->translationStats->total))
		{
			$this->startOffset = self::STATISTICS;
		}
		else
		{
			$this->startOffset = self::GETTNG_STARTED;
		}

		// we'll need some frontend language strings
		JFactory::getLanguage()->load('com_josetta', JPATH_ROOT);

		// read recent action logs from disk file
		$this->logs = JosettaadminHelper::getLogs();

		// add Josetta backend css
		$document = JFactory::getDocument();
		$document->addStyleSheet(JURI::root(true) . '/administrator/components/com_josetta/assets/css/josetta.css');

		// insert current version
		$current = JosettaadminHelper::getVersion();
		$document->addScriptDeclaration('var _josetta_version="' . $current . '";');

		// version prefix
		if (version_compare(JVERSION, '3.0', 'ge'))
		{
			$this->joomlaVersionPrefix = 'j3';
			// linck to update javascript
			$document->addScript(JUri::root(true) . '/media/com_josetta/js/j3_update.js');
		}
		else
		{
			$this->joomlaVersionPrefix = 'j2';
			// load js framework
			// include mootools BEFORE update.js. It may happen
			// that mootols takes longer than update.js to load, in which case
			// an error occurs as JSON.encode, used by update.js, is not yet defined
			JHtml::_('behavior.framework');

			// linck to update javascript
			$document->addScript(JUri::root(true) . '/media/com_josetta/js/j2_update.js');
		}

		// version specific css
		$document->addStyleSheet(JURI::root(true) . '/administrator/components/com_josetta/assets/css/' . $this->joomlaVersionPrefix . '_josetta.css');

		// version number display
		$this->footerText = JText::sprintf('COM_JOSETTA_FOOTER_' . strtoupper(JosettaadminHelper_General::getVersionType()), JosettaadminHelper::getVersion(), date('Y')
				,JosettaadminHelper_General::getLandingPageUrl());

		// toolbar: title and settings button
		$this->addToolbar();

		// add submenu bar
		JosettaadminHelper::addSubmenu('cpanel');
		if (version_compare(JVERSION, '3.0', 'ge'))
		{
			$this->sidebar = JHtmlSidebar::render();
		}
		parent::display($tpl);
	}

	protected function addToolbar()
	{
		if (version_compare(JVERSION, '3.0', 'ge'))
		{
			JToolbarHelper::title('Josetta, ' . JText::_('COM_JOSETTA_TITLE_TRANSLATORS'), 'josetta-main-toolbar');
		}
		else
		{
			// output toolbar ourselves, otherwise hard to override the admin template CSS
			// when we have a nice 48x48 icon, this can come back
			$html = '<div class="josetta-main-toolbar"><h2>' . JText::_('COM_JOSETTA_TITLE_TRANSLATORS') . '</h2></div>';
			JFactory::getApplication()->JComponentTitle = $html;
		}

		// acl to josetta helpscreen
		$canDo = JosettaHelper::getActions();
		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_josetta');
		}
	}
}
