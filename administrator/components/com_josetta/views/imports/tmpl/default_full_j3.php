<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   weeblr, llc (c) 2014
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.2.1.567
 * @date		2014-01-16
 */


// No direct access

defined('_JEXEC') or die('Restricted access');

if(!empty( $this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php
endif;

echo ShlHtmlBs_Helper::alert(JText::_('COM_JOSETTA_IMPORT_PROMPT'), 'info', $dismiss = false);
echo ShlHtmlBs_Helper::alert(JText::_('COM_JOSETTA_IMPORT_DELETE_IMPORT_PROMPT'), 'info', $dismiss = false);

?>

<form action="<?php echo JRoute::_('index.php?option=com_josetta&view=imports'); ?>" method="post" name="adminForm" id="adminForm">

	<table class="table table-striped">
		<thead>
			<tr>
				<th class="shl-centered" width="1%">
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>
				<th class="shl-centered" width="1%">
					<?php echo JText::_( 'JGRID_HEADING_ID'); ?>
				</th>
				<th class="shl-centered">
					<?php echo JText::_( 'COM_JOSETTA_IMPORT_CREATED'); ?>
				</th>
				<th class="shl-centered">
					<?php echo JText::_( 'COM_JOSETTA_IMPORT_CREATED_BY'); ?>
				</th>
				<th class="shl-centered">
					<?php echo JText::_( 'COM_JOSETTA_IMPORT_STATE'); ?>
				</th>
				<th>
					<?php echo JText::_( 'COM_JOSETTA_IMPORT_ERROR_CODE'); ?>
				</th>
			</tr>
		</thead>

		<tfoot>
			<tr>
				<td colspan="6">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
		<?php
		$n = count($this->items);
		foreach ($this->items as $i => $item) :
			?>
			<tr class="row<?php echo $i % 2; ?>">
				<td class="shl-centered"">
					<?php echo JHtml::_('grid.id', $i, $item->id); ?>
				</td>
				<td class="shl-centered">
					<?php echo (int) $item->id; ?>
				</td>

	      <td class="shl-centered">
					<?php echo $item->created; ?>
				</td>

				<td class="shl-centered">
					<?php echo JFactory::getUser( $item->created_by)->name; ?>
				</td>

				<td class="shl-centered">
					<?php echo JosettaadminHelper_Html::importStatusToText( $item->state); ?>
				</td>

				<td>
					<?php echo empty( $item->error_message) ? '' : $item->error_message; ?>
				</td>

			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>

<div class="josetta-footer-container">
	<?php echo $this->footerText; ?>
</div>

</div>
