<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   weeblr, llc (c) 2014
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.2.1.567
 * @date		2014-01-16
 */

// No direct access

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class JosettaadminViewImports extends ShlMvcView_Base
{
	public function display($tpl = null)
	{
		// version prefix
		$this->joomlaVersionPrefix = version_compare(JVERSION, '3.0', 'ge') ? 'j3' : 'j2';

		// we'll need some frontend language strings
		JFactory::getLanguage()->load('com_josetta', JPATH_ROOT);

		// add Josetta backend css
		$document = JFactory::getDocument();
		$document->addStyleSheet(JURI::root(true) . '/administrator/components/com_josetta/assets/css/josetta.css');

		// link to import javascript
		$document->addScript(JUri::root(true) . '/administrator/components/com_josetta/assets/js/import.js');

		// version number display
		$this->footerText = JText::sprintf('COM_JOSETTA_FOOTER_' . strtoupper(JosettaadminHelper_General::getVersionType()), JosettaadminHelper::getVersion(), date('Y'));

		// toolbar: title and settings button
		$this->addToolbar();

		// add submenu bar
		JosettaadminHelper::addSubmenu('imports');
		if (version_compare(JVERSION, '3.0', 'ge'))
		{
			$this->sidebar = JHtmlSidebar::render();
			ShlHtmlBs_helper::addBootstrapCss(JFactory::getDocument());
			ShlHtmlBs_helper::addBootstrapJs(JFactory::getDocument());
		}

		// read list of import operations
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->state = $this->get('State');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		parent::display('full_' . $this->joomlaVersionPrefix);
	}

	protected function addToolbar()
	{
		// output main title
		if (version_compare(JVERSION, '3.0', 'ge'))
		{
			JToolbarHelper::title('Josetta, ' . JText::_('COM_JOSETTA_TITLE_TRANSLATORS'), 'josetta-main-toolbar');
		}
		else
		{
			$html = '<div class="josetta-main-toolbar"><h2>' . JText::_('COM_JOSETTA_TITLE_TRANSLATORS') . '</h2></div>';
			JFactory::getApplication()->JComponentTitle = $html;
		}

		// acl to josetta helpscreen
		$canDo = JosettaHelper::getActions();
		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_josetta');
		}

		// add import button in toolbar
		$bar = JToolBar::getInstance('toolbar');
		$bar->addButtonPath(SHLIB_ROOT_PATH . 'toolbarbutton');

		$params['class'] = 'modaltoolbar btn-success';
		$params['size'] = array('x' => 600, 'y' => 400);
		$params['overlayOpacity'] = 0;
		$params['classWindow'] = 'josetta-popup';
		$params['classOverlay'] = 'josetta-popup';
		unset($params['onClose']);
		$url = 'index.php?option=com_josetta&c=wizard&task=start&tmpl=component&optype=import&opsubject=joomfish';

		// add buttons
		if (version_compare(JVERSION, '3.0', 'ge'))
		{
			$params['buttonClass'] = 'btn-success btn btn-small modal';
			$bar
				->appendButton('J3popuptoolbarbutton', 'importj3', JText::_('COM_JOSETTA_IMPORT_BUTTON'), $url, $params['size']['x'],
					$params['size']['y'], $top = 0, $left = 0, $onClose = '', $title = 'Josetta', $params);
			// delete imported data
			JToolBarHelper::deleteList(JText::_('COM_JOSETTA_IMPORT_DELETE_CONFIRM'));
		}
		else
		{
			// delete imported data
			JToolBarHelper::deleteList(JText::_('COM_JOSETTA_IMPORT_DELETE_CONFIRM'));
			JToolBarHelper::divider();
			$bar
				->appendButton('Shpopuptoolbarbutton', 'import', $url, JText::_('COM_JOSETTA_IMPORT_BUTTON'), $msg = '', $task = 'import',
					$list = false, $hidemenu = true, $params);
			// load mootools
			JHtml::_('behavior.framework');
			JHtml::_('behavior.modal');
		}
	}
}
