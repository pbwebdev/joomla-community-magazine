<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   weeblr, llc (c) 2014
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.2.1.567
 * @date		2014-01-16
 */

defined('_JEXEC') or die;

jimport('joomla.application.component.model');

/**
 * Table clas for Joomfish importer journal records
 *
 * @package     Josetta
 */
class JosettaadminTable_Jfjournal extends JTable {

  /**
   * Constructor
   *
   * @param   object  &$db  JDatabase connector object.
   *
   */
  public function __construct( &$db)	{

    parent::__construct('#__josetta_jf_journal', 'id', $db);
  }

}
