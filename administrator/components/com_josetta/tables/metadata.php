<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */


// no direct access
defined('_JEXEC') or die;


/**
 * @package		Joomla.Administrator
 * @subpackage	com_josetta
 */

class JosettaTableMetadata extends JTable {

  /**
   * Constructor
   *
   * @param object Database connector object
   */

  public function __construct( &$db) {
    parent::__construct( '#__josetta_metadata', 'id', $db );
  }

}
