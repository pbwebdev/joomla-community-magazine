<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   weeblr, llc (c) 2014
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.2.1.567
 * @date		2014-01-16
 */

defined('_JEXEC') or die;

jimport('joomla.application.component.model');

/**
 * Table clas for Joomfish importer import records
 *
 * @package     Josetta
 */
class JosettaadminTable_Jfimporter extends JTable {

  /**
   * Constructor
   *
   * @param   object  &$db  JDatabase connector object.
   *
   */
  public function __construct( &$db)	{

    parent::__construct('#__josetta_jf_imports', 'id', $db);
  }

  /**
   * Overrides JTable::store to set modified data and user id.
   *
   * @param   boolean  $updateNulls  True to update fields even if they are null.
   *
   * @return  boolean  True on success.
   *
   */
  public function store($updateNulls = false) {

    $date = JFactory::getDate();
    $user = JFactory::getUser();

    if ($this->id) {

      // Existing item
      $this->modified = $date->toSql();
      $this->modified_by = $user->get('id');
    } else {

      $this->created = $date->toSql();
      $this->created_by = $user->get('id');
    }

    return parent::store($updateNulls);
  }

}
