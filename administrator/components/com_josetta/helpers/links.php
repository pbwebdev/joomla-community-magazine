<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_JEXEC')) die('Direct Access to this location is not allowed.');

class JosettaadminHelper_Links {

  /**
   * Adjust a link to account for the change in ids due to importing
   * content and their translations into a new site
   * 
   * @param string $link original, internal link
   * @param string $importedContext the original item type (items to which the link belongs)
   * @param integer $originalId id of the original item
   * @param string $originalTitle title of the original item
   * @param string $originalLanguage language of the original item
   * @param integer $importId the current import run id
   */
  public static function updateContentLinkIds( $link, $importedContext, $originalId, $originalTitle, $originalLanguage, $importId) {

    if(empty($link) || substr( $link, 0, 10) != 'index.php?') {
      return $link;
    }

    // update Itemid
    $Itemid = preg_match( '/[&|\?]Itemid=([^&]+)/i', $link, $tmp) ? (int) $tmp[1] : 0;
    if(!empty( $Itemid)) {
      $createdItemid = ShlDbHelper::selectResult( '#__josetta_jf_journal', 'created_id'
          , array( 'imported_context' => $importedContext, 'imported_id' => $Itemid, 'language' => $originalLanguage));

      // now replace in link
      $createdItemid = empty( $createdItemid) ? '' : $createdItemid;
      $link = str_replace( 'Itemid=' . $Itemid, 'Itemid=' . $createdItemid, $link);
      ShlSystem_Log::info( 'josetta', 'Run #' . $importId. ', updated Itemid in ' . $importedContext . ' link ('. $originalId . ' - ' . $originalTitle . ' - ' . $originalLanguage .'), to %s, was %s', $link, $Itemid);
    }

    // update ids in com_content links
    $option = preg_match( '/[&|\?]option=([^&]*)/i', $link, $tmp) ? $tmp[1] : '';
    if($option == 'com_content') {
      $view = preg_match( '/[&|\?]view=([^&]*)/i', $link, $tmp) ? $tmp[1] : '';
      $id = preg_match( '/[&|\?]id=([^&]*)/i', $link, $tmp) ? $tmp[1] : '';
      switch($view) {
        case 'article':
          $createdContext = 'com_content_item';
          break;
        case 'category':
          $createdContext = 'categories_item';
          break;
      }
      if(!empty( $createdContext)) {
        $lang = preg_match( '/[&|\?]lang=([^&]*)/i', $link, $tmp) ? $tmp[1] : '';
        if(!empty( $lang)) {
          // a spcific language is set in the link, let's comply with it, instead
          // of using the current item language
          $languages = JLanguageHelper::getLanguages('sef');
          $linkLanguage = empty( $languages[$lang]) ? '' : $languages[$lang]->lang_code;
        }
        $language = empty( $linkLanguage) ? $originalLanguage : $linkLanguage;
        $createdId = ShlDbHelper::selectResult( '#__josetta_jf_journal', 'created_id'
            , array( 'created_context' => $createdContext, 'imported_id' => $id, 'language' => $language));
        $createdId = empty( $createdId) ? '' : $createdId;
        if(!empty( $createdId)) {
          $link = str_replace( '&id=' . $id, '&id=' . $createdId, $link);
          $link = str_replace( '?id=' . $id, '?id=' . $createdId, $link);
          ShlSystem_Log::info( 'josetta', 'Run #' . $importId. ', updated ' . $createdContext . ' id in menu item link ('
              . $originalId . ' - ' . $originalTitle . ' - ' . $originalLanguage .'), to %s, was %s', $link, $id);
        } else {
          ShlSystem_Log::info( 'josetta', 'Run #' . $importId. ', NOT UPDATED: ' . $createdContext . ' id in menu item link ('
              .  $originalId . ' - ' . $originalTitle . ' - ' . $originalLanguage .'), was %s, NO TRANSLATION', $link);
        }
      }
    }

    return $link;
  }

  /**
   * Update a link, accounting for changes in non-sef urls
   * format between Joomla! 1.5 and Joomla 1.6+
   * 
   * @param string $link
   */
  public static function updateLinksFormat( $link) {

    if(empty($link) || substr( $link, 0, 10) != 'index.php?') {
      return $link;
    }

    // using now com_users instead of com_user
    if (strpos($link, 'option=com_user&') !== false) {
      $link = str_replace('option=com_user&', 'option=com_users&', $link);
    }

    // Fixing com_content URLs
    if (strpos($link, 'option=com_content') !== false) {

      if (strpos($link, 'view=frontpage') !== false) {
        $link = 'index.php?option=com_content&view=featured';

      } else if(strpos($link, 'view=article') !== false && strpos($link, 'layout=form') !== false ) {
        // submit an article
        $link = 'index.php?option=com_content&view=form&layout=edit';
      } else {
        // Extract the id from the URL
        if (preg_match('|id=([0-9]+)|', $link, $tmp)) {

          $id = $tmp[1];
          if ( (strpos($link, 'layout=blog') !== false) AND
              ( (strpos($link, 'view=category') !== false) OR
                  (strpos($link, 'view=section') !== false) ) ) {
            $link = 'index.php?option=com_content&view=category&layout=blog&id='.$id;
          } elseif (strpos($link, 'view=section') !== false) {
            $link = 'index.php?option=com_content&view=category&layout=blog&id='.$id;
          }
        }
      }
    }

    // fixing weblinks URLs
    if (strpos($link, 'option=com_weblinks') !== false) {
      if (strpos($link, 'view=weblink') !== false && strpos($link, 'layout=form') !== false) {
        $link = 'index.php?option=com_weblinks&view=form&layout=edit';
      }
    }


    // Fixing com_users URLs
    if (strpos($link, 'option=com_users') !== false) {

      if(strpos($link, 'view=user') !== false && strpos($link, 'layout=form') !== false ) {
        // edit user profile
        $link = 'index.php?option=com_users&view=profile&layout=edit';
      } else if(strpos($link, 'view=user') !== false && strpos($link, 'task=edit') !== false ) {
        // edit user profile
        $link = 'index.php?option=com_users&view=profile&layout=edit';
      } else if(strpos($link, 'view=register') !== false ) {
        // register
        $link = 'index.php?option=com_users&view=registration';
      } else if(strpos($link, 'view=user') !== false ) {
        // view user profile
        $link = 'index.php?option=com_users&view=profile';
      }
    }

    return $link;
  }

}