<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_JEXEC'))
	die('Direct Access to this location is not allowed.');

class JosettaadminHelper_General
{
	public static function getLandingPageUrl($type = null)
	{
		if (!empty($type))
		{
			return $type;
		}
		switch (JOSETTA_VERSION_TYPE)
		{
			case 'full':
				return 'https://weeblr.com/fast-effective-translations-for-joomla/josetta?src=josetta_full';
				break;
			default:
				return 'https://weeblr.com/fast-effective-translations-for-joomla/josetta?src=josetta_free';
				break;
		}
	}
	
	public static function getVersionType($type = null)
	{
		if (!empty($type))
		{
			return $type;
		}
		switch (JOSETTA_VERSION_TYPE)
		{
			case 'full':
			case 'free':
				return JOSETTA_VERSION_TYPE;
				break;
			case 'free':
				return 'full';
				break;
			default:
				return 'free';
				break;
		}
	}

	/**
	 * Builds a string based on current Joomla version
	 * Format is 'j' followed by major version
	 */
	public static function getJoomlaVersionPrefix()
	{
		// version prefix
		if (version_compare(JVERSION, '3.0', 'ge'))
		{
			return 'j3';
		}
		else
		{
			return 'j2';
		}
	}

	public static function getController($name = 'default', $config = array(), $prefix = 'Josettaadmin')
	{

		// if no name, use default
		$name = empty($name) ? 'default' : $name;

		// find about controller class name
		$controllerClass = ucfirst($prefix) . 'Controller_' . ucfirst($name);

		// instantiate our class
		if (class_exists($controllerClass))
		{
			$controller = new $controllerClass($config);
		}
		else
		{
			die('Unable to create Josetta controller. Please contact support');
		}
		return $controller;
	}

	/**
	 * Builds an internal urls
	 *
	 * @param <array> $elements an array of key,value pairs, should not have option=
	 * @param string value of component, if not default
	 * @return <string> the urls
	 */
	public static function buildUrl($elements, $option = 'com_josetta')
	{

		$url = '';

		// if option var is set, place it at start of URL
		if (!empty($elements['option']))
		{
			$url = 'index.php?option=' . $elements['option'];
			unset($elements['option']);
		}

		$url = empty($url) ? 'index.php?' : $url;

		if (is_array($elements) && !empty($elements))
		{
			foreach ($elements as $key => $value)
			{
				if(is_array($value))
				{
					foreach ($value as $subKey => $subValue)
					{
						$url .= '&' . $key . '[' . $subKey . ']=' . $subValue;
					}
				}
				else
				{
					$url .= '&' . $key . '=' . $value;
				}
			}
		}

		return $url;
	}

	public static function getComponentUrl()
	{

		return 'administrator/components/com_josetta';
	}

	/**
	 * Create toolbar title for current view
	 *
	 * This one can ucstomize the class for styling
	 * plus the output can be used to
	 * simply display the title as opposed to
	 * using $mainframe to set the component
	 * title, which is not OK when used inside a modal box
	 *
	 * @param string $title text title
	 * @param string $icon the name of an image, which is used to calculate aclass name
	 * @param string $class the name of a wrapping class
	 */
	public static function makeToolbarTitle($title, $icon = 'generic.png', $class = 'header')
	{

		//strip the extension
		$icon = preg_replace('#\.[^.]*$#', '', $icon);

		$html = "<div class=\"$class icon-48-$icon\">\n";
		$html .= "$title\n";
		$html .= "</div>\n";

		return $html;

	}

	public static function getXML($data, $isFile = true)
	{

		// Disable libxml errors and allow to fetch error information as needed
		libxml_use_internal_errors(true);

		if ($isFile)
		{
			// Try to load the XML file
			$xml = simplexml_load_file($data);
		}
		else
		{
			// Try to load the XML string
			$xml = simplexml_load_string($data);
		}

		if (empty($xml))
		{
			// There was an error
			JError::raiseWarning(100, JText::_('JLIB_UTIL_ERROR_XML_LOAD'));

			if ($isFile)
			{
				JError::raiseWarning(100, $data);
			}

			foreach (libxml_get_errors() as $error)
			{
				JError::raiseWarning(100, 'XML: ' . $error->message);
			}
		}

		return $xml;
	}

}
