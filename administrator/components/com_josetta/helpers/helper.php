<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date        2015-12-23
 */

// No direct access
defined('_JEXEC') or die;

/**
 * Josetta helper.
 */
class JosettaadminHelper
{

	protected static $_version = '2.4.3.723';

	/**
	 * Return hardcoded version string
	 */
	public static function getVersion()
	{

		return self::$_version;

	}

	public static function getImportRootCategoryId($importId, $extension, $language)
	{
		static $_categoryIds = array();

		$signature = $importId . $extension . $language;
		if (empty($_categoryIds[$signature]))
		{
			// default to root
			$_categoryIds[$signature] = 1;

			try
			{
				// build a category record
				$item = array();
				$item['parent_id'] = 1;
				$item['title'] = 'Josetta - ' . $importId . ' - ' . $language;
				$item['extension'] = $extension;
				$item['description'] = JText::sprintf('COM_JOSETTA_IMPORT_ROOT_CATEGORY_DESCRIPTION', $importId, $language);
				$item['published'] = 1;
				$item['language'] = $language;
				$item['associations'] = array();

				// check category does not already exists
				$categoryId = ShlDbHelper::selectResult('#__categories', 'id',
					array('title' => $item['title'], 'extension' => $extension, 'language' => $language));
				if (empty($categoryId))
				{
					// use J! model to save a new category
					require_once JPATH_ADMINISTRATOR . '/components/com_categories/models/category.php';
					JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_categories/tables');

					// now actually save
					$categoryModel = new CategoriesModelCategory;
					$saved = $categoryModel->save($item);
					if ($saved)
					{
						$state = $categoryModel->get('state');
						$_categoryIds[$signature] = $state->get('category.id');
					}
					else
					{
						ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $categoryModel->getError());
					}
				}
				else
				{
					// category already exists
					$_categoryIds[$signature] = $categoryId;
				}
			}
			catch (Exception $e)
			{
				ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			}
		}

		return $_categoryIds[$signature];

	}

	public static function deleteImportRootCategory($importId, $extension, $languages = array())
	{
		// identify all categories that should be deleted
		try
		{
			$languages = empty($languages) ? JLanguageHelper::getLanguages('lang_code') : $languages;

			foreach ($languages as $language)
			{
				$categoryTitle = 'Josetta - ' . $importId . ' - ' . $language->lang_code;
				$categoryId = ShlDbHelper::selectResult('#__categories', 'id',
					array('title' => $categoryTitle, 'extension' => $extension, 'language' => $language->lang_code));
				if (!empty($categoryId))
				{
					self::deleteCategoryAndAssociations($categoryId);
				}
			}

			return true;
		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			return $e->getMessage();
		}
	}

	public static function getImportUncategorizedCategoryId($importId, $extension, $language)
	{
		static $_categoryIds = array();

		$signature = $importId . $extension . $language;
		if (empty($_categoryIds[$signature]))
		{
			// default to root
			$_categoryIds[$signature] = 1;

			try
			{
				// build a category record
				$item = array();
				$item['parent_id'] = 1;
				$item['title'] = 'Josetta - Uncategorised - ' . $importId . ' - ' . $language;
				$item['extension'] = $extension;
				$item['description'] = JText::sprintf('COM_JOSETTA_IMPORT_UNCATEGORIZED_CATEGORY_DESCRIPTION', $importId, $language);
				$item['published'] = 1;
				$item['language'] = $language;
				$item['associations'] = array();

				// check category does not already exists
				$categoryId = ShlDbHelper::selectResult('#__categories', 'id',
					array('title' => $item['title'], 'extension' => $extension, 'language' => $language));
				if (empty($categoryId))
				{
					// use J! model to save a new category
					require_once JPATH_ADMINISTRATOR . '/components/com_categories/models/category.php';
					JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_categories/tables');

					// now actually save
					$categoryModel = new CategoriesModelCategory;
					$saved = $categoryModel->save($item);
					if ($saved)
					{
						$state = $categoryModel->get('state');
						$_categoryIds[$signature] = $state->get('category.id');
					}
					else
					{
						ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $categoryModel->getError());
					}

				}
				else
				{
					// category already exists
					$_categoryIds[$signature] = $categoryId;
				}
			}
			catch (Exception $e)
			{
				ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			}
		}

		return $_categoryIds[$signature];
	}

	public static function deleteImportUncategorizedCategory($importId, $extension, $languages = array())
	{
		// identify all categories that should be deleted
		try
		{
			$languages = empty($languages) ? JLanguageHelper::getLanguages('lang_code') : $languages;

			foreach ($languages as $language)
			{
				$categoryTitle = 'Josetta - Uncategorised - ' . $importId . ' - ' . $language->lang_code;
				$categoryId = ShlDbHelper::selectResult('#__categories', 'id',
					array('title' => $categoryTitle, 'extension' => $extension, 'language' => $language->lang_code));
				if (!empty($categoryId))
				{
					self::deleteCategoryAndAssociations($categoryId);
				}
			}

			return true;
		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			return $e->getMessage();
		}
	}

	public static function deleteItemAssociations($context, $id)
	{
		// delete any associations, whether in Joomla or Josetta associations tables
		$associationKey = ShlDbHelper::selectResult('#__associations', array('key'), array('context' => $context, 'id' => $id));
		if (!empty($associationKey))
		{
			ShlDbHelper::delete('#__associations', array('key' => $associationKey));
			ShlDbHelper::delete('#__josetta_associations', array('key' => $associationKey));
		}
	}

	public static function deleteCategoryAndAssociations($categoryId)
	{
		try
		{
			JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_categories/tables');
			$table = JTable::getInstance('Category', 'CategoriesTable');
			$table->load($categoryId);
			$table->delete($categoryId); // looks like you need to load the table object before being able to delete it???

			// collect any association this category can be part of
			$model = new JosettaModel_Translate();
			$associatedItems = $model->getAssociatedItemsList($categoryId, 'categories_item');

			// and delete them
			foreach ($associatedItems as $associatedItem)
			{
				self::deleteItemAssociations('com_categories.item', $associatedItem->id);
			}

			return true;
		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			return false;
		}
	}

	public static function deleteArticleAndAssociations($articleId)
	{
		try
		{
			// collect any association this article can be part of
			// note: we must delete associations first, then article, as
			// some part of getAssociatedItemsList() need access to the
			// original article
			$model = new JosettaModel_Translate();
			$associatedItems = $model->getAssociatedItemsList($articleId, 'com_content_item');

			// and delete them
			foreach ($associatedItems as $associatedItem)
			{
				self::deleteItemAssociations('com_content.item', $associatedItem->id);
			}

			// now delete the item itself
			$table = JTable::getInstance('Content');
			$table->load($articleId);
			$table->delete($articleId); // looks like you need to load the table object before being able to delete it???

			return true;
		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			return false;
		}
	}

	/**
	 * Configure the Linkbar.
	 */
	public static function addSubmenu($viewName)
	{
		// make sure the language file is loaded
		$language = JFactory::getLanguage();
		$language->load('com_josetta.sys');

		// now we can create the sub menu items
		if (version_compare(JVERSION, '3.0', 'ge'))
		{
			JHtmlSidebar::addEntry(JText::_('COM_JOSETTA_MENU_CONTROL_PANEL'), 'index.php?option=com_josetta&view=cpanel', $viewName == 'cpanel');
			JHtmlSidebar::addEntry(JText::_('COM_JOSETTA_MENU_IMPORT'), 'index.php?option=com_josetta&view=imports', $viewName == 'imports');
			JHtmlSidebar::setAction('index.php?option=com_josetta&view=cpanel');
		}
		else
		{
			JSubMenuHelper::addEntry(JText::_('COM_JOSETTA_MENU_CONTROL_PANEL'), 'index.php?option=com_josetta&amp;view=cpanel', $viewName == 'cpanel');
			JSubMenuHelper::addEntry(JText::_('COM_JOSETTA_MENU_IMPORT'), 'index.php?option=com_josetta&amp;view=imports', $viewName == 'imports');
		}
	}

	/**
	 * Reads last few lines of last action log file
	 * for display in backend control panel
	 */
	public static function getLogs($tail = 30)
	{
		$logs = '';

		// base log file name pattern
		$fileNamePattern = 'log_josetta_[0-9]{4}-[0-9]{2}-[0-9]{2}.log.php';

		// iterate over file types, and collect file list
		$list = array();
		$basePath = JPATH_ROOT . '/logs';
		jimport('joomla.filesystem.folder');
		if (!JFolder::exists($basePath))
		{
			return $logs;
		}
		$files = JFolder::files($basePath, $fileNamePattern, $recurse = false, $fullpath = false);
		if (empty($files))
		{
			return $logs;
		}
		// extract each file date
		foreach ($files as $file)
		{
			$fileRecord = new StdClass();
			$fileRecord->filename = $file;
			// extract date from file name
			$date = array();
			preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', $file, $date);
			$fileRecord->date = $date[0];
			// if date falls within boundaries, list log file
			$list[] = clone ($fileRecord);
		}

		// step 2: we have data, need to sort by display parameter
		$sorted = uasort($list, array('JosettaadminHelper', 'sortLogFiles'));
		if (!$sorted)
		{
			return $logs;
		}

		$mostRecentFile = array_shift($list);
		jimport('joomla.filesystem.file');

		$rawLogs = file($basePath . '/' . $mostRecentFile->filename);
		if (empty($rawLogs))
		{
			return $logs;
		}

		// drop the first 6 lines, Joomla standard headers
		for ($i = 0; $i < 6; $i++)
		{
			array_shift($rawLogs);
		}

		// reverse it
		$rawLogs = array_reverse($rawLogs);

		// tail it
		if (!empty($tail))
		{
			$rawLogs = array_slice($rawLogs, 0, $tail);
		}

		// make it a string
		$logs = implode('<br />', $rawLogs);
		return $logs;
	}

	public static function sortLogFiles($a, $b)
	{
		if ($a->date == $b->date)
		{
			return 0;
		}
		return ($a->date > $b->date) ? -1 : 1;
	}

}
