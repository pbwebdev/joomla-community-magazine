<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_JEXEC'))
	die('Direct Access to this location is not allowed.');

class JosettaadminHelper_Html
{
	/**
	 * Method to create a select list
	 *
	 * @access  public
	 * @param array $data elements of the select list. An array of (id, title) arrays
	 * @param int ID of current item
	 * @param string name of select list
	 * @param boolean if true, changing selected item will submit the form (assume is an "adminForm")
	 * @param boolean, if true, a line 'Select all' is inserted at the start of the list
	 * @param string the "Select all" to be displayed, if $addSelectAll is true
	 * @return  string HTML output
	 */
	public static function buildSelectList($data, $current, $name, $autoSubmit = false, $addSelectAll = false, $selectAllTitle = '',
		$customSubmit = '')
	{
		// should we autosubmit ?
		$customSubmit = empty($customSubmit) ? ' onchange="document.adminForm.limitstart.value=0;document.adminForm.submit();"' : $customSubmit;
		$extra = $autoSubmit ? $customSubmit : '';

		// add select all option
		if ($addSelectAll)
		{
			array_unshift($data, JHTML::_('select.option', 0, $selectAllTitle, 'id', 'title'));
		}
		// use joomla lib to build the list
		return JHTML::_('select.genericlist', $data, $name, $extra, 'id', 'title', $current);
	}

	/**
	 * Method to create a select list
	 *
	 * @access  public
	 * @param array $data elements of the select list. An array of (id, title) arrays
	 * @param int ID of current item
	 * @param string name of select list
	 * @param boolean if true, changing selected item will submit the form (assume is an "adminForm")
	 * @param boolean, if true, a line 'Select all' is inserted at the start of the list
	 * @param string the "Select all" to be displayed, if $addSelectAll is true
	 * @return  string HTML output
	 */
	public static function buildGroupedSelectList($data, $current, $name, $autoSubmit = false, $addSelectAll = false, $selectAllTitle = '',
		$customSubmit = '')
	{
		// should we autosubmit ?
		$customSubmit = empty($customSubmit) ? ' onchange="document.adminForm.limitstart.value=0;document.adminForm.submit();"' : $customSubmit;
		$extra = $autoSubmit ? $customSubmit : '';

		// add select all option
		if ($addSelectAll)
		{
			array_unshift($data, JHTML::_('select.option', 0, $selectAllTitle, 'id', 'title'));
		}
		// use joomla lib to build the list
		return JHTML::_('select.groupedlist', $data, $name, array('option.key' => 'id', 'option.text' => 'title', 'list.select' => $current));
	}

	/**
	 * Wraps a text into a div to display a title visible by hovering
	 * over text
	 *
	 * @param string $text text to be displayed
	 * @param string $title title if any
	 */
	public static function wrapTitle($text, $title = '')
	{
		$html = empty($title) ? $text : '<div title="' . $title . '">' . $text . '</div>';
		return $html;
	}

	/**
	 * Wraps a text into a span to display a tooltip visible by hovering
	 * over text
	 *
	 * @param string $text text to be displayed
	 * @param string $title title if any
	 * @param string $tip tip, if any
	 */
	public static function wrapTip($text, $title = '', $tip = '', $class = 'hasTip')
	{
		$html = empty($title) ? $text
			: '<div ' . (empty($tip) ? '' : ' class="' . $class . '"') . ' title="' . $title . (empty($tip) ? '' : '::' . $tip) . '">' . $text
				. '</div>';
		return $html;
	}

	/**
	 * A copy of Joomla own modal helper function,
	 * giving access to more params
	 *
	 * @param $selector selector class to stitch modal javascript on
	 * @param $params an array of key/values pairs to be passed as options to SqueezeBox
	 */
	public static function modal($selector = 'a.modal', $params = array())
	{
		static $modals;
		static $included;

		$document = JFactory::getDocument();

		// Load the necessary files if they haven't yet been loaded
		if (!isset($included))
		{

			// load js framework
			JHtml::_('behavior.framework');

			// Load the javascript and css
			$uncompressed = JFactory::getConfig()->get('debug') ? '-uncompressed' : '';
			JHtml::_('script', 'system/modal' . $uncompressed . '.js', true, true);
			JHtml::_('stylesheet', 'system/modal.css', array(), true);

			// our flag to block opening several Squeezboxes
			$document = JFactory::getDocument();
			$document->addScriptDeclaration('var shAlreadySqueezed = false;');
			$document->addScriptDeclaration('var shReloadModal = true;');

			$included = true;
		}

		if (!isset($modals))
		{
			$modals = array();
		}

		$sig = md5(serialize(array($selector, $params)));
		if (isset($modals[$sig]) && ($modals[$sig]))
		{
			return;
		}

		// Setup options object
		$options = self::makeSqueezeboxOptions($params);

		// Attach modal behavior to document
		$document
			->addScriptDeclaration(
				"
        window.addEvent('domready', function() {

        SqueezeBox.initialize({" . $options . "});

        $$('" . $selector
					. "').each(function(el) {
        el.addEvent('click', function(e) {
        new Event(e).stop();
        if (!window.parent.shAlreadySqueezed) {
        window.parent.shAlreadySqueezed = true;
        window.parent.SqueezeBox.fromElement(el, {parse:'rel'});
  }
  });
  });
  });");

		// Set static array
		$modals[$sig] = true;
		return;
	}

	/**
	 * Builds up an html link using the various parts supplied
	 *
	 * @param $view a JView object, to be able to escape output text
	 * @param $linkData an array of key/value pairs to build up the target links
	 * @param $elementData an array holding element data : title, class, rel
	 * @param $modal boolean, if true, required stuff to make the link open in modal box is added
	 * @param $hasTip boolean, if true, required stuff to turn elementData['title'] into a tooltip is added
	 * @param $extra an array holding key/value pairs, will be added as raw attributes to the link
	 */
	public static function makeLink($view, $linkData, $elementData, $modal = false, $modalOptions = array(), $hasTip = false, $extra = array())
	{
		// calculate target link
		if ($modal)
		{
			$linkData['tmpl'] = 'component';
		}
		if (empty($linkData['option']))
		{
			$linkData['option'] = 'com_josetta';
		}
		$url = JosettadminHelper_General::buildUrl($linkData);
		$url = JRoute::_($url);

		// calculate title
		$title = empty($elementData['title']) ? '' : $elementData['title'];
		$title = is_null($view) ? $title : $view->escape($title);

		$attribs = array();

		// calculate class
		$class = empty($elementData['class']) ? '' : $elementData['class'];
		if ($hasTip)
		{
			$class .= ' ' . $hasTip;
		}

		// store title in attributes array
		if (!empty($title))
		{
			$attribs['title'] = $title;
		}

		// store in attributes array
		if (!empty($class))
		{
			$attribs['class'] = $class;
		}

		// calculate modal information
		$rel = empty($elementData['rel']) || is_null($view) ? '' : $view->escape($elementData['rel']);
		if ($modal)
		{
			$modalOptionsString = self::makeSqueezeboxOptions($modalOptions);
			$rel .= ' {handler: \'iframe\'' . (empty($modalOptionsString) ? '' : ', ' . $modalOptionsString) . '}';
		}

		// store in attributes array
		if (!empty($rel))
		{
			$attribs['rel'] = $rel;
		}

		// any custom attibutes ?
		if (!empty($extra))
		{
			foreach ($extra as $key => $value)
			{
				$attribs[$key] = $value;
			}
		}

		// finish link
		$anchor = empty($elementData['anchor']) ? $title : $elementData['anchor'];

		return JHTML::link($url, $anchor, $attribs);
	}

	public static function importStatusToText($status)
	{
		switch ($status)
		{
			case JosettaadminModel_Jfimporter::STATE_CREATED:
				$text = JText::_('COM_JOSETTA_IMPORT_STATE_CREATED');
				break;
			case JosettaadminModel_Jfimporter::STATE_RAW_DATA_IMPORTED:
				$text = JText::_('COM_JOSETTA_IMPORT_STATE_RAW_DATA_IMPORTED');
				break;
			case JosettaadminModel_Jfimporter::STATE_COMPLETED:
				$text = JText::_('COM_JOSETTA_IMPORT_STATE_COMPLETED');
				break;
			case JosettaadminModel_Jfimporter::STATE_DELETED:
				$text = JText::_('COM_JOSETTA_IMPORT_STATE_DELETED');
				break;
			case JosettaadminModel_Jfimporter::STATE_FAILED:
				$text = JText::_('COM_JOSETTA_IMPORT_STATE_FAILED');
				break;
			case JosettaadminModel_Jfimporter::STATE_LINKS_FIXED:
				$text = JText::_('COM_JOSETTA_IMPORT_STATE_LINKS_FIXED');
				break;
			default:
				$text = '';
		}

		return $text;
	}

	/**
	 * Method to render a Bootstrap modal
	 *
	 * @param   string  $selector  The ID selector for the modal.
	 * @param   array   $params    An array of options for the modal.
	 * @param   string  $footer    Optional markup for the modal footer
	 *
	 * @return  string  HTML markup for a modal
	 *
	 * @since   3.0
	 */
	public static function renderBootstrapModal($selector = 'modal', $params = array(), $footer = '')
	{
		// Ensure the behavior is loaded
		JHtml::_('bootstrap.modal', 'modal', $selector, $params);

		// compute dimensions
		$params['height'] = $params['height'] < 1 ? 'function(){ var h = jQuery(window).height()*' . $params['height'] . ';return h;}()'
			: $params['height'];
		$params['width'] = $params['width'] < 1 ? 'function(){ var w = jQuery(window).width()*' . $params['width'] . ';return w;}()'
			: $params['width'];

		$html = "<div class=\"shmodal hide \" id=\"" . $selector . "\">\n";
		$html .= "<div class=\"shmodal-header\">\n";
		$html .= "<button type=\"button\" class=\"close\" data-dismiss=\"modal\">×</button>\n";
		$html .= "<h3>" . $params['title'] . "</h3>\n";
		$html .= "</div>\n";
		$html .= "<div id=\"" . $selector . "-container\">\n";
		$html .= "</div>\n";
		$html .= "</div>\n";

		$html .= "<script>";
		$html .= "jQuery('#" . $selector . "').on('show', function () {\n";
		$html .= "var height = " . $params['height'] . ';';
		$html .= "var width = " . $params['width'] . ';';
		$html .= "$('" . $selector . "-container').innerHTML = '<div class=\"shmodal-body\"><iframe class=\"iframe\" src=\"" . $params['url']
			. "\" height=\"' + height + '\" width=\"' + width + '\" ></iframe></div>" . $footer . "';\n";
		$html .= "height = jQuery('#" . $selector . "').height();";
		$html .= "var pageheight = jQuery(window).height();";
		$html .= "var pagewidth = document.body.clientWidth;";
		$html .= "var shleft = (pagewidth - width) /2;\n";
		$html .= "var shtop = (pageheight - height) /2;\n";
		$html .= "jQuery('#" . $selector . "').css({'margin-top':shtop,'top':'0'});";
		$html .= "jQuery('#" . $selector . "').css({'margin-left':shleft,'left':'0'});";
		$html .= "});\n";
		$html .= "</script>";

		return $html;
	}
}
