<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.filesystem.file');

// load b/c classes
if (JFile::exists(JPATH_ADMINISTRATOR . '/components/com_josetta/classes/extensionplugin.php'))
{
	include_once JPATH_ADMINISTRATOR . '/components/com_josetta/classes/extensionplugin.php';
	/**
	 * B/c for base class for a Josetta plugin
	 *
	 * @package		Josetta
	 * @subpackage	com_josetta
	 */
	class JosettaClassesExtensionplugin extends JosettaadminClass_Extensionplugin
	{
	}

}
