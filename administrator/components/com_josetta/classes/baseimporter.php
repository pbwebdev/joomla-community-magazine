<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */


// no direct access
defined('_JEXEC') or die;

jimport( 'joomla.application.component.modeladmin');
jimport( 'joomla.html.parameter');

/**
 * @package		Joomla.Administrator
 * @subpackage	com_josetta
 */

class JosettaadminClass_Baseimporter extends ShlMvcModel_Admin {

  protected $_pkName = '';
  protected $_context = '';
  protected $_journalTable = null;
  protected $_originalLanguageItemId = 0;
  protected $_currentItemGroup = null;

  public function setContext( $context) {
    $this->_context = $context;
  }

  public function setPkName( $pkName) {
    $this->_pkName = $pkName;
  }

  /**
   * Receives a list of menu items, in an array
   * indexed by language, and create menu items
   * for each of them
   *
   * @param array of objects $items
   * @param object $journalTable JTable descendant to write to the journal
   * @return integer # of imported items, can be zero. -1 on error
   */
  public function importItemGroup( $itemGroup, $journalTable) {

    $importedCounter = 0;
    $defaultLanguage = JosettaHelper::getSiteDefaultLanguage();
    $this->_currentItemGroup = $itemGroup;
    
    if(empty( $itemGroup[$defaultLanguage])) {
      // try find some indication about the faulty menu item
      $descriptor = '';
      foreach( $itemGroup as $language => $item) {
        if( !empty( $item->name)) {
          $descriptor .= (empty( $descriptor) ? '' : ', ') . $language . ': ' . htmlspecialchars( $item->name);
        }
      }
      $this->setError( JText::_('COM_JOSETTA_IMPORT_ERROR_NO_DEFAULT_LANGUAGE_ITEM') . (empty($descriptor) ? '' : ' (' . $descriptor . ')' ));
      return -1;
    }

    // needed stuff
    $this->_journalTable = $journalTable;
    $this->_journalTable->created_context = $this->_context;

    $result = $this->_doImportItemFromGroup( $itemGroup[$defaultLanguage], $defaultLanguage);
    if($result < 0) {
      // there was an error
      return -1;
    }
    $importedCounter += $result;

    // process translations
    foreach( $itemGroup as $language => $item) {
      if($language != JosettaHelper::getSiteDefaultLanguage()) {
        $result = $this->_doImportItemFromGroup( $item, $language);
        if($result < 0) {
          return -1;
        }
        $importedCounter += $result;
      }
    }

    return $importedCounter;

  }

  /**
   * Store a new menu item
   *
   * @param object $item
   * @return integer 1 if success, 0 if item not stored, but not an error, -1 if error
   */
  protected function _doImportItemFromGroup( $item, $language) {

    $pkName = $this->_pkName;
    $defaultLanguage = JosettaHelper::getSiteDefaultLanguage();

    // process original language item
    $this->_journalTable->imported_id = $item->$pkName;
    if( $language == $defaultLanguage) {
      $result = $this->_importOriginal( $item, $language);
      $this->_originalLanguageItemId = $result;
    } else {
      $result = $this->_importTranslation( $item, $language);
    }
    if($result < 0) {
      // there was an error
      return -1;
    }
    if($result != 0) {

      $this->_journalTable->created_id = $result;
      $this->_journalTable->language = $language;
      // make sure a new journal entry is created
      $this->_journalTable->id = 0;
      // then store
      $saved = $this->_journalTable->store();
      if(!$saved) {
        $this->setError( $this->_journalTable->getError());
        return -1;
      }

      return 1;

    } else {
      // there was no db execution error
      // but the menu item was not created
      $error = $this->getError();
      $title = empty( $item->title) ? '(no title)' : $item->title;
      ShlSystem_Log::info( 'josetta', 'Run #' . $this->_journalTable->import_id . ', ' . $this->_context . ' item not imported: ' . $title
          .  ', ' . $language . ' (' . $this->_journalTable->imported_id . ')' . (empty( $error) ? '' : ', ' . $error));

      return 0;
    }

  }

  /**
   * Saves original item to Joomla!
   *
   * Needs to be overriden by each descendant
   *
   * @param object $item
   * @param string $language
   */
  protected function _importOriginal( $item, $language) {

    // return id of the saved item
    return 0;
  }

  /**
   * Use Josetta to save translations of a given item
   * Usually no need to override, as Josetta
   * handles everything automatically, based on the item
   * type (as described by $this->_context)
   *
   * @param object $item
   * @param string $language
   * @return integer 0 if not saved, but no error, -1 in case of error, saved item id
   */
  protected function _importTranslation( $item, $language) {

    // build a data record, as required by Josetta translate model
    $pkName = $this->_pkName;
    $data = array();

    // set type of items being translated
    $data['type'] = $this->_context;

    // items are considered translated
    $data['completion'] = 100;

    // add target language
    $data['target_language'] = $language;

    // apply various fixes/updates to the item
    $item = $this->_convertItem( $item, $language);

    // before passing it to translate model
    $namespace = $data['target_language'] . '_josetta_form';
    $data[$namespace] = (array) $item;

    // items are considered translated
    $data[str_replace( '-', '_', $language) . '_completion'] = 100;

    // store original language item id
    $data[$namespace]['ref_id'] = $this->_originalLanguageItemId;

    // we'l use Josetta to save the translation
    // and perform additional work such as
    // creating associations
    JLoader::register('JosettaModelTranslate', JPATH_SITE . '/components/com_josetta/models/translate.php');
    $model = ShlMvcModel_Base::getInstance( 'translate', 'JosettaModel');
    $saved = $model->save( $data);

    if($saved === false) {
      $errors = $model->getErrors();
      $text = empty($item->$pkName) ? ' (original item #' . $this->_originalLanguageItemId . ')' 
      : ' (menu item #' . $item->$pkName . ' - ' . $language . ')';
      foreach( $errors as $error) {
        $this->setError( $error . $text);
      }
      return -1;
    } else {
      $itemId = $model->getState( 'translatedSavedItemId');

      // return id of the saved item
      return $itemId;
    }

  }

  /**
   * Based on Jupgrade:
   *
   * jUpgrade
   *
   * @version		  $Id$
   * @package		  MatWare
   * @subpackage	com_jupgrade
   * @author      Matias Aguirre <maguirre@matware.com.ar>
   * @link        http://www.matware.com.ar
   * @copyright		Copyright 2006 - 2011 Matias Aguirre. All rights reserved.
   * @license		  GNU General Public License version 2 or later; see LICENSE.txt
   *
   * @param object $item
   */
  protected function _convertItem( $item, $language) {

    static $_extensions = null;

    if(is_null( $_extensions)) {
      $_extensions = ShlDbHelper::selectObjectList( '#__extensions', array('extension_id', 'element'), array( 'type' => 'component'),
          $aWhereData = array(), $orderBy = array(), $offset = 0, $lines = 0, $key = 'element');
    }

    // Fixing access
    $item->access++;
    // Fixing level : does not seem to exist??
    // $item->level++;
    // Fixing language
    $item->language = $language;
    // Fixing parent_id
    if (empty($item->parent_id)) {
      $item->parent_id = 1;
    }

    $item->title = $item->name;
    unset( $item->name);

    // use shared helper to account for changes in non-sef urls format
    // between J! 1.5 and J! 1.6+
    $item->link = JosettaadminHelper_Links::updateLinksFormat( $item->link);
    
    // build a dummy request array, that will be used by Josetta to identify
    // correctly the type of menu item
    $option = preg_match( '/[&|\?]option=([^&]*)/i', $item->link, $tmp) ? $tmp[1] : '';
    $view = preg_match( '/[&|\?]view=([^&]*)/i', $item->link, $tmp) ? $tmp[1] : '';
    $layout = preg_match( '/[&|\?]layout=([^&]*)/i', $item->link, $tmp) ? $tmp[1] : '';
    $item->request = array( 'option' => $option, 'view' => $view, 'layout' => $layout);

    // add component_id field
    if (!empty( $option)) {
      $item->component_id = empty( $_extensions[$option]) ? 0 : $_extensions[$option]->extension_id;
    }

    if ( (strpos($item->link, 'Itemid=') !== false) AND $item->type == 'menulink') {

      // Extract the Itemid from the URL
      if (preg_match('|Itemid=([0-9]+)|', $item->link, $tmp)) {
        $item_id = $tmp[1];

        $item->params = $item->params . "\naliasoptions=".$item_id;
        $item->type = 'alias';
        $item->link = 'index.php?Itemid=';
      }
    }

    // End fixing menus URL's

    // Converting params to JSON
    $item->params = $this->_convertParams( $item->params);

    // The Joomla 1.6 database structure does not allow duplicate aliases
    $count = 0;
    do {
      $alreadyInUse = ShlDbHelper::count( '#__menu', '*', array( 'alias' => $item->alias));
      if(!empty($alreadyInUse)) {
        $item->alias = $item->alias . '-' . $item->jta_import_id;
        if($count > 0) {
          $item->menutype .= '-' . $count;
        }
        $count++;
      }
    } while ($alreadyInUse > 0 and $count < 5);

    // fix the menu type: menu_types have been created under a different name
    // need to fix
    $item->menutype = ShlDbHelper::selectResult( '#__josetta_jf_journal', 'created_id', array( 'import_id' => $item->jta_import_id,
        'imported_context' => 'menu_types', 'created_context' => 'menu_types', 'imported_id' => $item->menutype));

    // kill the id, we always want to create a new item
    $pkName = $this->_pkName;
    $item->$pkName = 0;

    return $item;
  }


  /**
   * jUpgrade
   *
   * @version		  $Id$
   * @package		  MatWare
   * @subpackage	com_jupgrade
   * @author      Matias Aguirre <maguirre@matware.com.ar>
   * @link        http://www.matware.com.ar
   * @copyright		Copyright 2006 - 2011 Matias Aguirre. All rights reserved.
   * @license		  GNU General Public License version 2 or later; see LICENSE.txt
   */

  /**
   * Converts the params fields into a JSON string.
   *
   * @param	string	$params	The source text definition for the parameter field.
   *
   * @return	string	A JSON encoded string representation of the parameters.
   * @since	0.4.
   * @throws	Exception from the convertParamsHook.
   */
  protected function _convertParams($params)
  {
    $temp	= new JRegistry($params);
    $object	= $temp->toObject();

    // Fire the hook in case this parameter field needs modification.
    $this->convertParamsHook($object);

    return json_encode($object);
  }

  /**
   * A hook to be able to modify params prior as they are converted to JSON.
   *
   * @param	object	$object	A reference to the parameters as an object.
   *
   * @return	void
   * @since	0.4.
   * @throws	Exception
   */
  protected function convertParamsHook(&$object)
  {
    if (isset($object->menu_image)) {
      if((string)$object->menu_image == '-1'){
        $object->menu_image = '';
      }
    }
    if (isset($object->show_page_title)) {
      $object->show_page_heading = $object->show_page_title;
    }
  }

  /**
   * Not used, but need to implement as getForm() is declared
   * abstract in parent classes
   *
   * (non-PHPdoc)
   * @see JModelForm::getForm()
   */
  public function getForm($data = array(), $loadData = true) {

  }

}
