<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date        2015-12-23
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');

/**
 * Base class for a Josetta plugin
 * Performs most of the tasks in a generic form
 * based on a couple of xml files
 * describing the plugin
 * but methods can be directly overriden
 * in case plugins needs specifi behaviors
 *
 * @package        Josetta
 * @subpackage    com_josetta
 */
class JosettaadminClass_Extensionplugin extends JPlugin
{
	protected $_context = '';
	protected $_path = '';
	protected $_subTypePath = array();
	protected $_defaultTable = '';
	protected $_query = '';
	protected $_queryTranslated = '';
	protected $_queryNonTranslated = '';

	protected $_items = null;
	protected $_itemsCount = null;
	protected $_dataReadMethod = 'memory';

	/**
	 * Class constructor.
	 *
	 * @param    JDispatcher Object $subject  a dispatcher object for base class plugins
	 *
	 * @param   array $config A configuration array including optional elements such as session
	 *                          session_name, clientId and others. This is not exhaustive.
	 *
	 * @since   1.0
	 */
	public function __construct(&$subject, $config = array())
	{
		parent::__construct($subject, $config);
		$this->_setPath();
	}

	/*
	 * Clear the path of josetta element if exist
	 *
	 */
	protected function _setPath()
	{
		$this->_path = JPATH_PLUGINS . '/josetta/' . $this->_name;
	}

	/**
	 * Hook for 3rd party extensions to add a path to search for
	 * additional subtypes fields definitions
	 * To be used by extensions, for instance to handle specific menu items
	 * subtypes, or module subtypes
	 *
	 * @param string context
	 */
	public function onJosettaAddSubTypePath($context, $subType)
	{
		if (!empty($context) && ($context != $this->_context))
		{
			return;
		}

		// a 3rd party extension will use this hook to store a full path to
		// a directory where fields_*.xml files can be found
		// to be appended to the translation form,
		// for instance:
		// $this->_subTypePath[] = JPATH_PLUGINS . '/josetta_ext/' . $this->_name;
	}

	/**
	 * Load language files that may be need by the extension being
	 * translated
	 */
	public function loadLanguages()
	{
		$this->loadLanguage('joomla', JPATH_ADMINISTRATOR);
	}

	/**
	 * Get translated elements definitions
	 *
	 * This will be used by Josetta to display the
	 * input fields required to perform the translation
	 *
	 * Typically, this will read the xml definition file
	 * that comes with this plugin
	 *
	 * @param context name
	 *
	 * @return array
	 *
	 */
	public function onJosettaGetDefinitions($context = '')
	{
		if (!empty($context) && ($context != $this->_context))
		{
			return;
		}
		$definitions = $this->_getDefinitions();

		return $definitions;
	}

	/**
	 * Base class definition method to load xml
	 *
	 * @param   string $path path of xml in plugin.
	 *
	 * @return  xml
	 *
	 */
	protected function _getDefinitions()
	{
		$definitions = array();
		$fileName = $this->_path . '/definitions.xml';

		// check if file exists, to prevent run time errors
		jimport('joomla.filesystem.file');
		if (JFile::exists($fileName))
		{
			//return the xml path on josetta plugins
			$xml = file_get_contents($fileName);
			if (!empty($xml))
			{
				$definitions = JosettaadminHelper_General::getXml($xml, false);
			}
		}
		return $definitions;
	}

	/**
	 *
	 * Finds the sub type of a provided item
	 * usually by looking up the definition file
	 * but may be overriden if more complex logic is needed
	 *
	 * @param string $context
	 * @param mixed $item
	 */
	public function onJosettaGetSubType($context, $item)
	{
		$definitions = $this->_getDefinitions();

		$subTypeColumnName = empty($definitions->reference->subtype) ? '' : (string) $definitions->reference->subtype->column;
		$subType = empty($subTypeColumnName) || empty($item->$subTypeColumnName) ? '' : $item->$subTypeColumnName;

		// load languages for form and error messages
		$this->loadLanguages();

		// give a chance to 3rd party to add path to their own additional fields
		$dispatcher = JDispatcher::getInstance();

		// call plugin method onJosettaAddSubTypePath
		$results = $dispatcher->trigger('onJosettaAddSubTypePath', array($context, $subType));
		$this->_subTypePath = array_unique(array_merge($this->_subTypePath, $results));

		return $subType;
	}

	/**
	 *
	 * Compute the subtitle of a provided item
	 * Subtitle is used for display to the user of an item
	 * to provide more context
	 * By default, we use an item category
	 *
	 * @param string $context
	 * @param mixed $item
	 */
	public function onJosettaGetSubtitle($context, $item)
	{
		if (!empty($context) && ($context != $this->_context))
		{
			return;
		}

		$subTitle = '';
		if (empty($item))
		{
			return $subTitle;
		}

		// see if item has a catid field and fetch the category title if it does
		if (!empty($item->catid) && !is_array($item->catid))
		{
			$category = JosettaHelper::getCategoryDetails($item->catid);
			$subTitle = empty($category) ? '' : $category->title;
		}

		return $subTitle;
	}

	/**
	 *
	 * Find any filters that should apply to the modal popup
	 * used to select an existing content
	 *
	 * See for instance the categoriesitem plugin
	 *
	 * @param string $context
	 * @param mixed $item
	 * @return array of stdClass
	 */
	public function onJosettaGetUseExistingFilters($context, $item)
	{
		if (!empty($context) && ($context != $this->_context))
		{
			return;
		}

		$filters = array();

		return $filters;
	}

	/**
	 * Build up a list of form fields for this content type
	 * required to build a form to edit an item
	 *
	 * @param string $context
	 * @param string $subType
	 * @param string $targetLanguage
	 */
	public function onJosettaGetFields($context, $item, $targetLanguage = '')
	{
		if (!empty($context) && ($context != $this->_context))
		{
			return;
		}

		$fields = $this->_getFields($this->onJosettaGetSubType($context, $item), $targetLanguage);

		return $fields;
	}

	/**
	 * Load all fields definition types for a given
	 * object type, collating the 'common' set of fields
	 * with fields found, if any, in a 'subtype' xml file
	 *
	 * @param string $subType
	 * @param string $targetLanguage
	 */
	protected function _getFields($subType, $targetLanguage = '')
	{
		// check if file exists, to prevent run time errors
		jimport('joomla.filesystem.file');

		// then add the common fields (they'll end up first, due to how addChild() works
		$fileName = $this->_path . '/fields_common.xml';
		if (JFile::exists($fileName))
		{
			//return the xml path on josetta plugins
			$xml = file_get_contents($fileName);
			if (!empty($xml))
			{
				$commonFields = JosettaadminHelper_General::getXml($xml, false);
			}
		}

		// insert first fields for the subtypes, if any
		// as 3rd party extension can add their own fields
		// we also look up path that may have been added to the
		// _subTypePath property by said extensions plugins
		if (!empty($subType))
		{
			$pathList = array_merge($this->_subTypePath, array($this->_path));
			foreach ($pathList as $path)
			{
				$fileName = $path . '/fields_' . strtolower($subType) . '.xml';
				if (JFile::exists($fileName))
				{
					//return the xml path on josetta plugins
					$additionalXml = file_get_contents($fileName);
					if (!empty($additionalXml))
					{
						$additionalFields = JosettaadminHelper_General::getXml($additionalXml, false);
					}
				}
			}
		}

		if (empty($commonFields))
		{
			$fields = empty($additionalFields) ? null : $additionalFields;
		}
		else
		{
			$fields = $commonFields;
			// now collate the additional fields with the common ones
			if (!empty($additionalFields))
			{
				foreach ($additionalFields->fields as $field)
				{
					$this->_appendXmlChild($fields, $field);
				}
			}
		}

		// append custom fields
		$customXml = $this->_createFormAddCustomFields($targetLanguage);
		if (!empty($customXml))
		{
			$customFields = JosettaadminHelper_General::getXml('<josetta><fields>' . $customXml . '</fields></josetta>', false);
		}
		if (!empty($customFields))
		{
			foreach ($customFields->fields as $field)
			{
				$this->_appendXmlChild($fields, $field);
			}
		}

		return $fields;
	}

	/**
	 *
	 * Copied from J! library. How come php doesn't do that?
	 *
	 * @param SimpleXMLElement $appendTo
	 * @param SimpleXMLElement $child
	 */
	protected function _appendXmlChild($appendTo, $child)
	{
		// Add the new child node.
		$node = $appendTo->addChild($child->getName(), trim($child));

		// Add the attributes of the child node.
		foreach ($child->attributes() as $name => $value)
		{
			$node->addAttribute($name, $value);
		}

		// Add any children of the new node.
		foreach ($child->children() as $childChild)
		{
			$this->_appendXmlChild($node, $childChild);
		}
	}

	/**
	 * Get translated elements definitions
	 *
	 * This will be used by Josetta to display the
	 * input fields required to perform the translation
	 *
	 * Typically, this will read the xml definition file
	 * that comes with this plugin
	 *
	 * @param context name
	 *
	 * @return array
	 *
	 */
	public function onJosettaCreateFormXml($context, $item, $originalItem, $targetLanguage)
	{
		if (!empty($context) && ($context != $this->_context))
		{
			return;
		}

		if (empty($originalItem) || empty($originalItem->id))
		{
			return;
		}

		$xml = $this->_createFormXml($context, $item, $originalItem, $targetLanguage);

		return $xml;
	}

	/**
	 * Method to get the create xml format using Josetta Plugin.
	 *
	 *
	 * @return    mixed    the jform xml for the form.
	 */
	protected function _createFormXml($itemType, $item, $originalItem, $targetLanguage)
	{
		$definitions = $this->_getDefinitions();

		//return child element of fields child field tag in xml
		$subType = empty($subType) ? $this->onJosettaGetSubType($itemType, $originalItem) : '';

		//Building xml process begin
		$xml = '<?xml version="1.0" encoding="UTF-8" ?>' . "\n";
		$xml .= '<form>' . "\n";
		$xml .= '<fields>' . "\n";
		$xml .= '<fieldset name="josetta">' . "\n";

		$formFields = $this->_getFields($subType, $targetLanguage);
		foreach ($formFields->fields as $fields)
		{
			if (isset($fields->fieldset))
			{
				// fields nested inside a fieldset
				$fieldsetName = (string) $fields->fieldset->attributes()->name;
				$xml .= '</fieldset>' . "\n";
				$xml .= '</fields>' . "\n";
				$xml .= '<fields name="' . $fieldsetName . '">' . "\n";
				$xml .= '<fieldset name="' . $fieldsetName . '">' . "\n";
				foreach ($fields->fieldset->field as $field)
				{
					$xml .= $this->_outputFieldsXml($field, $itemType, $item, $originalItem, $targetLanguage);
				}
				$xml .= '</fieldset>' . "\n";
				$xml .= '</fields>' . "\n";
				$xml .= '<fields>' . "\n";
				$xml .= '<fieldset name="josetta">' . "\n";
			}
			else
			{
				// a list of fields
				foreach ($fields as $field)
				{
					$xml .= $this->_outputFieldsXml($field, $itemType, $item, $originalItem, $targetLanguage);
				}
			}
		}

		// hook for plugins to add custom fields
		$xml .= $this->_createFormAddCustomFields($targetLanguage);

		//Pass the id of existing element to translate
		$xml .= '<field name="ref_id" type="hidden" default="' . $originalItem->id . "\" />\n";
		$xml .= '<field name="ref_lang" type="hidden" default="' . $originalItem->language . "\" />\n";

		//Pass the id of translated element, if any
		$xml .= '<field name="id" type="hidden" default="' . (empty($item) || empty($item->id) ? 0 : $item->id) . "\" />\n";

		//Pass the publishing information of translated element
		if (!empty($definitions->reference->published->column))
		{
			$xml .= '<field name="' . (string) $definitions->reference->published->column . '" type="hidden" default="0" />' . "\n";
		}

		$xml .= '</fieldset>' . "\n";
		$xml .= '</fields>' . "\n";
		$xml .= '</form>' . "\n";

		//Build process of xml completed
		return $xml;
	}

	protected function _outputFieldsXml($field, $itemType, $item, $originalItem, $targetLanguage)
	{

		// array to hold required bits of data
		// that will be later rendered as XML
		// All bits are stored in an array so that
		// this array can be manipulated by 3rd party plugins
		// adding processing for their own custom fields
		$xmlData = new stdClass();

		// get the value of  <translate> tag
		$xmlData->shouldTranslate = (string) $field->translate;

		// get description, if any
		$xmlData->description = empty($field->description) ? '' : (string) $field->description;
		$xmlData->description = empty($xmlData->description) ? '' : 'description="' . $xmlData->description . '"';

		//get the value of  <addfieldpath> tag
		$xmlData->addFieldPath = is_null($field->addfieldpath) ? '' : (string) $field->addfieldpath;
		$xmlData->addFieldPath = empty($xmlData->addFieldPath) ? '' : 'addfieldpath="' . (string) $field->addfieldpath . '"';

		//get the value of  <size> tag
		$xmlData->size = (string) $field->size == '0' ? '' : 'size="' . (string) $field->size . '"';

		// read only tag
		$xmlData->readOnly = is_null($field->readonly) ? '' : (string) $field->readonly;
		$xmlData->readOnly = empty($xmlData->readOnly) ? '' : 'readonly="' . (string) $field->readonly . '"';

		// show_root tag for categories displays
		$xmlData->showRoot = is_null($field->show_root) ? '' : (string) $field->show_root;
		$xmlData->showRoot = empty($xmlData->showRoot) ? '' : 'show_root="' . (string) $field->show_root . '"';

		// read only tag
		$xmlData->default = is_null($field->default) ? '' : (string) $field->default;
		$xmlData->default = empty($xmlData->default) ? '' : 'default="' . (string) $field->default . '"';

		//get the value of  <length> tag
		$xmlData->maxLength = (string) $field->length == '0' ? '' : 'maxlength="' . (string) $field->length . '"';

		//get the value of  <type> tag
		$xmlData->fieldType = (string) $field->type;

		//get the value of  <filter> tag in field in context.xml
		$xmlData->filterType = empty($field->filter) ? '' : 'filter="' . (string) $field->filter . '"';

		$xmlData->subfield = '';
		$xmlData->other = '';
		$xmlData->class = 'class="josetta"';

		//check if translate tag is not set to no in xml
		if (strtolower($xmlData->shouldTranslate) != 'no')
		{
			switch ($xmlData->fieldType)
			{
				case 'josettalist':
				case 'list':
					//add option tag in list if present in jform
					foreach ($field->option as $option)
					{
						$xmlData->subfield .= '<option value="' . (string) $option->value . '">' . (string) $option->title . '</option>';
					}
					break;

				case 'category':
				case 'languagecategory':
					//add extension tag if type category is present
					//add option tag in list if present in jform
					foreach ($field->option as $option)
					{
						$xmlData->subfield .= '<option value="' . (string) $option->value . '">' . (string) $option->title . '</option>';
					}
					//Important for developer if using category type extension must be defined in xml
					$xmlData->other .= ' extension="' . (string) $field->extension . '"';
					$xmlData->other .= ' languages="' . $targetLanguage . '"';
					$multiple = !empty($field->multiple) && (string) $field->multiple == 'yes';
					$xmlData->other .= $multiple ? ' multiple="true"' : '';
					break;

				case 'languagecategoryparent':
					//add extension tag if type category is present
					//Important for developer if using category type extension must be defined in xml
					if (!empty($originalItem) && !empty($originalItem->extension))
					{
						$xmlData->other .= 'extension="' . $originalItem->extension . '"';
					}
					else
					{
						$extension = empty($field->extension) ? '' : $field->extension;
						$xmlData->other .= empty($extension) ? '' : 'extension="' . $extension . '"';
					}
					$xmlData->other .= ' languages="' . $targetLanguage . '"';
					break;

				case 'menuitem':
					//add option tag in list if present in jform
					foreach ($field->option as $option)
					{
						$xmlData->subfield .= '<option value="' . (string) $option->value . '">' . (string) $option->title . '</option>';
					}
					$xmlData->other .= ' language="' . $targetLanguage . '"';
					break;

				case 'languagemenuparent':
					//add option tag in list if present in jform
					foreach ($field->option as $option)
					{
						$xmlData->subfield .= '<option value="' . (string) $option->value . '">' . (string) $option->title . '</option>';
					}
					$xmlData->other .= ' languages="' . $targetLanguage . '"';
					break;

				case 'bannerclient':
					$xmlData->other .= ' extension="' . (string) $field->extension . '"';
					$xmlData->other .= ' language="' . $targetLanguage . '"';
					break;

				case 'request':
					break;

				case 'menumoduleassignment':
					$xmlData->other .= ' languages="' . $targetLanguage . '"';
					$xmlData->other .= ' displayoriginal="no"';
					break;

				default:
					// allow 3rd party plugins to modify/add custom fields xml
					$xmlData = $this->_output3rdPartyFieldsXml($xmlData, $field, $itemType, $item, $originalItem, $targetLanguage);

					break;
			}
			//if <require> tag is present in <field> in context.xml
			$xmlData->isRequired = (string) $field->require == 'yes' ? 'required="true"' : '';

			// compute change detection event
			$onChange = ' onchange="Josetta.itemChanged(this);"';

			//Build xml node for jforms
			$xml = ' <field  name="' . (string) $field->id . '" ' . $xmlData->class . $onChange . ' type="' . $xmlData->fieldType . '" label="'
				. (string) $field->name . '"   ' . $xmlData->isRequired . ' ' . $xmlData->addFieldPath . ' ' . $xmlData->size . ' '
				. $xmlData->maxLength . ' ' . $xmlData->filterType . ' ' . $xmlData->showRoot . ' ' . $xmlData->other . ' ' . $xmlData->description
				. ' ' . $xmlData->readOnly . ' ' . $xmlData->default . ' >' . $xmlData->subfield . '</field>' . "\n";
		}
		else
		{
			// pass fields not set to be translated as hidden fields
			$xml = '<field name="' . (string) $field->column . '" type="hidden" default="' . (string) $field->value . "\" />\n";
		}

		return $xml;
	}

	/**
	 * Hook for module to add their own fields processing
	 * to the form xml
	 *
	 * @return string
	 */
	protected function _output3rdPartyFieldsXml($xmlData, $field, $itemType, $item, $originalItem, $targetLanguage)
	{
		return $xmlData;
	}

	/**
	 * Hook for module to add raw fields definitions
	 * to the form xml
	 *
	 * @return string
	 */
	protected function _createFormAddCustomFields($targetLanguage)
	{
		return '';
	}

	/**
	 * Method to load an item from the database
	 *
	 * @param string $context a context string, to either process or ignore an event
	 * @param int $id id of the element
	 *
	 * @return  object  an object representing the item, with no particular type
	 *
	 */
	public function onJosettaLoadItem($context, $id = '')
	{
		if ((!empty($context) && ($context != $this->_context)) || (empty($id)))
		{
			return null;
		}

		$definitions = $this->_getDefinitions();

		try
		{
			$item = ShlDbHelper::selectObject((string) $definitions->table, '*', array((string) $definitions->reference->id->column => $id));
			$item = $this->_loadItemProcessParams($item, $context);
		}
		catch (Exception $e)
		{
			$item = null;
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
		}

		return $item;
	}

	/**
	 * Method to load an item from the database
	 *
	 * @param string $context a context string, to either process or ignore an event
	 *
	 * @return  object  an object representing the item, with no particular type
	 *
	 */
	protected function _loadItemProcessParams($item, $context)
	{
		if (!empty($item) && isset($item->params))
		{
			// handle params that are stored in the 'params' column, ie as json in a text field
			$subType = $this->onJosettaGetSubType($context, $item);
			$fields = $this->_getFields($subType);
			$parameters = new JRegistry;
			$parameters->loadString($item->params);

			foreach ($this->_getFields($subType)->fields as $fields)
			{
				if (isset($fields->fieldset))
				{
					// a fieldset
					foreach ($fields->fieldset->field as $field)
					{
						$this->_getFieldFromParameters($item, $field, $parameters);
					}
				}
				else
				{
					// a list of fields
					foreach ($fields as $field)
					{
						$this->_getFieldFromParameters($item, $field, $parameters);
					}
				}
			}
		}

		return $item;
	}

	protected function _getFieldFromParameters(&$item, $field, $parameters)
	{
		if (!empty($field->column) && (string) $field->column == 'params')
		{
			$propertyName = (string) $field->id;
			if (!empty($propertyName))
			{
				$item->$propertyName = $parameters->get($propertyName);
			}
		}

	}

	/**
	 * Method implementation on base class of context plugin to show data in grid using joomla framework.
	 *
	 * @param   string $context type of the element to load items
	 *
	 * @return  null
	 */
	public function onJosettaLoadItems($context, $state)
	{
		if ((!empty($context) && ($context != $this->_context)))
		{
			return null;
		}

		$db = ShlDbHelper::getDb();

		if ($this->_dataReadMethod == 'database')
		{
			$this->_items = array();

			$this->_buildItemsListQuery($state);
			try
			{
				$db->setQuery($this->_query, $state->get('list.start'), $state->get('list.limit'));
				$this->_items = $db->shlLoadObjectList();

			}
			catch (Exception $e)
			{
				$this->_items = array();
				ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			}
		}
		else
		{
			// process data in memory
			// load item type definition from plugin
			$definitions = $this->onJosettaGetDefinitions($this->_context);

			// type of content we're processing
			$type = $state->get('type');

			// build filters
			$mWhere = array();
			$aWhereData = array();

			// Filter by search in title.
			$search = $state->get('filter_search');
			$search = JString::strtolower($search);
			$column = (String) $definitions->reference->title->column;
			if ($search && $column)
			{
				$mWhere[] = "LOWER(" . $db->quoteName($column) . ") LIKE " . $db->Quote('%' . $db->escape($search, true) . '%');
			}

			// extract filters from the definitions
			$filtersData = $definitions->filters->filter;

			// process each filter
			if (!empty($filtersData))
			{
				foreach ($filtersData as $filter)
				{
					// shortcut to filter name
					$filterName = $type . '_' . (string) $filter->id;

					// get current value
					$current = $state->get($filterName, (string) $filter->default);

					// build a (part of a) query depending on the filter type
					switch ((string) $filter->type)
					{
						// we can add more filter types later if needed
						// default
						default:
							$allowNull = (string) $filter->allownull;
							if (!empty($current) || !empty($allowNull))
							{
								$mWhere[] = $db->quoteName((string) $filter->column) . ' = ' . $db->Quote($current);
							}
							break;
					}
				}
			}

			// There might be some permanent filtering of items to be displayed
			// for instance, do not display Trashed items
			$referenceFilter = $definitions->reference_filters->reference_filter;
			if (!empty($referenceFilter))
			{
				foreach ($referenceFilter as $filter)
				{
					// process first "inclusive" filters
					$inclusiveQuery = '';

					// there is a specific value required (in <value></value> tag)
					if (isset($filter->value))
					{
						$value = (string) $filter->value;
						if (!empty($value) || $value == '0')
						{
							$inclusiveQuery .= $db->quoteName((string) $filter->column) . ' = ' . $db->Quote($value);
						}
					}

					// there is a list of specific values to include (in <include></include> tag)
					if (isset($filter->include))
					{
						$includes = (string) $filter->include;
						if (!empty($includes) || $includes == '0')
						{
							$includes = explode(',', $includes);
							$includesQuery = $db->quoteName((string) $filter->column) . ' in (' . ShlDbHelper::arrayToQuotedList($includes) . ')';
							$inclusiveQuery = empty($inclusiveQuery) ? $includesQuery : '(' . $inclusiveQuery . ' or ' . $includesQuery . ')';
						}
					}

					// if any include or value tag was set in xml file, create the corresponding query
					if (!empty($inclusiveQuery))
					{
						$mWhere[] = $inclusiveQuery;
					}

					// now "exclusive" filter: there is a list of specific values to exclude (in <exclude></exclude> tag)
					if (isset($filter->exclude))
					{
						$excludes = (string) $filter->exclude;
						if (!empty($excludes) || $excludes == '0')
						{
							$excludes = explode(',', (string) $filter->exclude);
							$mWhere[] = $db->quoteName((string) $filter->column) . ' not in (' . ShlDbHelper::arrayToQuotedList($excludes) . ')';
						}
					}
				}
			}

			// language filter used to build use existing content
			$filterLang = $state->get('filter_lang');
			if (!empty($filterLang))
			{
				// a specific language was requested: we are probably on the translate page, in a modal window
				$mWhere[] = $db->quoteName('language') . ' = ' . $db->Quote($filterLang);
			}
			else
			{
				// no specific language requested, we are probably on the main translator page
				// show the default language for the site and the "all" language
				$languages = array();
				$languages[] = $db->Quote(JosettaHelper::getSiteDefaultLanguage());
				$languages[] = $db->Quote('*');
				$languagesList = implode(',', $languages);
				$mWhere[] = $db->quoteName('language') . ' in (' . $languagesList . ')';
			}

			// collate sub filters
			$mWhere = implode(' AND ', $mWhere);

			// find about columns to read from db
			$columns = array();

			// find about the ordering column to use
			$orderBy = $this->_getOrderingColumn();
			$orderBy = empty($orderBy) ? null : array($orderBy);

			// but always read the language column
			$columns[] = 'language';
			// read reference fields from xml and add to select list
			foreach ($definitions->reference as $reference)
			{
				foreach ($reference as $name => $ref)
				{
					$columns[] = (string) $ref->column;
				}
			}

			// find about the table id field
			$idFieldName = (string) $definitions->reference->id->column;

			// read raw data from database
			$rawItems = ShlDbHelper::selectObjectList((string) $definitions->table, $columns, $mWhere, $aWhereData, $orderBy, $offset = 0,
				$lines = 0, $idFieldName);
			$associations = ShlDbHelper::selectObjectList('#__josetta_associations', array('*'), array('context' => $this->_context),
				$aWhereData = array(), $orderBy = array(), $offset = 0, $lines = 0, $key = 'id');
			$metadata = ShlDbHelper::selectObjectList('#__josetta_metadata', '*', array('context' => $this->_context), $aWhereData = array(),
				$orderBy = array(), $offset = 0, $lines = 0, $key = 'item_id');

			// now apply filtering in memory, that is filtering based on data in the associations or metadata tables
			// filtering on target translation language value
			$translationLanguageFilter = $state->get('filter_translation_language');
			// filtering on completion value
			$translationStatusFilter = $state->get('filter_translation_status');

			foreach ($rawItems as $id => $article)
			{
				// find associated article id
				if (empty($associations[$id]))
				{
					// no item associated: keep if looking for all items
					if ($translationStatusFilter != JosettaHelper::TRANSLATION_STATUS_ONLY_COMPLETED
						&& $translationStatusFilter != JosettaHelper::TRANSLATION_STATUS_ONLY_STARTED
					)
					{
						continue;
					}
					else
					{
						unset($rawItems[$id]);
					}
				}
				else
				{
					// we have associated record(s)
					$key = $associations[$id]->key;
					$associatedRecords = array();
					foreach ($associations as $associationId => $association)
					{
						if ($associationId != $id && $association->key == $key)
						{
							$associatedRecords[$association->language] = $association;
						}
					}

					// we don't have any translation, but user has asked for one
					if (empty($associatedRecords) && $translationStatusFilter == JosettaHelper::TRANSLATION_STATUS_ONLY_COMPLETED
						&& (empty($translationLanguageFilter)
							|| (!empty($translationLanguageFilter) && empty($associatedRecords[$translationLanguageFilter])))
					)
					{
						unset($rawItems[$id]);
						continue;
					}

					// we don't have any translation, but user has asked for one
					if (empty($associatedRecords) && ($translationStatusFilter == JosettaHelper::TRANSLATION_STATUS_ONLY_STARTED))
					{
						unset($rawItems[$id]);
						continue;
					}

					// at this stage, we now there's at least one translation for this article
					// so we can look at conditions user has set for the status of this (these) translation(s)
					$shouldKeep = false;
					foreach ($associatedRecords as $associatedRecordLanguage => $associatedRecord)
					{
						// if this user is not allowed to view an item in a particular language
						// let's not include it in the filtering
						if (!JosettaHelper_Acl::authorize('josetta.canTranslate.' . $associatedRecordLanguage))
						{
							// we continue, which in effect will remove the item from display
							// as $shouldKeep will never be set to true
							continue;
						}
						switch ($translationStatusFilter)
						{
							case JosettaHelper::TRANSLATION_STATUS_ONLY_COMPLETED:
								if (!empty($metadata[$associatedRecord->id]) && $metadata[$associatedRecord->id]->completion == 100
									&& (empty($translationLanguageFilter) || $translationLanguageFilter == $associatedRecordLanguage)
								)
								{
									$shouldKeep = true;
								}
								break;
							case JosettaHelper::TRANSLATION_STATUS_ONLY_UNCOMPLETED:
								if (empty($metadata[$associatedRecord->id]) || $metadata[$associatedRecord->id]->completion != 100)
								{
									$shouldKeep = true;
								}
								break;
							case JosettaHelper::TRANSLATION_STATUS_ONLY_STARTED:
								if (!empty($metadata[$associatedRecord->id]) && $metadata[$associatedRecord->id]->completion == 0)
								{
									$shouldKeep = true;
								}
								break;
							default:
								$shouldKeep = true;
								break;
						}
					}

					if (!$shouldKeep)
					{
						unset($rawItems[$id]);
					}

				}
			}

			// finally slice items for display
			$this->_items = array_slice($rawItems, $state->get('list.start'), $state->get('list.limit'), $preserve_keys = true);

			// prepare result:
			$this->_itemsCount = count($rawItems);

		}
		return $this->_items;
	}

	/**
	 * Method implementation on base class of context plugin to show data in grid using joomla framework.
	 *
	 * @param   string $context type of the element to load items
	 *
	 * @return  null
	 */
	public function onJosettaGetItemsCount($context, $state)
	{
		if ((!empty($context) && ($context != $this->_context)))
		{
			return null;
		}

		if ($this->_dataReadMethod == 'database')
		{
			$this->_itemsCount = 0;

			try
			{
				$db = ShlDbHelper::getDb();
				$this->_getItemsCountQuery($state);
				$db->setQuery($this->_query);
				$this->_itemsCount = $db->shlLoadResult();

			}
			catch (Exception $e)
			{
				$this->_itemsCount = 0;
				ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			}
		}
		else
		{ // process data in memory
			if (is_null($this->_itemsCount))
			{
				$this->_itemsCount = 0;
				$this->onJosettaLoadItems($context, $state);
			}
		}

		return $this->_itemsCount;
	}

	/**
	 * Save an item after it has been translated
	 * This will be called by Josetta when a user clicks
	 * the Save button. The context is passed so
	 * that each plugin knows if it must process the data or not
	 *
	 * if $item->(id) is empty, this is
	 * a new item, otherwise we are updating the item
	 * The name of the (id) field may vary, but should already be set
	 * to the proper value before the data is sent to the plugin
	 * (by the translate model)
	 *
	 * $item->data contains the fields entered by the user
	 * that needs to be saved
	 *
	 * @param context type
	 * @param data in form of array
	 *
	 *return table id if data is inserted
	 *
	 *return false if error occurs
	 *
	 */
	public function onJosettaSaveItem($context, $item, &$errors)
	{
		if (($context != $this->_context))
		{
			return;
		}

		$errors = array();

		jimport('joomla.database.table');
		try
		{

			// Check for the special 'request' entry.
			if (!empty($item['type']) && $item['type'] == 'component' && isset($item['request']) && is_array($item['request'])
				&& !empty($item['request'])
			)
			{

				// Parse the submitted link arguments.
				$args = array();
				parse_str(parse_url($item['link'], PHP_URL_QUERY), $args);

				// Merge in the user supplied request arguments.
				$args = array_merge($args, $item['request']);
				$item['link'] = JosettaadminHelper_General::buildurl($args);
				unset($item['request']);

			}

			// pre-process params field, encoding its content to json
			$item = $this->_saveItemProcessParams($item, $context);

			// load languages for form and error messages
			$this->loadLanguages();

			// now actually store the record
			$table = $this->_getTable();
			if (!($table->save($item)))
			{
				// make sure error are displayed
				$errors = $table->getErrors();
				return false;
			}

		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			$errors[] = $e->getMessage();
			return false;
		}

		// return item id in the table
		return ($table->id);
	}

	/**
	 * Converts input fields values that must be encoded in a 'params' field
	 * when saved to database, using an INI format
	 *
	 * @param array $item input data, as entered by user
	 * @param string $context type of content being saved
	 */
	protected function _saveItemProcessParams($item, $context)
	{

		// get back original data, to access the params field
		$originalItemId = empty($item['ref_id']) ? $item['id'] : $item['ref_id'];
		$originalItem = $this->onJosettaLoadItem($context, $originalItemId);
		// handle params that are stored in the 'params' column, ie as json in a text field
		if (isset($originalItem->params) && !empty($originalItem))
		{
			$parameters = new JRegistry;
			$parameters->loadString($originalItem->params);
			$subType = $this->onJosettaGetSubType($context, $originalItem);

			foreach ($this->_getFields($subType)->fields as $fields)
			{
				if (isset($fields->fieldset))
				{
					// a fieldset
					foreach ($fields->fieldset->field as $field)
					{
						$this->_setParameterFromField($item, $field, $parameters);
					}
				}
				else
				{
					// a list of fields
					foreach ($fields as $field)
					{
						$this->_setParameterFromField($item, $field, $parameters);
					}
				}
			}

			$item['params'] = $parameters->toString();
		}

		return $item;
	}

	protected function _setParameterFromField($item, $field, &$parameters)
	{

		if (!empty($field->column) && (string) $field->column == 'params')
		{
			$propertyName = (string) $field->id;
			if (!empty($propertyName))
			{
				$parameters->set($propertyName, empty($item[$propertyName]) ? '' : $item[$propertyName]);
			}
		}

	}

	/**
	 * Format a field content for display on the main translator view
	 * (ie a grid)
	 *
	 * @param string $data the raw field data
	 * @param string $type the field type
	 * @return string the formatted string
	 */
	public function onJosettaGet3rdPartyFormatGridData($data, $type)
	{

		if (!empty($context) && ($context != $this->_context))
		{
			return;
		}

		$displayText = $data;

		// if plugin has some specific fields it must displays
		// handle formatting here and return as a string

		return $displayText;
	}

	/**
	 * Format a translation field for display on the translate view
	 *
	 * @param string $data the raw field data
	 * @param string $type the field type
	 * @return string the formatted string
	 */
	public function onJosettaGet3rdPartyFormatTranslationField($data, $type)
	{

		$displayText = null;

		// if plugin has some specific fields it must displays
		// handle formatting here and return as a string

		return $displayText;
	}

	/**
	 * Format a the original field value for display on the translate view
	 *
	 * @param object $originalItem the actual data of the original item
	 * @param string $originalFieldTitle the field title
	 * @param object $field the Joomla! field object
	 * @param string the formatted, ready to display, string
	 */
	public function onJosettaGet3rdPartyFormatOriginalField($originalItem, $originalFieldTitle, $field)
	{

		$displayText = null;

		// if plugin has some specific fields it must displays
		// handle formatting here and return as a string

		return $displayText;
	}

	/**
	 * Get html for buttons to be offered to users, to carry over
	 * translation: normally a copy button, and optionnally a suggest
	 * translation button
	 * Basic types fields display is builtin, but plugins can/will add
	 * their own fields, for which they should provide buttons
	 *
	 * Note: plugin can make use of JosettaHelper::getCopyButton()
	 * and JosesttaHelper::getSuggestButton() methods to add
	 * the common default buttons
	 * In the latter case, use also the @link JosettaHelper::canSuggestTranslation()
	 * method before adding the suggest button code, to
	 *
	 * @param object $form the Joomla! form object to which the field belongs
	 * @param object $field the Joomla! field object
	 * @param string $language target translation language
	 * @return string the formatted, ready to display, string
	 */
	public function onJosettaGet3rdPartycreateButton($form, $field, $language)
	{

		$buttonsHtml = null;

		// plugin can decide here what to display

		return $buttonsHtml;
	}

	/**
	 * Instantiate table object to read/save item
	 *
	 */
	protected function _getTable()
	{

		$table = JTable::getInstance($this->_defaultTable);

		return $table;
	}

	/**
	 * Build an SQL query to load the list of items from xml definition of <reference> tag from context.xml of josetta plugin.
	 *
	 * @return    JDatabaseQuery
	 */
	protected function _buildItemsListQuery($state)
	{

		// start building the main part of the query,
		$db = ShlDbHelper::getDb();

		//Load the defintion of xml of plugin
		$definitions = $this->onJosettaGetDefinitions($this->_context);
		if (empty($definitions))
		{
			return false;
		}

		$table = (string) $definitions->table;
		$query = '';

		// add language, which should always be read
		$query .= 'select ' . $db->quoteName('a.language');

		// add the ordering column
		// read reference fields from xml and add to select list
		foreach ($definitions->reference as $reference)
		{
			foreach ($reference as $name => $ref)
			{
				$query .= ',' . $db->quoteName('a.' . (string) $ref->column);
			}
		}

		// thus far, both sub queries are (almost) the same
		// just need to add the ordering field
		$column = $this->_getOrderingColumn();
		if (!empty($column))
		{
			$query .= ',' . $db->quoteName('a.' . $column) . ' as josetta_ordering';
		}
		$query .= ' from ' . $db->quoteName($table) . ' as a';

		// now build start of second query in union
		$this->_queryNonTranslated = $query;
		$this->_queryTranslated = $query;

		// join on completion value if set to
		$this->_buildJoinQuery($state);

		// get the filters definition from the plugin, so
		// that we can build the part of the query that deals with filters
		$this->_buildFilterQuery($state);

		// build the final query, through a union of the 2 partial queries
		$this->_query = '(' . $this->_queryNonTranslated . ') union (' . $this->_queryTranslated . ')';

		// Add the list ordering clause.
		$this->_buildOrderQuery($state);

		return $this->_query;
	}

	/**
	 * Build an SQL query to load the list of items from xml definition of <reference> tag from context.xml of josetta plugin.
	 *
	 * @return    JDatabaseQuery
	 */
	protected function _getItemsCountQuery($state)
	{

		// start building the main part of the query,
		$db = ShlDbHelper::getDb();

		//Load the defintion of xml of plugin
		$definitions = $this->onJosettaGetDefinitions($this->_context);
		if (empty($definitions))
		{
			return false;
		}

		//load table name from xml tag of context.xml file
		$table = (string) $definitions->table;
		$idFieldName = (string) $definitions->reference->id->column;

		// thus far, both sub queries are the same
		$this->_queryNonTranslated = 'select count(' . $db->quoteName('a.' . $idFieldName) . ') as josetta_count' . ' from ' . $db->quoteName($table)
			. ' as a';
		$this->_queryTranslated = 'select count(' . $db->quoteName('a.' . $idFieldName) . ')' . ' from ' . $db->quoteName($table) . ' as a';

		// join on completion value if set to
		$this->_buildJoinQuery($state);

		// get the filters definition from the plugin, so
		// that we can build the part of the query that deals with filters
		$this->_buildFilterQuery($state);

		// build the final query, through a union of the 2 partial queries
		$this->_query = 'select sum( josetta_count) from ( (' . $this->_queryNonTranslated . ') union (' . $this->_queryTranslated
			. ')) as josetta_dummy';

		return $this->_query;
	}

	/**
	 *
	 * Process generic filters definitions as read from plugin
	 * xml file, and turn them into a (part of a) query
	 *
	 * @param object $state
	 */
	protected function _buildJoinQuery($state)
	{

		$db = ShlDbHelper::getDb();

		//Load the defintion of xml of plugin
		$definitions = $this->onJosettaGetDefinitions($this->_context);
		if (empty($definitions))
		{
			return $query;
		}

		// fields we'll need to filter
		$type = $state->get('type');
		$idFieldName = (string) $definitions->reference->id->column;

		// join for items with no translations, ie no record in the associations table
		$this->_queryNonTranslated .= ' left join ' . $db->quoteName('#__josetta_associations') . ' as t ON ' . $db->quoteName('t.context') . ' = '
			. $db->Quote($type) . ' and ' . $db->quoteName('t.' . $idFieldName) . ' = ' . $db->quoteName('a.' . $idFieldName) . ' and '
			. $db->quoteName('t.' . 'language') . ' = ' . $db->quoteName('a.' . 'language');

		// still for not translated, join metadata
		$this->_queryNonTranslated .= ' left join ' . $db->quoteName('#__josetta_metadata') . ' as m on ' . $db->quoteName('m.item_id') . ' = '
			. $db->quoteName('a.' . $idFieldName) . ' and ' . $db->quoteName('m.context') . ' = ' . $db->quote($type);

		// now for items with translations
		$this->_queryTranslated .= ' inner join ' . $db->quoteName('#__josetta_associations') . ' as t ON ' . $db->quoteName('t.context') . ' = '
			. $db->Quote($type) . ' and ' . $db->quoteName('t.id') . ' = ' . $db->quoteName('a.' . $idFieldName) . ' and '
			. $db->quoteName('t.' . 'language') . ' = ' . $db->quoteName('a.' . 'language');

		// we look for the associated items
		$this->_queryTranslated .= ' inner join ' . $db->quoteName('#__josetta_associations') . ' as t2 ON ' . $db->quoteName('t2.key') . ' = '
			. $db->quoteName('t.key');

		// and then can load metadata
		$this->_queryTranslated .= ' inner join ' . $db->quoteName('#__josetta_metadata') . ' as m on ' . $db->quoteName('m.item_id') . ' = '
			. $db->quoteName('t2.id') . ' and ' . $db->quoteName('m.context') . ' = ' . $db->quote($type);

	}

	/**
	 *
	 * Process generic filters definitions as read from plugin
	 * xml file, and turn them into a (part of a) query
	 *
	 * @param object $state
	 */
	protected function _buildFilterQuery($state)
	{

		$db = ShlDbHelper::getDb();

		//Load the defintion of xml of plugin
		$definitions = $this->onJosettaGetDefinitions($this->_context);

		// type of content we're processing
		$type = $state->get('type');

		// buffers
		$whereNonTranslated = array();
		$whereTranslated = array();

		// Filter by search in title.
		$search = $state->get('filter_search');
		$search = JString::strtolower($search);
		if ($search)
		{
			if (!empty($definitions->reference->title->column))
			{
				$searchsql[] = "LOWER(a." . $db->quoteName((string) $definitions->reference->title->column) . ") LIKE "
					. $db->Quote('%' . $db->escape($search, true) . '%');
			}

			//implode function if more search occur in future
			$search = '(' . implode(') OR (', $searchsql) . ')';
			$whereNonTranslated[] = ' ' . $search;
			$whereTranslated[] = ' ' . $search;
		}

		// extract filters from the definitions
		$filtersData = $definitions->filters->filter;

		// process each filter
		if (!empty($filtersData))
		{
			foreach ($filtersData as $filter)
			{
				// shortcut to filter name
				$filterName = $type . '_' . (string) $filter->id;

				// get current value
				$current = $state->get($filterName, (string) $filter->default);

				// build a (part of a) query depending on the filter type
				switch ((string) $filter->type)
				{

					// we can add more filter types later if needed
					// default
					default:
						if (!empty($current))
						{
							$where = $db->quoteName('a.' . (string) $filter->column) . ' = ' . $db->Quote($current);
							$whereNonTranslated[] = ' ' . $where;
							$whereTranslated[] = ' ' . $where;
						}
						break;
				}
			}
		}

		// There might be some permanent filtering of items to be displayed
		// for instance, do not display Trashed items
		$referenceFilter = $definitions->reference_filters->reference_filter;
		if (!empty($referenceFilter))
		{
			foreach ($referenceFilter as $filter)
			{

				// process first "inclusive" filters
				$inclusiveQuery = '';

				// there is a specific value required (in <value></value> tag)
				if (isset($filter->value))
				{
					$value = (string) $filter->value;
					if (!empty($value) || $value == '0')
					{
						$inclusiveQuery .= $db->quoteName('a.' . (string) $filter->column) . ' = ' . $db->Quote($value);
					}
				}

				// there is a list of specific values to include (in <include></include> tag)
				if (isset($filter->include))
				{
					$includes = (string) $filter->include;
					if (!empty($includes) || $includes == '0')
					{
						$includes = explode(',', $includes);
						$includesQuery = $db->quoteName('a.' . (string) $filter->column) . ' in (' . ShlDbHelper::arrayToQuotedList($includes) . ')';
						$inclusiveQuery = empty($inclusiveQuery) ? $includesQuery : '(' . $inclusiveQuery . ' or ' . $includesQuery . ')';
					}
				}

				// if any include or value tag was set in xml file, create the corresponding query
				if (!empty($inclusiveQuery))
				{
					$whereNonTranslated[] = ' ' . $inclusiveQuery;
					$whereTranslated[] = ' ' . $inclusiveQuery;
				}

				// now "exclusive" filter: there is a list of specific values to exclude (in <exclude></exclude> tag)
				if (isset($filter->exclude))
				{
					$excludes = (string) $filter->exclude;
					if (!empty($excludes) || $excludes == '0')
					{
						$excludes = explode(',', (string) $filter->exclude);
						$where = $db->quoteName('a.' . (string) $filter->column) . ' not in (' . ShlDbHelper::arrayToQuotedList($excludes) . ')';
						$whereNonTranslated[] = ' ' . $where;
						$whereTranslated[] = ' ' . $where;
					}
				}
			}
		}

		// filtering on target translation language value
		$translationLanguageFilter = $state->get('filter_translation_language');
		// filtering on completion value
		$translationStatusFilter = $state->get('filter_translation_status');
		switch ($translationStatusFilter)
		{
			case JosettaHelper::TRANSLATION_STATUS_ONLY_COMPLETED:
				$whereNonTranslated[] = ' ' . $db->quoteName('m.completion') . ' is not null';
				$whereTranslated[] = ' ' . $db->quoteName('m.completion') . ' = ' . 100;
				if (!empty($translationLanguageFilter))
				{
					$whereTranslated[] = ' ' . $db->quoteName('m.language') . ' = ' . $db->quote($translationLanguageFilter);
				}
				break;
			case JosettaHelper::TRANSLATION_STATUS_ONLY_UNCOMPLETED:
				$whereTranslated[] = ' ' . $db->quoteName('m.completion') . ' <> ' . 100;
				break;

			case JosettaHelper::TRANSLATION_STATUS_ONLY_STARTED:
				$whereTranslated[] = ' ' . $db->quoteName('m.completion') . ' = ' . 0;
				if (!empty($translationLanguageFilter))
				{
					$whereTranslated[] = ' ' . $db->quoteName('m.language') . ' = ' . $db->quote($translationLanguageFilter);
				}
				break;
			default:
				break;
		}

		// language filter used to build use existing content
		$filterLang = $state->get('filter_lang');
		if (!empty($filterLang))
		{
			// a specific language was requested: we are probably on the translate page, in a modal window
			$where = $db->quoteName('a.language') . ' = ' . $db->Quote($filterLang);
			$whereNonTranslated[] = ' ' . $where;
			$whereTranslated[] = ' ' . $where;
		}
		else
		{

			// no specific language requested, we are probably on the main translator page
			// show the default language for the site and the "all" language
			$languages = array();
			$languages[] = $db->Quote(JosettaHelper::getSiteDefaultLanguage());
			$languages[] = $db->Quote('*');
			$languagesList = implode(',', $languages);
			$where = $db->quoteName('a.language') . ' in (' . $languagesList . ')';
			$whereNonTranslated[] = ' ' . $where;
			$whereTranslated[] = ' ' . $where;
		}

		// make sure we select only untranslated items in the untranslated query
		$whereNonTranslated[] = ' ' . $db->quoteName('t.context') . ' is null';

		// build the statements
		if (!empty($whereNonTranslated))
		{
			$this->_queryNonTranslated .= ' where ' . implode($whereNonTranslated, ' and ');
		}
		if (!empty($whereTranslated))
		{
			$this->_queryTranslated .= ' where ' . implode($whereTranslated, ' and ');
		}

	}

	/**
	 *
	 * Process ordering filters
	 * turn them into a (part of a) query
	 *
	 * @param object $state
	 */
	protected function _buildOrderQuery($state)
	{

		$db = ShlDbHelper::getDb();

		$this->_query .= ' order by ' . $db->escape('josetta_ordering');

	}

	/**
	 *
	 * Find which table column to sort by
	 *
	 * @param object $state
	 */
	protected function _getOrderingColumn()
	{

		//Load the defintion of xml of plugin
		$definitions = $this->onJosettaGetDefinitions($this->_context);
		if (empty($definitions))
		{
			return null;
		}

		// look up references and search for the 'title' field
		foreach ($definitions->reference as $reference)
		{
			foreach ($reference as $name => $ref)
			{
				if ($name == 'title')
				{
					return (string) $ref->column;
				}
			}
		}

		return null;

	}

}
