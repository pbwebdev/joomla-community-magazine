<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

/**
 * Translator controller class.
 */
class JosettaadminController_Default extends ShlMvcController_Base
{
	public function display($cachable = false, $urlparams = false)
	{
		$app = JFactory::getApplication();

		// force cpanel view if missing
		$view = $app->input->getCmd('view');
		if (empty($view))
		{
			$app->input->set('view', 'cpanel');
		}

		return parent::display($cachable, $urlparams);
	}

	public function remove()
	{
		// Check for request forgeries
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$app = JFactory::getApplication();

		// Initialise variables.
		$importIds = $app->input->getVar('cid', array(), '', 'array');
		$results = array();

		if (!empty($importIds))
		{
			$josettaImportModel = ShlMvcModel_Base::getInstance('Jfimporter', 'JosettaadminModel_');
			foreach ($importIds as $importId)
			{
				$status = $josettaImportModel->deleteImportedData($importId);
				if ($status)
				{
					$results[] = JText::sprintf('COM_JOSETTA_IMPORT_IMPORTED_DATA_DELETED', $importId, JText::_('COM_JOSETTA_SUCCESS'));
					// update import status
					$josettaImportModel->setImportState(JosettaadminModel_Jfimporter::STATE_DELETED, $error = '', $importId);
				}
				else
				{
					$results[] = JText::sprintf('COM_JOSETTA_IMPORT_IMPORTED_DATA_DELETED', $importId,
						JText::_('COM_JOSETTA_FAILURE') . ' (' . $josettaImportModel->getError() . ')');
				}
			}

			// build result message
			$messageString = implode('</li><li>', $results);
			$app->enqueuemessage('<li>' . $messageString . '</li>');
		}

		$this->setRedirect(JRoute::_('index.php?option=com_josetta&view=imports', false));
	}

}
