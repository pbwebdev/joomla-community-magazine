<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

defined('_JEXEC') or die('');

/**
 * Joomla! Content translation Plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	josetta.contentitem
 */

class plgJosettaContentitem extends JosettaadminClass_Extensionplugin
{

	protected $_context = 'com_content_item';
	protected $_defaultTable = 'content';

	protected $_items = null;
	protected $_itemsCount = null;

	/**
	 * Load language files that may be need by the extension being
	 * translated
	 */
	public function loadLanguages()
	{
		parent::loadLanguages();

		$language = JFactory::getLanguage();
		//load the administrator english language of the component if language translation is not available
		$language->load('com_content', JPATH_ADMINISTRATOR, 'en-GB', true);
		//load the administrator default language of the component if language translation is not available
		$language->load('com_content', JPATH_ADMINISTRATOR, null, true);
	}

	/**
	 * Method to build the dropdown of josetta translator screen
	 *
	 * @return array
	 *
	 */
	public function onJosettaGetTypes()
	{
		//return the protected variable $_context and label of component
		$item = array($this->_context => JText::_('COM_JOSETTA_JOOMLA_ARTICLES'));
		$items[] = $item;
		return $items;
	}

	/**
	 * Method to load an item from the database
	 *
	 * @param string $context a context string, to either process or ignore an event
	 * @param int $id id of the element
	 *
	 * @return  object  an object representing the item, with no particular type
	 *
	 */
	public function onJosettaLoadItem($context, $id = '')
	{
		if ((!empty($context) && ($context != $this->_context)) || (empty($id)))
		{
			return;
		}

		//call the parent base class method to load the article raw data
		$article = parent::onJosettaLoadItem($context, $id);

		//add articletext field from existing data
		$article->articletext = trim($article->fulltext) != '' ? $article->introtext . "<hr id=\"system-readmore\" />" . $article->fulltext
			: $article->introtext;

		return $article;
	}

	/**
	 * Save an item after it has been translated
	 * This will be called by Josetta when a user clicks
	 * the Save button. The context is passed so
	 * that each plugin knows if it must process the data or not
	 *
	 * if $item->(id) is empty, this is
	 * a new item, otherwise we are updating the item
	 * The name of the (id) field may vary, but should already be set
	 * to the proper value before the data is sent to the plugin
	 * (by the translate model)
	 *
	 * $item->data contains the fields entered by the user
	 * that needs to be saved
	 *
	 *@param context type
	 *@param data in form of array
	 *
	 *@return integer table id if data is inserted
	 *
	 *@return boolean false if error occurs
	 *
	 */
	public function onJosettaSaveItem($context, $item, &$errors)
	{
		if (($context != $this->_context))
		{
			return;
		}

		// call parent to save item in the content table, based on
		// information stored in the plugin xml file
		$savedItemId = parent::onJosettaSaveItem($context, $item, $errors);

		// content items required a bit more work: handling of the 'featured'
		// feature. Item id needs also be stored in the #__content_frontpage table
		if (!empty($savedItemId))
		{
			$featuredSaved = $this->_featured($savedItemId, $item['featured']);
			// we don't act on $featuredSaved being false, as the rest of the item has
			// bee saved. However, an error message has been queued for display by _featured()
		}

		//return the id of table
		return $savedItemId;
	}

	/**
	 * Method to toggle the featured setting of articles.
	 *
	 * Based on same method in Joomla! content article model
	 * How I whish I could simply re-use them!
	 *
	 * @param	array	The ids of the items to toggle.
	 * @param	int		The value to toggle to.
	 *
	 * @return	boolean	True on success.
	 */
	protected function _featured($pks, $value = 0)
	{
		// Sanitize the ids.
		$pks = is_array($pks) ? $pks : (array) $pks;
		JArrayHelper::toInteger($pks);

		if (empty($pks))
		{
			return false;
		}

		// get a Joomla! #__content_frontpage table object
		JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_content/tables');
		$table = JTable::getInstance('Featured', 'ContentTable');

		try
		{
			$db = ShlDbHelper::getDb();

			if ((int) $value == 0)
			{
				// Adjust the mapping table.
				// Clear the existing featured setting
				$db
					->setQuery('DELETE FROM #__content_frontpage' . ' WHERE content_id IN (' . implode(',', $pks) . ')');
				$db->execute();
			}
			else
			{
				// first, we find out which of our new featured articles are already featured.
				$query = $db->getQuery(true);
				$query->select('f.content_id');
				$query->from('#__content_frontpage AS f');
				$query->where('content_id IN (' . implode(',', $pks) . ')');
				$db->setQuery($query);

				$old_featured = $db->shlLoadColumn();

				// we diff the arrays to get a list of the articles that are newly featured
				$new_featured = array_diff($pks, $old_featured);

				// Featuring.
				$tuples = array();
				foreach ($new_featured as $pk)
				{
					$tuples[] = '(' . $pk . ', 0)';
				}
				if (count($tuples))
				{
					$db
						->setQuery(
							'INSERT INTO #__content_frontpage (' . $db->quoteName('content_id') . ', ' . $db->quoteName('ordering') . ')'
								. ' VALUES ' . implode(',', $tuples));
					$db->execute();
				}
			}

		}
		catch (Exception $e)
		{
			JosettaHelper::enqueueMessages($e->getMessage(), 'error');
			return false;
		}

		$table->reorder();

		// TODO: should clear cache?
		// probably not, better let admin do that
		// unless we check publication and completion status
		// and even that is not foolproof
		// $this->cleanCache();

		return true;
	}

}
