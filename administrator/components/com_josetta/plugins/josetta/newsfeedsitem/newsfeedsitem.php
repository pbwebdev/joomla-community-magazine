<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

defined('_JEXEC') or die('');

/**
 * Joomla! newsfeeds translation Plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	josetta.Newsfeedsitem
 */

class plgJosettaNewsfeedsItem extends JosettaadminClass_Extensionplugin
{
	protected $_context = 'com_newsfeeds_item';
	protected $_defaultTable = 'Newsfeed';

	/**
	 * Load language files that may be need by the extension being
	 * translated
	 */
	public function loadLanguages()
	{
		parent::loadLanguages();

		$language = JFactory::getLanguage();
		//load the administrator english language of the component if language translation is not available
		$language->load('com_newsfeeds', JPATH_ADMINISTRATOR, 'en-GB', true);
		//load the administrator default language of the component if language translation is not available
		$language->load('com_newsfeeds', JPATH_ADMINISTRATOR, null, true);
	}

	/**
	 * Method to build the dropdown of josetta translator screen
	 *
	 * @return array
	 *
	 */
	public function onJosettaGetTypes()
	{
		//return the protected variable $_context and label of component
		$item = array($this->_context => JText::_('COM_JOSETTA_PLUGIN_NEWSFEEDS_TITLE'));
		$items[] = $item;
		return $items;
	}

	/**
	 * Instantiate table object to read/save item
	 *
	 */
	protected function _getTable()
	{
		JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_newsfeeds/tables');
		$table = JTable::getInstance($this->_defaultTable, 'NewsfeedsTable');

		return $table;
	}

}
