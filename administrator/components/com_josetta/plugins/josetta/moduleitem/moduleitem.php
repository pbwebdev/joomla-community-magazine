<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

defined('_JEXEC') or die('');

/**
 * Joomla! Module translation Plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	josetta.moduleitem
 */

class plgJosettaModuleitem extends JosettaadminClass_Extensionplugin
{
	protected $_context = 'module_item';
	protected $_defaultTable = 'module';

	/**
	 * Load language files that may be need by the extension being
	 * translated
	 */
	public function loadLanguages()
	{
		parent::loadLanguages();

		$language = JFactory::getLanguage();
		//load the administrator english language of the component if language translation is not available
		$language->load('com_modules', JPATH_ADMINISTRATOR);
		$language->load('com_categories', JPATH_ADMINISTRATOR);

		$language->load('mod_login');
		$language->load('mod_articles_category');
		$language->load('mod_articles_news');
		$language->load('mod_articles_popular');
		$language->load('mod_breadcrumbs');
		$language->load('mod_feed');
		$language->load('mod_random_image');
		$language->load('mod_search');
		$language->load('mod_finder');
		$language->load('mod_syndicate');
		$language->load('mod_weblinks');
		$language->load('mod_wrapper');
		$language->load('mod_banners');
		$language->load('mod_menu');
	}

	/**
	 * Method to build the dropdown of josetta translator screen
	 *
	 * @return array
	 *
	 */
	public function onJosettaGetTypes()
	{
		//return the protected variable $_context and label of component
		$item = array($this->_context => JText::_('COM_JOSETTA_PLUGIN_MODULES_TITLE'));
		$items[] = $item;
		return $items;
	}

	/**
	 *
	 * @see JosettaadminClass_Extensionplugin::onJosettaLoadItem()
	 */
	public function onJosettaLoadItem($context, $id = '')
	{
		if ((!empty($context) && ($context != $this->_context)) || (empty($id)))
		{
			return null;
		}

		$item = parent::onJosettaLoadItem($context, $id);

		// Determine the page assignment mode.
		try
		{
			$item->assigned_data = ShlDbHelper::selectResultArray('#__modules_menu', 'menuid', array('moduleid' => $id));
			if (empty($item->assigned_data))
			{
				// For an existing module it is assigned to none.
				$item->assignment = '-';
				$item->assigned_data = '';
			}
			else
			{
				if ($item->assigned_data[0] > 0)
				{
					$item->assignment = +1;
				}
				elseif ($item->assigned_data[0] < 0)
				{
					$item->assignment = -1;
				}
				else
				{
					$item->assignment = 0;
				}
				$item->assigned_data = serialize($item->assigned_data);
			}
		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			$item->assignment = '-';
			$item->assigned_data = '';
		}
		return $item;
	}

	public function onJosettaSaveItem($context, $item, &$errors)
	{
		if (($context != $this->_context))
		{
			return;
		}

		$errors = array();

		// pre-process params field, encoding its content to json
		$item = $this->_saveItemProcessParams($item, $context);

		// load languages for form and error messages
		$this->loadLanguages();

		// use J! model to save
		require_once JPATH_ADMINISTRATOR . '/components/com_modules/models/module.php';

		$moduleModel = new ModulesModelModule;
		$saved = $moduleModel->save($item);
		$moduleId = 0;
		if ($saved)
		{
			// note: we cannot just use $categoryModel->getState( 'category.id') to get
			// back the saved id, because when doing getState(), the populateState()
			// method is called first, and it erases the stored category id
			// as it uses $app->input->getInt( 'id') to initialize the 'category.id' state
			$state = $moduleModel->get('state');
			$moduleId = $state->get('module.id');
		}
		else
		{
			$errors = $moduleModel->getErrors();
		}

		//return the id of table
		return $moduleId;
	}
}
