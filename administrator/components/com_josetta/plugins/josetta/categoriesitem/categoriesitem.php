<?php
/**


 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

defined('_JEXEC') or die('');

jimport('joomla.application.categories');

/**
 * Joomla! Categories translation Plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	josetta.categoriesitem
 */

class plgJosettaCategoriesitem extends JosettaadminClass_Extensionplugin
{
	protected $_context = 'categories_item';
	protected $_defaultTable = 'category';

	/**
	 * Load language files that may be need by the extension being
	 * translated
	 */
	public function loadLanguages()
	{
		parent::loadLanguages();

		$language = JFactory::getLanguage();
		//load the administrator english language of the component if language translation is not available
		$language->load('com_categories', JPATH_ADMINISTRATOR, 'en-GB', true);
		//load the administrator default language of the component if language translation is not available
		$language->load('com_categories', JPATH_ADMINISTRATOR, null, true);
	}

	/**
	 * Method to build the dropdown of josetta translator screen
	 *
	 * @return array
	 *
	 */
	public function onJosettaGetTypes()
	{
		//return the protected variable $_context and label of component
		$item = array($this->_context => JText::_('COM_JOSETTA_CATEGORIES'));
		$items[] = $item;
		return $items;
	}

	/**
	 *
	 * Compute the subtitle of a provided item
	 * Subtitle is used for display to the user of an item
	 * to provide more context
	 * By default, we use an item category
	 *
	 * @param string $context
	 * @param mixed $item
	 */
	public function onJosettaGetSubtitle($context, $item)
	{
		if (!empty($context) && ($context != $this->_context))
		{
			return;
		}

		$subTitle = '';
		if (empty($item))
		{
			return $subTitle;
		}

		if (!empty($item->parent_id) && !is_array($item->parent_id))
		{
			$category = JosettaHelper::getCategoryDetails($item->parent_id);
			$subTitle = empty($category) ? '' : $category->title;
		}

		return $subTitle;
	}

	/**
	 *
	 * Find any filters that should apply to the modal popup
	 * used to select an existing content
	 *
	 * @param string $context
	 * @param mixed $item
	 * @return array of stdClass
	 */
	public function onJosettaGetUseExistingFilters($context, $item)
	{
		if (!empty($context) && ($context != $this->_context))
		{
			return;
		}

		$filters = array();
		if (empty($item))
		{
			return $filters;
		}

		// when translating a category with an existing
		// category, they should be both for the same
		// extension
		$filter = new stdClass();
		$filter->name = $context . '_filter_extension';
		$filter->value = empty($item->extension) ? '' : $item->extension;
		$filters[] = $filter;

		// prevent user from changing filter_extension
		$filter = new stdClass();
		$filter->name = 'disable_' . $context . '_filter_extension';
		$filter->value = 1;
		$filters[] = $filter;

		return $filters;
	}

	/**
	 * Save an item after it has been translated
	 * This will be called by Josetta when a user clicks
	 * the Save button. The context is passed so
	 * that each plugin knows if it must process the data or not
	 *
	 * if $item->(id) is empty, this is
	 * a new item, otherwise we are updating the item
	 * The name of the (id) field may vary, but should already be set
	 * to the proper value before the data is sent to the plugin
	 * (by the translate model)
	 *
	 * $item->data contains the fields entered by the user
	 * that needs to be saved
	 *
	 *@param context type
	 *@param data in form of array
	 *
	 *return table id if data is inserted
	 *
	 *return false if error occurs
	 *
	 */
	public function onJosettaSaveItem($context, $item, &$errors)
	{
		if (($context != $this->_context))
		{
			return;
		}

		$errors = array();

		// model needs associations in the data, we must add it
		if(version_compare(JVERSION, '3', 'ge'))
		{
		    require_once JPATH_ADMINISTRATOR . '/components/com_categories/helpers/association.php';
		    $item['associations'] = CategoriesHelper::getAssociations($item['id'], 'com_categories');
		    if(empty($item['associations']) && !empty($item['ref_lang']))
		    {
		    	// create default associations
		    	$item['associations'] = array($item['ref_lang'] => $item['ref_id'], $item['language'] => $item['id']);
		    }
		}
		
		// use J! model to save
		require_once JPATH_ADMINISTRATOR . '/components/com_categories/models/category.php';
		JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_categories/tables');

		// load languages for form and error messages
		$this->loadLanguages();

		// now actually save
		$categoryModel = new CategoriesModelCategory;
		$saved = $categoryModel->save($item);
		$categoryId = 0;
		if ($saved)
		{
			// note: we cannot just use $categoryModel->getState( 'category.id') to get
			// back the saved id, because when doing getState(), the populateState()
			// method is called first, and it erases the stored category id
			// as it uses $app->input->getInt( 'id') to initialize the 'category.id' state
			$state = $categoryModel->get('state');
			$categoryId = $state->get('category.id');
		}
		else
		{
			$errors = $categoryModel->getErrors();
		}

		//return the id of table
		return $categoryId;
	}

	/**
	 *
	 * Find which table column to sort by
	 *
	 * @param object $state
	 */
	protected function _getOrderingColumn()
	{
		return 'lft';

	}

}
