<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// Security check to ensure this file is being included by a parent file.
defined( '_JEXEC' ) or die;

/**
 * Installation/Uninstallation script
 *
 */

class plgSystemJosettaInstallerScript {


  public function install($parent) {
  }

  public function uninstall($parent) {

  }

  public function update($parent) {
  }

  public function preflight($type, $parent) {
  }

  public function postflight($type, $parent) {

    // enable ourselves
    $db = JFactory::getDbo();
    $query = $db->getQuery( true);
    
    $query->update( '#__extensions');
    $query->where( $db->quoteName( 'type') . '=' . $db->Quote( 'plugin'));
    $query->where( $db->quoteName( 'element') . '=' . $db->Quote( 'josetta'));
    $query->where( $db->quoteName( 'folder') . '=' . $db->Quote( 'system'));
    $query->set( $db->quoteName( 'enabled') . '=' . $db->Quote( 1));
    
    $db->setQuery( $query);
    $db->execute();
  }

}