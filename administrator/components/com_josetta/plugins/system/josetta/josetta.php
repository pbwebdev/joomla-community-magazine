<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');
jimport('joomla.filesystem.file');

/**
 * Josetta system plugin
 *
 * @author
 */
class plgSystemJosetta extends JPlugin
{

	public function onAfterInitialise()
	{
		// load b/c classes
		if (JFile::exists(JPATH_ADMINISTRATOR . '/components/com_josetta/classes/compat.php'))
		{
			include_once JPATH_ADMINISTRATOR . '/components/com_josetta/classes/compat.php';
		}

		// default path to layouts
		define('JOSETTA_LAYOUTS_PATH', JPATH_ROOT . '/components/com_josetta/layouts');

		// versionning  full | free
		define('JOSETTA_VERSION_TYPE', 'free');
	}

	/**
	 * Force Josetta template, regardless of menu item
	 */
	public function onAfterRoute()
	{
		$app = JFactory::getApplication();
		if($app->isSite())
		{
			$option = $app->input->getCmd('option');
			if ('com_josetta' == $option)
			{
				$app->setTemplate('atomic_josetta');
			}
		}
	}
}
