/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   weeblr, llc (c) 2014
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.2.1.567
 * @date		2014-01-16
 */

function shStopEvent(event) {

  // cancel the event
  new Event(event).stop();

}

function shProcessToolbarClick(id, pressbutton) {

  if (pressbutton != 'cancel') {
    var el = document.getElementById(id);
    if (typeof this.baseurl == 'undefined') {
      this.baseurl = [];
    }
    if (typeof this.baseurl[pressbutton] == 'undefined') {
      this.baseurl[pressbutton] = el.href;
    }
    var url = baseurl[pressbutton];
    var cid = document.getElementsByName('cid[]');
    var list = '';
    if (cid) {
      var length = cid.length;
      for ( var i = 0; i < length; i++) {
        if (cid[i].checked) {
          list += '&cid[]=' + cid[i].value;
        }
      }
    }
    url += list;
    el.href = url;
    window.parent.SqueezeBox.fromElement(el, {parse:'rel'});
  }

  return false;
}
