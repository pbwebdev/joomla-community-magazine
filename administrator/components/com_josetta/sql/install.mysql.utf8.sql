
CREATE TABLE IF NOT EXISTS `#__josetta_associations` (
  `id` VARCHAR(50) NOT NULL COMMENT 'A reference to the associated item.',
  `context` VARCHAR(50) NOT NULL COMMENT 'The context of the josetta associated item.',
  `key` CHAR(32) NOT NULL COMMENT 'The key for the josetta association computed from an md5 on associated ids.',
  `language` char(7) NOT NULL,
  PRIMARY KEY `idx_context_id` (`context`, `id`),
  INDEX `idx_key` (`key`),
  INDEX `idx_language` (`language`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__josetta_metadata` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`context` varchar(20) NOT NULL,
`item_id` int(11) NOT NULL,
`completion` int(11) NOT NULL,
`language` char(7) NOT NULL,
PRIMARY KEY (`id`),
INDEX `idx_language` (`language`)
) DEFAULT CHARSET=utf8 ;
