<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   weeblr, llc (c) 2014
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.2.1.567
 * @date		2014-01-16
 */

// Security check to ensure this file is being included by a parent file.
defined('_JEXEC') or die;

// add/update a Josetta User group
// 1 - check if not already existing
$db = JFactory::getDbo();
$query = $db->getQuery(true);
$query->select('id');
$query->from('#__usergroups');
$query->where($db->quoteName('title') . '=' . $db->quote('Josetta'));
$db->setQuery($query);
$groupId = $db->loadResult();
// 2 - if not existing, add a user group
if (empty($groupId))
{
	include_once JPATH_ADMINISTRATOR . '/components/com_users/models/group.php';
	$josettaGroup = array('title' => 'Josetta', 'parent_id' => 0, 'id' => 0);
	$model = new UsersModelGroup();
	$saved = $model->save($josettaGroup);

	// need to check errors and display message
	if (!$saved)
	{
		echo 'Error creating Josetta user group: ' . $model->getError() . '<br />';
	}

	// get the written group id
	$groupId = $model->getState('group.id');

	if (empty($groupId))
	{
		echo 'Error reading just created Josetta user group<br />';
	}

}

// add a sub group per language
$languages = JLanguageHelper::getLanguages();
// drop default language
jimport('joomla.application.component.helper');
$params = JComponentHelper::getParams('com_languages');
jimport('joomla.application.helper');
$client = JApplicationHelper::getClientInfo(0);
$defaultLanguage = $params->get($client->name, 'en-GB');
foreach ($languages as $language)
{
	if ($language->lang_code != $defaultLanguage)
	{
		// we collect all languages except the default one
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from('#__usergroups');
		$query->where($db->quoteName('title') . '=' . $db->quote($language->lang_code));
		$query->where($db->quoteName('parent_id') . '=' . $groupId);
		$db->setQuery($query);
		$langGroupId = $db->loadResult();
		if (empty($langGroupId))
		{
			// no sub group for this language, create it
			include_once JPATH_ADMINISTRATOR . '/components/com_users/models/group.php';
			$langGroup = array('title' => $language->lang_code, 'parent_id' => $groupId, 'id' => 0);
			$model = new UsersModelGroup();
			$saved = $model->save($langGroup);

			// need to check errors and display message
			if (!$saved)
			{
				echo 'Error creating Josetta ' . $language->title . ' user group: ' . $model->getError() . '<br />';
			}
		}
	}
}

// add/update a Josetta access level
// 1 - check if not already existing
$query = $db->getQuery(true);
$query->select('id');
$query->from('#__viewlevels');
$query->where($db->quoteName('title') . '=' . $db->quote('Josetta'));
$db->setQuery($query);
$levelId = $db->loadResult();

$query->clear();
$query->select('max(ordering) as maximum');
$query->from('#__viewlevels');
$db->setQuery($query);
$maxOrdering = $db->loadResult();

// 2 - if not existing, add a user group
if (!empty($groupId) && empty($levelId))
{

	include_once JPATH_ADMINISTRATOR . '/components/com_users/models/level.php';
	$josettaRules = '[' . $groupId . ']';
	// now save view level
	$josettaLevel = array('title' => 'Josetta', 'rules' => $josettaRules, 'ordering' => $maxOrdering + 1);
	$model = new UsersModelLevel();
	$saved = $model->save($josettaLevel);
	$levelId = $model->getState('level.id');

	// need to check errors and display message
	if (!$saved)
	{
		echo 'Error creating Josetta access level: ' . $model->getError() . '<br />';
	}
}
