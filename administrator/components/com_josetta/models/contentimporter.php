<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   weeblr, llc (c) 2014
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.2.1.567
 * @date		2014-01-16
 */


// no direct access
defined('_JEXEC') or die;

jimport( 'joomla.application.component.modeladmin');
jimport( 'joomla.html.parameter');

/**
 * @package		Josetta
 */

class JosettaadminModel_Contentimporter extends JosettaadminClass_Baseimporter {

  protected $_pkName = 'id';
  protected $_context = 'com_content_item';
  protected $_journalTable = null;
  protected $_originalLanguageItemId = 0;
  protected $_journalRecord = null;
  protected $_importId = 0;


  protected function _importOriginal( $item, $language) {

    // apply various fixes/updates to the menu item
    $item = $this->_convertItem($item, $language);

    // now actually save
    $articleModel = new JosettaadminModel_Content();
    $saved = $articleModel->save( (array) $item);

    // note: we cannot just use $categoryModel->getState( 'category.id') to get
    // back the saved id, because when doing getState(), the populateState()
    // method is called first, and it erases the stored category id
    // as it uses JRequest::getInt( 'id') to initialize the 'category.id' state
    $state = $articleModel->get( 'state');
    $articleId = $state->get( 'content.id');

    $errors = $articleModel->getErrors();
    if(!empty( $errors)) {
      foreach( $errors as $error) {
        $this->setError( $error);
      }
      return 0;
    }

    //return the id of table
    return $articleId;

  }

  /**
   * Performs whatever fix is needed to an imported
   * menu item
   *
   * @param object $journalRecord a record from the import journal table
   * @param integer $importId current import run id, for logging purpose
   * @return boolean true on success, error message stored in this model
   * @throw Exception
   */
  public function postProcessImportedItem( $journalRecord, $importId) {

    $processed = true;
    $this->_journalRecord = $journalRecord;
    $this->_importId = $importId;

    // load menu item
    $item = ShlDbHelper::selectObject( '#__content', '*', array( 'id' => $journalRecord->created_id));

    // quick check in case there's nothing to do
    $articleText = trim( $item->introtext) . trim( $item->fulltext);
    if(empty($articleText)) {
      return $processed;
    }

    // now extract all links in the article text, and update them with new ids and format
    $regex  = '#href="index.php\?([^"]*)#mu';
    $item->introtext = preg_replace_callback( $regex, array( 'JosettaadminModel_Contentimporter', '_processLink'), $item->introtext);
    $this->_checkPregError( $item->introtext);
    $item->fulltext = preg_replace_callback( $regex, array( 'JosettaadminModel_Contentimporter', '_processLink'), $item->fulltext);
    $this->_checkPregError( $item->fulltext);

    // any change?
    $processedArticleText = trim( $item->introtext) . trim( $item->fulltext);

    // has anything changed?
    $modified = $processedArticleText != $articleText;

    // write back to db if modified
    if($modified) {
      ShlDbHelper::update( '#__content', array( 'introtext' => $item->introtext, 'fulltext' => $item->fulltext), array( 'id' => $journalRecord->created_id));
    }

    return $processed;
  }

  protected function _checkPregError( $buffer) {

    if ($buffer === null) {
      switch (preg_last_error()) {
        case PREG_BACKTRACK_LIMIT_ERROR:
          $message = "PHP regular expression limit reached (pcre.backtrack_limit)";
          break;
        case PREG_RECURSION_LIMIT_ERROR:
          $message = "PHP regular expression limit reached (pcre.recursion_limit)";
          break;
        case PREG_BAD_UTF8_ERROR:
          $message = "Bad UTF8 passed to PCRE function";
          break;
        default:
          $message = "Unknown PCRE error calling PCRE function";
      }
      throw new Exception( $message);
    }

  }

  public function _processLink( & $matches) {

    $original	= $matches[0];
    $url = $matches[1];
    $url = 'index.php?' . str_replace('&amp;', '&', $url);

    // first step, adjust format of non-sef url
    $formatteddUrl	= JosettaadminHelper_Links::updateLinksFormat( $url);
    if($url !== $formatteddUrl) {
      ShlSystem_Log::info( 'josetta', 'Run #' . $this->_importId . ', changed internal link from ' . $url . ' to ' . $formatteddUrl);
    }

    // second, adjust ids of items linked to
    $fixedUrl = JosettaadminHelper_Links::updateContentLinkIds( $formatteddUrl, $this->_journalRecord->imported_context
        , $this->_journalRecord->created_id, 'N/A', $this->_journalRecord->language, $this->_importId);
    if($fixedUrl !== $formatteddUrl) {
      ShlSystem_Log::info( 'josetta', 'Run #' . $this->_importId . ', changed ids in internal link from ' . $formatteddUrl . ' to ' . $fixedUrl);
    }

    // prepare for return
    $fixedUrl = 'href="' . $fixedUrl;

    return $fixedUrl;

  }

  protected function _importTranslation( $item, $language) {

    $itemId = parent::_importTranslation($item, $language);

    if((int)$itemId > 0) {
      // bad fix: data stored as params are hard to pass to the Josetta model
      // so we just write them directly into the db record
      ShlDbHelper::update( '#__content', array( 'attribs' => $item->attribs), array( 'id' => $itemId));
    }

    return $itemId;

  }

  /**
   * Based on Jupgrade:
   *
   * jUpgrade
   *
   * @version		  $Id$
   * @package		  MatWare
   * @subpackage	com_jupgrade
   * @author      Matias Aguirre <maguirre@matware.com.ar>
   * @link        http://www.matware.com.ar
   * @copyright		Copyright 2006 - 2011 Matias Aguirre. All rights reserved.
   * @license		  GNU General Public License version 2 or later; see LICENSE.txt
   *
   * @param object $item
   */
  protected function _convertItem( $item, $language) {

    $pkName = $this->_pkName;

    // Sections are categories now
    $item->extension = 'com_content';

    // Fixing access
    $item->access++;
    // Fixing language
    $item->language = $language;

    // Correct state
    if ($item->state == -1) {
      $item->state = 2;
    }

    /*
     * Prevent JGLOBAL_ARTICLE_MUST_HAVE_TEXT error
    */
    if (trim($item->introtext) == '' && trim($item->fulltext) == '') {
      $item->introtext = '---';
    }

    // prevent errors when article text has not been translated
    $trimmedTitle = trim($item->title);
    if (empty($trimmedTitle)) {
      // use title of default language instead
      $item->title = $this->_currentItemGroup[JosettaHelper::getSiteDefaultLanguage()]->title;
      ShlSystem_Log::info( 'josetta', 'Run #' . $this->_importId . ', content item is missing title, using original language title for ' . $language . ': ' . $item->title);
    }

    // update the 'featured' column, based on data from the content_frontpage import
    $featured = ShlDbHelper::count( '#__josetta_jf_content_frontpage', 'jta_id', array( 'content_id' => $item->$pkName));
    $item->featured = empty( $featured) ? 0 : 1;

    // Fixing catid
    // Find under which id this article category was imported
    //  and use that as a parent id
    $previousCatid = $item->catid;
    if(empty( $item->section) && empty( $item->catid)) {
      // this is an uncategorized item. Try fetch the current language uncategorized cat, creating it
      // if it doesn't exist
      $item->catid = JosettaadminHelper::getImportUncategorizedCategoryId( $item->jta_import_id, 'com_content', $language);
    } else {
      // regular categorized item, try fetch the category as it was imported
      $item->catid = ShlDbHelper::selectResult( '#__josetta_jf_journal', 'created_id'
          , array('imported_context' => 'categories', 'imported_id' => $previousCatid, 'language' => $language));

      if(empty( $item->catid)) {

        // no luck! two options: either that category has not been translated in the original site, in such case we'll create it
        // or this was an uncategorized item. In the latter case, we simply create an "Uncategorised" category

        // we'll create one if so
        ShlSystem_Log::info( 'josetta', 'Run #' . $item->jta_import_id . ', we have a translated article (' . $item->$pkName . ') but belonging to category (' . $previousCatid . ') that has not been translated. Creating a category on the fly');
        $newCat = array();
        $newCat['parent_id'] = JosettaadminHelper::getImportRootCategoryId( $item->jta_import_id, 'com_content', $language);
        $newCat['title'] = JText::sprintf( 'COM_JOSETTA_IMPORT_AUTO_CREATED_CATEGORY', $item->jta_import_id, $language);
        $newCat['extension'] = 'com_content';
        $newCat['description'] = JText::sprintf( 'COM_JOSETTA_IMPORT_AUTO_CREATED_CATEGORY_DESC', $item->title, $item->$pkName, $previousCatid, $item->jta_import_id);
        $newCat['published'] = 1;
        $newCat['language'] = $language;

        // use J! model to save a new category
        require_once JPATH_ADMINISTRATOR.'/components/com_categories/models/category.php';
        JTable::addIncludePath( JPATH_ADMINISTRATOR.'/components/com_categories/tables');

        // now actually save
        $categoryModel = new CategoriesModelCategory;
        $saved = $categoryModel->save( $newCat);
        if($saved) {
          $state = $categoryModel->get( 'state');
          $item->catid = $state->get( 'category.id');
          // write that to journal, using a separate object
          $journalTable = new JosettaadminTable_Jfjournal( JFactory::getDbo());
          $journalTable->import_id = $item->jta_import_id;
          $journalTable->imported_context = 'categories';
          $journalTable->imported_id = $previousCatid;
          $journalTable->created_context = 'categories_item';
          $journalTable->created_id = $item->catid;
          $journalTable->language = $language;
          $saved = $journalTable->store();
          if(!$saved) {
            ShlSystem_Log::error( 'josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $journalTable->getError());
          }

        } else {

          // not able to create a category, insert in import root category
          $item->parent_id = JosettaadminHelper::getImportRootCategoryId( $item->jta_import_id, 'com_content', $language);
          ShlSystem_Log::error( 'josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $categoryModel->getError());
        }
      }
    }


    // Converting params to JSON
    $item->attribs = $this->_convertParams( $item->attribs);

    // The Joomla 1.6 database structure does not allow duplicate aliases
    if(empty( $item->alias)) {
      $item->alias = JApplication::stringURLSafe( $item->title);
    }
    $count = 0;
    do {
      $alreadyInUse = ShlDbHelper::count( '#__content', '*', array( 'alias' => $item->alias));
      if(!empty($alreadyInUse)) {
        if($count > 0) {
          $item->alias .= '-' . $count;
        } else {
          $item->alias = JApplication::stringURLSafe( $item->alias . '-' . $language . '-' . $item->jta_import_id);
        }
        $count++;
      }
    } while ($alreadyInUse > 0 and $count < 10);


    // kill the id, we always want to create a new item
    $item->$pkName = 0;

    return $item;
  }


  /**
   * jUpgrade
   *
   * @version		  $Id$
   * @package		  MatWare
   * @subpackage	com_jupgrade
   * @author      Matias Aguirre <maguirre@matware.com.ar>
   * @link        http://www.matware.com.ar
   * @copyright		Copyright 2006 - 2011 Matias Aguirre. All rights reserved.
   * @license		  GNU General Public License version 2 or later; see LICENSE.txt
   */

  /**
   * Converts the params fields into a JSON string.
   *
   * @param	string	$params	The source text definition for the parameter field.
   *
   * @return	string	A JSON encoded string representation of the parameters.
   * @since	0.4.
   * @throws	Exception from the convertParamsHook.
   */
  protected function _convertParams($params)
  {
    $temp	= new JRegistry($params);
    $object	= $temp->toObject();

    // Fire the hook in case this parameter field needs modification.
    $this->convertParamsHook($object);

    return json_encode($object);
  }

  /**
   * A hook to be able to modify params prior as they are converted to JSON.
   *
   * @param	object	$object	A reference to the parameters as an object.
   *
   * @return	void
   * @since	0.4.
   * @throws	Exception
   */
  protected function convertParamsHook( &$object)
  {
    $object->show_parent_category = isset($object->show_parent_category) ? $object->show_parent_category : "";
    $object->link_parent_category = isset($object->link_parent_category) ? $object->link_parent_category : "";
    $object->link_author = isset($object->link_author) ? $object->link_author : "";
    $object->show_publish_date = isset($object->show_publish_date) ? $object->show_publish_date : "";
    $object->show_item_navigation = isset($object->show_item_navigation) ? $object->show_item_navigation : "";
    $object->show_icons = isset($object->show_icons) ? $object->show_icons : "";
    $object->show_vote = isset($object->show_vote) ? $object->show_vote : "";
    $object->show_hits = isset($object->show_hits) ? $object->show_hits : "";
    $object->show_noauth = isset($object->show_noauth) ? $object->show_noauth : "";
    $object->alternative_readmore = isset($object->alternative_readmore) ? $object->alternative_readmore : "";
    $object->article_layout = isset($object->article_layout) ? $object->article_layout : "";
    $object->show_publishing_options = isset($object->show_publishing_options) ? $object->show_publishing_options : "";
    $object->show_article_options = isset($object->show_article_options) ? $object->show_article_options : "";
    $object->show_urls_images_backend = isset($object->show_urls_images_backend) ? $object->show_urls_images_backend : "";
    $object->show_urls_images_frontend = isset($object->show_urls_images_frontend) ? $object->show_urls_images_frontend : "";
  }

  /**
   * Not used, but need to implement as getForm() is declared
   * abstract in parent classes
   *
   * (non-PHPdoc)
   * @see JModelForm::getForm()
   */
  public function getForm($data = array(), $loadData = true) {

  }

}
