<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   weeblr, llc (c) 2014
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.2.1.567
 * @date		2014-01-16
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of imports records.
 *
 * @package		Josetta
 */
class JosettaadminModelImports extends ShlMvcModel_List {

  /**
   * Build an SQL query to load the list data.
   *
   * @return	JDatabaseQuery
   */
  protected function getListQuery()
  {
    // Create a new query object.
    $db = ShlDbHelper::getDb();
    $query	= $db->getQuery(true);
    $user	= JFactory::getUser();

    // Select the required fields from the table.
    $query->select( '*')->from('#__josetta_jf_imports as a');

    // Add the list ordering clause.
    $query->order( $db->escape( 'a.created DESC'));

    return $query;
  }
}
