<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   weeblr, llc (c) 2014
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.2.1.567
 * @date		2014-01-16
 */


// no direct access
defined('_JEXEC') or die;

jimport( 'joomla.application.component.modeladmin');
jimport( 'joomla.html.parameter');

/**
 * @package		Josetta
 */

class JosettaadminModel_Menuitemsimporter extends JosettaadminClass_Baseimporter {

  protected $_pkName = 'id';
  protected $_context = 'com_menus_item';

  protected function _importOriginal( $item, $language) {

    // apply various fixes/updates to the menu item
    $item = $this->_convertItem($item, $language);

    // use Joomla model to save the item
    // now we can create the menu item. We can't use directly Joomla item model
    // has it has an hardcoded JPATH_COMPONENT require_once that fails if used
    // from another extension than com_menus
    $model = new JosettaadminmenusModel_Item();
    $saved = $model->save( (array) $item);
    $state = $model->get( 'state');
    $menuItemId = $state->get( 'item.id');

    $errors = $model->getErrors();
    if(!empty( $errors)) {
      foreach( $errors as $error) {
        $this->setError( $error . ' (menu item #' . $item->id . ' - ' . $language . ')');
      }
      return 0;
    }

    // return id of the saved item
    return $menuItemId;
  }

  /**
   * Performs whatever fix is needed to an imported
   * menu item
   *
   * @param object $journalRecord a record from the import journal table
   * @param integer $importId current import run id, for logging purpose
   * @return boolean true on success, error message stored in this model
   * @throw Exception
   */
  public function postProcessImportedItem( $journalRecord, $importId) {

    $processed = true;

    // load menu item
    $item = ShlDbHelper::selectObject( '#__menu', '*', array( 'id' => $journalRecord->created_id));

    // we need a link to fix it, and one that's an internal link
    if(empty($item->link) || substr( $item->link, 0, 10) != 'index.php?') {
      return $processed;
    }

    // need to save flag
    $modified = false;

    $processedLink = JosettaadminHelper_Links::updateContentLinkIds( $item->link, $journalRecord->imported_context, $item->id, $item->title, $item->language, $importId);
    $modified = $item->link != $processedLink;
    if($modified) {
      $item->link = $processedLink;
      // write back
      ShlDbHelper::update( '#__menu', array( 'link' => $item->link), array( 'id' => $journalRecord->created_id));
    }

    return $processed;
  }

  protected function _importTranslation( $item, $language) {

    // sanity check: many translations appear to be partial. Just don't import
    if(empty( $item->link)) {
      $this->setError( JText::_( 'COM_JOSETTA_IMPORT_ERROR_MENU_ITEM_MALFORMED'));
      return 0;
    }

    // nuke home flag
    // when importing a joomfish translation, home flag will be set if original language
    // is home. However, menutype will also be the same as original language item.
    // Joomla won't accept that, as it only wants one home page per menu
    // for non-default languages, we kill the home flag
    $item->home = 0;

    // store translation
    $itemId = parent::_importTranslation( $item, $language);

    if((int)$itemId > 0) {
      // bad fix: data stored as params are hard to pass to the Josetta model
      // so we just write them directly into the db record
      ShlDbHelper::update( '#__menu', array( 'params' => $item->params), array( 'id' => $itemId));
    }

    return $itemId;

  }

  /**
   * Based on Jupgrade:
   *
   * jUpgrade
   *
   * @version		  $Id$
   * @package		  MatWare
   * @subpackage	com_jupgrade
   * @author      Matias Aguirre <maguirre@matware.com.ar>
   * @link        http://www.matware.com.ar
   * @copyright		Copyright 2006 - 2011 Matias Aguirre. All rights reserved.
   * @license		  GNU General Public License version 2 or later; see LICENSE.txt
   *
   * @param object $item
   */
  protected function _convertItem( $item, $language) {

    static $_extensions = null;

    if(is_null( $_extensions)) {
      $_extensions = ShlDbHelper::selectObjectList( '#__extensions', array('extension_id', 'element'), array( 'type' => 'component'),
          $aWhereData = array(), $orderBy = array(), $offset = 0, $lines = 0, $key = 'element');
    }

    // Fixing access
    $item->access++;
    // Fixing level : does not seem to exist??
    // $item->level++;
    // Fixing language
    $item->language = $language;
    // Fixing parent_id
    if (empty($item->parent_id)) {
      $item->parent_id = 1;
    }

    $item->title = $item->name;
    unset( $item->name);

    // use shared helper to account for changes in non-sef urls format
    // between J! 1.5 and J! 1.6+
    $item->link = JosettaadminHelper_Links::updateLinksFormat( $item->link);
    
    // build a dummy request array, that will be used by Josetta to identify
    // correctly the type of menu item
    $option = preg_match( '/[&|\?]option=([^&]*)/i', $item->link, $tmp) ? $tmp[1] : '';
    $view = preg_match( '/[&|\?]view=([^&]*)/i', $item->link, $tmp) ? $tmp[1] : '';
    $layout = preg_match( '/[&|\?]layout=([^&]*)/i', $item->link, $tmp) ? $tmp[1] : '';
    $item->request = array( 'option' => $option);
    if(!empty($view)) {
      $item->request['view'] = $view;
    }
    if(!empty($layout)) {
      $item->request['layout'] = $layout;
    }

    // add component_id field
    if (!empty( $option)) {
      $item->component_id = empty( $_extensions[$option]) ? 0 : $_extensions[$option]->extension_id;
    }

    // handle aliases
    if ( (strpos($item->link, 'Itemid=') !== false) && $item->type == 'menulink') {

      // Extract the Itemid from the URL
      if (preg_match('|Itemid=([0-9]+)|', $item->link, $tmp)) {
        $item_id = $tmp[1];

        $item->params = $item->params . "\naliasoptions=".$item_id;
        $item->type = 'alias';
        $item->link = 'index.php?Itemid=';
      }
    }

    // Converting params to JSON
    $item->params = $this->_convertParams( $item->params);

    // The Joomla 1.6 database structure does not allow duplicate aliases
    $item->alias = $this->_fixAlias( $item->alias, $item->jta_import_id, $language);

    // fix the menu type: menu_types have been created under a different name
    // need to fix
    $item->menutype = ShlDbHelper::selectResult( '#__josetta_jf_journal', 'created_id', array( 'import_id' => $item->jta_import_id,
        'imported_context' => 'menu_types', 'created_context' => 'menu_types', 'imported_id' => $item->menutype));

    // kill the id, we always want to create a new item
    $pkName = $this->_pkName;
    $item->$pkName = 0;

    return $item;
  }

  protected function _fixAlias( $alias, $importId, $language) {

    $count = 0;
    do {
      $alreadyInUse = ShlDbHelper::count( '#__menu', '*', array( 'alias' => $alias));
      if(!empty($alreadyInUse)) {
        if($count > 0) {
          $alias .= '-' . $count;
        } else {
          $alias = JApplication::stringURLSafe( $alias . '-' . $language . '-' . $importId);
        }
        $count++;
      }
    } while ($alreadyInUse > 0 and $count < 10);

    return $alias;
  }

}
