<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   weeblr, llc (c) 2014
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.2.1.567
 * @date		2014-01-16
 */


// no direct access
defined('_JEXEC') or die;

jimport( 'joomla.application.component.modeladmin');
jimport( 'joomla.html.parameter');

/**
 * @package		com_josetta
 * @subpackage	com_josetta
 */

class JosettaadminModel_Categoriesimporter extends JosettaadminClass_Baseimporter {

  protected $_pkName = 'id';
  protected $_context = 'categories_item';
  protected $_journalTable = null;
  protected $_originalLanguageItemId = 0;


  /**
   * Receives a list of items, in an array
   * indexed by language, and create menu items
   * for each of them
   *
   * Categories are filtered so that we only import com_content
   * categories, that is categories with 'section' being numeric
   *
   * @param array of objects $items
   * @param object $journalTable JTable descendant to write to the journal
   * @return integer # of imported items, can be zero. -1 on error
   */
  public function importItemGroup( $itemGroup, $journalTable) {

    // check if section is numeric, on default language item
    $defaultLanguage = JosettaHelper::getSiteDefaultLanguage();
    if(empty( $itemGroup[$defaultLanguage])) {
      $this->setError( JText::_('COM_JOSETTA_IMPORT_ERROR_NO_DEFAULT_LANGUAGE_ITEM'));
      return -1;
    }
    $section = (int) $itemGroup[$defaultLanguage]->section;
    if(empty( $section)) {
      return 0;
    }

    return parent::importItemGroup( $itemGroup, $journalTable);

  }

  protected function _importOriginal( $item, $language) {

    // apply various fixes/updates to the menu item
    $item = $this->_convertItem($item, $language);

    // use J! model to save
    require_once JPATH_ADMINISTRATOR.'/components/com_categories/models/category.php';
    JTable::addIncludePath( JPATH_ADMINISTRATOR.'/components/com_categories/tables');

    // now actually save
    $categoryModel = new CategoriesModelCategory;
    $saved = $categoryModel->save( (array) $item);

    // note: we cannot just use $categoryModel->getState( 'category.id') to get
    // back the saved id, because when doing getState(), the populateState()
    // method is called first, and it erases the stored category id
    // as it uses JRequest::getInt( 'id') to initialize the 'category.id' state
    $state = $categoryModel->get( 'state');
    $categoryId = $state->get( 'category.id');

    $errors = $categoryModel->getErrors();
    if(!empty( $errors)) {
      foreach( $errors as $error) {
        $this->setError( $error);
      }
      return 0;
    }

    //return the id of table
    return $categoryId;

  }

  protected function _importTranslation( $item, $language) {

    $itemId = parent::_importTranslation($item, $language);

    if((int)$itemId > 0) {
      // bad fix: data stored as params are hard to pass to the Josetta model
      // so we just write them directly into the db record
      ShlDbHelper::update( '#__categories', array( 'params' => $item->params), array( 'id' => $itemId));
    }

    return $itemId;

  }

  /**
   * Based on Jupgrade:
   *
   * jUpgrade
   *
   * @version		  $Id$
   * @package		  MatWare
   * @subpackage	com_jupgrade
   * @author      Matias Aguirre <maguirre@matware.com.ar>
   * @link        http://www.matware.com.ar
   * @copyright		Copyright 2006 - 2011 Matias Aguirre. All rights reserved.
   * @license		  GNU General Public License version 2 or later; see LICENSE.txt
   *
   * @param object $item
   */
  protected function _convertItem( $item, $language) {

    // Sections are categories now
    $item->extension = 'com_content';

    // Fixing access
    $item->access++;
    // Fixing level
    $item->level = null;
    // Fixing language
    $item->language = $language;

    // Fixing parent_id
    // categories parent is a section (as J! 1.5 has only sections/cat)
    // section has already been imported so we'll fetch
    // the id it was imported under, and use that as a parent id
    if (empty($item->parent_id)) {
      $item->parent_id = ShlDbHelper::selectResult( '#__josetta_jf_journal', 'created_id'
          , array('imported_context' => 'sections', 'imported_id' => $item->section, 'language' => $language));
      if(empty( $item->parent_id)) {

        // user may have translated category, but not the parent section?
        // we'll create one if so
        ShlSystem_Log::info( 'josetta', 'Run #' . $item->jta_import_id . ', we have a translated category (' . $item->id . ') but parent section (' . $item->section . ') has not been translated. Creating a parent category on the fly');
        $newCat = array();
        $newCat['parent_id'] = JosettaadminHelper::getImportRootCategoryId( $item->jta_import_id, 'com_content', $language);
        $newCat['title'] = JText::sprintf( 'COM_JOSETTA_IMPORT_AUTO_CREATED_PARENT', $item->jta_import_id, $language);
        $newCat['extension'] = 'com_content';
        $newCat['description'] = JText::sprintf( 'COM_JOSETTA_IMPORT_AUTO_CREATED_PARENT_DESC', $item->title, $item->section, $item->jta_import_id);
        $newCat['published'] = 1;
        $newCat['language'] = $language;

        // use J! model to save a new category
        require_once JPATH_ADMINISTRATOR.'/components/com_categories/models/category.php';
        JTable::addIncludePath( JPATH_ADMINISTRATOR.'/components/com_categories/tables');

        // now actually save
        $categoryModel = new CategoriesModelCategory;
        $saved = $categoryModel->save( $newCat);
        if($saved) {
          $state = $categoryModel->get( 'state');
          $item->parent_id = $state->get( 'category.id');
          // write that to journal, using a separate object
          $journalTable = new JosettaadminTable_Jfjournal( JFactory::getDbo());
          $journalTable->import_id = $item->jta_import_id;
          $journalTable->imported_context = 'sections';
          $journalTable->imported_id = $item->section;
          $journalTable->created_context = 'categories_item';
          $journalTable->created_id = $item->parent_id;
          $journalTable->language = $language;
          $saved = $journalTable->store();
          if(!$saved) {
            ShlSystem_Log::error( 'josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
          }

        } else {
          $item->parent_id = JosettaadminHelper::getImportRootCategoryId( $item->jta_import_id, 'com_content', $language);
          ShlSystem_Log::error( 'josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $categoryModel->getError());
        }

      }

    }

    // Converting params to JSON
    $item->params = $this->_convertParams( $item->params);

    // The Joomla 1.6 database structure does not allow duplicate aliases
    $count = 0;
    do {
      $alreadyInUse = ShlDbHelper::count( '#__categories', '*', array( 'alias' => $item->alias));
      if(!empty($alreadyInUse)) {
        if($count > 0) {
          $item->alias .= '-' . $count;
        } else {
          $item->alias = JApplication::stringURLSafe( $item->alias . '-' . $language . '-' . $item->jta_import_id);
        }
        $count++;
      }
    } while ($alreadyInUse > 0 and $count < 10);


    // kill the id, we always want to create a new item
    $pkName = $this->_pkName;
    $item->$pkName = 0;

    return $item;
  }


  /**
   * jUpgrade
   *
   * @version		  $Id$
   * @package		  MatWare
   * @subpackage	com_jupgrade
   * @author      Matias Aguirre <maguirre@matware.com.ar>
   * @link        http://www.matware.com.ar
   * @copyright		Copyright 2006 - 2011 Matias Aguirre. All rights reserved.
   * @license		  GNU General Public License version 2 or later; see LICENSE.txt
   */

  /**
   * Converts the params fields into a JSON string.
   *
   * @param	string	$params	The source text definition for the parameter field.
   *
   * @return	string	A JSON encoded string representation of the parameters.
   * @since	0.4.
   * @throws	Exception from the convertParamsHook.
   */
  protected function _convertParams($params)
  {
    $temp	= new JRegistry($params);
    $object	= $temp->toObject();

    // Fire the hook in case this parameter field needs modification.
    $this->convertParamsHook($object);

    return json_encode($object);
  }

  /**
   * A hook to be able to modify params prior as they are converted to JSON.
   *
   * @param	object	$object	A reference to the parameters as an object.
   *
   * @return	void
   * @since	0.4.
   * @throws	Exception
   */
  protected function convertParamsHook(&$object)
  {
    if (isset($object->menu_image)) {
      if((string)$object->menu_image == '-1'){
        $object->menu_image = '';
      }
    }
    if (isset($object->show_page_title)) {
      $object->show_page_heading = $object->show_page_title;
    }
  }

  /**
   * Not used, but need to implement as getForm() is declared
   * abstract in parent classes
   *
   * (non-PHPdoc)
   * @see JModelForm::getForm()
   */
  public function getForm($data = array(), $loadData = true) {

  }

}
