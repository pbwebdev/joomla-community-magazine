<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */


// no direct access
defined('_JEXEC') or die;

jimport( 'joomla.application.component.modeladmin');

/**
 * @package		Josetta
 */

class JosettaadminModel_Content extends ShlMvcModel_Admin {

  /**
   * The URL option for the component.
   *
   * @var    string
   * @since  11.1
   */
  protected $option = 'com_josetta';
  
  public function __construct($config = array()) {

    $config['name'] = 'content';
    parent::__construct( $config);

  }

  /**
   * Method to get a table object, load it if necessary.
   *
   * @access	public
   * @param	string The table name. Optional.
   * @param	string The class prefix. Optional.
   * @param	array	Configuration array for model. Optional.
   * @return	object	The table
   * @since	1.5
   */
  public function getTable($name='', $prefix='Table', $options = array())
  {

    $table = parent::getTable( $name, 'JTable', $options);

    return $table;
  }

  /**
   * Not used, but need to implement as getForm() is declared
   * abstract in parent classes
   *
   * (non-PHPdoc)
   * @see JModelForm::getForm()
   */
  public function getForm($data = array(), $loadData = true) {

  }

}
