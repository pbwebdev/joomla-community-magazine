<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   weeblr, llc (c) 2014
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.2.1.567
 * @date		2014-01-16
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * @package		Josetta
 */

class JosettaadminModel_Jfimporter extends ShlMvcModel_Admin
{

	const MENU_TYPES = 'menu_types';
	const MENU_ITEMS = 'menu_items';
	const SECTIONS = 'sections';
	const CATEGORIES = 'categories';
	const ARTICLES = 'content';
	const FRONTPAGE = 'content_frontpage';
	const COMPONENTS = 'components';

	const STATE_CREATED = 0;
	const STATE_RAW_DATA_IMPORTED = 1;
	const STATE_COMPLETED = 2;
	const STATE_FAILED = 3;
	const STATE_DELETED = 4;
	const STATE_LINKS_FIXED = 100;

	// override automatic behavior of Joomla JModel constructor
	protected $option = 'com_josetta';

	protected $_itemTypes = array(self::MENU_TYPES, self::MENU_ITEMS, self::SECTIONS, self::CATEGORIES, self::ARTICLES, self::COMPONENTS,
		self::FRONTPAGE);

	protected $_start = 0;
	protected $_batchSize = 0;

	public function setStart($start)
	{
		$this->_start = $start;
	}

	/**
	 * Prepare model for import actions
	 * If an importId is passed, data from this
	 * previously started import is loaded
	 * Else a new import record is created.
	 *
	 * @param integer $importId
	 * @return boolean true on success
	 */
	public function setup($importId = null, $batchSize = 0)
	{

		$importId = (int) $importId;
		$table = $this->getTable();
		if (empty($importId))
		{
			$status = $table->store();
		}
		else
		{
			$status = $table->load($importId);
		}

		if ($status)
		{
			$primaryKeyName = $table->getKeyName();
			$this->setState('import_id', $table->$primaryKeyName);
		}

		if ($status)
		{
			// make sure we have a tmp/com_josetta root folder for our imported data
			// won't delete anything if already exists
			jimport('joomla.filesystem.folder');
			$status = JFolder::create(JosettaadminHelper_Files::getTempPath() . '/com_josetta/' . $table->$primaryKeyName);
			if (!$status)
			{
				$this->setError(JText::sprintf('COM_JOSETTA_ERROR_CANT_CREATE_FOLDER', $targetExtractDir));
			}
		}

		$this->_batchSize = $batchSize;

		return $status;
	}

	/**
	 * Update the state of current import run in
	 * the import table
	 *
	 * @param integer $state Use class constants STATE_* for values
	 * @param string optional error message
	 * @param integer $importId optional id of import for which we want to set state
	 */
	public function setImportState($state, $error = '', $importId = null)
	{

		try
		{

			$importId = is_null($importId) ? $this->getState('import_id') : $importId;
			ShlDbHelper::update('#__josetta_jf_imports', array('state' => $state, 'error_message' => $error), array('id' => $importId));
			return true;
		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			$this->setError($e->getMessage());
			return false;
		}
	}

	/**
	 * Unzip an uploaded import file into a specific directory
	 * for further processing
	 * The uploaded file is moved in /tmp/com_josetta
	 * then is unzipped into /tmp/com_josetta/{import_id}
	 *
	 * @param array $uploadedFileRecord the uploaded file record as provided by PHP
	 */
	public function unzipUploadedFile($uploadedFileRecord)
	{

		// check uploaded file for sanity
		if (empty($uploadedFileRecord) || !empty($uploadedFileRecord['error']))
		{
			$errorCode = empty($uploadedFileRecord['error']) ? 0 : $uploadedFileRecord['error'];
			$this->setError(JText::sprintf('COM_JOSETTA_ERROR_FILE_UPLOAD', $uploadedFileRecord['error']));
			return false;
		}

		// must be a zip file
		$types = array('zip', 'application/zip', 'application/x-zip', 'application/x-compressed', 'application/x-zip-compressed', 'multipart/x-zip',
			'application/octet-stream');
		// crappy check: appears some browser/server combination can return empty uploaded file type
		// so don't check nothing in such case
		if (!empty($uploadedFileRecord['type']) && !in_array($uploadedFileRecord['type'], $types))
		{
			$this->setError(JText::_('COM_JOSETTA_ERROR_WRONG_FILE_TYPE') . ' (' . htmlspecialchars($uploadedFileRecord['type']) . ')');
			return false;
		}

		// move the zipped file to /tmp/com_josetta
		$targetRootDir = JosettaadminHelper_Files::getTempPath() . '/com_josetta';
		$target = $targetRootDir . '/' . $uploadedFileRecord['name'];
		$result = move_uploaded_file($uploadedFileRecord['tmp_name'], $target);
		if (!$result)
		{
			$this->setError(JText::_('COM_JOSETTA_ERROR_UNABLE_TO_MOVE_FILE'));
			return false;
		}

		// unzip it to /tmp/com_josetta/{import_id}
		$zipper = new JosettaadminClass_Pclzip($targetRootDir . '/' . $uploadedFileRecord['name']);
		$targetExtractDir = $targetRootDir . '/' . $this->getState('import_id');

		// unzip
		$unzipped = $zipper->extract(PCLZIP_OPT_PATH, $targetExtractDir);
		if ($unzipped < 1)
		{
			$this->setError(JText::sprintf('COM_JOSETTA_ERROR_FILE_UNZIP', $unzipped));
			return false;
		}

		return true;

	}

	/**
	 * Read list of raw data input files from source directory
	 * and store it into raw data database tables
	 * Data stored inside those database tables will later
	 * be imported as proper Joomla! objects into
	 * Joomla! own database tables
	 *
	 * @param array of string $itemTypes Use class constant
	 * @return array of integer number of items imported, per type
	 */
	public function getRawDataInputFiles($itemTypes = null)
	{

		// default to reading all item types files
		$itemTypes = is_null($itemTypes) ? $this->_itemTypes : $itemTypes;

		// start with empty file list
		$filesList = array();

		// directory where we should look for import files
		$sourceDir = JosettaadminHelper_Files::getTempPath() . '/com_josetta/' . $this->getState('import_id');
		jimport('joomla.filesystem.folder');
		if (!JFolder::exists($sourceDir))
		{
			$this->setError(JText::sprintf('COM_JOSETTA_ERROR_NO_IMPORT_INPUT_DIR', $sourceDir));
			return false;
		}

		foreach ($itemTypes as $itemType)
		{

			$filesList[$itemType] = array();

			// load list of files
			$pattern = 'com_josetta_export_' . $itemType . '[_]?[0-9]*\.dat';
			$inputFiles = JFolder::files($sourceDir, $pattern, $recurse = false, $full = true);

			// make sure numbered files, if any, are in the right order
			natsort($inputFiles);

			$filesList[$itemType] = $inputFiles;
		}

		return $filesList;
	}

	/**
	 * Finds next in series of item types
	 *
	 * @param string $itemType
	 * @return string next item type, or null if none found
	 */
	public function getNextItemType($itemType = '')
	{

		$listSize = count($this->_itemTypes);

		// no current item type, we want the first one
		if (empty($itemType))
		{
			return $this->_itemTypes[0];
		}

		// find current item type position in list
		$indexOf = array_search($itemType, $this->_itemTypes);
		if ($indexOf === false)
		{
			return '';
		}

		if ($indexOf + 1 >= $listSize)
		{
			// end of list reached
			return '';
		}
		else
		{
			return $this->_itemTypes[$indexOf + 1];
		}

	}

	/**
	 * Actually read items from an import file, and create matching records
	 * in a temporary database table
	 *
	 * @param string $itemType type of item, use class constant for that
	 * @param string $inputFile full path name of file to import
	 */
	public function importRawDataToDb($itemType, $inputFile)
	{

		$itemsImported = 0;
		jimport('joomla.filesystem.file');
		if (!JFile::exists($inputFile))
		{
			$this->setError(JText::sprintf('COM_JOSETTA_ERROR_INVALID_IMPORT_INPUT_FILE', $inputFile));
			return -1;
		}

		// read serialized data
		$rawFileContent = JFile::read($inputFile);
		if ($rawFileContent === false)
		{
			$this->setError(JText::sprintf('COM_JOSETTA_ERROR_INVALID_IMPORT_INPUT_FILE', $inputFile));
			return -1;
		}

		// some data, now unzerialize to get a usable PHP object
		$records = unserialize($rawFileContent);
		if ($records === false)
		{
			$this->setError(JText::sprintf('COM_JOSETTA_ERROR_INVALID_DATA_IN_IMPORT_INPUT_FILE', $inputFile));
			return -1;
		}

		// looks good, we can now store in temp tables. Start by deciding what table to write to
		$tableName = '#__josetta_jf_' . $itemType;
		try
		{
			if (in_array($itemType, array(self::MENU_TYPES, self::COMPONENTS, self::FRONTPAGE)))
			{
				// those special tables are not translatable, thus import data is not indexed
				// per language
				foreach ($records as $item)
				{
					if (!empty($item))
					{
						$this->_writeRawDataToDb($tableName, '', $item);
						$itemsImported++;
					}
				}
			}
			else
			{
				// regular table are translatable, thus import data
				// is indexed in language and we must iterate over each language
				// However, we only import items which language is available on the target site
				$availableLanguages = JLanguageHelper::getLanguages('lang_code');
				foreach ($records as $language => $items)
				{
					foreach ($items as $item)
					{
						if (empty($item))
						{
							continue;
						}

						// some version of Joomfish return null values for published field of some items
						if (in_array($itemType, array(self::MENU_ITEMS, self::CATEGORIES, self::SECTIONS)))
						{
							if (!isset($item->published) || is_null($item->published))
							{
								// just skip this item
								continue;
							}
						}

						// same for the state field, for some other item types
						if (in_array($itemType, array(self::ARTICLES)))
						{
							if (!isset($item->state) || is_null($item->state))
							{
								// just skip this item
								continue;
							}
						}

						// proceed
						if (array_key_exists($language, $availableLanguages))
						{
							$this->_writeRawDataToDb($tableName, $language, $item);
							$itemsImported++;
						}
					}
				}
			}
		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			$this->setError($e->getMessage());
			return -1;
		}

		return $itemsImported;
	}

	/**
	 * Write an imported raw data record to databse
	 *
	 * @param string $tableName
	 * @param string $language
	 * @param array $item
	 * @return boolean true on success
	 * @throw Exception on db write failure
	 */
	protected function _writeRawDataToDb($tableName, $language, $item)
	{

		// remove unwamnted properties, that may be there if
		// user has modified the db table strucure of their site
		// from the default Joomla structure (yes, it happens)
		$item = $this->_removeBadColumns($tableName, $item);

		// now store to our temp tables
		$recordArray = JArrayHelper::fromObject($item, $recurse = false);
		$recordArray['jta_import_id'] = $this->getState('import_id');
		$recordArray['jta_language'] = $language;
		ShlDbHelper::insert($tableName, $recordArray);

		return true;
	}

	/**
	 * Removed properties of an object that are not a target db table
	 * columns, in preparation for writing that object to the db
	 *
	 * @param string $tableName
	 * @param object $item
	 * @return object $item
	 */
	protected function _removeBadColumns($tableName, $item)
	{

		if (empty($tableName) || empty($item))
		{
			return $item;
		}

		$tableColumns = $this->_getDBTableColumns($tableName);
		if (empty($tableColumns))
		{
			return $item;
		}
		$cleanedItem = new stdClass();
		foreach ($tableColumns as $columnName => $columnSpecs)
		{
			if (isset($item->$columnName))
			{
				$cleanedItem->$columnName = $item->$columnName;
			}
		}
		return $cleanedItem;
	}

	protected function _getDBTableColumns($tableName)
	{

		static $_columns = array();

		if (empty($columns[$tableName]))
		{
			$db = ShlDbHelper::getDb();
			$columns[$tableName] = $db->getTableColumns($tableName);
			$columns[$tableName] = empty($columns[$tableName]) ? array() : $columns[$tableName];
		}

		return $columns[$tableName];
	}

	/**
	 * The big eraser: delete all data imported, for a given import id
	 * Includes all temporary tables AND all Joomla! items!!
	 * Deletion is based on records found in the journal table
	 *
	 * Handle with care!
	 *
	 * @param integer $importId
	 */
	public function deleteImportedData($importId)
	{

		$importId = (int) $importId;
		if (empty($importId))
		{
			return false;
		}

		try
		{

			$this->deleteTemporaryTable($importId);
			$this->deleteImportedJoomlaItems($importId);

			return true;

		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			$this->setError($e->getMessage());
		}

		return false;
	}

	public function deleteTemporaryTable($importId)
	{

		$importId = (int) $importId;
		$condition = empty($importId) ? array() : array('jta_import_id' => $importId);

		ShlDbHelper::delete('#__josetta_jf_categories', $condition);
		ShlDbHelper::delete('#__josetta_jf_components', $condition);
		ShlDbHelper::delete('#__josetta_jf_content', $condition);
		ShlDbHelper::delete('#__josetta_jf_content_frontpage', $condition);
		ShlDbHelper::delete('#__josetta_jf_menu_items', $condition);
		ShlDbHelper::delete('#__josetta_jf_menu_types', $condition);
		ShlDbHelper::delete('#__josetta_jf_sections', $condition);

	}

	public function deleteImportedJoomlaItems($importId)
	{
		// don't allow empty importId to delete
		// all items, as there can be too many
		$importId = (int) $importId;
		if (empty($importId))
		{
			return false;
		}

		$importedItems = ShlDbHelper::selectObjectList('#__josetta_jf_journal', '*', array('import_id' => $importId));

		// we'll have to delete associations as well!!
		// both in Josetta and Joomla! associations tables
		foreach ($importedItems as $importedItem)
		{
			switch ($importedItem->created_context)
			{
				case self::MENU_TYPES:
					ShlDbHelper::delete('#__menu_types', array('menutype' => $importedItem->created_id));
					break;
				case 'com_menus_item':
					$table = JTable::getInstance('Menu');
					$table->delete($importedItem->created_id);
					// delete any associations, whether in Joomla or Josetta associations tables
					ShlDbHelper::delete('#__associations', array('context' => 'com_menus.item', 'id' => $importedItem->created_id));
					ShlDbHelper::delete('#__josetta_associations',
						array('context' => JosettaHelper::context2Joomla('com_menus_item'), 'id' => $importedItem->created_id));
					break;
				case 'categories_item':
					JosettaadminHelper::deleteCategoryAndAssociations($importedItem->created_id);
					break;
				case 'com_content_item':
					JosettaadminHelper::deleteArticleAndAssociations($importedItem->created_id);
					break;
				case 'content_frontpage':
					ShlDbHelper::delete('#__content_frontpage', array('content_id' => $importedItem->created_id));
					break;
			}
		}

		// delete root categories that were created
		JosettaadminHelper::deleteImportRootCategory($importId, 'com_content');
		JosettaadminHelper::deleteImportUncategorizedCategory($importId, 'com_content');

		// finally clear up journal
		ShlDbHelper::delete('#__josetta_jf_journal', array('import_id' => $importId));

	}

	/**
	 * Import items of certain types into
	 * actual Joomla 2.5 items
	 *
	 * @param array of strings $itemTypes Use class constant
	 * @return integer # of items imported, or -1 in case of error
	 */
	public function importItems($itemType)
	{
		try
		{
			// load Josetta language file, needed for logged messages
			$language = JFactory::getLanguage();
			//load the administrator english language of the component if language translation is not available
			$language->load('com_josetta', JPATH_ROOT, 'en-GB', true);
			//load the administrator default language of the component if language translation is not available
			$language->load('com_josetta', JPATH_ROOT, null, true);

			$imported = 0;
			$methodName = '_import' . ucFirst($itemType);
			if (method_exists($this, $methodName))
			{
				$imported = $this->$methodName();
			}
			return $imported;

		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			$this->setError($e->getMessage());
		}

		return -1;
	}

	/**
	 * Post process previously imported items
	 * Mostly fixing links when items ids have been changed
	 * This cannot be done on the fly, while importing items
	 * as an item can link to another one, which has not
	 * yet been imported. So this can only be done
	 * after all items have been created, and we know
	 * their new ids
	 *
	 * @return integer # of items processed, or -1 in case of error
	 */
	public function postProcessImportedItems()
	{

		try
		{

			$processed = 0;

			$nameQuoted = array('#__josetta_jf_journal', 'import_id', 'imported_context');
			$quoted = array($this->getState('import_id'), 'menu_items', 'content', (int) $this->_start, (int) $this->_batchSize);
			$journalRecords = ShlDbHelper::quoteQuery('select * from ?? where ?? = ? and ?? in (?,?) limit ?, ?', $nameQuoted, $quoted)
				->shlLoadObjectList();

			if (empty($journalRecords))
			{
				return $processed;
			}

			foreach ($journalRecords as $journalRecord)
			{
				$recordProcessed = $this->_postProcessImportedItem($journalRecord);
				if (!$recordProcessed)
				{
					throw new Exception($this->getError());
				}
				$processed++;
			}

			return $processed;

		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			$this->setError($e->getMessage());
		}

		return -1;
	}

	/**
	 * Implements import of Joomla 1.5.x menu_types
	 *
	 * @return integer # of items imported, or -1 in case of error
	 */
	protected function _importMenu_types()
	{

		try
		{
			// 1 - find if there is any data available to import
			$items = ShlDbHelper::selectObjectList('#__josetta_jf_menu_types', '*', array('jta_import_id' => $this->getState('import_id')));
			if (empty($items))
			{
				return 0;
			}
			$importedItemsCount = 0;

			// 2 - Pre-Process and Import
			$journalTable = new JosettaadminTable_Jfjournal(JFactory::getDbo());
			$journalTable->imported_context = 'menu_types';
			$journalTable->created_context = 'menu_types';
			$journalTable->language = '';
			$menuTypeTable = JTable::getInstance('menuType');

			foreach ($items as $item)
			{
				// we don't care about keeping the id, menutypes are
				// identified by the menutype column
				$item->id = 0;

				// save in journal
				$journalTable->id = 0; // create a new row each time
				$journalTable->import_id = $item->jta_import_id;
				$journalTable->imported_id = $item->menutype;

				// menu type must be unique, and easily identified
				// note: menutype has a 24 chars limit, so must be sure, the new
				// menutype is unique within this limit
				$item->menutype = JString::substr('jta-' . $item->jta_import_id . '-' . $item->menutype, 0, 23);
				$item->title = 'Josetta - ' . $item->jta_import_id . ' - ' . $item->title;

				// create item
				$saved = $menuTypeTable->save((array) $item);
				if (!$saved)
				{
					throw new Exception($menuTypeTable->getError());
				}

				// store final menu type, usually will be unchanged though
				$journalTable->created_id = JApplication::stringURLSafe($item->menutype);
				$saved = $journalTable->store();
				if (!$saved)
				{
					throw new Exception($journalTable->getError());
				}

				$importedItemsCount++;
			}

			return $importedItemsCount;

		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			$this->setError($e->getMessage());
		}

		return -1;
	}

	/**
	 * Implements import of Joomla 1.5.x menu items
	 *
	 * @return integer # of items imported, or -1 in case of error
	 */
	protected function _importMenu_items()
	{

		return $this->_importGeneric('menu_items');

	}

	/**
	 * Implements import of Joomla 1.5.x sections
	 *
	 * @return integer # of items imported, or -1 in case of error
	 */
	protected function _importSections()
	{

		return $this->_importGeneric('sections');

	}

	/**
	 * Implements import of Joomla 1.5.x categories
	 *
	 * @return integer # of items imported, or -1 in case of error
	 *
	 */
	protected function _importCategories()
	{

		return $this->_importGeneric('categories');

	}

	/**
	 * Implements import of Joomla 1.5.x content articles
	 * Readinf records from the raw data import tables is more complex
	 * than for other types, as we want to break import into small batches
	 * However we also want for any original language item
	 * to have all it's translations in the same batch
	 * (vs just reading the next 20 items, which may or may not include
	 * all translations for all original language items in the batch)
	 *
	 * @return integer # of items imported, or -1 in case of error
	 */
	protected function _importContent()
	{

		// 1 - find if there is any data available to import
		$items = ShlDbHelper::selectObjectList('#__josetta_jf_content', '*',
			array('jta_import_id' => $this->getState('import_id'), 'jta_language' => JosettaHelper::getSiteDefaultLanguage()), $aWhereData = array(),
			$orderBy = array(), $offset = (int) $this->_start, $lines = (int) $this->_batchSize);

		if (empty($items))
		{
			return array();
		}

		// build a list of ids of those batch of original language items
		$idList = array();
		foreach ($items as $item)
		{
			$idList[] = $item->id;
		}

		// now read their translations, if any
		$query = 'select * from ?? where ?? = ? and ?? <> ? and ?? in (' . ShlDbHelper::arrayToIntvalList($idList) . ')';
		$nameQuoted = array('#__josetta_jf_content', 'jta_import_id', 'jta_language', 'id');
		$quoted = array($this->getState('import_id'), JosettaHelper::getSiteDefaultLanguage());
		$translations = ShlDbHelper::quoteQuery($query, $nameQuoted, $quoted)->shlLoadObjectList();

		// now read translations for these items, and
		// build a more convenient array
		$itemGroups = array();
		foreach ($items as $item)
		{
			$itemGroups[$item->id][$item->jta_language] = clone $item;
		}
		foreach ($translations as $translation)
		{
			$itemGroups[$translation->id][$translation->jta_language] = clone $translation;
		}

		return $this->_importGeneric('content', $itemGroups);

	}

	/**
	 * Implements import of Joomla 1.5.x frontpage records
	 *
	 * Note: translations of a frontpage content item are also put on the frontpage
	 *
	 * @return integer # of items imported, or -1 in case of error
	 */
	protected function _importContent_frontpage()
	{

		try
		{
			// 1 - find if there is any data available to import
			$items = ShlDbHelper::selectObjectList('#__josetta_jf_content_frontpage', '*', array('jta_import_id' => $this->getState('import_id')));
			if (empty($items))
			{
				return 0;
			}
			$importedItemsCount = 0;

			// 2 - Pre-Process and Import
			$journalTable = new JosettaadminTable_Jfjournal(JFactory::getDbo());
			$journalTable->imported_context = 'content_frontpage';
			$journalTable->created_context = 'content_frontpage';
			$journalTable->language = '';

			JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_content/tables');
			$contentFrontpageTable = JTable::getInstance('Featured', 'ContentTable');

			foreach ($items as $item)
			{

				// search for the new item id this item was saved under, for each available language
				$journalRecords = ShlDbHelper::selectObjectList('#__josetta_jf_journal', '*',
					array('import_id' => $this->getState('import_id'), 'imported_context' => 'content', 'imported_id' => $item->content_id));

				// prepare to save in journal
				$journalTable->id = 0; // create a new row each time
				$journalTable->import_id = $item->jta_import_id;
				$journalTable->imported_id = $item->content_id;

				// iterate over each language translation
				foreach ($journalRecords as $journalRecord)
				{

					// we only add the frontpage record IF default language
					// reason is that Josetta has already done that when saving translations
					// in non-default languages
					if ($journalRecord->language == JosettaHelper::getSiteDefaultLanguage())
					{
						$item->content_id = $journalRecord->created_id;
						ShlDbHelper::insert('#__content_frontpage', array('content_id' => $item->content_id));

						// store operation in journal
						$journalTable->language = $journalRecord->language;
						$journalTable->created_id = $journalRecord->created_id;
						$saved = $journalTable->store();
						if (!$saved)
						{
							throw new Exception($journalTable->getError());
						}

						$importedItemsCount++;
					}
				}

			}

			$contentFrontpageTable->reorder();
			return $importedItemsCount;

		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			$this->setError($e->getMessage());
		}

		return -1;
	}

	/**
	 * Implements import of Joomla 1.5.x menu items
	 *
	 * @param string type of object to import
	 * @param array optional, sorted by language list of items
	 * @return integer # of items imported, or -1 in case of error
	 */
	protected function _importGeneric($type, $itemGroups = null)
	{

		try
		{
			if (is_null($itemGroups))
			{
				$itemGroups = $this->_genericReadItemsGroups($type);
			}
			if (empty($itemGroups))
			{
				return 0;
			}

			// initialize import count
			$importedItemsCount = 0;

			// we'll need a custom importer model for that
			$importerModel = ShlMvcModel_Base::getInstance(ucfirst(str_replace('_', '', $type)) . 'importer', 'JosettaadminModel_');

			// and ability to write to the journal
			$journalTable = new JosettaadminTable_Jfjournal(JFactory::getDbo());
			$journalTable->import_id = $this->getState('import_id');
			$journalTable->imported_context = $type;

			foreach ($itemGroups as $itemGroup)
			{

				$imported = $importerModel->importItemGroup($itemGroup, $journalTable);
				if ($imported < 0)
				{
					throw new Exception($importerModel->getError());
				}
				else
				{
					$importedItemsCount += $imported;
				}
			}

			return $importedItemsCount;

		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			$this->setError($e->getMessage());
		}

		return -1;

	}

	protected function _genericReadItemsGroups($type)
	{
		// 1 - find if there is any data available to import
		$items = ShlDbHelper::selectObjectList('#__josetta_jf_' . strtolower($type), '*', array('jta_import_id' => $this->getState('import_id')),
			$aWhereData = array(), $orderBy = array(), $offset = (int) $this->_start, $lines = (int) $this->_batchSize);

		if (empty($items))
		{
			return array();
		}

		// build a more convenient array
		$itemGroups = array();
		foreach ($items as $item)
		{
			$itemGroups[$item->id][$item->jta_language] = clone $item;
		}

		return $itemGroups;
	}

	/**
	 * Performs whatever fix is needed to an imported
	 * item content
	 *
	 * @param object $journalRecord a record from the import journal table
	 */
	protected function _postProcessImportedItem($journalRecord)
	{

		$processed = 0;
		$methodName = '_postProcess' . ucFirst($journalRecord->imported_context);
		if (method_exists($this, $methodName))
		{
			$processed = $this->$methodName($journalRecord);
		}
		return $processed;
	}

	/**
	 * Fix links on a menu item
	 *
	 * @param object $journalRecord
	 */
	protected function _postProcessMenu_items($journalRecord)
	{

		return $this->_postProcessGeneric($journalRecord);

	}

	/**
	 * Fix links on an article
	 *
	 * @param object $journalRecord
	 */
	protected function _postprocessContent($journalRecord)
	{

		return $this->_postProcessGeneric($journalRecord);
	}

	/**
	 * Implements post processing of imported items
	 * calling on specific models based on item type
	 *
	 * @param object $journalRecord
	 * @return boolean true if success, error message is stored in this model
	 */
	protected function _postProcessGeneric($journalRecord)
	{

		try
		{

			// we'll need a custom importer model for that
			$importerModel = ShlMvcModel_Base::getInstance(ucfirst(str_replace('_', '', $journalRecord->imported_context)) . 'importer',
				'JosettaadminModel_');
			$processed = $importerModel->postProcessImportedItem($journalRecord, $this->getState('import_id'));

			return $processed;

		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			$this->setError($e->getMessage());
		}

		return false;

	}

	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $name     The table name. Optional.
	 * @param   string  $prefix   The class prefix. Optional.
	 * @param   array   $options  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   11.1
	 */
	public function getTable($name = '', $prefix = 'JosettaadminTable', $options = array())
	{
		if (empty($prefix))
		{
			$prefix = 'JosettaadminTable';
		}

		return parent::getTable($name, $prefix, $options);
	}

	/**
	 * Stock method to auto-populate the model state.
	 *
	 * @return  void
	 *
	 */
	protected function populateState()
	{

		// Initialise variables.
		$table = $this->getTable();

		// Get the pk of the record from the request.
		$importId = JRequest::getInt('import_id');
		if (!empty($importId))
		{
			$this->setState('import_id', $importId);
		}

		// Load the parameters.
		$value = JComponentHelper::getParams($this->option);
		$this->setState('params', $value);

	}

	/**
	 * Not used, but need to implement as getForm() is declared
	 * abstract in parent classes
	 *
	 * (non-PHPdoc)
	 * @see JModelForm::getForm()
	 */
	public function getForm($data = array(), $loadData = true)
	{

	}

}
