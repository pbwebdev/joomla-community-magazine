<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// No direct access.
defined('_JEXEC') or die;


require_once JPATH_ROOT . '/components/com_josetta/helpers/helper.php';
// add modal css and js
ShlHtmlBs_helper::addBootstrapCss(JFactory::getDocument());
ShlHtmlBs_helper::addBootstrapJs(JFactory::getDocument());

$modalParams = array();
$targetUrl = JURI::root() . 'index.php?option=com_josetta&view=translator&Itemid=' . JosettaHelper::getJosettaItemid();
$modalTitle = JText::_('MOD_JOSETTA_JUMP');
$modalParams['linkTitle'] = $modalTitle;
$modalParams['linkType'] = 'a';
$modalParams['linkClass'] = 'josetta_modal';
$modalLink = ShlHtmlModal_helper::modalLink('mod_josetta_jump', ShlHtmlBs_Helper::iconglyph($modalParams['linkTitle'], 'globe'), $targetUrl,
				0.95, 0.8, $top = 0,
				$left = 0, $onClose = '', $modalTitle, $modalParams);
?>

<div class="btn-group multilanguage"><?php echo $modalLink; ?></div>
