<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// No direct access.
defined('_JEXEC') or die;

JHtml::_('behavior.modal');

require_once JPATH_ROOT . '/components/com_josetta/helpers/helper.php';
$targetUrl = JURI::root() . 'index.php?option=com_josetta&view=translator&Itemid=' . JosettaHelper::getJosettaItemid();

$document = JFactory::getDocument();
$document->addScriptDeclaration( 'var josetta_front_end_window = null;
    function josetta_jump_to( url) {
        josetta_front_end_window = window.open( url, "Josetta_jump").focus();
    }
    ');


?>

<span class="multilanguage"><a target="Josetta jump" onclick="josetta_jump_to( '<?php echo $targetUrl; ?>');return false;" href="<?php echo $targetUrl; ?>" ><?php echo JText::_('MOD_JOSETTA_JUMP');?></a></span>


