<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   weeblr, llc (c) 2012
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     1.3.2.448
 * @date		2012-11-28
 */

// No direct access.
defined('_JEXEC') or die;

require_once JPATH_ROOT . '/components/com_josetta/helpers/helper.php';
$targetUrl = JURI::root() . 'index.php?option=com_josetta&view=translator&Itemid=' . JosettaHelper::getJosettaItemid();

?>

<span class="multilanguage"><a class="modal" href="<?php echo $targetUrl;?>" rel="{handler:'iframe', size:{x:window.getSize().x*0.95,y:window.getSize().y*0.90}}"><?php echo JText::_('MOD_JOSETTA_JUMP');?></a></span>

