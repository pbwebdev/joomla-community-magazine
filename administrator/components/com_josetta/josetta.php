<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 * 
 */

// no direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_josetta'))
{
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

// check shLib is available
if (!defined('SHLIB_VERSION'))
{
	$app = JFactory::getApplication();
	$app->JComponentTitle = '<div class="pagetitle"><h2>Josetta translations manager</h2></div>';
	$app
		->enqueuemessage('Josetta requires the shLib system plugin to be enabled, but you appear to have disabled it. Please enable it again!',
			'error');
	return;
}

// autoload our stuff
$path = JPATH_ROOT . '/administrator/components/com_josetta';
ShlSystem_Autoloader::registerPrefix('Josettaadmin', $path, $isPackage = false);
$path = JPATH_ROOT . '/components/com_josetta';
ShlSystem_Autoloader::registerPrefix('Josetta', $path, $isPackage = false);

// Ensure the behavior is loaded
JHtml::_('behavior.framework');
if (version_compare(JVERSION, '3.0', 'ge'))
{
	JHtml::_('bootstrap.framework');
}

$app = JFactory::getApplication();

// find about specific controller requested
$cName = $app->input->getCmd('c');

// get controller from factory
$controller = JosettaadminHelper_General::getController($cName);

// read and execute task
$controller->execute($app->input->getCmd('task'));
$controller->redirect();
