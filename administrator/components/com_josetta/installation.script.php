<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date        2015-12-23
 */

// Security check to ensure this file is being included by a parent file.
defined('_JEXEC') or die;

defined('JOSETTA_VERSION_TYPE') or define('JOSETTA_VERSION_TYPE', 'free');

/**
 * Installation/Uninstallation script
 *
 */
class Com_JosettaInstallerScript
{
	const MIN_JOOMLA_VERSION = '3.4.0';
	const MAX_JOOMLA_VERSION = '4';
	const MIN_SHLIB_VERSION  = '0.2.2';
	const MAX_SHLIB_VERSION  = '0';

	private $_shlibVersion = '';
	private $_skipInstall  = array();

	public function install($parent)
	{
	}

	public function uninstall($parent)
	{
		$this->_doUninstall($parent);
	}

	public function update($parent)
	{
	}

	public function preflight($route, $installer)
	{

		if ($route == 'install' || $route == 'update')
		{
			// check Joomla! version
			if (version_compare(JVERSION, self::MIN_JOOMLA_VERSION, '<') || version_compare(JVERSION, self::MAX_JOOMLA_VERSION, 'ge'))
			{
				JFactory::getApplication()
				        ->enqueueMessage(
					        sprintf('Josetta requires Joomla! version between %s and less than %s (you are using %s). Aborting installation',
						        self::MIN_JOOMLA_VERSION, self::MAX_JOOMLA_VERSION, JVERSION));
				return false;
			}

			// make sure resource manager is available, we'll need it during plugins installs
			if (!class_exists('ShlSystem_Resourcemanager'))
			{
				require_once $installer->getParent()->getPath('source') . '/admin/plugins/system/shlib/shl_packages/system/resourcemanager.php';
			}

			// check authorization to install for shared resources
			$newVersionFile = $installer->getParent()->getPath('source') . '/admin/plugins/system/shlib/shlib.xml';
			$this->_shlibVersion = ShlSystem_Resourcemanager::getXmlFileVersion($newVersionFile);
			$installCheckResult = ShlSystem_Resourcemanager::canInstall('shlib', $this->_shlibVersion, $allowDowngrade = false, self::MIN_SHLIB_VERSION, self::MAX_SHLIB_VERSION);

			if ($installCheckResult->canInstall == 'no')
			{
				JFactory::getApplication()
				        ->enqueueMessage(
					        'Cannot install Josetta: not allowed to install shLib version ' . $this->_shlibVersion . ': ' . $installCheckResult->reason,
					        'error');
			}
			if ($installCheckResult->canInstall == 'skip')
			{
				$this->_shlibVersion = '';
				$this->_skipInstall[] = 'shlib';
				JFactory::getApplication()
				        ->enqueueMessage('shLib: skipping install of shLib version ' . $this->_shlibVersion . ': ' . $installCheckResult->reason);
			}

			$canInstall = $installCheckResult->canInstall != 'no';

			return $canInstall;
		}
	}

	public function postflight($type, $parent)
	{
		$status = $this->_doInstallUpdate($parent);

		if (!$status)
		{
			return false;
		}

		// installed a shared resource? register it with version
		if (!in_array('shlib', $this->_skipInstall))
		{
			ShlSystem_Resourcemanager::registerResource('shlib', $this->_shlibVersion);
		}

		// register that we are now using shLib
		ShlSystem_Resourcemanager::register(array('resource' => 'shlib', 'context' => 'com_josetta', 'min_version' => self::MIN_SHLIB_VERSION));
	}

	/**
	 * Implementation of install/uninstall scripts
	 */

	private function _doInstallUpdate($parent)
	{
		$app = JFactory::getApplication();

		// trick of the day: we must fetch an instance of the db using the db helper
		// before installing the newest version of shLib system plugin. This will
		// force a decorated db instance to be created and stored, using the shlib
		// db class version that matches that of the shlib db helper class
		// As there was interface changes betwen shLib 0.1.x and 0.2.x, this prevents
		// "method not existing" errors when installing a newer version over an old one
		// make sure resource manager is available, we'll need it during plugins installs
		if (defined('SHLIB_ROOT_PATH'))
		{
			$db = ShlDbHelper::getInstance();
			$this->_shInstallPluginGroup('system');
		}
		else
		{
			$db = JFactory::getDbo();
			// shLib is not installed yet, let's make it available to us
			$this->_shInstallPluginGroup('system');
			if (!JFile::exists(JPATH_ROOT . '/plugins/system/shlib/shlib.php'))
			{
				JFactory::getApplication()
				        ->enqueuemessage('shLib was not installed properly, cannot continue. Please try uninstalling and installing again');
				return false;
			}
			require_once JPATH_ROOT . '/plugins/system/shlib/shlib.php';
			$config = array('type' => 'system', 'name' => 'shlib', 'params' => '');
			$dispatcher = JDispatcher::getInstance();
			$shLibPlugin = new plgSystemShlib($dispatcher, $config);
			$shLibPlugin->onAfterInitialise();
		}

		// install all our plugins
		$this->_shInstallPluginGroup('josetta');
		if ('free' == 'full')
		{
			$this->_shInstallPluginGroup('josetta_ext');
		}

		// allow ourselves use shLib library
		// but do this only on sites where shLib is not installed already
		// as this may otherwise yield fatal errors (redefined constants)
		// on sites running old versions
		if (!defined('SHLIB_ROOT_PATH'))
		{
			require_once JPATH_ROOT . '/plugins/system/shlib/shlib.php';
			$config = array('type' => 'system', 'name' => 'shlib', 'params' => '');
			$shLibPlugin = new plgSystemShlib(JDispatcher::getInstance(), $config);
			$shLibPlugin->onAfterInitialise();
		}
		// apply various DB updates
		$this->_updateDatabase();

		// acl management
		if ('free' == 'full')
		{
			if (file_exists(JPATH_ADMINISTRATOR . '/components/com_josetta/installation.acl.full.php'))
			{
				include JPATH_ADMINISTRATOR . '/components/com_josetta/installation.acl.full.php';
			}
		}

		// make sur permissions record are set straight
		$query = $db->getQuery(true);
		$query->select('rules');
		$query->from('#__assets');
		$query->where($db->quoteName('name') . '=' . $db->quote('com_josetta'));
		$db->setQuery($query);
		$existingRules = $db->loadResult();
		if (empty($existingRules) || $existingRules == '{}')
		{
			// write valid default value into assets record
			$defaultRule = '{"core.admin":[],"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}';
			$query->update($db->quoteName('#__assets'))->set($db->quoteName('rules') . '=' . $db->quote($defaultRule))
			      ->where($db->quoteName('name') . '=' . $db->quote('com_josetta'));
			$db->setQuery($query);
			$result = $db->query();
		}

		// add a menu type
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from('#__menu_types');
		$query->where($db->quoteName('menutype') . '=' . $db->quote('josetta'));
		$db->setQuery($query);
		$menuTypeId = $db->loadResult();
		// 2 - if not existing, add a user group
		if (empty($menuTypeId))
		{
			include_once JPATH_ADMINISTRATOR . '/components/com_menus/models/menu.php';
			$menuType = array('menutype' => 'josetta', 'title' => 'Josetta menu', 'description' => 'Josetta translation manager menu');
			$model = new MenusModelMenu();
			$saved = $model->save($menuType);

			// need to check errors and display message
			if (!$saved)
			{
				echo 'Error creating Josetta menu type: ' . $model->getError() . '<br />';
			}
		}

		// install atomic_josetta template
		$sourcePath = JPATH_ADMINISTRATOR . '/components/com_josetta/templates';
		if (!JFolder::exists($sourcePath))
		{
			echo 'Unable to install Josetta template, missing from source ZIP file!<br />';
			$templateExtensionId = false;
		}
		else
		{
			$templateExtensionId = $this->_shInstallTemplate('atomic_josetta', $sourcePath);
			if (empty($templateExtensionId))
			{
				echo 'Error installing atomic_josetta template<br />';
			}
		}

		// add a menu item, language = all
		// Removed access level = Josetta for the menu item, as this would prevent us
		// from doing a proper redirect to login page, then come back to Josetta
		// if user is not logged in (as Joomla intercepts request, and only
		// displays a "login first message")
		// attach atomic_josetta template to our josetta menu item
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from('#__menu');
		$query->where($db->quoteName('menutype') . '=' . $db->quote('josetta'));
		$db->setQuery($query);
		$menuItemId = $db->loadResult();
		// 2 - if not existing, add a menu item
		if (empty($menuItemId))
		{
			// we'll need to read the template style id, so as to link the menu item to it
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('id')->from('#__template_styles')->where($db->quoteName('template') . '=' . $db->Quote('atomic_josetta'));
			$templateStyleId = $db->setQuery($query)->loadResult();

			$error = $db->getErrorMsg();
			if (empty($templateStyleId) || !empty($error))
			{
				echo 'Error reading template style: ' . $error . '<br />';
			}
			else
			{
				// now we can create the menu item. We can't use directly Joomla item model
				// has it has an hardcoded JPATH_COMPONENT require_once that fails if used
				// from another extension than com_menus
				include_once JPATH_ADMINISTRATOR . '/components/com_josetta/models/item.php';

				// fetch installed Josetta extension id, as menu item needs that
				$query = $db->getQuery(true);
				$query->select('extension_id')->from('#__extensions')->where($db->quoteName('type') . '=' . $db->Quote('component'))
				      ->where($db->quoteName('element') . '=' . $db->Quote('com_josetta'));
				$componentId = $db->setQuery($query)->loadResult();
				$error = $db->getErrorMsg();
				if (!empty($error))
				{
					echo 'Error reading just installed com_josetta extension id, cannot create front end menu item: ' . $error . '<br />';
				}
				else
				{
					// prepare menu item record
					$menuItem = array('id'        => 0, 'menutype' => 'josetta', 'title' => JText::_('COM_JOSETTA_TRANSLATOR_FRONT_END_MENU_ITEM_TITLE'),
					                  'link'      => 'index.php?option=com_josetta&view=translator', 'type' => 'component', 'component_id' => $componentId,
					                  'published' => 1, 'parent_id' => 1, 'level' => 1, 'language' => '*', 'template_style_id' => $templateStyleId);
					$model = new JosettaadminmenusModel_Item();
					$saved = $model->save($menuItem);
					$menuItemId = $model->getState('item.id');
				}
				// need to check errors and display message
				if (!$saved)
				{
					echo 'Error creating Josetta menu item: ' . $model->getError() . '<br />';
				}
			}
		}

		// add a menu module, language = all, set it to Josetta access level, and NOT published
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from('#__modules');
		$query->where($db->quoteName('title') . '=' . $db->quote('Josetta'));
		$query->where($db->quoteName('module') . '=' . $db->quote('mod_menu'));
		if ('free' == 'full')
		{
			$query->where($db->quoteName('access') . '=' . $levelId);
		}
		$db->setQuery($query);
		$moduleId = $db->loadResult();
		// 2 - if not existing, add a user group
		if (empty($moduleId))
		{
			include_once JPATH_ADMINISTRATOR . '/components/com_modules/models/module.php';
			$params = array('menutype' => 'josetta');
			$moduleValues = array('title'     => 'Josetta', 'note' => JText::_('COM_JOSETTA_MODULE_WARNING'), 'position' => 'position-7',
			                      'published' => 0, 'module' => 'mod_menu', 'params' => $params, 'language' => '*');
			if ('free' == 'full')
			{
				$moduleValues['access'] = $levelId;
			}
			$model = new ModulesModelModule();
			$saved = $model->save($moduleValues);
			$moduleId = $model->getState('module.id');

			// need to check errors and display message
			if (!$saved)
			{
				echo 'Error creating Josetta menu type: ' . $model->getError() . '<br />';
			}
		}

		// install Josetta jump admin module to provide quick access to front end from backend
		$sourcePath = JPATH_ADMINISTRATOR . '/components/com_josetta/modules';
		if (!JFolder::exists($sourcePath))
		{
			echo 'Unable to install Josetta admin module, missing from source ZIP file!<br />';
			$templateExtensionId = false;
		}
		else
		{
			$moduleExtensionId = $this->_shInstallModule('mod_josetta_jump', $sourcePath);
			if (empty($moduleExtensionId))
			{
				echo 'Error installing Josetta jump administrator module<br />';
			}
		}

		// add a menu module, language = all, set it to Josetta access level, and NOT published
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from('#__modules');
		$query->where($db->quoteName('position') . '=' . $db->quote('status'));
		$query->where($db->quoteName('module') . '=' . $db->quote('mod_josetta_jump'));
		$query->where($db->quoteName('client_id') . '= 1');
		$db->setQuery($query);
		$moduleId = $db->loadResult();
		// 2 - if not existing, add the module
		if (empty($moduleId))
		{
			include_once JPATH_ADMINISTRATOR . '/components/com_modules/models/module.php';
			// load module language files
			$language = JFactory::getLanguage();
			// load the administrator english language of the module if language translation is not available
			$language->load('mod_josetta_jump', JPATH_ADMINISTRATOR . '/modules/mod_josetta_jump', 'en-GB', true);
			// load the administrator default language of the module if language translation is not available
			$language->load('mod_josetta_jump', JPATH_ADMINISTRATOR, null, true);

			// insert a module record
			$params = array('');
			$moduleValues = array('title'    => JText::_('MOD_JOSETTA_JUMP'), 'note' => JText::_('MOD_JOSETTA_JUMP_XML_DESCRIPTION'),
			                      'position' => 'status', 'published' => 1, 'module' => 'mod_josetta_jump', 'client_id' => 1, 'params' => $params, 'language' => '*');
			if ('free' == 'full')
			{
				$moduleValues['access'] = $levelId;
			}
			$model = new ModulesModelModule();
			$saved = $model->save($moduleValues);
			$moduleId = $model->getState('module.id');

			// need to check errors and display message
			if (!$saved)
			{
				echo 'Error creating Josetta jump administrator module: ' . $model->getError() . '<br />';
			}
		}

		return true;
	}

	/**
	 * Performs pre-uninstall backup of configuration
	 *
	 * @param object $parent
	 */
	private function _doUninstall($parent)
	{

		// install all our plugins
		if ('free' == 'full')
		{
			$this->_shUninstallPluginGroup('josetta_ext');
		}
		$this->_shUninstallPluginGroup('josetta');

		// identify our system plugin
		$systemPlugin = $this->_getPlugin('system', 'josetta');
		if (!empty($systemPlugin))
		{
			$this->_shUninstallPlugin($systemPlugin);
		}

		// unregister from shLib, then possibly uninstall it
		if (!class_exists('ShlSystem_Resourcemanager'))
		{
			if (JFile::exists(JPATH_ROOT . '/plugins/system/shlib/shl_packages/system/resourcemanager.php'))
			{
				require_once JPATH_ROOT . '/plugins/system/shlib/shl_packages/system/resourcemanager.php';
			}
			else
			{
				JFactory::getApplication()->enqueuemessage('shLib was not installed properly. Cannot uninstall whatever was installed');
			}
		}

		if (class_exists('ShlSystem_Resourcemanager'))
		{
			ShlSystem_Resourcemanager::unregister('shlib', 'com_josetta');
			if (ShlSystem_Resourcemanager::canUninstall('shlib'))
			{
				$systemPlugin = $this->_getPlugin('system', 'shlib');
				if (!empty($systemPlugin))
				{
					$this->_shUninstallPlugin($systemPlugin);
				}
			}
		}

		// delete instances of the Josetta backend quick jump module
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->delete('#__modules');
		$query->where($db->quoteName('client_id') . '=' . $db->quote(1));
		$query->where($db->quoteName('module') . '=' . $db->quote('mod_josetta_jump'));
		$db->setQuery($query);
		$db->execute();
		$error = $db->getErrorMsg();
		if (!empty($error))
		{
			echo 'Error deleting Josetta jump administrator module: ' . $error . '<br />';
		}

		// uninstall Josetta backend quick jump module
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('extension_id');
		$query->from('#__extensions');
		$query->where($db->quoteName('element') . '=' . $db->quote('mod_josetta_jump'));
		$query->where($db->quoteName('type') . '=' . $db->quote('module'));
		$query->where($db->quoteName('client_id') . '=' . $db->quote(1));
		$db->setQuery($query);
		$moduleId = (int) $db->loadResult();
		if (!empty($moduleId))
		{
			$removed = $this->_shUninstallModule($moduleId);
		}
		if (empty($removed))
		{
			echo 'Unable to uninstall Josetta jump administrator module<br />';
		}

		// delete josetta menu module
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->delete('#__modules');
		$query->where($db->quoteName('title') . '=' . $db->quote('Josetta'));
		$query->where($db->quoteName('module') . '=' . $db->quote('mod_menu'));
		$db->setQuery($query);
		$db->execute();
		$error = $db->getErrorMsg();
		if (!empty($error))
		{
			echo 'Error deleting josetta menu module: ' . $error . '<br />';
		}

		// delete josetta menu item
		$query = $db->getQuery(true);
		$query->delete('#__template_styles')->where($db->quoteName('template') . '=' . $db->Quote('atomic_josetta'));
		$db->setQuery($query);
		$db->execute();
		$error = $db->getErrorMsg();
		if (!empty($error))
		{
			echo 'Error deleting josetta template style: ' . $error . '<br />';
		}

		$query = $db->getQuery(true);
		$query->delete('#__menu')->where($db->quoteName('menutype') . '=' . $db->quote('josetta'));
		$db->setQuery($query);
		$db->execute();
		$error = $db->getErrorMsg();
		if (!empty($error))
		{
			echo 'Error deleting josetta menu items: ' . $error . '<br />';
		}

		// delete josetta menu
		$query = $db->getQuery(true);
		$query->delete('#__menu_types')->where($db->quoteName('menutype') . '=' . $db->quote('josetta'));
		$db->setQuery($query);
		$db->execute();
		$error = $db->getErrorMsg();
		if (!empty($error))
		{
			echo 'Error deleting josetta menu: ' . $error . '<br />';
		}

		// uninstall atomic_josetta template
		$query = $db->getQuery(true);
		$query->select('extension_id');
		$query->from($db->quoteName('#__extensions'));
		$query->where($db->quoteName('type') . '=' . $db->quote('template'));
		$query->where($db->quoteName('element') . '=' . $db->quote('atomic_josetta'));
		$db->setQuery($query);
		$templateId = (int) $db->loadResult();
		$removed = false;
		if (!empty($templateId))
		{
			$removed = $this->_shUninstallTemplate($templateId);
		}
		if (empty($removed))
		{
			echo 'Unable to uninstall atomic_josetta template<br />';
		}

		/**
		 * Access level and user group are left behind for now. User
		 * should delete them manually using Joomla! backend interface.
		 *  This can be achieved programmatically, but not so easily
		 *  as the user performing the uninstall is most likely
		 *  also a member of the Josetta user group. This will make
		 *  the joomla! usergroup model delete method fails.
		 *  The user needs first to remove herself from that group. However
		 *  she doesn't do it before uninstalling, it will be too late, as she
		 *  cannot re-run the uninstaller a second time.
		 *  Thus I prefer to leave the group and access level behind
		 *  and leave the Joomla code handle deletion and its dependencies
		 */
		echo JText::_('COM_JOSETTA_UNINSTALL_DELETE_ACL_MANUALLY') . '<br />';

		// remove Josetta access level
		/*
		 $query = $db->getQuery(true);
		$query->delete('#__viewlevels');
		$query->where( $db->quoteName( 'title') . '=' . $db->quote( 'Josetta'));
		$db->setQuery( $query);
		$db->execute();
		$error = $db->getErrorMsg();
		if ( !empty( $error)) {
		echo 'Error deleting josetta access level: ' . $error . '<br />';
		}
		
		// remove Josetta User group
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->delete('#__usergroups');
		$query->where( $db->quoteName( 'title') . '=' . $db->quote( 'Josetta'));
		$db->setQuery( $query);
		$db->execute();
		$error = $db->getErrorMsg();
		if ( !empty( $error)) {
		echo 'Error deleting josetta user group: ' . $error . '<br />';
		}
		 */
	}

	/**
	 * Performs update to db stucture on existing setups
	 */
	private function _updateDatabase()
	{

		// get a db instance
		$db = JFactory::getDBO();

		/* version 1.0.4: now storing language in #__josetta_associations
		 * to allow better display (filtering) when showing items list to translators
		
		CREATE TABLE IF NOT EXISTS `#__josetta_associations` (
		    `id` VARCHAR(50) NOT NULL COMMENT 'A reference to the associated item.',
		    `context` VARCHAR(50) NOT NULL COMMENT 'The context of the josetta associated item.',
		    `key` CHAR(32) NOT NULL COMMENT 'The key for the josetta association computed from an md5 on associated ids.',
		    `language` char(7) NOT NULL,
		    PRIMARY KEY `idx_context_id` (`context`, `id`),
		    INDEX `idx_key` (`key`),
		    INDEX `idx_language` (`language`)
		) DEFAULT CHARSET=utf8;
		
		 */
		try
		{
			$list = $db->getTableList();
			$fullTable = str_replace('#__', $db->getPrefix(), '#__josetta_associations');
			if (!in_array($fullTable, $list))
			{
				JFactory::getApplication()->enqueueMessage('Returning josetta assoc table not found ' . $fullTable . ' in ' . print_r($list, true));
				return true;
			}
		}
		catch (Exception $e)
		{
			JFactory::getApplication()->enqueueMessage('Install error: ' . $e->getMessage());
			return false;
		}

		// get list of columns
		if (method_exists($db, 'getTableFields'))
		{
			$columns = $db->getTableFields('#__josetta_associations');
			$columns = empty($columns['#__josetta_associations']) ? array() : $columns['#__josetta_associations'];
		}
		else
		{
			$columns = $db->getTableColumns('#__josetta_associations');
		}

		// build required statements
		$subQueries = array();

		if (empty($columns['language']))
		{
			$subQueries[] = "add `language` char(7) NOT NULL";
		}

		if (!empty($subQueries))
		{
			// run query
			$this->_runDBStructureUpdateQuery('#__josetta_associations', $subQueries);

			// fill up language column
			$this->_runDBLanguageDataUpdateQuery('#__josetta_associations');
		}

		/* version 1.0.4: now storing language in #__josetta_metadata
		 * to allow better display (filtering) when showing items list to translators
		
		CREATE TABLE IF NOT EXISTS `#__josetta_metadata` (
		    `id` int(11) NOT NULL AUTO_INCREMENT,
		    `context` varchar(20) NOT NULL,
		    `item_id` int(11) NOT NULL,
		    `completion` int(11) NOT NULL,
		    `language` char(7) NOT NULL,
		    PRIMARY KEY (`id`),
		    INDEX `idx_language` (`language`)
		) DEFAULT CHARSET=utf8 ;
		
		 */

		// get list of columns
		if (method_exists($db, 'getTableFields'))
		{
			$columns = $db->getTableFields('#__josetta_metadata');
			$columns = empty($columns['#__josetta_metadata']) ? array() : $columns['#__josetta_metadata'];
		}
		else
		{
			$columns = $db->getTableColumns('#__josetta_metadata');
		}

		// build required statements
		$subQueries = array();

		if (empty($columns['language']))
		{
			$subQueries[] = "add `language` char(7) NOT NULL";
		}

		if (!empty($subQueries))
		{
			// run query
			$this->_runDBStructureUpdateQuery('#__josetta_metadata', $subQueries);

			// fill up language column
			$this->_runDBLanguageDataUpdateQuery('#__josetta_metadata', $itemIdFieldName = 'item_id');
		}

		try
		{

			// run fix: prior to 1.2.3, there might be some orphaned
			// rows in associations tables, which cause issues
			// when running some queries
			$menuItemsAssociationsRecords = ShlDbHelper::selectAssocList('#__associations');
			// second fix: if some rows where created with com_menus_item and at the same time, in Joomla backend
			// the same menu item was also associated, then when com_menus_item is replaced with com_menus.item, this causes
			// a "duplicate PK" error. We need to identify this situation and delete the faulty lines
			if (!empty($menuItemsAssociationsRecords))
			{
				$indexedList = array();
				foreach ($menuItemsAssociationsRecords as $record)
				{
					if (empty($indexedList[$record['key']]))
					{
						$indexedList[$record['key']] = array();
					}
					$indexedList[$record['key']][] = $record;
				}
				foreach ($indexedList as $key => $keyedRecords)
				{

					// only one record for a key? bad: that's an orphaned record
					if (count($keyedRecords) == 1)
					{
						ShlDbHelper::delete('#__associations', array('id' => $keyedRecords[0]['id']));
					}
					else
					{
						foreach ($keyedRecords as $keyedRecordIndex => $keyedRecord)
						{
							if ($keyedRecord['context'] == 'com_menus_item')
							{
								// a wrong context: we must either
								// - change it to com_menus.item
								// - or delete it if there is already a record with
								// - the correct context
								$action = 'update';
								foreach ($keyedRecords as $index => $lookupRecord)
								{
									if ($index == $keyedRecordIndex)
									{
										// same record, jump to next
										continue;
									}
									if ($lookupRecord['id'] == $keyedRecord['id'])
									{
										// must delete our record, there's already one with the
										// correct context
										$action = 'delete';
										break;
									}
								}
								if ($action == 'delete')
								{
									// delete record
									ShlDbHelper::delete('#__associations', array('id' => $keyedRecord['id'], 'context' => 'com_menus_item'));
								}
								else
								{
									// update context in record
									ShlDbHelper::update('#__associations', array('id' => $keyedRecord['id'], 'context' => 'com_menus.item'),
										array('id' => $keyedRecord['id'], 'context' => 'com_menus_item'));
								}
							}
						}
					}
				}
			}

			// read fixed data
			$menuItemsAssociationsRecords = ShlDbHelper::selectAssocList('#__associations');

			// Copy existing menu items associations from Joomla associations table to ours
			if (!empty($menuItemsAssociationsRecords))
			{
				if (file_exists(JPATH_ROOT . '/components/com_josetta/helpers/helper.php'))
				{
					include_once JPATH_ROOT . '/components/com_josetta/helpers/helper.php';
					foreach ($menuItemsAssociationsRecords as $record)
					{
						$record['context'] = JosettaHelper::joomla2Context($record['context']);
						$recordExists = ShlDbHelper::count('#__josetta_associations', 'id',
							array('context' => $record['context'], 'id' => $record['id']));
						if (empty($recordExists))
						{
							ShlDbHelper::insertUpdate('#__josetta_associations', $record);
						}
					}

					// and make sure we're also adding language column
					$this->_runDBLanguageDataUpdateQuery('#__josetta_associations');
				}
			}

			// 1.2.0 various tables added. Cannot use Joomla sql update, as we cannot create a record in the #__schemas table!!
			$queries = array(
				"CREATE TABLE IF NOT EXISTS `#__josetta_jf_imports` (
          `id` int(11) NOT NULL auto_increment,
          `state` tinyint(3) unsigned NOT NULL default '0',
          `error_message` VARCHAR(255) NOT NULL default '',
          `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
          `created_by` int(10) unsigned NOT NULL DEFAULT '0',
          `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
          `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
          PRIMARY KEY  (`id`)
      ) DEFAULT CHARSET=utf8 ;",
				"CREATE TABLE IF NOT EXISTS `#__josetta_jf_journal` (
          `id` int(11) NOT NULL auto_increment,
          `import_id` int(11) NOT NULL default '0',
          `language` char(7) NOT NULL default '',
          `imported_context` VARCHAR(50) NOT NULL,
          `imported_id` VARCHAR(50) NOT NULL,
          `created_context` VARCHAR(50) NOT NULL,
          `created_id` VARCHAR(50) NOT NULL,
          `state` tinyint(3) unsigned NOT NULL default '0',
          `error_message` VARCHAR(255) NOT NULL default '',
          PRIMARY KEY  (`id`),
          INDEX `idx_import_id` (`import_id`)
      ) DEFAULT CHARSET=utf8 ;",
				"CREATE TABLE IF NOT EXISTS `#__josetta_jf_sections` (
          `jta_id` int(11) NOT NULL auto_increment,
          `jta_import_id` int(11) NOT NULL default '0',
          `jta_language` char(7) NOT NULL,
          `id` int(11) NOT NULL default '0',
          `title` varchar(255) NOT NULL default '',
          `name` varchar(255) NOT NULL default '',
          `alias` varchar(255) NOT NULL default '',
          `image` TEXT NOT NULL default '',
          `scope` varchar(50) NOT NULL default '',
          `image_position` varchar(30) NOT NULL default '',
          `description` text NOT NULL,
          `published` tinyint(1) NOT NULL default '0',
          `checked_out` int(11) unsigned NOT NULL default '0',
          `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
          `ordering` int(11) NOT NULL default '0',
          `access` tinyint(3) unsigned NOT NULL default '0',
          `count` int(11) NOT NULL default '0',
          `params` text NOT NULL,
          PRIMARY KEY  (`jta_id`),
          KEY `idx_id` (`id`),
          KEY `idx_scope` (`scope`)
      ) DEFAULT CHARSET=utf8 ;",
				"CREATE TABLE IF NOT EXISTS `#__josetta_jf_categories` (
          `jta_id` int(11) NOT NULL auto_increment,
          `jta_import_id` int(11) NOT NULL default '0',
          `jta_language` char(7) NOT NULL,
          `id` int(11) NOT NULL default '0',
          `parent_id` int(11) NOT NULL default 0,
          `title` varchar(255) NOT NULL default '',
          `name` varchar(255) NOT NULL default '',
          `alias` varchar(255) NOT NULL default '',
          `image` varchar(255) NOT NULL default '',
          `section` varchar(50) NOT NULL default '',
          `image_position` varchar(30) NOT NULL default '',
          `description` text NOT NULL,
          `published` tinyint(1) NOT NULL default '0',
          `checked_out` int(11) unsigned NOT NULL default '0',
          `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
          `editor` varchar(50) default NULL,
          `ordering` int(11) NOT NULL default '0',
          `access` tinyint(3) unsigned NOT NULL default '0',
          `count` int(11) NOT NULL default '0',
          `params` text NOT NULL,
          PRIMARY KEY  (`jta_id`),
          KEY `idx_id` (`id`),
          KEY `cat_idx` (`section`,`published`,`access`),
          KEY `idx_access` (`access`),
          KEY `idx_checkout` (`checked_out`)
      ) DEFAULT CHARSET=utf8 ;",
				"CREATE TABLE IF NOT EXISTS `#__josetta_jf_content` (
          `jta_id` int(11) NOT NULL auto_increment,
          `jta_import_id` int(11) NOT NULL default '0',
          `jta_language` char(7) NOT NULL,
          `id` int(11) NOT NULL default '0',
          `title` varchar(255) NOT NULL default '',
          `alias` varchar(255) NOT NULL default '',
          `title_alias` varchar(255) NOT NULL default '',
          `introtext` mediumtext NOT NULL,
          `fulltext` mediumtext NOT NULL,
          `state` tinyint(3) NOT NULL default '0',
          `sectionid` int(11) unsigned NOT NULL default '0',
          `mask` int(11) unsigned NOT NULL default '0',
          `catid` int(11) unsigned NOT NULL default '0',
          `created` datetime NOT NULL default '0000-00-00 00:00:00',
          `created_by` int(11) unsigned NOT NULL default '0',
          `created_by_alias` varchar(255) NOT NULL default '',
          `modified` datetime NOT NULL default '0000-00-00 00:00:00',
          `modified_by` int(11) unsigned NOT NULL default '0',
          `checked_out` int(11) unsigned NOT NULL default '0',
          `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
          `publish_up` datetime NOT NULL default '0000-00-00 00:00:00',
          `publish_down` datetime NOT NULL default '0000-00-00 00:00:00',
          `images` text NOT NULL,
          `urls` text NOT NULL,
          `attribs` text NOT NULL,
          `version` int(11) unsigned NOT NULL default '1',
          `parentid` int(11) unsigned NOT NULL default '0',
          `ordering` int(11) NOT NULL default '0',
          `metakey` text NOT NULL,
          `metadesc` text NOT NULL,
          `access` int(11) unsigned NOT NULL default '0',
          `hits` int(11) unsigned NOT NULL default '0',
          `metadata` TEXT NOT NULL DEFAULT '',
          PRIMARY KEY  (`jta_id`),
          KEY `idx_id` (`id`),
          KEY `idx_section` (`sectionid`),
          KEY `idx_access` (`access`),
          KEY `idx_checkout` (`checked_out`),
          KEY `idx_state` (`state`),
          KEY `idx_catid` (`catid`),
          KEY `idx_createdby` (`created_by`)
      ) DEFAULT CHARSET=utf8 ;",
				"CREATE TABLE IF NOT EXISTS `#__josetta_jf_content_frontpage` (
          `jta_id` int(11) NOT NULL auto_increment,
          `jta_import_id` int(11) NOT NULL default '0',
          `jta_language` char(7) NOT NULL,
          `content_id` int(11) NOT NULL default '0',
          `ordering` int(11) NOT NULL default '0',
          PRIMARY KEY  (`jta_id`),
          KEY `idx_id` (`content_id`)
      ) DEFAULT CHARSET=utf8 ;",
				"CREATE TABLE IF NOT EXISTS `#__josetta_jf_menu_types` (
          `jta_id` int(11) NOT NULL auto_increment,
          `jta_import_id` int(11) NOT NULL default '0',
          `jta_language` char(7) NOT NULL,
          `id` INTEGER UNSIGNED NOT NULL default '0',
          `menutype` VARCHAR(75) NOT NULL DEFAULT '',
          `title` VARCHAR(255) NOT NULL DEFAULT '',
          `description` VARCHAR(255) NOT NULL DEFAULT '',
          PRIMARY KEY  (`jta_id`),
          KEY `idx_id` (`id`)
      ) DEFAULT CHARSET=utf8 ;",
				"CREATE TABLE IF NOT EXISTS `#__josetta_jf_menu_items` (
          `jta_id` int(11) NOT NULL auto_increment,
          `jta_import_id` int(11) NOT NULL default '0',
          `jta_language` char(7) NOT NULL,
          `id` int(11) NOT NULL default '0',
          `menutype` varchar(75) default NULL,
          `name` varchar(255) default NULL,
          `alias` varchar(255) NOT NULL default '',
          `link` text,
          `type` varchar(50) NOT NULL default '',
          `published` tinyint(1) NOT NULL default 0,
          `parent` int(11) unsigned NOT NULL default 0,
          `componentid` int(11) unsigned NOT NULL default 0,
          `sublevel` int(11) default 0,
          `ordering` int(11) default 0,
          `checked_out` int(11) unsigned NOT NULL default 0,
          `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
          `pollid` int(11) NOT NULL default 0,
          `browserNav` tinyint(4) default 0,
          `access` tinyint(3) unsigned NOT NULL default 0,
          `utaccess` tinyint(3) unsigned NOT NULL default 0,
          `params` text NOT NULL,
          `lft` int(11) unsigned NOT NULL default 0,
          `rgt` int(11) unsigned NOT NULL default 0,
          `home` INTEGER(1) UNSIGNED NOT NULL DEFAULT 0,
          PRIMARY KEY  (`jta_id`),
          KEY `idx_id` (`id`),
          KEY `componentid` (`componentid`,`menutype`,`published`,`access`),
          KEY `menutype` (`menutype`)
      ) DEFAULT CHARSET=utf8 ;",
				"CREATE TABLE IF NOT EXISTS `#__josetta_jf_components` (
          `jta_id` int(11) NOT NULL auto_increment,
          `jta_import_id` int(11) NOT NULL default '0',
          `jta_language` char(7) NOT NULL,
          `id` int(11) NOT NULL default '0',
          `name` varchar(50) NOT NULL default '',
          `link` varchar(255) NOT NULL default '',
          `menuid` int(11) unsigned NOT NULL default '0',
          `parent` int(11) unsigned NOT NULL default '0',
          `admin_menu_link` varchar(255) NOT NULL default '',
          `admin_menu_alt` varchar(255) NOT NULL default '',
          `option` varchar(50) NOT NULL default '',
          `ordering` int(11) NOT NULL default '0',
          `admin_menu_img` varchar(255) NOT NULL default '',
          `iscore` tinyint(4) NOT NULL default '0',
          `params` text NOT NULL,
          `enabled` tinyint(4) NOT NULL default '1',
          PRIMARY KEY  (`jta_id`),
          KEY `idx_id` (`id`),
          KEY `parent_option` (`parent`, `option`(32))
      ) DEFAULT CHARSET=utf8 ;");

			if ('free' == 'full')
			{
				foreach ($queries as $query)
				{
					$db->setQuery($query);
					$db->execute();
					$error = $db->getErrorNum();
					if (!empty($error))
					{
						$app = JFactory::getApplication();
						$app->enqueueMessage('Error while upgrading the database: ' . $db->getErrorMsg(true));
					}
				}
			}
		}
		catch (Exception $e)
		{
			$app = JFactory::getApplication();
			$app
				->enqueueMessage(
					'Error while upgrading the database: ' . $e->getMessage()
					. '.<br />Josetta will probably not operate properly. Please uninstall it, then try again after checking your database server setup. Contact us in case this happens again.',
					'error');
		}
	}

	/**
	 * Run a set of SQL instructions agains a table in one row
	 *
	 * @param string $table
	 * @param array $subQueries array of strings, containing valid SQL
	 */
	private function _runDBStructureUpdateQuery($table, $subQueries)
	{
		if (!empty($subQueries))
		{
			// get a db instance
			$db = JFactory::getDBO();

			// aggregate sub-queries
			$subQueries = implode(', ', $subQueries);

			// prepend query
			$query = 'alter table ' . $db->quoteName($table) . $subQueries;

			// run query
			$db->setQuery($query);
			$db->execute();
			$error = $db->getErrorNum();
			if (!empty($error))
			{
				$app = JFactory::getApplication();
				$app
					->enqueueMessage(
						'Error while upgrading the database structure : ' . $db->getErrorMsg(true)
						. '. Josetta will probably not operate properly. Please uninstall it, then try again after checking your database server setup. Contact us in case this happens again.');
			}
		}
	}

	/**
	 * Update metadata and association tables data so that new language
	 * column is filled up correctly
	 *
	 * @param string $table
	 * @param string the name of the item id field to use to identify an item
	 */
	private function _runDBLanguageDataUpdateQuery($table, $itemIdFieldName = 'id')
	{
		try
		{
			$updatedCounter = 0;

			// structure has been updated, however, we need to update existing records
			// also, and update them to the new format
			// 1 . Read any record with no language field
			$records = ShlDbHelper::selectAssocList($table, '*', array('language' => ''));

			// 2 - Iterate over records, reading using their type and id
			// to read the original record from db
			if (!empty($records))
			{
				foreach ($records as $record)
				{
					switch ($record['context'])
					{
						case 'categories_item':
							$idFieldName = 'id';
							$dataTable = '#__categories';
							break;
						case 'com_contact_item':
							$idFieldName = 'id';
							$dataTable = '#__contact_details';
							break;
						case 'com_content_item':
							$idFieldName = 'id';
							$dataTable = '#__content';
							break;
						case 'com_menus_item':
							$idFieldName = 'id';
							$dataTable = '#__menu';
							break;
						case 'module_item':
							$idFieldName = 'id';
							$dataTable = '#__modules';
							break;
						case 'com_newsfeeds_item':
							$idFieldName = 'id';
							$dataTable = '#__newsfeeds';
							break;
						case 'com_weblinks_item':
							$idFieldName = 'id';
							$dataTable = '#__weblinks';
							break;
					}

					// only update if record is for one of the known content type
					if (!empty($dataTable))
					{
						// we now know where to look for the language value of that record. Read it
						$record['language'] = ShlDbHelper::selectResult($dataTable, 'language', array($idFieldName => $record[$itemIdFieldName]));

						// update the original record and save it
						ShlDbHelper::update($table, $record, array('context' => $record['context'], 'id' => $record['id']));

						$updatedCounter++;
					}
				}

				// show something
				if (!empty($updatedCounter))
				{
					$app = JFactory::getApplication();
					$app
						->enqueueMessage(
							'Updated ' . $updatedCounter . ' records from ' . $table
							. ' table in database to new structure required for this new version.');
				}
			}
		}
		catch (Exception $e)
		{
			$app = JFactory::getApplication();
			$app
				->enqueueMessage(
					'Error while upgrading the database (language) : ' . $e->getMessage()
					. '.<br />Josetta will probably not operate properly. Please uninstall it, then try again after checking your database server setup. Contact us in case this happens again.',
					'error');
		}
	}

	/**
	 * Install all plugins available in a given group
	 * (c) weeblr, llc 2011
	 *
	 * @param string $group name of group
	 * @return boolean, true if success
	 */
	private function _shInstallPluginGroup($group)
	{

		$app = JFactory::getApplication();

		$sourcePath = JPATH_ADMINISTRATOR . '/components/com_josetta/plugins/' . $group;
		if (!JFolder::exists($sourcePath))
		{
			$app->enqueueMessage('Trying to install empty plugin group: ' . $group);
			return true;
		}

		// as each plugin resides in its own subDir, we must iterate over all sub dirs
		$folderList = JFolder::folders($sourcePath);
		if (empty($folderList))
		{
			$app->enqueueMessage('Trying to install empty plugin group, folder is empty: ' . $sourcePath);
			return true;
		}

		// process each plugin
		$errors = false;
		foreach ($folderList as $folder)
		{
			// install the plugin itself
			$status = $this->_shInstallPlugin($group, $folder, $sourcePath, $enable = true);
			// set flag if an error happened, but keep installing
			// other plugins
			$errors = $errors && $status;
			// also display status
			if (!$status)
			{
				$app->enqueueMessage('Error installing Josetta plugin from ' . $folder);
			}
		}

		// return true if no error at all
		return $errors == false;
	}

	/**
	 * Actually install an individual plugin into a Joomla! site
	 * (c) weeblr, llc 2011
	 *
	 * @param string $group exact name of the group this plugin belongs to
	 * @param string $name exact name of the plugin, ie the element field in the extensions db table
	 * @param string $basePath full path to the install file dir
	 * @param boolean $enable if true, the plugin will be enabled after install
	 */
	private function _shInstallPlugin($group, $name, $sourcePath, $enable = true)
	{

		if (in_array($name, $this->_skipInstall))
		{
			return true;
		}

		$app = JFactory::getApplication();

		// first read if plugin is already installed, we'll need that later
		// to decide if we allow ourselves to enable it automatically
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('extension_id')->from('#__extensions')->where('type =' . $db->Quote('plugin'))->where('folder = ' . $db->Quote($group))
		      ->where('element =' . $db->Quote($name));
		$installed = $db->setQuery($query)->loadResult();

		if ($error = $db->getErrorMsg())
		{
			JError::raiseWarning(500, $error);
			return false;
		}

		// if already installed, we can't override the "enabled" status,
		// user may have disabled plugins for good reasons
		$enable = empty($installed) && $enable;

		// use J! installer to fully install the plugin
		$installer = new JInstaller;
		$result = $installer->install($sourcePath . '/' . $name);

		if (!$result)
		{
			$app->enqueueMessage('Error installing Josetta plugin: ' . $group . ' / ' . $name);
		}
		else if ($enable)
		{
			$result = $this->_shEnablePlugin($group, $name);
		}

		return $result;
	}

	/**
	 * Enable a plugin
	 *
	 * @param string $group exact name of the group this plugin belongs to
	 * @param string $name exact name of the plugin, ie the element field in the extensions db table
	 */
	private function _shEnablePlugin($group, $name)
	{

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->update('#__extensions')->set('enabled=' . $db->Quote('1'))->where('type =' . $db->Quote('plugin'))
		      ->where('folder = ' . $db->Quote($group))->where('element =' . $db->Quote($name));
		$plugin = $db->setQuery($query)->execute();

		if ($error = $db->getErrorMsg())
		{
			JError::raiseWarning(500, $error);
			return false;
		}

		return true;
	}

	/**
	 * Actually install an individual plugin into a Joomla! site
	 * (c) weeblr, llc 2011
	 *
	 * @param string $name exact name of the plugin, ie the element field in the extensions db table
	 * @param string $basePath full path to the install file dir
	 * @param integer id in the extensions table
	 */
	private function _shInstallTemplate($name, $sourcePath)
	{

		$app = JFactory::getApplication();

		// workaround for J! 2.5.1 template installer issue
		// As of 2012-02-19, this has already been fixed in J! svn
		// Hopefully, this should not cause any side effect
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->quoteName('extension_id'));
		$query->from($db->quoteName('#__extensions'));
		$query->where($db->quoteName('type') . ' = ' . $db->quote('template'));
		$query->where($db->quoteName('element') . ' = ' . $db->quote($name));
		$db->setQuery($query);

		// use J! installer to fully install the plugin
		$installer = new JInstaller;
		$result = $installer->install($sourcePath . '/' . $name);

		return $result;
	}

	/**
	 *
	 * Uninstall all plugins from a given plugin group
	 * (c) weeblr, llc 2011
	 *
	 * @param string $group exact name of a plugin group
	 */
	private function _shUninstallPluginGroup($group)
	{
		$app = JFactory::getApplication();

		// required joomla library
		jimport('joomla.plugin.helper.php');

		// import the plugin files
		$pluginsList = $this->_shGetPluginsList($group);

		if (empty($pluginsList))
		{
			$app->enqueueMessage('Trying to uninstall empty plugin group, folder is empty: ' . $group);
			return true;
		}

		// process each plugin
		$errors = false;
		foreach ($pluginsList as $plugin)
		{
			// install the plugin itself
			$status = $this->_shUninstallPlugin($plugin);
			// set flag if an error happened, but keep installing
			// other plugins
			$errors = $errors && $status;
		}

		// return true if no error at all
		return $errors == false;
	}

	/**
	 * Return an object representing the plugin
	 * described by a folder and name
	 */
	private function _getPlugin($group, $name)
	{
		$plugins = $this->_shGetPluginsList($group);
		if (!empty($plugins))
		{
			foreach ($plugins as $plugin)
			{
				if ($plugin->name == $name)
				{
					return $plugin;
				}
			}
		}

		return null;
	}

	/**
	 *
	 * Performs uninstall of a given plugin
	 * (c) weeblr, llc 2011
	 *
	 * @param object $plugin record read from DB of the plugin details in the #__extensions table
	 */
	private function _shUninstallPlugin($plugin)
	{
		// use J! installer to fully uninstall the plugin
		$installer = new JInstaller;
		$result = $installer->uninstall('plugin', $plugin->id);

		if (!$result)
		{
			echo 'Error uninstalling Josetta plugin: ' . $plugin->type . ' / ' . $plugin->name . '<br />';
		}

		return $result;
	}

	/**
	 *
	 * Performs uninstall of a given template
	 *
	 * (c) weeblr, llc 2011
	 *
	 * @param object template record read from DB of the plugin details in the #__extensions table
	 */
	private function _shUninstallTemplate($templateId)
	{
		// use J! installer to fully uninstall the plugin
		$installer = new JInstaller;
		$result = $installer->uninstall('template', $templateId);

		if (!$result)
		{
			echo 'Error uninstalling atomic_josetta template<br/>';
		}

		return $result;
	}

	private function _shInstallModule($module, $source)
	{
		$app = JFactory::getApplication();

		$path = $source . '/' . $module;
		$installer = new JInstaller;
		$result = $installer->install($path);

		if (!$result)
		{
			$app->enqueueMessage('Error installing Josetta admin module: ' . $module);
		}
		return $result;
	}

	/**
	 *
	 * Performs uninstall of a given plugin
	 * (c) weeblr, llc 2011
	 *
	 * @param object $plugin record read from DB of the plugin details in the #__extensions table
	 */
	private function _shUninstallModule($moduleId)
	{
		// use J! installer to fully uninstall the module
		$installer = new JInstaller;
		$result = $installer->uninstall('module', $moduleId);

		if (!$result)
		{
			echo 'Error installing Josetta admin module: <br />';
		}

		return $result;
	}

	/**
	 * Must read plugins list from db, JPluginHelper will only read
	 * published plugins
	 * (c) weeblr, llc 2011
	 *
	 * @param string $group exact name of plugin group to read
	 */
	private function _shGetPluginsList($group)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('extension_id as id, folder AS type, element AS name, params')->from('#__extensions')->where('type =' . $db->Quote('plugin'))
		      ->where('folder = ' . $db->Quote($group));

		$plugins = $db->setQuery($query)->loadObjectList();

		if ($error = $db->getErrorMsg())
		{
			JError::raiseWarning(500, $error);
			return false;
		}

		return $plugins;
	}
}
