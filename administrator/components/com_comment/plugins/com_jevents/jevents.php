<?php
/**
 * @package    - com_comment
 * @author     : DanielDimitrov - compojoom.com
 * @date: 29.03.13
 *
 * @copyright  Copyright (C) 2008 - 2013 compojoom.com . All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */

defined('_JEXEC') or die('Restricted access');

class ccommentComponentJEventsPlugin extends ccommentComponentPlugin
{

	/**
	 * With this function we determine if the comment system should be executed for this
	 * content Item
	 * @return bool
	 */
	public function isEnabled()
	{
		$config = ccommentConfig::getConfig('com_jevents');
		$row = $this->row;

		$contentIds = $config->get('basic.exclude_content_items', array());
		$categories = $config->get('basic.categories', array());
		$include = $config->get('basic.include_categories', 0);

		/* doc id excluded ? */
		if (in_array((($row->_ev_id == 0) ? -1 : $row->_ev_id), $contentIds))
		{
			return false;
		}

		/*category included or excluded ?*/
		$result = in_array((($row->_catid == 0) ? -1 : $row->_catid), $categories);
		if (($include && !$result) || (!$include && $result))
		{
			return false;
		}

		/* include and not found OR exclude and found */

		return true;
	}

	/**
	 * This function decides whether to show the comments
	 * in an article/item or to show the readmore link
	 *
	 * If it returns true - the comments are shown
	 * If it returns false - the setShowReadon function will be called
	 *
	 * @param int - the content/item id
	 *
	 * @return boolean
	 */
	public function isSingleView()
	{
		$input = JFactory::getApplication()->input;
		$option = $input->getCmd('option', '');
		$view = $input->getCmd('view', '');
		$task = $input->getCmd('task', '');

		if ($option == 'com_jevents' && $view == 'day')
		{
			return true;
		}

		if ($option == 'com_jevents' && $task == "icalrepeat.detail")
		{
			return true;
		}

		return false;
	}

	/**
	 * This function determines whether to show the comment count or not
	 * @return bool
	 */
	public function showReadOn()
	{
		$config = ccommentConfig::getConfig('com_jevents');
		return $config->get('layout.show_readon');
	}

	public function getLink($contentId, $commentId = 0, $xhtml = true)
	{
		$add = '';
		// if we have a row - use the info in it

		if ($commentId)
		{
			$add = "#!/ccomment-comment=$commentId";
		}

		if (isset($this->row->alias))
		{
			$alias = ':' . $this->row->alias;
		}
		else
		{
			$alias = ':' . $this->getAlias($contentId);
		}
		$itemId = '&Itemid=' . ccommentHelperUtils::getItemId('com_jevents');

		$url = JRoute::_('index.php?option=com_jevents&task=icalrepeat.detail&evid=' . $contentId . $alias . $itemId, $xhtml) . $add;


		return $url;
	}

	private function getAlias($id)
	{
		$db = JFactory::getDBO();
		$query = 'SELECT summary FROM ' . $db->qn('#__jevents_vevdetail')
			. ' WHERE evdet_id = ' . $db->Quote($id);
		$db->setQuery($query, 0, 1);
		return $db->loadObject()->summary;
	}

	/**
	 * Returns the id of the author of an item
	 *
	 * @param int $contentId
	 *
	 * @return mixed
	 */
	public function getAuthorId($contentId)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('created_by')->from('#__jevents_vevent')
			->where('ev_id = ' . $db->q($contentId));

		$db->setQuery($query, 0, 1);
		$author = $db->loadObject();
		if ($author)
		{
			return $author->created_by;
		}
		return false;
	}

	public function getItemTitles($ids)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('evdet_id as id , summary as title')->from('#__jevents_vevdetail')
			->where('evdet_id IN (' . implode(',', $ids) . ')');

		$db->setQuery($query);
		return $db->loadObjectList('id');
	}
}