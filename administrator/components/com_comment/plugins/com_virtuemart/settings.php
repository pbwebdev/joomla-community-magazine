<?php
/**
 * @author Daniel Dimitrov - compojoom.com
 * @date: 18.02.13
 *
 * @copyright  Copyright (C) 2008 - 2013 compojoom.com . All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */

defined('_JEXEC') or die('Restricted access');


class ccommentComponentVirtuemartSettings extends ccommentComponentSettings
{
	/**
	 * categories option list used to display the include/exclude category list in setting
	 * must return an array of objects (id,title)
	 *
	 * @return array() - associative array (id, title)
	 */
	public function getCategories()
	{
		$database = JFactory::getDBO();

		$query 	= 'SELECT ' . $database->qn('virtuemart_category_id') . ' AS id , '
			. $database->qn('category_name') . 'AS title'
			. ' FROM ' . $database->qn('#__virtuemart_categories_en_gb');
		$database->setQuery( $query );
		$catoptions = $database->loadObjectList();

		return $catoptions;
	}

}