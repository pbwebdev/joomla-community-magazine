<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

defined('_JEXEC') or die;

/**
 * This layout only insert javascript to close a modal windows
 */

if (empty($displayData->refreshAfter))
{
	$timeout = 1500;
}
else if ($displayDaya->refreshAfter == 'now')
{
	$timeout = 0;
}
else
{
	$timeout = $displayData->refreshAfter;
}

// where to send parent?
$refreshTo = empty($displayData->refreshTo) ? 'window.parent.location.href' : $displayData->refreshTo;

// modal title
$modalTitle = empty($displayData->modalTitle) ? JText::_('COM_JOSETTA_PLEASE_WAIT', true) : JText::_($displayData->modalTitle, true);

// close a modal window
if (empty($timeout))
{
	JFactory::getDocument()->addScriptDeclaration('window.parent.location.href=' . $refreshTo);
}
else
{
	JFactory::getDocument()
		->addScriptDeclaration(
			'
			shlBootstrap.setModalTitleFromModal("' . $modalTitle . '");
			setTimeout( function() {
			window.parent.location.href=window.parent.location.href;
				}, ' . $timeout . ');
		');
}
