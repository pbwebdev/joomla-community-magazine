<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   weeblr, llc (c) 2014
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.2.1.567
 * @date		2014-01-16
 */

defined('_JEXEC') or die;

/**
 * This layout displays message or error, insde a bootstrap alert box
 */

$content = '<a title="' . JText::_('COM_JOSETTA_TRANSLATE_SUGGEST_HELP')
			. '" class="josetta_button hasTip" onclick="Josetta.suggestTranslation(\'' . $displayData['field']->title . '\', \'' . $displayData['language'] . '\', '
			. (strtolower($displayData['field']->type) == 'editor' ? "'editor'" : "'text'") . ', \'' . JSession::getFormToken() . '\');">'
			. JText::_('COM_JOSETTA_TRANSLATE_SUGGEST') . '</a>';

echo $content;