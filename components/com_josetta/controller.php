<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */
 
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

/**
 * Josetta Component Controller
 *
 * @package		Joomla.Site
 * @subpackage	com_josetta
 * @since 1.5
 */

class JosettaController extends ShlMvcController_Base
{
        /**
	 * @var		string	The default view for the display method.
	 *
	 * @since	0.0.1
	 *
	 * @see		JController::$default_view
	 */
        protected $default_view = 'translator';
}