<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.controller');

/**
 * Josetta component controller.
 *
 * @package		Joomla.Site
 * @subpackage	com_josetta
 */

class JosettaControllerTranslate extends ShlMvcController_Base
{
	protected $_jtaNamespace = 'jform';

	/**
	 * Override for access control
	 * @see JController::display()
	 */
	public function display($cachable = false, $urlparams = false)
	{
		$app = JFactory::getApplication();
		// check permissions
		$language = $app->input->getCmd('jlang');
		$authorized = JosettaHelper_Acl::authorize('josetta.canTranslate.' . $language);
		if (!$authorized)
		{
			$targetUrl = JRoute::_('index.php?option=com_josetta&view=noauth&Itemid=' . JosettaHelper::getJosettaItemid(), false);
			$app->redirect($targetUrl);
		}

		parent::display($cachable, $urlparams);
	}

	/**
	 *
	 */
	public function selectExistingContent()
	{
		JSession::checkToken('get') or jexit('Invalid Token');

		$app = JFactory::getApplication();

		// check permissions
		$language = $app->input->getCmd('jlang');
		$authorized = JosettaHelper_Acl::authorize('josetta.canTranslate.' . $language);
		if (!$authorized)
		{
			$targetUrl = JRoute::_('index.php?option=com_josetta&view=noauth&Itemid=' . JosettaHelper::getJosettaItemid(), false);
			JFactory::getApplication()->redirect($targetUrl);
		}

		// before using a pre-existing content element, we're going to check
		// if it's not already associated with another piece of content
		// as this would be the start of a big mess
		$existingContentId = $app->input->getInt($language . '_exist_content_id');
		if (!empty($existingContentId))
		{
			$model = ShlMvcModel_Base::getInstance('translate', 'JosettaModel');

			// first check Josetta association table
			$alreadyAssociatedItems = $model->getAssociatedItemsList($existingContentId);
			if (!empty($alreadyAssociatedItems))
			{
				// which item are we editing?
				$cid = $app->input->getVar('cid', array(0), null, 'array');
				$currentlyEditedItemId = (int) $cid[0];
				$sourceLanguage = $app->input->getCmd('source_language');

				if (!empty($alreadyAssociatedItems[$sourceLanguage]) && $alreadyAssociatedItems[$sourceLanguage]->id != $currentlyEditedItemId)
				{

					// no good, the requested item is associated with something, and its not the currently translated item
					// error out
					//load definitions
					$context = $model->getState('type');
					$definitions = JosettaHelper::getJosettaDefinitions($context);
					$titleColumnName = (string) $definitions->reference->title->column;
					$title = $alreadyAssociatedItems[$sourceLanguage]->$titleColumnName;
					$app
						->enqueueMessage(
							JText::sprintf('COM_JOSETTA_ERROR_CANNOT_USE_EXISTING_ALREADY_ASSOCIATED', $alreadyAssociatedItems[$sourceLanguage]->id,
								$title), 'error');
					$targetLanguage = $app->input->getCmd('source_language');
					$targetUrl = JRoute::_(
						'index.php?option=com_josetta&view=combo&cid[]=' . $currentlyEditedItemId . '&jlang=' . $targetLanguage . '&Itemid='
							. JosettaHelper::getJosettaItemid(), false);
					$app->redirect($targetUrl);
				}
			}

			// the do the same with Joomla own association table, in case some
			// associations was added manually from the backend
			$alreadyAssociatedItems = $model->getAssociatedItemsList($existingContentId, null, '#__associations');
			if (!empty($alreadyAssociatedItems))
			{
				// which item are we editing?
				$cid = $app->input->getVar('cid', array(0), null, 'array');
				$currentlyEditedItemId = (int) $cid[0];
				$sourceLanguage = $app->input->getCmd('source_language');

				if (!empty($alreadyAssociatedItems[$sourceLanguage]) && $alreadyAssociatedItems[$sourceLanguage]->id != $currentlyEditedItemId)
				{

					// no good, the requested item is associated with something, and its not the currently translated item
					// error out
					//load definitions
					$context = $model->getState('type');
					$definitions = JosettaHelper::getJosettaDefinitions($context);
					$titleColumnName = (string) $definitions->reference->title->column;
					$title = $alreadyAssociatedItems[$sourceLanguage]->$titleColumnName;
					$otherItemTitle = $alreadyAssociatedItems[$targetLanguage]->$titleColumnName;
					$app
						->enqueueMessage(
							JText::sprintf('COM_JOSETTA_ERROR_DISCREPANCY_ITEM_ALREADY_ASSOCIATED_IN_JOOMLA',
								$alreadyAssociatedItems[$sourceLanguage]->id, $title, $currentlyEditedItemId, $otherItemTitle), 'error');
					$targetLanguage = $app->input->getCmd('jlang');
					$targetUrl = JRoute::_(
						'index.php?option=com_josetta&view=combo&cid[]=' . $currentlyEditedItemId . '&jlang=' . $targetLanguage . '&Itemid='
							. JosettaHelper::getJosettaItemid(), false);
					$app->redirect($targetUrl);
				}
			}
		}

		return parent::display();
	}

	/**
	 * Dissociate a translated element from its orginal item
	 */
	public function dissociate()
	{
		JSession::checkToken() or jexit('Invalid Token');

		$app = JFactory::getApplication();
		// check permissions
		$data = JosettaHelper::getInputArray($app->input, 'post');
		$targetLanguage = empty($data['target_language']) ? '' : $data['target_language'];
		$this->_jtaNamespace = $targetLanguage . '_josetta_form';
		$authorized = JosettaHelper_Acl::authorize('josetta.canTranslate.' . $targetLanguage);
		if (!$authorized)
		{
			$targetUrl = JRoute::_('index.php?option=com_josetta&view=noauth&Itemid=' . JosettaHelper::getJosettaItemid(), false);
			$app->redirect($targetUrl);
		}

		// which item are we editing?
		$currentlyEditedItemId = empty($data[$this->_jtaNamespace]['id']) ? 0 : (int) $data[$this->_jtaNamespace]['id'];

		// if not saved yet, cannot dissociate!
		if (empty($currentlyEditedItemId))
		{
			JosettaHelper::enqueueMessages(JText::sprintf('COM_JOSETTA_ERROR_NEED_TO_SAVE_BEFORE_DISSOCIATING'));
		}
		else
		{
			// use model to dissociate
			$model = ShlMvcModel_Base::getInstance('translate', 'JosettaModel');
			$dissociated = $model->dissociate($data, $currentlyEditedItemId);

			if ($dissociated)
			{
				// redirect to translator page
				$title = empty($data[$this->_jtaNamespace]['title']) ? '' : ' - ' . $data[$this->_jtaNamespace]['title'];
				JosettaHelper::enqueueMessages(
					JText::sprintf('COM_JOSETTA_ITEM_WAS_DISSOCIATED', JosettaHelper::getLanguageTitle($targetLanguage), $currentlyEditedItemId,
						$title, $data['type']), 'message');
				$targetUrl = JRoute::_('index.php?option=com_josetta&view=translator&Itemid=' . JosettaHelper::getJosettaItemid(), false);
				JFactory::getApplication()->redirect($targetUrl);
			}
			else
			{
				$errors = $model->getErrors();
				JosettaHelper::enqueueMessages($errors);
				JFactory::getApplication()->enqueueMessage(JText::_('COM_JOSETTA_ERROR_UNABLE_TO_DISSOCIATE'), 'error');
			}
		}

		// redirect to translate page
		$targetUrl = JRoute::_(
			'index.php?option=com_josetta&view=translate&cid[]=' . $data[$this->_jtaNamespace]['ref_id'] . '&jlang=' . $targetLanguage . '&Itemid='
				. JosettaHelper::getJosettaItemid(), false);
		JFactory::getApplication()->redirect($targetUrl);

	}

	/**
	 * Method to redirect controller to translate edit screen.
	 *
	 * @return	void
	 * @since	1.6
	 */
	public function edit()
	{

		$this->display();

	}

	/**
	 * Method to save a josetta item.
	 *
	 * @return	void
	 */
	public function save()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit('Invalid Token');

		$app = JFactory::getApplication();
		$task = $app->input->getVar('task', '');
		$data = JosettaHelper::getInputArray($app->input, 'post');

		// check permissions
		$targetLanguage = empty($data['target_language']) ? '' : $data['target_language'];
		$authorized = JosettaHelper_Acl::authorize('josetta.canTranslate.' . $targetLanguage);
		if (!$authorized)
		{
			$targetUrl = JRoute::_('index.php?option=com_josetta&view=noauth&Itemid=' . JosettaHelper::getJosettaItemid(), false);
			$app->redirect($targetUrl);
		}

		$this->_jtaNamespace = $targetLanguage . '_josetta_form';

		//store the data in session variables to show the same data with error screen
		// or when user clicked Apply, rather than Save and close
		$app->setUserState("com_josetta.translator.translate.data", $data[$this->_jtaNamespace]);

		//initialize variable
		$model = ShlMvcModel_Base::getInstance('translate', 'JosettaModel');
		$saved = $model->save($data);

		//if the data show validation errors

		//if model show error
		$errors = $model->getErrors();
		// Push up to three validation messages out to the user.
		for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++)
		{
			if ($errors[$i] instanceof Exception)
			{
				$app->enqueueMessage($errors[$i]->getMessage(), 'error');
			}
			else
			{
				$app->enqueueMessage($errors[$i], 'error');
			}
		}
		if ($saved === false)
		{
			//set the message
			$this->setMessage(JText::sprintf('COM_JOSETTA_TRANSLATION_SAVE_FAILED', $model->getError()), 'error');

			//Redirect back to edit screen
			$url = 'index.php?option=com_josetta&view=translate&type=' . $data['type'] . '&jlang=' . $targetLanguage . '&cid[]='
				. $data[$this->_jtaNamespace]['ref_id'];
			if (($data[$targetLanguage . '_exist_content_id'] != 0))
			{
				// user was trying to use an existing content, let's pass again that information
				$url .= '&' . $targetLanguage . '_exist_content_id=' . $data[$targetLanguage . '_exist_content_id'];
			}

			$this->setRedirect(JRoute::_($url . '&Itemid=' . JosettaHelper::getJosettaItemid(), false));
			return false;
		}

		// prepare redirect to next page, with message
		$this->setMessage(JText::_('COM_JOSETTA_TRANSLATION_SAVE_SUCCESS'));

		// clear the session data
		$app->setUserState('com_josetta.translator.translate.data', array());

		if ($task == 'save')
		{
			// redirect to main translator page if user asked to "Close"
			$this
				->setRedirect(
					JRoute::_('index.php?option=com_josetta&view=translator&type=' . $data['type'] . '&Itemid=' . JosettaHelper::getJosettaItemid(),
						false));

		}
		else if ($task == 'apply')
		{
			// Stay on same page if user clicked Save
			$this
				->setRedirect(
					JRoute::_(
						'index.php?option=com_josetta&view=translate&type=' . $data['type'] . '&jlang=' . $targetLanguage . '&cid='
							. $data[$this->_jtaNamespace]['ref_id'] . '&Itemid=' . JosettaHelper::getJosettaItemid(), false));
		}
	}
	/**
	 * Method to save a josetta item when user click on apply
	 *
	 * @return	void
	 */

	public function apply()
	{
		//Redirect to save method
		$this->save();
	}

	public function cancel()
	{
		$app = JFactory::getApplication();

		//empty the session data, to clear entry forms
		$app->setUserState('com_josetta.translator.translate.data', array());

		// then go to main translator page
		$this->setRedirect(JRoute::_('index.php?option=com_josetta&view=translator&Itemid=' . JosettaHelper::getJosettaItemid(), false));

	}

}
