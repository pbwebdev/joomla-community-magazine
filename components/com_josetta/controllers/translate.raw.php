<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.controller');

/**
 * Josetta component controller.
 *
 * @package		Joomla.Site
 * @subpackage	com_josetta
 */

class JosettaControllerTranslate extends ShlMvcController_Base
{
	/**
	 * Fetch translation suggestions from external service
	 * and return it as json
	 */
	public function suggest()
	{
		// CRSF
		JSession::checkToken('get') or jexit('Invalid Token');

		$suggestion = '';
		$app = JFactory::getApplication();

		// get input
		$sourceLanguage = $app->input->getCmd('src', 'en-GB');
		$targetLanguage = $app->input->getCmd('tgt', 'en-GB');

		//no need if same language
		if ($sourceLanguage == $targetLanguage)
		{
			$this->_output($suggestion);
		}

		// type of text to be translated (plain vs html)
		$type = $app->input->getString('type');

		// text to be translated: allow HTML if text/html type
		$rawText  = $app->input->post->getRaw('txt');
		// use a custom Filter, with 2 lasts params set to 1, otherwise the ->input->getHtml() filters out
		// all tags whatever the user is entitled to.
		$htmlFilter = JFilterInput::getInstance(array(),array(), 1, 1);
		$text = $type == 'text/html' ? $htmlFilter->clean($rawText) : $app->input->getString('txt');

		// ask dedicated model to perform translation
		$model = $model = ShlMvcModel_Base::getInstance('suggest' . JosettaadminHelper_General::getVersionType(), 'JosettaModel');
		$translation = $model->translate($sourceLanguage, $targetLanguage, $text, $type);

		// done
		$this->_output($translation);
	}

	protected function _output($response)
	{
		// send header
		if ($response->status != 200)
		{
			$this->_sendHeader($response->status);
		}
		echo json_encode($response);

		// Send the response.
		JFactory::getApplication()->close();
	}

	/**
	 * Sends out response header, according to http status code
	 * @param unknown_type $code
	 */
	protected function _sendHeader($code)
	{
		$header = ShlSystem_Http::getHeader($code, '');
		if (!headers_sent())
		{
			header($header->raw);
		}
	}

}
