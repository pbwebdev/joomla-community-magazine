<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.controller');

/**
 * Josetta component controller.
 *
 * @package		Joomla.Site
 * @subpackage	com_josetta
 */

class JosettaControllerTranslate extends ShlMvcController_Base
{

	protected $_jtaNamespace = 'jform';

	/**
	 * Override for access control
	 * @see JController::display()
	 */
	public function display($cachable = false, $urlparams = false)
	{
		$app = JFactory::getApplication();
		// check permissions
		$language = $app->input->getCmd('jlang');
		$authorized = JosettaHelper_Acl::authorize('josetta.canTranslate.' . $language);
		if (!$authorized)
		{
			$targetUrl = JRoute::_('index.php?option=com_josetta&view=noauth&Itemid=' . JosettaHelper::getJosettaItemid(), false);
			$app->redirect($targetUrl);
		}

		parent::display($cachable, $urlparams);
	}

	/**
	 * Dissociate a translated element from its orginal item
	 */
	public function dissociate()
	{
		JSession::checkToken() or jexit('Invalid Token');

		$app = JFactory::getApplication();
		// check permissions
		$data = JosettaHelper::getInputArray($app->input, 'post');
		$targetLanguage = empty($data['target_language']) ? '' : $data['target_language'];
		$this->_jtaNamespace = $targetLanguage . '_josetta_form';
		$authorized = JosettaHelper_Acl::authorize('josetta.canTranslate.' . $targetLanguage);
		if (!$authorized)
		{
			$targetUrl = JRoute::_('index.php?option=com_josetta&view=noauth&Itemid=' . JosettaHelper::getJosettaItemid(), false);
			$app->redirect($targetUrl);
		}

		// which item are we editing?
		$currentlyEditedItemId = empty($data[$this->_jtaNamespace]['id']) ? 0 : (int) $data[$this->_jtaNamespace]['id'];

		// if not saved yet, cannot dissociate!
		if (empty($currentlyEditedItemId))
		{
			JosettaHelper::enqueueMessages(JText::sprintf('COM_JOSETTA_ERROR_NEED_TO_SAVE_BEFORE_DISSOCIATING'));
		}
		else
		{
			// use model to dissociate
			$model = ShlMvcModel_Base::getInstance('translate', 'JosettaModel');
			$dissociated = $model->dissociate($data, $currentlyEditedItemId);

			if ($dissociated)
			{
				// redirect to translator page
				$title = empty($data[$this->_jtaNamespace]['title']) ? '' : ' - ' . $data[$this->_jtaNamespace]['title'];
				JosettaHelper::enqueueMessages(
					JText::sprintf('COM_JOSETTA_ITEM_WAS_DISSOCIATED', JosettaHelper::getLanguageTitle($targetLanguage), $currentlyEditedItemId,
						$title, $data['type']), 'message');
				$targetUrl = JRoute::_('index.php?option=com_josetta&view=translator&Itemid=' . JosettaHelper::getJosettaItemid(), false);
				$app->redirect($targetUrl);
			}
			else
			{
				$errors = $model->getErrors();
				JosettaHelper::enqueueMessages($errors);
				$app->enqueueMessage(JText::_('COM_JOSETTA_ERROR_UNABLE_TO_DISSOCIATE'), 'error');
			}
		}

		// redirect to translate page
		$targetUrl = JRoute::_(
			'index.php?option=com_josetta&view=translate&cid[]=' . $data[$this->_jtaNamespace]['ref_id'] . '&jlang=' . $targetLanguage . '&Itemid='
				. JosettaHelper::getJosettaItemid(), false);
		$app->redirect($targetUrl);
	}

	/**
	 * Method to redirect controller to translate edit screen.
	 *
	 * @return	void
	 * @since	1.6
	 */
	public function edit()
	{
		$this->display();
	}

	/**
	 * Method to save a josetta item.
	 *
	 * @return	void
	 */
	public function save()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit('Invalid Token');

		$app = JFactory::getApplication();
		$task = $app->input->getCmd('task', '');
		$data = JosettaHelper::getInputArray($app->input, 'post');

		// check permissions
		$targetLanguage = empty($data['target_language']) ? '' : $data['target_language'];
		$authorized = JosettaHelper_Acl::authorize('josetta.canTranslate.' . $targetLanguage);
		if (!$authorized)
		{
			$targetUrl = JRoute::_('index.php?option=com_josetta&view=noauth&Itemid=' . JosettaHelper::getJosettaItemid(), false);
			$app->redirect($targetUrl);
		}

		$this->_jtaNamespace = $targetLanguage . '_josetta_form';

		//initialize variable
		$model = ShlMvcModel_Base::getInstance('translate', 'JosettaModel');
		$saved = $model->save($data);
		$savedId = $saved ? $model->getState('translatedSavedItemId') : 0;

		//if model show error
		$errors = $model->getErrors();

		// Push up to three validation messages out to the user.
		// the JsonResponse object will pick them up from the message queue
		for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++)
		{
			if ($errors[$i] instanceof Exception)
			{
				$app->enqueueMessage($errors[$i]->getMessage(), 'message');
			}
			else
			{
				$app->enqueueMessage($errors[$i], 'message');
			}
		}

		if ($saved === false)
		{
			$response = false;
			$error = true;
			$message = JText::_('COM_JOSETTA_TRANSLATION_SAVE_FAILED');
		}
		else
		{ // successfully saved item
			$response = $savedId;
			$error = false;
			$message = JText::_('COM_JOSETTA_TRANSLATION_SAVE_SUCCESS') . '  (' . $targetLanguage . ')';
			$app->enqueueMessage($message);
		}

		echo new JosettaHelper_Jsonresponse($response, $message, $error);
	}

	/**
	 * Method to save a josetta item when user click on apply
	 *
	 * @return	void
	 */

	public function apply()
	{
		//Redirect to save method
		$this->save();
	}

	public function cancel()
	{
		$app = JFactory::getApplication();

		//empty the session data, to clear entry forms
		$app->setUserState('com_josetta.translator.translate.data', array());

		// then go to main translator page
		$this->setRedirect(JRoute::_('index.php?option=com_josetta&view=translator&Itemid=' . JosettaHelper::getJosettaItemid(), false));
	}
}
