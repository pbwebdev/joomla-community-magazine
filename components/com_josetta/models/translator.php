<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.model');
jimport('joomla.application.component.helper');
jimport('joomla.application.component.modellist');


/**
 * Methods supporting a list of josetta context records.
 *
 * @package		com_josetta
 * @subpackage	com_josetta
 */
class JosettaModelTranslator extends ShlMvcModel_List
{

  protected $_extendedContext = null;

  /**
   * Method to auto-populate the model state.
   *
   * Note. Calling getState in this method will result in recursion.
   *
   * @return	void
   * @since	1.6
   */
  protected function populateState($ordering = null, $direction = null) {

    static $_previousExtendedContext = null;

    $app = JFactory::getApplication();

    $view = $app->input->getCmd('view');
    $this->setState( 'view', $view);

    // base context for storing items per page, start page, filters, etc
    $this->context = 'com_josetta_' . $view;

    // don't mix up modal boxes with regular lists
    $tmpl = $app->input->getCmd( 'tmpl');
    $this->context .= $tmpl == 'component' ? '_modal' : '';

    // get/store the object type for the view
    $type = $app->getUserStateFromRequest( $this->context . '.type', 'type', 'com_content_item','string');
    $this->setState('type', $type);

    // get/store all other display parameters in the view+object type context
    $search = $app->getUserStateFromRequest( $this->context . '+' . $type . '.filter_search', 'filter_search');
    $this->setState('filter_search', $search);

    $filter_order = $app->getUserStateFromRequest( $this->context . '+' . $type . '.filter_order', 'filter_order', 'id', 'cmd');
    $this->setState('filter_order', $filter_order);

    $filter_order_Dir = $app->getUserStateFromRequest( $this->context . '+' . $type . '.filter_order_Dir', 'filter_order_Dir', 'ASC', 'word');
    $this->setState('filter_order_Dir',  $filter_order_Dir);

    $filter_lang = $app->input->getCmd('filter_lang');
    $this->setState('filter_lang', $filter_lang);

    // only handle advanced filtering on our own translator page
    // but not on modal popups, which are used to display regular Joomla! content for 
    // 'Use existing content' feature
    if($tmpl != 'component') {

      // WARNING: getUserStateFromRequest will reset the limitstart value, in most cases, to 0, as we pass a default value (TRANSLATION_STATUS_ONLY_UNCOMPLETED)
      $filterTranslationStatus = $app->getUserStateFromRequest( $this->context . '+' . $type . '.filter_translation_status', 'filter_translation_status', JosettaHelper::TRANSLATION_STATUS_ONLY_UNCOMPLETED);
      $this->setState('filter_translation_status', $filterTranslationStatus);
      if(empty($filterTranslationStatus) || $filterTranslationStatus == JosettaHelper::TRANSLATION_STATUS_ONLY_UNCOMPLETED) {
        // if showing all items, regardless of completion, we don't select either on language
        $filterTranslationLanguage = 0;
        $this->setState('filter_translation_language_hide', 1);
      } else {
        $filterTranslationLanguage = $app->getUserStateFromRequest( $this->context . '+' . $type . '.filter_translation_language', 'filter_translation_language');
      }
      $this->setState('filter_translation_language', $filterTranslationLanguage);
    } else {
      $filterTranslationStatus = 0;
      $filterTranslationLanguage = 0;
    }

    //Load the defintion of xml of plugin
    $definitions = JosettaHelper::getJosettaDefinitions( $type);
    // extract filters from the definitions
    $filtersData = $definitions->filters->filter;

    // don't mix up modal boxes with regular lists
    // append each filter
    if(!empty ($filtersData)) {
      foreach( $filtersData as $filter) {
        $filterName = $type . '_' . (string) $filter->id;
        $value = $app->getUserStateFromRequest( $this->context . '+' . $type . '.' . $filterName, $filterName, '', 'string');
        $this->setState( $filterName, $value);
      }
    }

    // get extended context from plugins, ie: one that characterized the full set of filters
    // including those that are plugin-specific, so that we can tell
    // if pagination needs to be reset
    $this->getExtendedContext( $type, $search, $filterTranslationStatus, $filterTranslationLanguage);

    // list limits must be dependant on content type
    // get the extended context stored in session
    if(is_null( $_previousExtendedContext)) {
      // echo 'Extended context: ' . $this->_extendedContext . '<br />';
      $_previousExtendedContext = $app->getUserState( $this->context . '.list_extended_context', '');
      if( $_previousExtendedContext == $this->_extendedContext) {

        // echo 'Extended context unchanged: <b>' . $_previousExtendedContext . '</b><br />';
        // we are in same view, we can read list limit and start from session
        $limit = $app->getUserStateFromRequest( $this->_extendedContext . '.list.limit', 'limit', $app->getCfg('list_limit'));
        $limitstart = $app->getUserStateFromRequest( $this->_extendedContext . '.limitstart', 'limitstart', 0);
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
        // echo '--> Limit: '.$limit . ' - start: ' . $limitstart . '<br />';
        // Check if the ordering field is in the white list, otherwise use the incoming value.
        $orderingValue = $app->getUserStateFromRequest( $this->context . '+' . $type . '.ordercol', 'filter_order', $ordering);
        // Check if the ordering direction is valid, otherwise use the incoming value.
        $directionValue = $app->getUserStateFromRequest( $this->context . '+' . $type . '.orderdirn', 'filter_order_Dir', $direction);

      } else {
        // echo 'Extended context changed from <b>' . $_previousExtendedContext . '</b> to <b>' . $this->_extendedContext . '</b><br />';
        // we have switched from one type of view to another (maybe user changed one of the
        // filters values), we should reuse values stored in session, but not
        // values that may be hardcoded in the request (as form hidden fields)
        // as they relate to the previous page
        // we are in same view, we can read list limit and start from session
        $limit = $app->getUserState( $this->_extendedContext . '.list.limit', $app->getCfg('list_limit'));
        $limitstart = $app->getUserState( $this->_extendedContext . '.limitstart', 0);
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
        // echo '--> Limit: '.$limit . ' - start: ' . $limitstart . '<br />';
        // Check if the ordering field is in the white list, otherwise use the incoming value.
        $orderingValue = $app->getUserState( $this->context . '+' . $type . '.ordercol', 'filter_order', $ordering);
        // Check if the ordering direction is valid, otherwise use the incoming value.
        $directionValue = $app->getUserState( $this->context . '+' . $type . '.orderdirn', 'filter_order_Dir', $direction);

      }

      $this->setState('list.limit', $limit);
      $app->setUserState( $this->_extendedContext . '.list.limit', $limit);
      $this->setState('list.start', $limitstart);
      $app->setUserState( $this->_extendedContext . 'limitstart', $limitstart);
      if (!in_array($orderingValue, $this->filter_fields)) {
        $orderingValue = $ordering;
        $app->setUserState( $this->context . '+' . $type . '.ordercol', $orderingValue);
      }
      $this->setState('list.ordering', $orderingValue);
      if (!in_array(strtoupper($directionValue), array('ASC', 'DESC', ''))) {
        $directionValue = $direction;
        $app->setUserState( $this->context . '+' . $type . '.orderdirn', $directionValue);
      }
      $this->setState('list.direction', $directionValue);
    }

    $app->setUserState( $this->context . '.list_extended_context', $this->_extendedContext);

  }

  /**
   * Method to laod the list of  table coloumn from defination of <reference> tag from context.xml of josetta plugin.
   *
   ** @param      context type
   *
   * @return	JXMLElement Object
   * @since	1.6
   *
   */
  public function getReferenceFields($context ) {

    $definitions = JosettaHelper::getJosettaDefinitions( $this->getState('type'));

    //return child element of reference tag in xml
    return $definitions->reference;
  }

  /**
   * Method to get a list of articles.
   * Overridden to add a check for access levels.
   *
   * @return	mixed	An array of data items on success, false on failure.
   * @since	1.6.1
   */
  public function getItems() {

    // Get a storage key.
    $store = $this->getStoreId();

    // Try to load the data from internal storage.
    if (isset($this->cache[$store])) {
      return $this->cache[$store];
    }

    $context = $this->getState('type');
    JosettaHelper::loadPlugins();
    $dispatcher = JDispatcher::getInstance();

    // need to pass the view and the whole state (start, limit, ordering, direction) to the plugin
    $state = $this->getState();

    //call to onJosettaloaditemss plugin mtehod to load the xml
    $items = $dispatcher->trigger( 'onJosettaLoadItems', array( $context, $state));
    $items = is_array( $items) ? $items[0] :$items;

    // Add the items to the internal cache.
    $this->cache[$store] = $items;

    return $this->cache[$store];
  }

  /**
   * Method to get the total number of items for the data set.
   *
   * @return  integer  The total number of items available in the data set.
   *
   * @since   11.1
   */
  public function getTotal()
  {
    // Get a storage key.
    $store = $this->getStoreId('getTotal');

    // Try to load the data from internal storage.
    if (isset($this->cache[$store])) {
      return $this->cache[$store];
    }

    // Load the total.
    // need to pass the view and the whole state (start, limit, ordering, direction) to the plugin
    $state = $this->getState();

    // use model to load item list to be displayed
    $context = $this->getState('type');
    JosettaHelper::loadPlugins();
    $dispatcher = JDispatcher::getInstance();
    $items = $dispatcher->trigger( 'onJosettaGetItemsCount', array( $context, $state));
    $total = is_array( $items) ? $items[0] :$items;

    // Add the total to the internal cache.
    $this->cache[$store] = (int) $total;

    return $this->cache[$store];
  }

  /**
   * Ask the plugin for a string representing the full state of all filters
   * applied to the current view. Will be used to store limit, limit start
   *
   */
  public function getExtendedContext( $pluginType, $search, $filterTranslationStatus, $filterTranslationLanguage)
  {

    // Try to load the data from internal storage.
    if ( !isset($this->_extendedContext)) {
      $app = JFactory::getApplication();

      // base context
      $context = $this->context . '+' . $pluginType;

      // now add plugin-specific filters:

      //Load the defintion of xml of plugin
      $definitions = JosettaHelper::getJosettaDefinitions( $pluginType);

      // extract filters from the definitions
      $filtersData = $definitions->filters->filter;
       
      // append each filter
      $filtersContext = '';
      if(!empty ($filtersData)) {
        foreach( $filtersData as $filter) {
          $filterName = $pluginType . '_' . (string) $filter->id;
          $value = $app->input->getVar( $filterName, null);
          if(is_null($value)) {
            // we don't use directly getUserStateFromRequest() as this would store the request value
            // in state
            $value = $app->getUserState( $context . '.' . $filterName, '', 'string');
          }
          $filtersContext .= empty( $value) ? '' : ('+' . $filterName . '(' .(string)$value) . ')';
        }
      }

      $this->_extendedContext = $context . '_' . $search. '_' . $filterTranslationStatus. '_' . $filterTranslationLanguage . '_' . $filtersContext;
    }

    return $this->_extendedContext;
  }

}

