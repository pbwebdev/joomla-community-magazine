<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.model');
jimport('joomla.application.component.modelform');

/**
 * Translate Model
 *
 * @package		com_josetta
 * @subpackage	com_josetta
 */
class JosettaModelTranslate extends ShlMvcModel_Form
{
	protected $_jtaNamespace = 'jform';

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return	void
	 * @since	1.6
	 */

	protected function populateState()
	{
		$app = JFactory::getApplication();

		$option = $app->input->getCmd('option');
		$this->setState('option', $option);

		$view = $app->input->getCmd('view');
		$this->setState('view', $view);

		$type = $app->getUserStateFromRequest($option . $view . '.type', 'type', 'com_content_item', 'string');
		$this->setState('type', $type);

		$id = $app->input->getInt('id');
		$this->setState('id', $id);

		$cid = $app->input->getVar('cid', array(0), null, 'array');
		$this->setState('originalItemId', (int) $cid[0]);

		$jlang = $app->getUserStateFromRequest($option . $view . '.jlang', 'jlang');
		$this->setState('jlang', $jlang);

		//load context id used when user click on use existing state button
		$exist_content_id = $app->input->getInt($jlang . '_exist_content_id');
		$this->setState($jlang . '_exist_content_id', $exist_content_id);

		$dirty = $app->input->getVar('dirty', array(0), null, 'array');
		$this->setState('dirty', $dirty);

		// List state information.
		parent::populateState();
	}

	public function getForm($data = array(), $loadData = true)
	{

	}

	/**
	 * Load the content of the item to be translated, in the source language
	 *
	 * @return	object
	 */
	public function getOriginalItem()
	{
		//Load the xml defination from state
		$id = $this->getState('originalItemId');
		$item = null;
		if (!empty($id))
		{
			$item = $this->_loadJosettaItem($this->getState('type'), $id);
		}

		return $item;
	}

	/**
	 * Method to load field and data of original form .
	 *
	 * @param string $language language code for the language we are translating to
	 * @param	array	$data		Data for the form.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 *
	 * @return	mixed	A JForm object on success, false on failure
	 */

	public function getJosettaForm($targetLanguage, $data = array(), $loadData = true)
	{
		$this->_jtaNamespace = $targetLanguage . '_josetta_form';

		// this will be loaded with a content element id if the user has clicked
		// on the "Use existing content" button
		$existingContentId = $this->getState($targetLanguage . '_exist_content_id');

		$item = new stdClass();

		// If user has not requested any specific pre-existing content,
		// search for an item already associated with the one we want to translate
		// if not found, then translation starts from a clean sheet
		if (empty($existingContentId) || $targetLanguage != $this->getState('jlang'))
		{
			// Search for already associated element
			$translatedItemId = $this->getAssociatedItemId($this->getState('originalItemId'), $targetLanguage);
			if (!empty($translatedItemId))
			{
				// if any found, load its content
				$item = $this->_loadJosettaItem($this->getState('type'), $translatedItemId);
			}
		}
		else
		{
			// item user wants to use as a translation is not already associated with another one
			// we can load and use it
			$item = $this->_loadJosettaItem($this->getState('type'), $existingContentId);
		}

		// Build a xml string usable by Joomla! form library
		// that will be used to display an entry form
		// The xml is build using information supplied by the plugin
		// handling the item content type
		$formXml = $this->_createXml($item, $targetLanguage);

		// Get the form.
		$josettaForm = $this
			->loadForm('com_josetta.content.josettaitem_' . $targetLanguage, $formXml, array('control' => $this->_jtaNamespace, 'load_data' => true));

		if (empty($josettaForm))
		{
			return false;
		}

		// now attach data to that form. We first get the list of fields
		// then hidden fields are pulled from the original item
		// while other fields are pulled from current item (if we
		// are editing an existing translation) or left alone if
		// this is a new translation
		$originalItem = $this->getOriginalItem();
		$dispatcher = JDispatcher::getInstance();
		$fields = $dispatcher->trigger('onJosettaGetFields', array($this->getState('type'), $originalItem, $targetLanguage));
		$fields = empty($fields) ? null : $fields[0];

		foreach ($fields->fields as $someFields)
		{
			if (isset($someFields->fieldset))
			{
				// fields nested inside a fieldset

				// process each field
				foreach ($someFields->field as $field)
				{
					if ((string) $field->column != 'language' && (string) $field->translate == 'no')
					{
						// hidden field, let's pass original item data
						$fieldName = (string) $field->column;
						if (isset($originalItem->$fieldName))
						{
							$item->$fieldName = $originalItem->$fieldName;
						}
					}
				}

			}
			else
			{
				// a list of fields
				// process each field
				foreach ($someFields->field as $field)
				{
					if ((string) $field->column != 'language' && (string) $field->translate == 'no')
					{
						// hidden field, let's pass original item data
						$fieldName = (string) $field->column;
						if (isset($originalItem->$fieldName))
						{
							$item->$fieldName = $originalItem->$fieldName;
						}
					}
				}
			}
		}

		// pre-fill language in case of new translation
		if (empty($item->language))
		{
			$item->language = $targetLanguage;
		}

		$josettaForm->bind($item);

		return $josettaForm;
	}

	/**
	 * Method to get the create xml format using Josetta Plugin.
	 *
	 *
	 * @return	mixed	the jform xml for the form.
	 */
	private function _createXml($item, $targetLanguage)
	{
		// it is actually each plugin that creates the XML
		// (using the default code in plugin base class)
		// so as to be able to add custom fields
		// based on the edited item type

		$dispatcher = JDispatcher::getInstance();
		$xml = $dispatcher->trigger('onJosettaCreateFormXml', array($this->getState('type'), $item, $this->getOriginalItem(), $targetLanguage));
		$xml = empty($xml) ? null : $xml[0];

		return $xml;
	}

	/**
	 * Method to get the data that should be injected in the form from the plugin method.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */

	private function _loadJosettaItem($context, $id, $asArray = false)
	{
		//helper method to load josetta plugin
		JosettaHelper::loadPlugins();

		$items = array();
		$dispatcher = JDispatcher::getInstance();
		$items = $dispatcher->trigger('onJosettaLoadItem', array($context, $id));
		$item = empty($items) ? null : $items[0];

		if ($asArray)
		{
			jimport('joomla.utilities.arrayhelper');
			$item = JArrayHelper::fromObject($item, $recurse = false);
		}
		return $item;

	}

	/**
	 * Method to save the form data.
	 *
	 * TODO: put associations handling into its own model
	 *
	 * @param	array	The form data.
	 * @return	boolean	True on success.
	 */
	public function save($data)
	{

		//Get the data from controller post data is allowed
		$context = $data['type'];

		// compute the namespace of the form being saved
		$this->_jtaNamespace = $data['target_language'] . '_josetta_form';

		//check if user select the use existing button
		if (!empty($data[$data['target_language'] . '_exist_content_id']) && (int) $data[$data['target_language'] . '_exist_content_id'] != 0)
		{
			$data[$this->_jtaNamespace]['id'] = (int) $data[$data['target_language'] . '_exist_content_id'];
		}

		// Call the save method of the appropriate plugin, if any, to save the input data
		// helper method to load josetta plugin
		JosettaHelper::loadPlugins();
		$dispatcher = JDispatcher::getInstance();
		$saveErrors = array();
		$translatedSavedItemId = $dispatcher->trigger('onJosettaSaveItem', array($context, $data[$this->_jtaNamespace], &$saveErrors));
		$translatedSavedItemId = (int) $translatedSavedItemId[0];

		// display errors
		if (!empty($saveErrors))
		{
			foreach ($saveErrors as $error)
			{
				$this->setError($error);
			}
		}

		// In case of error, don't go further, msg has been enqueued for user
		if (empty($translatedSavedItemId))
		{
			//check  in controller if return save set error
			return false;
		}
		else
		{
			$this->setState('translatedSavedItemId', $translatedSavedItemId);
		}

		// as only language-specific content can be associated with each other,
		// we must make sure the original content has a language
		// if not (ie set to "All"), we set it to the default language.
		$originalItem = $this->_loadJosettaItem($context, $data[$this->_jtaNamespace]['ref_id'], $asArray = true);
		if (!empty($originalItem) && $originalItem['language'] == '*')
		{

			$originalItem['language'] = JosettaHelper::getSiteDefaultLanguage();
			$saveErrors = array();
			$originalSavedItemId = $dispatcher->trigger('onJosettaSaveItem', array($context, $originalItem, &$saveErrors));
			$originalSavedItemId = (int) $originalSavedItemId[0];

			// display errors
			if (!empty($saveErrors))
			{
				foreach ($saveErrors as $error)
				{
					$this->setError($error);
				}
			}

			// In case of error, don't go further, msg has been enqueued for user
			if (empty($originalSavedItemId))
			{
				//check  in controller if return save set error
				return false;
			}
		}

		// Item has been saved, now we create/update association data with the original language content item
		try
		{
			// Search for existing translations associated with the original item
			$associatedItems = $this->getAssociatedItemsList($data[$this->_jtaNamespace]['ref_id'], $context);

			// prepare an array of associated items using the format
			// used by Joomla to then compute the association key
			$associations = array();
			foreach ($associatedItems as $lang => $item)
			{
				$associations[$item->language] = $item->id;
			}

			// 1.0.4: as Joomla associations table don't have a language column, we stop using
			// it actively, but instead just duplicate our records into it
			$associationTable = '#__josetta_associations';

			if (!empty($associations))
			{
				// we already have some associated items
				// delete those existing association records, so that
				// we can later write a new set of associations, including
				// the new/updated translation we are storing
				ShlDbHelper::runQuotedQuery('delete from ?? where ?? = ? and ?? in (' . ShlDbHelper::arrayToIntvalList($associations) . ')',
					array($associationTable, 'context', 'id'), array($context));
			}
			// record for the original item
			$associations[$originalItem['language']] = $data[$this->_jtaNamespace]['ref_id'];

			// record for the newly translated item
			$associations[$data['target_language']] = $translatedSavedItemId;

			// Format associations array according to Joomla standard
			$associations = array_unique($associations);

			// key that characterizes all associated translated material
			$key = md5(json_encode($associations));

			// prepare to insert one record per translated item
			$db = ShlDbHelper::getDb();
			$query = $db->getQuery(true);
			$query->insert($associationTable);

			foreach ($associations as $tag => $id)
			{
				if (!empty($id))
				{
					$query->values($id . ',' . $db->quote($context) . ',' . $db->quote($key) . ',' . $db->quote($tag));
				}
			}

			try
			{
				$db->setQuery($query)->execute();
			}
			catch (Exception $e)
			{
				ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
				$this->setError($e->getMessage());
				return false;
			}

			// duplicate this data into the standard Joomla associations table
			if (JosettaHelper::hasJoomlaAssociation($context, $isJosettaContext = true))
			{
				$associationTable = '#__associations';

				// is the item being saved already associatied in Joomla! own table?
				$alreadyAssociatedItems = $this->getAssociatedItemsList($data[$this->_jtaNamespace]['ref_id'], $context, $associationTable);
				$updateAssociations = true;
				if (!empty($alreadyAssociatedItems))
				{
					// the item being saved might be associated with the same original item
					// in which case there is nothing to do
					if (!empty($alreadyAssociatedItems[$originalItem['language']])
						&& $alreadyAssociatedItems[$originalItem['language']]->id != $data[$this->_jtaNamespace]['ref_id'])
					{
						// error, this item is already associated with another, we can't delete that
						$definitions = JosettaHelper::getJosettaDefinitions($context);
						$titleColumnName = (string) $definitions->reference->title->column;
						$title = $alreadyAssociatedItems[$data['target_language']]->$titleColumnName;
						$otherItemTitle = $alreadyAssociatedItems[$originalItem['language']]->$titleColumnName;
						$this
							->setError(
								JText::sprintf('COM_JOSETTA_ERROR_DISCREPANCY_ITEM_ALREADY_ASSOCIATED_IN_JOOMLA',
									$data[$this->_jtaNamespace]['ref_id'], $title, $alreadyAssociatedItems[$originalItem['language']]->id,
									$otherItemTitle));
						$updateAssociations = false;
					}
					else
					{
						// regular case: delete existing associations, so that we can recreate them
						// including the newly translated item
						ShlDbHelper::runQuotedQuery('delete from ?? where ?? = ? and ?? in (' . ShlDbHelper::arrayToIntvalList($associations) . ')',
							array($associationTable, 'context', 'id'), array(JosettaHelper::context2Joomla($context)));
					}
				}
				if ($updateAssociations)
				{
					// item being saved was not associated with anything in Joomla table,
					// or we deleted those associations
					// just add the assocations information
					$query = $db->getQuery(true);
					$query->insert($associationTable);

					foreach ($associations as $tag => $id)
					{
						if (!empty($id))
						{
							$query->values($id . ',' . $db->quote(JosettaHelper::context2Joomla($context)) . ',' . $db->quote($key));
						}
					}

					try
					{
						$db->setQuery($query)->execute();
					}
					catch (Exception $e)
					{
						ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
						$this->setError($e->getMessage());
						return false;
					}

				}

			}
			// finally update some metadata about the translation (currently only completion state)

			// read if any metada is already in table
			$metadata = ShlDbHelper::selectObject('#__josetta_metadata', 'id', array('item_id' => $translatedSavedItemId, 'context' => $context));

			// Get an instance of the metadata table.
			$metadataTable = JTable::getInstance('metadata', 'josettatable');
			if (!empty($data[$data['target_language'] . '_exist_content_id']))
			{

				// if user has asked to simply use a pre-existing item as a translation
				if (!empty($metadata->id))
				{
					// and this item has some metadata recorded, delete them
					$metadataTable->delete($metadata->id);
				}

			}
			else if (!empty($metadata->id))
			{
				// if not using a pre-existing item (ie user has translated the item herself)
				// load any existing metadata, so that we can update it
				$metadataTable->load($metadata->id);
			}

			// update metadata record with current data
			$metadataRecord['item_id'] = $translatedSavedItemId;
			$metadataRecord['completion'] = $data[str_replace('-', '_', $data['target_language']) . '_completion'];
			$metadataRecord['context'] = $context;
			$metadataRecord['language'] = $data['target_language'];
			if (!$metadataTable->save($metadataRecord))
			{
				$error = $metadataTable->getError();
				ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $metadataTable->getError());
				$this->setError($metadataTable->getError());
				return false;
			}

			// final step: logging:
			// Saving a %s translation to %s for item %d (%s) as item %d (%s)"
			// and then read up the xml describing this content
			$definitions = JosettaHelper::getJosettaDefinitions($context);
			// compute a representative name for the original item
			$titleFieldName = (string) $definitions->reference->title->column;

			ShlSystem_Log::info('josetta', JText::_('COM_JOSETTA_LOG_TRANSLATION_SAVED'), $context, $data['target_language'],
				(int) $originalItem['id'], JString::substr($originalItem[$titleFieldName], 0, 40), $translatedSavedItemId,
				JString::substr($data[$this->_jtaNamespace][$titleFieldName], 0, 40));

		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			$this->setError($e->getMessage());
			return false;
		}

		return true;
	}

	/**
	 * Dissociate an existing translation from the original
	 * (translation is not deleted, only dissociated)
	 *
	 * @param	array	The form data.
	 * @param int id of currently edited item
	 *
	 * @return	boolean	True on success.
	 */
	public function dissociate($data, $currentlyEditedItemId)
	{
		// What type of content are we processing?
		$context = $data['type'];

		// find about existing associations for this item
		try
		{
			// Search for existing translations associated with the original item
			$associatedItems = $this->getAssociatedItemsList($currentlyEditedItemId, $context);

			if (empty($associatedItems))
			{
				// no associations? error out, cannot dissociate if not associated
				$this->setError(JText::_('COM_JOSETTA_ERROR_DISSOCIATING_COULD_NOT_FIND_ASSOCIATION'));
				return false;
			}

			// prepare an array of associated items using the format
			// used by Joomla to then compute the association key
			$associations = array();
			foreach ($associatedItems as $lang => $item)
			{
				$associations[$item->language] = $item->id;
			}

			// clone for future use
			$associationsIdList = $associations;

			// 1.0.4: as Joomla associations table don't have a language column, we stop using
			// it actively, but instead just duplicate our records into it
			$associationTable = '#__josetta_associations';

			if (!empty($associations))
			{

				// we already have some associated items
				// delete those existing association records, so that
				// we can later write a new set of associations, excluding
				// the one we want to dissociate
				ShlDbHelper::runQuotedQuery('delete from ?? where ?? = ? and ?? in (' . ShlDbHelper::arrayToIntvalList($associationsIdList) . ')',
					array($associationTable, 'context', 'id'), array($context));

			}

			// remove current item from the associations list
			unset($associations[$data['target_language']]);

			// if there was more than one translation
			// we have removed one, but we must keep the others
			// so let's write updated associations records
			if (count($associations) > 1)
			{

				// there were one or more other translations
				// we rebuild the new set of associations records
				// according to Joomla standard
				$associations = array_unique($associations);

				// key that characterizes all associated translated material
				$key = md5(json_encode($associations));

				// prepare to insert one record per translated item
				$db = ShlDbHelper::getDb();
				$query = $db->getQuery(true);
				$query->insert($associationTable);

				foreach ($associations as $tag => $id)
				{
					if (!empty($id))
					{
						$query->values($id . ',' . $db->quote($context) . ',' . $db->quote($key) . ',' . $db->quote($tag));
					}
				}

				try
				{
					$db->setQuery($query)->execute();
				}
				catch (Exception $e)
				{
					ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
					$this->setError($e->getMessage());
					return false;
				}
			}

			// if this is a menu item, we duplicate the same data into Joomla! own association table
			if (JosettaHelper::hasJoomlaAssociation($context, $isJosettaContext = true))
			{

				$associationTable = '#__associations';

				// we already have some associated items
				// delete those existing association records, so that
				// we can later write a new set of associations, excluding
				// the one we want to dissociate
				if (!empty($associations))
				{
					ShlDbHelper::runQuotedQuery(
						'delete from ?? where ?? = ? and ?? in (' . ShlDbHelper::arrayToIntvalList($associationsIdList) . ')',
						array($associationTable, 'context', 'id'), array(JosettaHelper::context2Joomla($context)));
				}

				// if there was more than one translation
				// we have removed one, but we must keep the others
				// so let's write updated associations records
				if (count($associations) > 1)
				{

					// key that characterizes all associated translated material
					$key = md5(json_encode($associations));

					// prepare to insert one record per translated item
					$db = ShlDbHelper::getDb();
					$query = $db->getQuery(true);
					$query->insert($associationTable);

					foreach ($associations as $tag => $id)
					{
						if (!empty($id))
						{
							$query->values($id . ',' . $db->quote(JosettaHelper::context2Joomla($context)) . ',' . $db->quote($key));
						}
					}

					try
					{
						$db->setQuery($query)->execute();
					}
					catch (Exception $e)
					{
						ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
						$this->setError($e->getMessage());
						return false;
					}

				}

			}

			// finally delete metadata about the translation (currently only completion state)

			// read if any metada is already in table
			$metadata = ShlDbHelper::delete('#__josetta_metadata', array('item_id' => $currentlyEditedItemId, 'context' => $context));

			// final step: logging:
			// %s translation %d%s (type %s) was dissociated from original item %d
			ShlSystem_Log::info('josetta', JText::_('COM_JOSETTA_LOG_TRANSLATION_DISSOCIATED'),
				JosettaHelper::getLanguageTitle($data['target_language']), $currentlyEditedItemId,
				JString::substr($data[$this->_jtaNamespace]['title'], 0, 40), $context, $data[$this->_jtaNamespace]['ref_id']);

		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			$this->setError($e->getMessage());
			return false;
		}

		return true;
	}

	/**
	 * Method to get the id of associated data.
	 *
	 * @param	id of the context to be translated.
	 * @param       language code en-GB
	 *
	 * @return      Itemid of associated item
	 * @return	Null if empty.
	 */
	public function getAssociatedItemId($originalId, $jlang)
	{

		// Read all items associated with the current one, if any
		$items = $this->getAssociatedItemsList($originalId);

		// now search for the associated items in the language we're looking for
		if (empty($jlang))
		{
			//if language is empty load the language of translation from state variables
			$jlang = $this->getState('jlang');
		}

		foreach ($items as $tag => $item)
		{
			if ($tag == $jlang)
			{
				//Return id of translated context
				return $item->id;
			}
		}

		return null;
	}

	/**
	 * Method to get the array of items of associated data.
	 *
	 * // TODO: remove storage in $_items entirely (now bypassed by $forceUpdate params)
	 * We can't store those items, as associations may vary during the page load
	 * while an item or a set of items are saved (during import of data for instance)
	 * Stroring associated items in a cache prevents returning up to date information
	 *
	 * @param	id of the context to be translated.
	 * @param       context type
	 *
	 * @return      array
	 */
	public function getAssociatedItemsList($originalId, $context = '', $table = '', $forceUpdate = true)
	{

		static $_items = array();

		if (empty($context))
		{
			//if context is empty load the context of translation from state variables
			$context = $this->getState('type');
		}

		// Association table name
		$associationTable = empty($table) ? '#__josetta_associations' : $table;
		$key = md5($originalId . $context . $associationTable);

		if (!isset($_items[$key]) || $forceUpdate)
		{
			//load definition
			$definitions = JosettaHelper::getJosettaDefinitions($context);

			// compensate for different context format
			if ($table == '#__associations' && JosettaHelper::hasJoomlaAssociation($context, $isJosettaContext = true))
			{
				$context = JosettaHelper::context2Joomla($context);
			}

			//load name of table where id is to be read from, from plugin definition
			$originalTable = (string) $definitions->table;

			//load definition of published information from xml tag
			$associations = array();
			$db = ShlDbHelper::getDb();
			$query = $db->getQuery(true);
			$query->from($originalTable . ' as t');
			$query->leftJoin($associationTable . ' as a ON a.id=t.id AND a.context=' . $db->quote($context));
			$query->leftJoin($associationTable . ' as a2 ON a.key=a2.key');
			$query->leftJoin($originalTable . ' as t2 ON a2.id=t2.id');
			$query->leftJoin('#__josetta_metadata as m on m.item_id=t2.id and m.context=' . $db->quote($context));
			$query->where('t.id=' . (int) $originalId);
			$query->select('t2.*');
			$query->select('m.completion');
			$db->setQuery($query);

			try
			{
				$items = $db->shlLoadObjectList('language');
			}
			catch (Exception $e)
			{
				ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
				JError::raiseWarning(500, $e->getMessage());
				return false;
			}

			// shumisha: temporary: clean up the result: if no association exists
			// this query still returns a record, with empty object properties
			// which then breaks everything
			$_items[$key] = array();
			if (!empty($items))
			{
				foreach ($items as $item)
				{
					if (!empty($item->language))
					{
						$_items[$key][$item->language] = clone $item;
					}
				}
			}
		}

		return $_items[$key];
	}

	/**
	 *  Method to get the amount of completion information
	 *  for one of the translations of a given, source language, item
	 *
	 * @params integer id id of item we're looking for translations completion information
	 * @params string $context descriptor of the item type
	 * @params string $languageCodethe language code of desired completion value
	 *
	 * @return integer
	 */
	public function getCompletionValue($id, $context, $languageCode)
	{
		$completion = 0;

		if (!empty($id))
		{
			$associatedItemId = $this->getAssociatedItemId($id, $languageCode);
			if (!empty($associatedItemId))
			{
				try
				{
					$item = ShlDbHelper::selectObject('#__josetta_metadata', 'completion',
						array('item_id' => $associatedItemId, 'context' => $context));
					$completion = empty($item) ? 0 : (int) $item->completion;
				}
				catch (Exception $e)
				{
					ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
				}
			}
		}
		return $completion;
	}
}

class JosettaModel_Translate extends JosettaModelTranslate
{

}
