<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

defined('JPATH_BASE') or die;

JFormHelper::loadFieldClass('list');
require_once JPATH_ADMINISTRATOR . '/components/com_menus/models/fields/menutype.php';

/**
 * Form Field class for the Joomla Framework.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_menus
 * @since		1.6
 */
class JFormFieldJosettaMenutype extends JFormFieldMenutype
{
  /**
   * The form field type.
   *
   * @var		string
   * @since	1.6
   */
  protected $type = 'Josettamenutype';

  /**
   * Method to get certain otherwise inaccessible properties from the form field object.
   *
   * @param   string  $name  The property name for which to the the value.
   *
   * @return  mixed  The property value or null.
   *
   */
  public function __get($name)
  {

    switch ($name)
    {
      case 'element':
        return $this->$name;
        break;
    }

    $value = parent::__get( $name);
    return $value;
  }

  /**
   * Method to get the field input markup.
   *
   * @return	string	The field input markup.
   * @since	1.6
   */
  protected function getInput()
  {
    // Initialise variables.
    $html 		= array();
    $recordId	= (int) $this->form->getValue('id');
    $size		= ($v = $this->element['size']) ? ' size="'.$v.'"' : '';
    $class		= ($v = $this->element['class']) ? ' class="'.$v.'"' : 'class="text_area"';

    // Get a reverse lookup of the base link URL to Title
    ShlMvcModel_Base::addIncludePath( JPATH_ADMINISTRATOR . '/components/com_menus/models');
    $model 	= ShlMvcModel_Base::getInstance('menutypes', 'menusModel');
    $rlu 	= $model->getReverseLookup();

    switch ($this->value)
    {
      case 'url':
        $value = JText::_('COM_MENUS_TYPE_EXTERNAL_URL');
        break;

      case 'alias':
        $value = JText::_('COM_MENUS_TYPE_ALIAS');
        break;

      case 'separator':
        $value = JText::_('COM_MENUS_TYPE_SEPARATOR');
        break;

      default:
        $link	= $this->form->getValue('link');
      // Clean the link back to the option, view and layout
      $value	= JText::_(JArrayHelper::getValue($rlu, MenusHelper::getLinkKey($link)));
      break;
    }
    // Load the javascript and css
    JHtml::_('behavior.framework');
    JHtml::_('behavior.modal');

    $html[] = '<input type="text" readonly="readonly" disabled="disabled" value="'.$value.'"'.$size.$class.' />';
    $html[] = '<input type="button" value="'.JText::_('JSELECT').'" onclick="SqueezeBox.fromElement(this, {handler:\'iframe\', size: {x: 600, y: 450}, url:\''.JRoute::_('index.php?option=com_menus&view=menutypes&tmpl=component&recordId='.$recordId).'\'})" />';
    $html[] = '<input type="hidden" name="'.$this->name.'" value="'.htmlspecialchars($this->value, ENT_COMPAT, 'UTF-8').'" />';

    return implode("\n", $html);
  }
}
