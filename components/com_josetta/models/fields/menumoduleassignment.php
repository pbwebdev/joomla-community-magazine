<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

defined('JPATH_PLATFORM') or die;

/**
 * Form Field class for the Joomla Platform.
 * Supports an HTML select list of categories
 *
 * @package     Joomla.Platform
 * @subpackage  Form
 * @since       11.1
 */
class JFormFieldMenumoduleassignment extends JFormField {


  public $type = 'MenuModuleAssignment';

  /**
   * Method to get certain otherwise inaccessible properties from the form field object.
   *
   * @param   string  $name  The property name for which to the the value.
   *
   * @return  mixed  The property value or null.
   *
   */
  public function __get($name)
  {

    switch ($name)
    {
      case 'element':
        return $this->$name;
        break;
    }

    $value = parent::__get( $name);
    return $value;
  }

  protected function getInput() {

    // Initialize related data.
    require_once JPATH_ADMINISTRATOR.'/components/com_menus/helpers/menus.php';
    require_once JPATH_ADMINISTRATOR.'/components/com_modules/helpers/modules.php';

    $assignmentValue = $this->form->getValue( 'assignment');
    $assignmentValue = is_null( $assignmentValue) ? '-' : $assignmentValue;
    $assignedValues = $this->form->getValue( 'assigned_data');
    $assignedValues = is_null( $assignedValues) ? array() : unserialize( $assignedValues);

    $languagesList = (string) $this->element['languages'];
    $languages = explode( ',', $languagesList);
    $menuTypes = MenusHelper::getMenuLinks($menuType = null, $parentId = 0, $mode = 0, $published=array(), $languages);

    $jsFormControl = str_replace('-', '_', $this->formControl);

    ob_start();
    ?>
    		<script type="text/javascript">
    			window.addEvent('domready', function(){
    				validate();
    				document.getElements('select').addEvent('change', function(e){validate();});
    			});
    			function validate(){
    				var value	= document.id('<?php echo $jsFormControl; ?>_assignment').value;
    				var list	= document.id('<?php echo $jsFormControl; ?>_menu-assignment');
    				if(value == '-' || value == '0'){
    					$$('.<?php echo $jsFormControl; ?>-assignments-button').each(function(el) {el.setProperty('disabled', true); });
    					list.getElements('input').each(function(el){
    						el.setProperty('disabled', true);
    						if (value == '-'){
    							el.setProperty('checked', false);
    						} else {
    							el.setProperty('checked', true);
    						}
    					});
    				} else {
    					$$('.<?php echo $jsFormControl; ?>-assignments-button').each(function(el) {el.setProperty('disabled', false); });
    					list.getElements('input').each(function(el){
    						el.setProperty('disabled', false);
    					});
    				}
    			}
    		</script>

    			<label id="<?php echo $jsFormControl; ?>_menus-lbl" for="<?php echo $jsFormControl; ?>_menus"><?php echo JText::_('COM_MODULES_MODULE_ASSIGN'); ?></label>

    			<fieldset id="<?php echo $jsFormControl; ?>_menus" class="radio">
    				<select name="<?php echo $this->formControl; ?>[assignment]" id="<?php echo $jsFormControl; ?>_assignment" onchange="Josetta.itemChanged('<?php echo $jsFormControl; ?>_menus-lbl');">
    					<?php
    					  echo JHtml::_('select.options', ModulesHelper::getAssignmentOptions( $clientId = 0), 'value', 'text', $assignmentValue, true);
    					?>
    				</select>

    			</fieldset>

    			<label id="<?php echo $jsFormControl; ?>_menuselect-lbl" for="<?php echo $jsFormControl; ?>_menuselect"><?php echo JText::_('COM_JOSETTA_MENU_SELECTION'); ?></label>

    			<button type="button" class="<?php echo $jsFormControl; ?>-assignments-button jform-rightbtn" onclick="$$('.<?php echo $jsFormControl; ?>chk-menulink').each(function(el) { el.checked = !el.checked; });Josetta.itemChanged('<?php echo $jsFormControl; ?>_menus-lbl');">
    				<?php echo JText::_('COM_JOSETTA_SELECTION_INVERT'); ?>
    			</button>

    			<button type="button" class="<?php echo $jsFormControl; ?>-assignments-button jform-rightbtn" onclick="$$('.<?php echo $jsFormControl; ?>chk-menulink').each(function(el) { el.checked = false; });Josetta.itemChanged('<?php echo $jsFormControl; ?>_menus-lbl');">
    				<?php echo JText::_('COM_JOSETTA_SELECTION_NONE'); ?>
    			</button>

    			<button type="button" class="<?php echo $jsFormControl; ?>-assignments-button jform-rightbtn" onclick="$$('.<?php echo $jsFormControl; ?>chk-menulink').each(function(el) { el.checked = true; });Josetta.itemChanged('<?php echo $jsFormControl; ?>_menus-lbl');">
    				<?php echo JText::_('COM_JOSETTA_SELECTION_ALL'); ?>
    			</button>

    			<div class="josetta_clr"></div>

    			<div id="<?php echo $jsFormControl; ?>_menu-assignment">

    			<?php echo JHtml::_('tabs.start', 'module-menu-assignment-tabs', array('useCookie'=>1));?>

    			<?php foreach ($menuTypes as &$type) :

    				$count 	= count($type->links);
    				$i		= 0;
    				if ($count) :

    				echo JHtml::_('tabs.panel', $type->title ? $type->title : $type->menutype, $type->menutype.'-details');
    				?>
    				<div class="josetta_clr"></div>

    				<ul class="menu-links">
    					<?php
    					foreach ($type->links as $link) :
    						if (trim($assignmentValue) == '-'):
    							$checked = '';
    						elseif ($assignmentValue == 0):
    							$checked = ' checked="checked"';
    						elseif ($assignmentValue < 0):
    							$checked = in_array(-$link->value, $assignedValues) ? ' checked="checked"' : '';
    						elseif ($assignmentValue > 0) :
    							$checked = in_array($link->value, $assignedValues) ? ' checked="checked"' : '';
    						endif;
    					?>
    					<li class="menu-link">
    						<input type="checkbox" class="<?php echo $jsFormControl; ?>chk-menulink" name="<?php echo $this->formControl; ?>[assigned][]" value="<?php echo (int) $link->value;?>" id="link<?php echo (int) $link->value;?>"<?php echo $checked;?> onchange="Josetta.itemChanged('<?php echo $jsFormControl; ?>_menus-lbl');"/>
    						<label for="link<?php echo (int) $link->value;?>">
    							<?php echo $link->text; ?>
    						</label>
    					</li>
    					<?php if ($count > 20 && ++$i == ceil($count/2)) :?>
    					</ul><ul class="menu-links">
    					<?php endif; ?>

    					<?php endforeach; ?>
    				</ul>

    				<?php endif; ?>

    			<?php endforeach; ?>

    			<?php echo JHtml::_('tabs.end');?>

    			</div>
   <?php
   $output = ob_get_contents();
   ob_end_clean();

   return $output;

  }

}
