<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

defined('JPATH_PLATFORM') or die;

JFormHelper::loadFieldClass( 'list');
JFormHelper::loadFieldClass( 'category');

/**
 * Form Field class for the Joomla Platform.
 * Supports an HTML select list of categories
 *
 * @package     Joomla.Platform
 * @subpackage  Form
 * @since       11.1
 */
class JFormFieldLanguageCategory extends JFormFieldCategory {


  public $type = 'LanguageCategory';

  /**
   * Method to get certain otherwise inaccessible properties from the form field object.
   *
   * @param   string  $name  The property name for which to the the value.
   *
   * @return  mixed  The property value or null.
   *
   */
  public function __get($name)
  {

    switch ($name)
    {
      case 'element':
        return $this->$name;
        break;
    }

    $value = parent::__get( $name);
    return $value;
  }

  /**
   * Method to get the field options for category
   * Use the extension attribute in a form to specify the.specific extension for
   * which categories should be displayed.
   * Use the show_root attribute to specify whether to show the global category root in the list.
   *
   * @return  array    The field option objects.
   *
   * @since   11.1
   */
  protected function getOptions()
  {
    // Initialise variables.
    $extension = $this->element['extension'] ? (string) $this->element['extension'] : (string) $this->element['scope'];
    $published = (string) $this->element['published'];
    $languages = (string) $this->element['languages'];
    $name = (string) $this->element['name'];

    // insert custom options passed in xml file
    $options = array();
    if(!is_null( $this->element->option)) {
      foreach( $this->element->option as $option) {
        $options[] = JHtml::_('select.option', $option->attributes()->value, JText::_((string) $option));
      }
    }

    // Load the category options for a given extension.
    if (!empty($extension)) {

      // Filter over published state or not depending upon if it is present.
      if ($published) {
        $categoriesoptions = JosettaHelper::getCategoryOptionsPerLanguage( $extension, array( 'filter.published' => explode(',', $published), 'filter.languages' => explode( ',', $languages)));
      } else {
        $categoriesoptions = JosettaHelper::getCategoryOptionsPerLanguage( $extension, array( 'filter.languages' => explode( ',', $languages)));
      }

      $options = array_merge( $options, $categoriesoptions);

      if (isset($this->element['show_root'])) {
        array_unshift($options, JHtml::_('select.option', '0', JText::_('JGLOBAL_ROOT')));
      }
    } else {
      JError::raiseWarning(500, JText::_('JLIB_FORM_ERROR_FIELDS_CATEGORY_ERROR_EXTENSION_EMPTY'));
    }

    // Merge any additional options in the XML definition.
    //$options = array_merge(parent::getOptions(), $options); // don't merge, that would read non desired categories. However, what else could come from "additional options in the XML definition"?

    return $options;
  }

}
