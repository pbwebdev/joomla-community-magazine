<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

defined('JPATH_BASE') or die;

JFormHelper::loadFieldClass('list');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_menus
 * @since		1.6
 */
class JFormFieldLanguageMenuParent extends JFormFieldList
{
  /**
   * The form field type.
   *
   * @var		string
   * @since	1.6
   */
  protected $type = 'LanguageMenuParent';

  /**
   * Method to get certain otherwise inaccessible properties from the form field object.
   *
   * @param   string  $name  The property name for which to the the value.
   *
   * @return  mixed  The property value or null.
   *
   */
  public function __get($name)
  {

    switch ($name)
    {
      case 'element':
        return $this->$name;
        break;
    }

    $value = parent::__get( $name);
    return $value;
  }

  /**
   * Method to get the field options.
   *
   * @return	array	The field option objects.
   * @since	1.6
   */
  protected function getOptions()
  {
    // Initialize variables.
    $options = array();
    $languages = (string) $this->element['languages'];

    $db = ShlDbHelper::getDb();
    $query = $db->getQuery(true);

    $query->select('a.id AS value, a.title AS text, a.level');
    $query->from('#__menu AS a');
    $query->join('LEFT', $db->quoteName('#__menu').' AS b ON a.lft > b.lft AND a.rgt < b.rgt');

    if ($menuType = $this->form->getValue('menutype')) {
      $query->where('a.menutype = '.$db->quote($menuType));
    }
    else {
      $query->where('a.menutype != '.$db->quote(''));
    }

    // only front end
    $query->where( 'a.client_id=0');

    // Prevent parenting to children of this item.
    if ($id = $this->form->getValue('id')) {
      $query->join('LEFT', $db->quoteName('#__menu').' AS p ON p.id = '.(int) $id);
      $query->where('NOT(a.lft >= p.lft AND a.rgt <= p.rgt)');
    }

    $query->where('a.published != -2');
    $query->group('a.id, a.title, a.level, a.lft, a.rgt, a.menutype, a.parent_id, a.published');
    $query->order('a.lft ASC');

    // added language filtering
    if ($this->element['languages']) {
      $languages = explode( ',', $this->element['languages']);
      $quotedLanguages = array();
      foreach( $languages as $language) {
        $quotedLanguages[] = $db->quote( $language);
      }
      if(!empty( $quotedLanguages)) {
        // NOTE: root category is allowed, despite it not having a language
        // as otherwise we wouldn't be able to translate a top level menu
        $query->where('a.language IN (' . implode(',', $quotedLanguages) . ')' . ' OR a.parent_id = 0');
      }
    }

    // Get the options.
    $db->setQuery($query);

    try {
      $options = $db->shlLoadObjectList();
    } catch (Exception $e) {
      ShlSystem_Log::error( 'josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
      JError::raiseWarning(500, $db->getErrorMsg());
      $options = array();
    }

    // Pad the option text with spaces using depth level as a multiplier.
    for ($i = 0, $n = count($options); $i < $n; $i++) {
      $options[$i]->text = str_repeat('- ', $options[$i]->level).$options[$i]->text;
      // fix translation
      if($options[$i]->level == 0) {
        $options[$i]->text = JText::_('COM_MENUS_ITEM_ROOT');
      }
    }
    return $options;
  }
}
