<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

defined('JPATH_BASE') or die;

/**
 * Supports a modal contact picker.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_contact
 * @since		1.6
 */
class JFormFieldModal_Contacts extends JFormField
{
  /**
   * The form field type.
   *
   * @var		string
   * @since	1.6
   */
  protected $type = 'Modal_Contacts';

  /**
   * Method to get the field input markup.
   *
   * @return	string	The field input markup.
   * @since	1.6
   */
  protected function getInput()
  {
    // Load the javascript
    JHtml::_('behavior.framework');
    JHtml::_('behavior.modal', 'a.modal');

    // Build the script.
    $script = array();
    $script[] = '	function jSelectChart_'.$this->id.'(id, name, object) {';
    $script[] = '		document.id("'.$this->id.'_id").value = id;';
    $script[] = '		document.id("'.$this->id.'_name").value = name;';
    $script[] = '		SqueezeBox.close();';
    $script[] = '	}';

    // Add the script to the document head.
    JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));

    // Get the title of the linked chart
    $db = ShlDbHelper::getDb();
    $db->setQuery(
        'SELECT name' .
        ' FROM #__contact_details' .
        ' WHERE id = '.(int) $this->value
    );

    try {
      $title = $db->shlLoadResult();
    } catch (Exception $e) {
      ShlSystem_Log::error( 'josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
      JError::raiseWarning(500, $e->getMessage());
    }
    
    if (empty($title)) {
      $title = JText::_('COM_CONTACT_SELECT_A_CONTACT');
    }

    $link = 'index.php?option=com_josetta&amp;view=contacts&amp;layout=modal&amp;Itemid='.JosettaHelper::getJosettaItemid().'&amp;tmpl=component&amp;function=jSelectChart_'.$this->id;

    $html = "\n".'<div class="fltlft"><input type="text" id="'.$this->id.'_name" value="'.htmlspecialchars($title, ENT_QUOTES, 'UTF-8').'" disabled="disabled" /></div>';
    $html .= '<div class="button2-left"><div class="blank"><a class="modal" title="'.JText::_('COM_CONTACT_CHANGE_CONTACT_BUTTON').'"  href="'.$link.'" rel="{handler: \'iframe\', size: {x: 900, y: 450}}">'.JText::_('COM_CONTACT_CHANGE_CONTACT_BUTTON').'</a></div></div>'."\n";
    // The active contact id field.
    if (0 == (int)$this->value) {
      $value = '';
    } else {
      $value = (int)$this->value;
    }

    // class='required' for client side validation
    $class = '';
    if ($this->required) {
      $class = ' class="required modal-value"';
    }

    $html .= '<input type="hidden" id="'.$this->id.'_id"'.$class.' name="'.$this->name.'" value="'.$value.'" />';

    return $html;
  }
}
