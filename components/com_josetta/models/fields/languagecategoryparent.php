<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

defined('JPATH_BASE') or die;

JFormHelper::loadFieldClass('list');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_categories
 * @since		1.6
 */
class JFormFieldLanguageCategoryParent extends JFormFieldList
{
  /**
   * The form field type.
   *
   * @var		string
   * @since	1.6
   */
  protected $type = 'LanguageCategoryParent';

  /**
   * Method to get certain otherwise inaccessible properties from the form field object.
   *
   * @param   string  $name  The property name for which to the the value.
   *
   * @return  mixed  The property value or null.
   *
   */
  public function __get($name)
  {

    switch ($name)
    {
      case 'element':
        return $this->$name;
        break;
    }

    $value = parent::__get( $name);
    return $value;
  }

  protected function getOptions()
  {
    // Initialise variables.
    $options = array();
    $name = (string) $this->element['name'];

    // Let's get the id for the current item, either category or content item.
    $jinput = JFactory::getApplication()->input;
    // Load the category options for a given extension.

    // For categories the old category is the category id or 0 for new category.
    $oldCat = $this->form->getValue( 'id', 0);
    $oldParent = $this->form->getValue($name, 0);
    $extension = $this->element['extension'] ? (string) $this->element['extension'] : (string) $jinput->get('extension','com_content');

    $db	= ShlDbHelper::getDb();
    $query = $db->getQuery(true);

    $query->select('a.id AS value, a.title AS text, a.level, a.published');
    $query->from('#__categories AS a');
    $query->join('LEFT', $db->quoteName('#__categories').' AS b ON a.lft > b.lft AND a.rgt < b.rgt');

    // Filter by the extension type
    $query->where('(a.extension = '.$db->quote($extension).' OR a.parent_id = 0)');

    // If parent isn't explicitly stated but we are in com_categories assume we want parents
    if ($oldCat != 0)
    {
      // Prevent parenting to children of this item.
      // To rearrange parents and children move the children up, not the parents down.
      $query->join('LEFT', $db->quoteName('#__categories').' AS p ON p.id = '.(int) $oldCat);
      $query->where('NOT(a.lft >= p.lft AND a.rgt <= p.rgt)');

      $rowQuery	= $db->getQuery(true);
      $rowQuery->select('a.id AS value, a.title AS text, a.level, a.parent_id');
      $rowQuery->from('#__categories AS a');
      $rowQuery->where('a.id = ' . (int) $oldCat);
      $db->setQuery($rowQuery);
      try {
        $row = $db->shlLoadObject();
      } catch (Exception $e) {
        $row = null;
        ShlSystem_Log::error( 'josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
      }
    }

    $query->where('a.published IN (0,1)');

    // added language filtering
    if ($this->element['languages']) {
      $languages = explode( ',', $this->element['languages']);
      $quotedLanguages = array();
      foreach( $languages as $language) {
        $quotedLanguages[] = $db->quote( $language);
      }
      if(!empty( $quotedLanguages)) {
        // NOTE: root category is allowed, despite it not having a language
        // as otherwise we wouldn't be able to translate a top level category
        $query->where('a.language IN (' . implode(',', $quotedLanguages) . ')' . ' OR a.parent_id = 0');
      }
    }

    $query->group('a.id, a.title, a.level, a.lft, a.rgt, a.extension, a.parent_id');
    $query->order('a.lft ASC');

    // Get the options.
    $db->setQuery($query);

    try {
      $options = $db->shlLoadObjectList();
    } catch (Exception $e) {
      ShlSystem_Log::error( 'josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
      JError::raiseWarning(500, $e->getMessage());
      $options = array();
    }

    // Pad the option text with spaces using depth level as a multiplier.
    for ($i = 0, $n = count($options); $i < $n; $i++)
    {
      // Translate ROOT
      if ($options[$i]->level == 0)
      {
        $options[$i]->text = JText::_('JGLOBAL_ROOT_PARENT');
      }
      if ($options[$i]->published == 1)
      {
        $options[$i]->text = str_repeat('- ', $options[$i]->level). $options[$i]->text ;
      }
      else
      {
        $options[$i]->text = str_repeat('- ', $options[$i]->level). '[' .$options[$i]->text . ']';
      }
    }


    // Get the current user object.
    $user = JFactory::getUser();

    // For new items we want a list of categories you are allowed to create in.
    if ($oldCat == 0)
    {
      foreach ($options as $i => $option)
      {
        // To take save or create in a category you need to have create rights for that category
        // unless the item is already in that category.
        // Unset the option if the user isn't authorised for it. In this field assets are always categories.
        if ($user->authorise('core.create', $extension . '.category.' . $option->value) != true )
        {
          unset($options[$i]);
        }
      }
    }
    // If you have an existing category id things are more complex.
    else
    {
      // If you are only allowed to edit in this category but not edit.state, you should not get any
      // option to change the category parent for a category or the category for a content item,
      // but you should be able to save in that category.
      foreach ($options as $i => $option)
      {
        if ($user->authorise('core.edit.state', $extension . '.category.' . $oldCat) != true && !isset($oldParent))
        {
          if ($option->value != $oldCat  )
          {
            unset($options[$i]);
          }
        }
        if ($user->authorise('core.edit.state', $extension . '.category.' . $oldCat) != true
            && (isset($oldParent)) && $option->value != $oldParent)
        {
          unset($options[$i]);
        }

        // However, if you can edit.state you can also move this to another category for which you have
        // create permission and you should also still be able to save in the current category.
        if (($user->authorise('core.create', $extension . '.category.' . $option->value) != true)
            && ($option->value != $oldCat && !isset($oldParent)))
        {
          {
            unset($options[$i]);
          }
        }
        if (($user->authorise('core.create', $extension . '.category.' . $option->value) != true)
            && (isset($oldParent)) && $option->value != $oldParent)
        {
          {
            unset($options[$i]);
          }
        }
      }
    }
    if ((isset($row) && !isset($options[0])) && isset($this->element['show_root']))
    {
      if ($row->parent_id == '1') {
        $parent = new stdClass();
        $parent->text = JText::_('JGLOBAL_ROOT_PARENT');
        array_unshift($options, $parent);
      }
      array_unshift($options, JHtml::_('select.option', '0', JText::_('JGLOBAL_ROOT_PARENT')));
    }

    return $options;

  }

}
