<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   weeblr, llc (c) 2014
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.2.1.567
 * @date		2014-01-16
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.model');

/**
 * Translation suggestion Model
 *
 * @package		com_josetta
 * @subpackage	com_josetta
 */
class JosettaModelSuggestfull extends ShlMvcModel_Base
{

	protected $_translationServiceParams = null;

	// Bing Translator
	protected $_BingEndPoint = 'http://api.microsofttranslator.com/V2/Http.svc/Translate';
	protected $_BingAccessToken = null;
	protected $_BingAccessTokenEndPoint = 'https://datamarket.accesscontrol.windows.net/v2/OAuth2-13/';
	protected $_BingAccessTokenScopeUrl = 'http://api.microsofttranslator.com';
	protected $_BingAccessTokenGrantType = 'client_credentials';

	/**
	 * Query a translation service to get an automated
	 * suggestion for a given translation
	 *
	 * @param string $sourceLanguage (with -, not _)
	 * @param string $targetLanguage
	 * @param string $text the text to be translated, as UTF-8
	 * @param string $type type of text to be translated, text/plan vs text/html
	 */
	public function translate($sourceLanguage, $targetLanguage, $text, $type)
	{
		$notSetResponse = new stdClass();
		$notSetResponse->status = 501;
		$notSetResponse->msg = JText::_('COM_JOSETTA_SUGGESTION_ERROR_SERVICE_NOT_SET');
		$notSetResponse->text = '';

		try
		{
			// get params:
			$this->_translationServiceParams = JComponentHelper::getParams('com_josetta');

			// check if a service is enabled
			$translationService = $this->_translationServiceParams->get('translation_service_type');
			if (empty($translationService))
			{
				return $notSetResponse;
			}

			// use appropriate handler
			$methodName = '_' . ucfirst($translationService) . 'Translate';
			if (is_callable(array($this, $methodName)))
			{
				$response = $this->$methodName($sourceLanguage, $targetLanguage, $text, $type);
			}
			else
			{
				$response = $notSetResponse;
			}
		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $e->getMessage());
			return $notSetResponse;
		}

		return $response;
	}

	protected function _BingTranslate($sourceLanguage, $targetLanguage, $text, $type)
	{
		$response = new stdClass();
		$response->status = 200;
		$response->msg = '';
		$response->text = '';

		// first get access token
		$this->_BingGetAccessToken();
		if (empty($this->_BingAccessToken))
		{
			// probably unauthorized access, bad credentials
			$response->status = 501;
			$response->msg = JText::_('COM_JOSETTA_SUGGESTION_ERROR_SERVICE_NOT_SET');
			return $response;
		}

		// we have a token, we can query for translation
		// first we need an http client
		$httpClient = $this->_getHttpClient();
		$httpClient->setMethod(Zendshl_Http_Client::GET);

		// set target URL for Bing translator service
		$httpClient->setUri($this->_BingEndPoint);

		// set authorization header
		$httpClient->setHeaders('Authorization', 'Bearer ' . $this->_BingAccessToken);
		$httpClient->setHeaders('Content-Type', 'text/xml');

		// various parts of the URL request:
		jimport('joomla.string.string');
		$requestParams = array('text' => trim($text), 'from' => $this->_formatLanguage('bing', $sourceLanguage),
			'to' => $this->_formatLanguage('bing', $targetLanguage), 'contentType' => $type);
		foreach ($requestParams as $key => $value)
		{
			$httpClient->setParameterGet($key, $value);
		}

		// then perform the request
		$rawResponse = $this->_httpQuery($httpClient);

		if (!empty($rawResponse))
		{
			// extract useful data from raw response
			$xmlResponse = simplexml_load_string($rawResponse->getBody());
			foreach ((array) $xmlResponse[0] as $txt)
			{
				$response->text = $txt;
			}
		}
		// finally return a response object
		return $response;
	}

	protected function _BingGetAccessToken()
	{
		// get cache object
		$cache = JFactory::getCache('josetta_bing_access_token');
		$cache->setLifetime(8); // cache result for 8 minutes
		$cache->setCaching(1); // force caching on
		$this->_BingAccessToken = $cache->call(array($this, '_doBingGetAccessToken'));

		// empty, clear the cache, as we want to try again next time rather than wait for cache expiration
		if (empty($this->_BingAccessToken))
		{
			$cache->clean('josetta_bing_access_token');
		}
	}

	public function _doBingGetAccessToken()
	{
		// get an http client
		$httpClient = $this->_getHttpClient();
		$httpClient->setMethod(Zendshl_Http_Client::POST);

		// set end point for token requests
		$httpClient->setUri($this->_BingAccessTokenEndPoint);

		// attach required POST data
		$params = array('grant_type' => $this->_BingAccessTokenGrantType, 'scope' => $this->_BingAccessTokenScopeUrl,
			'client_id' => $this->_translationServiceParams->get('bing_client_id'),
			'client_secret' => $this->_translationServiceParams->get('bing_client_secret'));
		foreach ($params as $key => $value)
		{
			$httpClient->setParameterPost($key, $value);
		}

		// request access token
		$rawResponse = $this->_httpQuery($httpClient);

		// response is json
		$accessToken = empty($rawResponse) ? null : json_decode($rawResponse->getBody());

		// check error conditions
		if (empty($accessToken))
		{
			ShlSystem_Log::logError(__METHOD__ . ': Could not get token from Bing');
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, 'Could not get token from Bing');
			return '';
		}

		if (!empty($accessToken->error))
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__, __METHOD__, __LINE__, $accessToken->error_description);
			return '';
		}

		// return actual value
		return $accessToken->access_token;
	}

	protected function _httpQuery($httpClient)
	{
		// request file content
		$adapters = array('Zendshl_Http_Client_Adapter_Curl', 'Zendshl_Http_Client_Adapter_Socket');
		$rawResponse = null;

		foreach ($adapters as $adapter)
		{
			try
			{
				$httpClient->setAdapter($adapter);
				$rawResponse = $httpClient->request();
				break;
			}
			catch (Exception $e)
			{ // need that to be Exception, so as to catch Zendshl_Exceptions.. as well
			// we failed, let's try another method
				$rawResponse = null;
			}
		}

		return $rawResponse;
	}

	protected function _getHttpClient()
	{
		// get an http client
		$httpClient = new Zendshl_Http_Client;

		// set general params
		$httpClient
			->setConfig(
				array('maxredirects' => 0, 'timeout' => 10, 'disable_curl_ssl_verify' => false, 'disable_uri_validation' => true));
		$httpClient->setEncType();

		return $httpClient;
	}

	protected function _formatLanguage($service, $language)
	{
		switch ($service)
		{
			case 'bing':
				$bits = explode('-', $language);
				$language = $bits[0];
				break;
			default:
				break;
		}

		return $language;
	}
}

