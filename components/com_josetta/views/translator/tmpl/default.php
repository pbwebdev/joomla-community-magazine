<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// no direct access
defined('_JEXEC') or die;
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('behavior.keepalive');
$listOrder	=  $this->state->get('filter_order', 'ordering');
$listDirn	=  $this->state->get('filter_order_Dir', 'ASC');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task) {
            if (task == 'translate.edit') {
        	if (document.adminForm.boxchecked.value==0)
                {
                alert('<?php echo JTEXT::_('COM_JOSETTA_TRANSLATION_SELECT_WARNING', $jsSafe = true); ?>');
                    }
                    else{
                        	Joomla.submitform(task);
                        }
	}
        }
</script>

<form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="adminForm" id="adminForm">

    <div class="jta_fixed_filters_container">
	  <?php

	  echo $this->loadTemplate('standard_filters');
	  echo $this->loadTemplate('items_filters');

	  ?>
	  </div>

	  <div class="jta_fluid_content_container">

    <table class="josettalist">
		<thead>
			<tr>

				<?php echo $this->loadTemplate('head');?>
			</tr>
		</thead>
                <tbody id="josetta_tbody">
                  <?php
                    if(!empty($this->items))  {
                      echo $this->loadTemplate('body');
                    }
                  ?>
                </tbody>

                <tfoot>
                <tr>
                      <?php echo $this->loadTemplate('footer');?>
                </tr>
                </tfoot>
	</table>
	</div>

                <input type="hidden" name="task" value="" />
                <input type="hidden" name="view" value="translator" />
                <input type="hidden" name="jlang" value="" />
                <input type="hidden" name="Itemid" value="<?php echo JFactory::getApplication()->input->getInt('Itemid');?>" />
		<input type="hidden" name="boxchecked" value="0" />
                <input type = "hidden" name = "option" value = "<?php echo $this->option;?>" />
		<input type="hidden" id="filter_order" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
                <?php echo JHtml::_('form.token'); ?>

</form>

<hr />
<div class="josetta-frontend-footer-container">
	<?php
	echo $this->footerText;
	?>
</div>
