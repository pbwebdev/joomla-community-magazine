<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// no direct access
defined('_JEXEC') or die;
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$document = JFactory::getDocument();
// backend list styles
$document->addStyleSheet( JURI::root(true).'/media/system/css/adminlist.css');

$columnsCount = 3;

?>
<div class="josetta-modal">
<form
	action="<?php echo JRoute::_('index.php?option=com_josetta&view=translator&layout=modal&tmpl=component&Itemid=' . JosettaHelper::getJosettaItemid()); ?>"
	method="post" name="adminForm" id="adminForm">


	<fieldset id="filter-bar">
		<div class="filter-search josetta_float_left">
			<input type="text" name="filter_search" id="filter_search" value="<?php  echo $this->escape($this->state->get('filter_search')); ?>" />
			<button type="submit">
				<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>
			</button>
			<button type="button"	onclick="document.id('filter_search').value='';this.form.submit();">
				<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>
			</button>
		</div>

		<div class="filter-select josetta_float_right">
			<?php
			if(!empty($this->filters))
			{
			  foreach($this->filters as  $filter)
			  {
			    echo $filter;
			  }
			}
			?>
		</div>

	</fieldset>

	<table class="adminlist">
		<thead>
			<tr>
				<th width="1%" class="nowrap"><?php echo JHtml::_('grid.sort',  'JGRID_HEADING_ID', 'id', $listDirn, $listOrder); ?>
				</th>
				<?php
				if(!empty($this->fields->title->column)):
				?>
				<th class="title"><?php echo JHtml::_('grid.sort', 'JGLOBAL_TITLE',  (string) $this->fields->title->column, $listDirn, $listOrder); ?>
				</th>
				<?php endif; ?>

				<th><?php echo JText::_( 'JGRID_HEADING_LANGUAGE'); ?>
				</th>
				<?php
          if(isset($this->fields->last_modified_by->column)): ?>
				<th>
				  <?php
				    $columnsCount++;
				     echo JText::_('COM_JOSETTA_TRANSLATION_MODIFIED_BY');
				  ?>
				</th>
				<?php endif;?>
				<?php
        if(isset($this->fields->last_modified->column)): ?>
				<th>
				  <?php
				   $columnsCount++;
				   echo JText::_('COM_JOSETTA_TRANSLATION_MODIFIED');
				  ?>
				</th>
				<?php endif;?>
			</tr>
		</thead>

		<tbody id="josetta_tbody">

			<?php
			foreach ($this->items as $i => $item) :

			?>
			<tr class="josetta_modal_item row<?php echo $i % 2; ?>">

				<td>
				  <a class="josetta_modal_item" onclick="javascript:if (window.parent) window.parent.JosettaUseExistingContent( <?php echo $this->escape($item->id); ?>,'<?php echo $this->state->get('filter_lang'); ?>')"><?php echo $this->escape($item->id); ?>
				  </a>
				</td>

				<td>
				  <?php
				  if(!empty($this->fields->title->column)):
            $column = (string) $this->fields->title->column;
				  ?>
					  <a onclick="javascript:if (window.parent) window.parent.JosettaUseExistingContent( <?php echo $this->escape($item->id); ?>,'<?php echo $this->state->get('filter_lang'); ?>')">
						  <?php echo JosettaHelper::formatGridData( $this->escape( $item->$column),'text'); ?>
				    </a>

				  <?php endif; ?>
				</td>

				<td class="center">
				  <?php
				  echo JosettaHelper::formatGridData( $this->escape($item->language),'language');
    			?>
    		</td>

    		<?php
          if(isset($this->fields->last_modified_by->column)): ?>
				<td class="center"><?php
				  $column = (string) $this->fields->last_modified_by->column;
				  echo $this->escape( $this->escape( JosettaHelper::formatGridData( $item->$column, 'user')));
				  ?>
				</td>
				<?php endif;?>
				<?php
        if(isset($this->fields->last_modified->column)): ?>
				<td class="center"><?php
				  $column = (string) $this->fields->last_modified->column;
				  $lastModifiedDate = JosettaHelper::formatGridData( $item->$column, 'date');
				  echo $this->escape( $this->escape( $lastModifiedDate));
				  ?>
				</td>
				<?php endif;?>

			</tr>

			<?php endforeach; ?>

			<tr>
				<td colspan="<?php echo $columnsCount; ?>"><?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>

		</tbody>
	</table>

	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_lang" value="<?php echo $this->state->get('filter_lang'); ?>" />
	<input type="hidden" name="option" value="<?php echo $this->option;?>" />
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
	<?php echo JHtml::_('form.token'); ?>

</form>
</div>