<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

foreach ($this->items as $i => $item) :
?>
<tr class="josetta-row-<?php echo $i % 2; ?>">
	<td class="center">
	<?php echo JHtml::_('grid.id', $i, $item->id); ?>
	</td>
	<td>
	<?php echo $this->escape($item->id); ?>
	</td>
	<td width="50%">
	<?php

	$tooltipData = JosettaHelper::getAssociationsTooltipData($item->id, $this->state->get('type'));

	if (!empty($this->fields->title->column) && !empty($this->fields->level->column))
	{

		$columnName = (string) $this->fields->title->column;
		$level = (string) $this->fields->level->column;
		$href = JRoute::_(
			'index.php?option=com_josetta&view=combo&cid[]=' . $item->id . '&type=' . $this->state->get('type') . '&Itemid='
				. JosettaHelper::getJosettaItemid());
		$link = '<a class="hasTip" href="' . $href . '" title="' . $tooltipData->tooltip . '">'
			. JosettaHelper::formatGridData($item->$columnName, 'text') . '</a>';
		echo JosettaHelper::formatGridData(array($link, $item->$level), 'level');
	}
	elseif (!empty($this->fields->title->column))
	{

		$columnName = (string) $this->fields->title->column;
		$href = JRoute::_(
			'index.php?option=com_josetta&view=combo&cid[]=' . $item->id . '&type=' . $this->state->get('type') . '&Itemid='
				. JosettaHelper::getJosettaItemid());
		echo '<a class="hasTip" href="' . $href . '" title="' . $tooltipData->tooltip . '">'
			. JosettaHelper::formatGridData($item->$columnName, 'text') . '</a>';

	}

	?>
	</td>

	<td class="center hasTip" title="<?php echo $tooltipData->tooltip; ?>">
	  <?php
	if (count($this->translationLanguagesList) > 4)
	{
		// display only a counter
		echo $tooltipData->translationsCount;
	}
	else
	{
		// display a series of flags
		foreach ($this->translationLanguagesList as $languageCode => $languageName)
		{
			if (!in_array($languageCode, $tooltipData->translatedLanguages))
			{
				// no translation for this language, display empty space
				echo '<span class="list-flag" >&nbsp;</span>';
			}
			else
			{
				// there is a translation, display a flag
				echo '<span class="list-language-code" >' . JosettaHelper::formatGridData($languageCode, 'flag') . '</span>';
			}
		}
	}
	  ?>
	</td>

	<td class="center">
	<?php
	echo JosettaHelper::formatGridData($this->escape($item->language), 'language');
	if (!empty($this->fields->last_modified_by->column)) :
		$columnName = (string) $this->fields->last_modified_by->column;
	?>
			<td class="center">
	      <?php echo JosettaHelper::formatGridData($this->escape($item->$columnName), 'user'); ?>
			</td>
    <?php endif;

	if (!empty($this->fields->last_modified->column)) :
		$column = (string) $this->fields->last_modified->column;
		$lastModifiedDate = $item->$column;

	?>
	<td class="center"><?php echo JosettaHelper::formatGridData($this->escape($lastModifiedDate), 'date'); ?>
	</td>
	<?php endif; ?>
</tr>
<?php endforeach; ?>
