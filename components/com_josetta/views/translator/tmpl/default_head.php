<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
$listOrder	=  $this->state->get('filter_order', 'ordering');
$listDirn	=  $this->state->get('filter_order_Dir', 'ASC');
?>
<th width="1%"><input type="checkbox" name="checkall-toggle" value=""
	onclick="Joomla.checkAll(this)" />
</th>

<th width="1%" class="nowrap">
<?php 
  echo JText::_( 'JGRID_HEADING_ID');
  ?>
</th>

<?php
if(!empty($this->fields->title->column)):
?>
<th class="title">
<?php 
  echo JText::_( 'JGLOBAL_TITLE');
  ?>
</th>

<?php endif; ?>
<th>
<?php echo JText::_('COM_JOSETTA_TRANSLATION'); ?>
</th>
<th>
<?php echo JText::_( 'JGRID_HEADING_LANGUAGE'); ?>
</th>

<?php
if(!empty($this->fields->last_modified_by->column)): ?>
<th>
<?php echo JText::_('COM_JOSETTA_TRANSLATION_MODIFIED_BY'); ?>
</th>

<?php endif;?>
<?php
if(!empty($this->fields->last_modified->column)): ?>
<th>
<?php echo JText::_('COM_JOSETTA_TRANSLATION_MODIFIED'); ?>
</th>

<?php endif;?>



