<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

?>


<fieldset class="filter-bar josetta_items_filters">

	<div class="filter-select josetta_float_right">


	<?php echo JText::_('COM_JOSETTA_CONTENT_TYPE'); ?>
		<select name="type" class="inputbox"
			onchange="document.adminForm.view.value='translator';document.adminForm.filter_order.value='id';this.form.submit()">
			<?php echo JHtml::_('select.options', $this->josettaTypes, 'value', 'text', $this->state->get('type') );?>
		</select>
    <?php
    if(!empty($this->filters)) {
      foreach($this->filters as  $filter) {
        echo $filter;
      }
    }
    ?>

</div>
</fieldset>
