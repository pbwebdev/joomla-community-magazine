<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
$sum=4;
if(!empty($this->fields->title->column)) {
  $sum++;
}
if(!empty($this->fields->last_modified_by->column)) {
  $sum++;
}
if(!empty($this->fields->last_modified->column)) {
  $sum++;
}

?>

<td colspan="<?php echo $sum;?>">

<?php echo $this->pagination->getListFooter(); ?>

</td>
