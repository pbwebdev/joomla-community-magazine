<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

?>


<fieldset class="filter-bar josetta_standard_filters">
	<div class="filter-search josetta_float_left">
		<label class="filter-search-lbl" for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?>
		</label>
		<input type="text" name="filter_search" id="filter_search"
			value="<?php  echo $this->escape( $this->state->get( 'filter_search')); ?>" />
		<button type="submit">

		<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
		<button type="button"
			onclick="document.id('filter_search').value='';this.form.submit();">
			<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>

    <?php
      if(!empty( $this->translationStatusFilter)) :
    ?>
        <select name="filter_translation_status" id="filter_translation_status" class="inputbox"
         	onchange="document.adminForm.view.value='translator';this.form.submit()">
        	<?php echo JHtml::_('select.options', $this->translationStatusFilter, 'value', 'text', $this->state->get('filter_translation_status') );?>
       	</select>
    <?php
      endif;

      if(empty( $this->translationLanguageFilterHide) && !empty( $this->translationLanguageFilter)) :
    ?>
        <select name="filter_translation_language" id="filter_translation_language" class="inputbox"
         	onchange="document.adminForm.view.value='translator';this.form.submit()">
        	<?php echo JHtml::_('select.options', $this->translationLanguageFilter, 'value', 'text', $this->state->get('filter_translation_language', 0) );?>
       	</select>
    <?php
      endif;
    ?>
	</div>

</fieldset>
