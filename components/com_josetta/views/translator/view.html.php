<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of context items.
 *
 * @package		Joomla.Site
 * @subpackage	com_josetta
 * @since		1.6
 */
class JosettaViewTranslator extends ShlMvcView_Base
{

	protected $items;
	protected $pagination;
	protected $state;
	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$app = JFactory::getApplication();
		$document = JFactory::getDocument();
		$option = $app->input->getCmd('option');
		$view = $app->input->getCmd('view');

		// insert needed css files
		$document->addStyleSheet(JURI::root(true) . '/media/com_josetta/css/josetta.css');

		//Load the instance of model
		$model = ShlMvcModel_Base::getInstance('Translator', 'JosettaModel');
		$this->items = $model->getItems();
		$this->pagination = $model->getPagination();
		$this->state = $model->getState();

		// make sure pagination object maintains filter language throughout
		$this->pagination->setAdditionalUrlParam('filter_lang', $this->state->get('filter_lang', null));
		$this->pagination
			->setAdditionalUrlParam('filter_translation_status',
				$this->state->get('filter_translation_status', JosettaHelper::TRANSLATION_STATUS_ONLY_UNCOMPLETED));
		$this->pagination->setAdditionalUrlParam('filter_translation_language', $this->state->get('filter_translation_language', null));

		//list of type of plugin available in array
		$this->josettaTypes = JosettaHelper::getJosettaContentTypes();

		//load the view
		$this->view = $view;
		$this->option = $app->input->getCmd('option');

		//load the fields for heading of grid
		$this->fields = $model->getReferenceFields($this->state->get('type'));

		// build the content type filter
		$context = $option . '_' . $view;
		// don't mix up modal boxes with regular lists
		$tmpl = $app->input->getCmd('tmpl');
		$context .= $tmpl == 'component' ? '_modal' : '';

		$this->filters = $this->_buildFiltersHtml($this->state->get('type'), $context);

		// create the links to each possible translations
		$this->languagesList = JosettaHelper::getJosettaLanguage();
		$this->translationLanguagesList = array();
		foreach ($this->languagesList as $code => $name)
		{
			if (JosettaHelper_Acl::authorize('josetta.canTranslate.' . $code) && $code != JosettaHelper::getSiteDefaultLanguage())
			{
				$this->translationLanguagesList[$code] = $name;
			}
		}

		// build the translation status selector and target language data
		$this->translationStatusFilter = $this->_getTranslationStatusOptions();
		$this->translationLanguageFilter = $this->_getTranslationLanguageOptions();
		$this->translationLanguageFilterHide = $this->state->get('filter_translation_language_hide', 0);

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// version number display
		$this->footerText = JText::sprintf('COM_JOSETTA_FRONTEND_FOOTER_' . strtoupper(JosettaadminHelper_General::getVersionType()),
			JosettaadminHelper::getVersion(), date('Y'), JosettaadminHelper_General::getLandingPageUrl());

		parent::display($tpl);
	}

	/**
	 * Builds a list of options for the translation target language filter
	 *
	 * @return array of options
	 *
	 */
	protected function _getTranslationLanguageOptions()
	{
		$currentTranslationStatusFilter = $this->state->get('filter_translation_status', JosettaHelper::TRANSLATION_STATUS_ONLY_UNCOMPLETED);

		// prompt string
		$options = array(0 => JText::_('COM_JOSETTA_TRANSLATION_STATUS_TO_ANY_LANGUAGE'));

		foreach ($this->languagesList as $languageCode => $languageName)
		{
			if (JosettaHelper_Acl::authorize('josetta.canTranslate.' . $languageCode))
			{
				$options[$languageCode] = JText::sprintf('COM_JOSETTA_TRANSLATION_STATUS_TO_A_LANGUAGE', $languageName);
			}
		}

		return $options;
	}

	/**
	 * Builds a list of options for the translation status filter
	 *
	 * @return array of options
	 *
	 */
	protected function _getTranslationStatusOptions()
	{
		$options = array(0 => JText::_('COM_JOSETTA_TRANSLATION_STATUS_SELECT_ONE'),
			JosettaHelper::TRANSLATION_STATUS_ONLY_UNCOMPLETED => JText::_('COM_JOSETTA_TRANSLATION_STATUS_ONLY_UNCOMPLETED'),
			JosettaHelper::TRANSLATION_STATUS_ONLY_COMPLETED => JText::_('COM_JOSETTA_TRANSLATION_STATUS_ONLY_COMPLETED'));
		/*,
		 * Not implemented
		 * JosettaHelper::TRANSLATION_STATUS_ONLY_STARTED => JText::_( 'COM_JOSETTA_TRANSLATION_STATUS_ONLY_NOT_STARTED'),
		 */

		return $options;
	}

	/**
	 * Function to build the filter available in xml defination of plugins
	 *
	 * @param model
	 * @param prefix
	 * @param filtername
	 *
	 * @return mixed html
	 *
	 */
	protected function _buildFiltersHtml($type, $prefix)
	{
		// get the filters definition from the plugin
		$definitions = JosettaHelper::getJosettaDefinitions($type);
		$filtersData = $definitions->filters->filter;
		// prepare an array to hold the html for each filter
		// if there are more than one
		$josettaFilters = array();
		$filterWrapper = '<div class="josetta-filter-wrapper">%s</div>';
		if (!empty($filtersData))
		{
			foreach ($filtersData as $filter)
			{
				// get current value
				$app = JFactory::getApplication();
				$filterName = $type . '_' . (string) $filter->id;
				$current = $app->getUserStateFromRequest($prefix . '+' . $type . '.' . $filterName, $filterName, (string) $filter->default);
				switch ((string) $filter->type)
				{
					case 'category':
					// this is a category, so use Joomla html helper to build the drop down
						$filterHtml = '';
						$filterHtml .= JText::_('COM_JOSETTA_CONTENT_CATEGORY');
						$filterHtml .= '<select name="' . $filterName . '" id="' . $filterName . '" class="inputbox" onchange="this.form.submit()">'
							. "\n";
						$filterHtml .= '<option value="0">' . JText::_('JOPTION_SELECT_CATEGORY') . '</option>' . "\n";
						$categoriesOptionsHtml = JHtml::_('category.options', (string) $filter->extension);
						$filterHtml .= JHtml::_('select.options', $categoriesOptionsHtml, 'value', 'text', (int) ($current)) . "\n";
						$filterHtml .= "</select>\n";
						break;
					case 'languagecategory':
					// this is a category, so use Joomla html helper to build the drop down
						$filterHtml = '';
						$filterHtml .= JText::_('COM_JOSETTA_CONTENT_CATEGORY');
						$filterHtml .= '<select name="' . $filterName . '" id="' . $filterName . '" class="inputbox" onchange="this.form.submit()">'
							. "\n";
						$filterHtml .= '<option value="0">' . JText::_('JOPTION_SELECT_CATEGORY') . '</option>' . "\n";
						$filterLang = $this->state->get('filter_lang', null);
						$filterLang = empty($filterLang) ? JosettaHelper::getSiteDefaultLanguage() : $filterLang;
						$categoriesSelectConfig = array('filter.published' => array(0, 1), 'filter.languages' => array('*', $filterLang));
						$categoriesOptionsHtml = JosettaHelper::getCategoryOptionsPerLanguage((string) $filter->extension, $categoriesSelectConfig);
						$filterHtml .= JHtml::_('select.options', $categoriesOptionsHtml, 'value', 'text', (int) ($current)) . "\n";
						$filterHtml .= "</select>\n";
						break;
					case 'extension':
						$filterHtml = '';
						$filterHtml .= JText::_('COM_JOSETTA_TRANSLATE_EXTENSION');
						$disabled = JFactory::getApplication()->input->getInt('disable_' . $filterName);
						$disabled = empty($disabled) ? '' : 'disabled="disabled"';
						$filterHtml .= '<select ' . $disabled . ' name="' . $filterName . '" id="' . $filterName
							. '" class="inputbox" onchange="this.form.submit()">' . "\n";
						$filterHtml .= '<option value="0">' . JText::_('COM_JOSETTA_TRANSLATE_SELECT_EXTENSION') . '</option>' . "\n";
						$extensionsOptionsHtml = JosettaHelper::buildComponentList();
						$filterHtml .= JHtml::_('select.options', $extensionsOptionsHtml, 'value', 'text', $current) . "\n";
						$filterHtml .= "</select>\n";

						break;
					case 'menutype':
						$filterHtml = '';
						$filterHtml .= '<select name="' . $filterName . '" id="' . $filterName . '" class="inputbox" onchange="this.form.submit()">'
							. "\n";
						$filterHtml .= '<option value="0">' . JText::_('COM_JOSETTA_TRANSLATE_MENU') . '</option>' . "\n";
						$filterHtml .= JHtml::_('select.options', JHtml::_('menu.menus'), 'value', 'text', $current) . "\n";
						$filterHtml .= "</select>\n";
						break;

					case 'sitemodule':
						$filterHtml = '';
						$filterHtml .= '<select name="' . $filterName . '" id="' . $filterName . '" class="inputbox" onchange="this.form.submit()">'
							. "\n";
						$filterHtml .= '<option value="0">' . JText::_('COM_JOSETTA_SELECT_MODULE_TYPE') . '</option>' . "\n";
						$siteModulesListOptionsHtml = JosettaHelper::getSiteModuleListOptions();
						$filterHtml .= JHtml::_('select.options', $siteModulesListOptionsHtml, 'value', 'text', $current) . "\n";
						$filterHtml .= "</select>\n";
						break;

					case 'josettalist':
						$filterHtml = '';
						$filterHtml .= JText::_('JSTATUS');
						$filterHtml .= '<select name="' . $filterName . '" id="' . $filterName . '" class="inputbox" onchange="this.form.submit()">'
							. "\n";
						$options[0] = JHtml::_('select.option',	0, JText::_('JUNPUBLISHED'));
						$options[1] = JHtml::_('select.option',	1, JText::_('JPUBLISHED'));
						$options[2] = JHtml::_('select.option',	2, JText::_('JARCHIVED'));

						$filterHtml .= JHtml::_('select.options', $options, 'value', 'text', $current) . "\n";
						$filterHtml .= "</select>\n";
						break;

					default:
					// give a chance to 3rd party plugins to add their own filters
						$thirdPartyFilter = JosettaHelper::get3rdPartyFilter($type, (string) $filter->type, $filterName, $current);

						// and merge them with built in ones
						if (!empty($thirdPartyFilter))
						{
							$filterHtml = $thirdPartyFilter;
						}

						break;
				}

				if (!empty($filterHtml))
				{
					$josettaFilters[] = str_replace('%s', $filterHtml, $filterWrapper);
					$filterHtml = '';
				}
			}
		}

		return $josettaFilters;
	}

}
