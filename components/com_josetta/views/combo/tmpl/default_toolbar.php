<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// no direct access
defined('_JEXEC') or die;

?>

<div id="toolbar" class="toolbar-list">
	<ul>
		<li id="<?php echo $this->jsTargetLanguage[$this->languageCode]; ?>_toolbar-apply" class="button ">
		<a href="javascript:void(0)" onclick="javascript:Josetta.save( 'translate.apply')" id="<?php echo $this->jsTargetLanguage[$this->languageCode]; ?>_toolbar-apply-link" class="toolbar" >
				<span class="icon-32-apply"> </span><?php echo JText::_('JAPPLY'); ?>
		</a>
		</li>
		<li id="<?php echo $this->jsTargetLanguage[$this->languageCode]; ?>_toolbar-save" class="button">
		<a href="javascript:void(0)" onclick="javascript:Josetta.save( 'translate.save')" id="<?php echo $this->jsTargetLanguage[$this->languageCode]; ?>_toolbar-save-link" class="toolbar">
				<span class="icon-32-save"> </span><?php echo JText::_("COM_JOSETTA_SAVE_AND_CLOSE"); ?>
		</a>
		</li>
		<li id="<?php echo $this->jsTargetLanguage[$this->languageCode]; ?>_toolbar-popup-preview" class="button">
		<a id="<?php echo $this->jsTargetLanguage[$this->languageCode]; ?>_toolbar-popup-preview-link"
		  title="<?php echo JText::_('COM_JOSETTA_USE_CONTENT') . '::' . JText::_('COM_JOSETTA_HELP_USE_CONTENT'); ?>"
			rel="{handler: &#039;iframe&#039;, size: {x: 800, y: 500}, onClose: function() {}}"
			href="<?php echo $this->useExistingContentUrl[$this->languageCode]; ?>"
			class="modal hasTip"> <span class="icon-32-preview"> </span><?php echo JText::_("COM_JOSETTA_BUTTON_USE_CONTENT"); ?>
		</a>
		</li>

		<?php if ($this->showDissociateButton[$this->languageCode]) : ?>

		<li id="<?php echo $this->languageCode; ?>_toolbar-dissociate" class="button" >
		<a id="<?php echo $this->languageCode; ?>_toolbar-dissociate-link" href="javascript:void(0)"
		title="<?php echo JText::_('COM_JOSETTA_BUTTON_DISSOCIATE') . '::' . JText::_('COM_JOSETTA_HELP_DISSOCIATE'); ?>"
			class="toolbar hasTip"
			onclick="javascript:Josetta.dissociate('<?php echo $this->jsTargetLanguage[$this->languageCode]; ?>')">
				<span class="icon-32-dissociate">	</span><?php echo JText::_("COM_JOSETTA_BUTTON_DISSOCIATE"); ?>
		</a>
		</li>

		<?php endif; ?>

		<li id="<?php $this->jsTargetLanguage[$this->languageCode]; ?>_toolbar-cancel" class="button">
		<a id="<?php $this->jsTargetLanguage[$this->languageCode]; ?>_toolbar-cancel-link" href="javascript:void(0)"
			class="toolbar"
			onclick="javascript:Josetta.cancel()">
				<span class="icon-32-cancel"> </span><?php echo JText::_("JCANCEL"); ?>
		</a>
		</li>
	</ul>
	<div class="josetta_clr"></div>
</div>
