<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// no direct access
defined('_JEXEC') or die;

echo JHtml::_('tabs.start', 'jta_translate_combo', array('allowAllClose' => true));
foreach ($this->languagesList as $languageCode => $languageName)
{

	$this->languageCode = $languageCode;
	$tabTitle = JosettaHelper::formatGridData($languageCode, 'flag');
	$tabTitle .= '&nbsp;&nbsp;' . JText::sprintf('COM_JOSETTA_TRANSLATION_STATUS_TO_A_LANGUAGE', $languageName);
	echo str_replace( '<a href="', '<a id="jta_lang_'. $this->jsTargetLanguage[$languageCode] . '" href="', JHtml::_('tabs.panel', $tabTitle, 'jta_lang_' . $this->jsTargetLanguage[$languageCode]));

	echo $this->loadTemplate('form');

}

echo JHtml::_('tabs.end');

