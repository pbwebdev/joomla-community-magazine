<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// no direct access
defined('_JEXEC') or die;
?>

<span class="jta_page_subtitle">
  <?php
    echo $this->escape( $this->translate_language_title[$this->languageCode]);
    echo '&nbsp;';
    echo $this->escape( $this->originalItemTitle);
  ?>
</span>
<span class="jta_page_subtitle_small">
  <?php
  if (!empty( $this->original_item_subtitle)) {
    echo '&nbsp;(' . $this->escape( $this->original_item_subtitle) . ')';
  }
  ?>
</span>
<?php if(!empty($this->translatedItemId[$this->languageCode])): ?>
<span class="jta_page_subtitle_right">
  <?php echo JText::sprintf( 'COM_JOSETTA_TRANSLATED_ITEM_ID', $this->translatedItemId[$this->languageCode]); ?>
</span>
<?php endif; ?>
<hr />
<form
	action="<?php echo JRoute::_('index.php?option=com_josetta&view=combo');?>"
	method="post" name="adminForm" id="translate-form-<?php echo $this->jsTargetLanguage[$this->languageCode]; ?>"
	class="form-validate">

	<?php
	  echo $this->loadTemplate( 'toolbar');
  ?>

	<div id="josetta_container">

		<fieldset class="josettaoptions">
			<legend><?php  echo JText::_('COM_JOSETTA');?></legend>

			<?php
			if(!empty($this->publishingRealFieldName)) {
        		echo JText::_('COM_JOSETTA_TRANSLATE_TITLE_PUBLISHED');
	      		?>

				<input type="checkbox" name="<?php echo $this->publishingFieldName[$this->languageCode]; ?>"
					value="1" <?php echo $this->published[$this->languageCode]==1 ?'checked="yes"':''; ?>
					onchange="Josetta.itemChanged(this);"
					onclick="Josetta.copyHiddenField('<?php echo $this->publishingRealFieldName; ?>', '<?php echo $this->jsTargetLanguage[$this->languageCode]; ?>')"
					id="<?php echo $this->publishingFieldName[$this->languageCode]; ?>" />

				<?php
			}
        echo ' ';
        echo JText::_('COM_JOSETTA_TRANSLATE_TITLE_COMPLETION') . '&nbsp;' . $this->completionSelectList[$this->languageCode];
      ?>
    </fieldset>

    <?php
    foreach($this->josettaForm[$this->languageCode]->getFieldset() as $field):

      $fieldTitle = $field->fieldname;
      if(strtolower($field->type) == 'hidden'):
        echo $field->input;
      else:
    ?>
		<fieldset class="josettaMainoptions">
    	<legend><?php echo $field->label; ?></legend>
    	  <table class="josetta_form_field_container">
          <tr>
      <?php
         // NOTE: right now, it appears we're putting the same, original, content twice. The second copy is meant
         // to be used by the "use original" button, and must be kept untouched
         // the first copy will probably be a "cleaned up" one, at least in the case of html content
         $displayOriginal = is_null( $field->element) ? 'yes' : $field->element->attributes()->displayoriginal;
         $displayOriginal = empty( $displayOriginal) || $displayOriginal != 'no';
         $readOnly = is_null( $field->element) ? 'no' : $field->element->attributes()->readonly;
         $readOnly = !empty( $readOnly) && $readOnly != 'no';

         // first column: original value
         if( $displayOriginal) {
           $colSpan = $readOnly ? 'colspan="3"' : '';
           echo '<td ' . $colSpan . 'class="josetta_form_field_original"><div class="josetta_float_left">';
           if(isset($this->originalItem->$fieldTitle) || ( !empty($this->originalItem->request) && isset($this->originalItem->request[$fieldTitle]))) {
             echo JosettaHelper::formatOriginalField( $this->originalItem, $fieldTitle, $field);
             echo '<span id="' . $this->jsTargetLanguage[$this->languageCode] . '_refdata_' . $fieldTitle . '" class="refform">';
             echo $this->originalItem->$fieldTitle;
             echo '</span>';
           }
           echo '</div></td>';
         } else {
           echo '<td></td>';
         }

         // second column: copy button
         if( $displayOriginal) {
           if($readOnly) {
             echo '<td></td>';
           } else {
             echo '<td class="josetta_form_field_buttons">' . JosettaHelper::createButton( $this->josettaForm[$this->languageCode], $field, $this->jsTargetLanguage[$this->languageCode]) . '</td>';
           }
         }

         // third column: translation field
         if($readOnly) {
           echo '<td></td>';
         } else {
           $colSpan = $displayOriginal ? 'colspan="3"' : '';
           echo '<td ' . $colSpan . ' class="josetta_form_field_translation">' . JosettaHelper::formatTranslationField( $this->josettaForm[$this->languageCode], $field) . '</td>';
         }
      ?>
          </tr>
        </table>
	  </fieldset>

<?php
  endif;
  endforeach;
?>
	</div>

	<input type="hidden" name="type" value="<?php echo $this->josetta_type ; ?>" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="option"	value="com_josetta" />
	<input type="hidden" name="<?php echo $this->languageCode; ?>_exist_content_id" value="<?php echo $this->state->get('exist_content_id'); ?>" />
	<input type="hidden" name="target_language" value="<?php echo $this->languageCode; ?>" />

  <?php echo JHtml::_('form.token'); ?>

</form>

<hr />
<div class="josetta-frontend-footer-container">
	<?php
	echo $this->footerText;
	?>
</div>
