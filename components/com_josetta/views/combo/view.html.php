<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date        2015-12-23
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to translate Josetta interface.
 *
 * @package        Josetta
 * @subpackage    com_josetta
 */
class JosettaViewCombo extends ShlMvcView_Base
{

	/**
	 * Display the view
	 */

	public function display($tpl = null)
	{

		$app = JFactory::getApplication();
		$document = JFactory::getDocument();

		// load translate model
		$model = ShlMvcModel_Base::getInstance('translate', 'JosettaModel');

		// Initialiase State variables.
		$this->state = $model->getState();

		// Initialiase Read only variables
		$this->originalItem = $model->getOriginalItem();

		// get the context type from state variables
		$this->josetta_type = $this->state->get('type');

		// and then read up the xml describing this content
		$definitions = JosettaHelper::getJosettaDefinitions($this->josetta_type);

		// compute a representative name for the original item
		$titleField = (string) $definitions->reference->title->column;
		$this->originalItemTitle = $this->originalItem->$titleField;

		// hack: this should be handled by plugins, but does require quite a bit of work
		// adding lines to the definitions files, and one or more methods to the plugin to
		// provide the textual value from the id
		// so we'll just do a helper method with a switch for the most common cases
		$this->original_item_subtitle = JosettaHelper::getItemSubtitle($this->josetta_type, $this->originalItem);

		// Get the name of the database field that contains the publication status
		$this->publishingRealFieldName = (string) $definitions->reference->published->column;

		// build the array for drop down of completion
		$defaultValues = array('0' => JText::_('COM_JOSETTA_TRANSLATE_COMPLETION'), "25" => "25%", "50" => "50%", "75" => "75%", "100" => "100%");

		// version number display
		$this->footerText = JText::sprintf('COM_JOSETTA_FRONTEND_FOOTER_' . strtoupper(JosettaadminHelper_General::getVersionType()), JosettaadminHelper::getVersion(), date('Y'), JosettaadminHelper_General::getLandingPageUrl());

		// import tabs
		jimport('joomla.html.pane');

		//import javascript framework
		JHtml::_('behavior.framework');

		// add css and js
		$type = JosettaadminHelper_General::getVersionType();
		$document = JFactory::getDocument();
		$document->addStyleSheet(JURI::root(true) . '/templates/system/css/general.css');
		$document->addScript(JURI::root(true) . '/media/system/js/tabs.js');
		$document->addStyleSheet(JURI::root(true) . '/media/com_josetta/css/josetta.css');
		$document->addStyleSheet(JURI::root(true) . '/media/com_josetta/css/buttons.' . $type . '.css');
		$document->addStyleSheet(JURI::root(true) . '/media/com_josetta/css/tabs.css');
		$document->addScript(JURI::root(true) . '/media/com_josetta/js/translate.js');
		$document->addScript(JURI::root(true) . '/media/com_josetta/js/suggest.' . $type . '.js');
		$document->addScript(JURI::root(true) . '/media/com_josetta/js/utf8_encode.js');
		$document->addScript(JURI::root(true) . '/media/com_josetta/js/sha1.js');

		// get the set/getContent methods for editor
		$editor = JFactory::getEditor();
		$getContent = $editor->getContent('__EDITOR_INSTANCE__');
		$getContent = str_replace("'__EDITOR_INSTANCE__'", 'instance', $getContent);
		$getContent = str_replace('"__EDITOR_INSTANCE__"', 'instance', $getContent);  // codeMirror
		// fix for TinyMCE: Joomla 3.4.3 code works only if there's one editor instance on page
		$getContent = str_replace('tinyMCE.activeEditor.', 'tinyMCE.get(instance).', $getContent);

		$setContent = $editor->setContent('__EDITOR_INSTANCE__', '__EDITOR_CONTENT__');
		// fix for TinyMCE: Joomla 3.4.3 code works only if there's one editor instance on page
		$setContent = str_replace('tinyMCE.activeEditor.', 'tinyMCE.get(instance).', $setContent);

		$setContent = str_replace("'__EDITOR_INSTANCE__'", 'instance', $setContent);
		$setContent = str_replace('"__EDITOR_INSTANCE__"', 'instance', $setContent);  // codeMirror
		$setContent = str_replace("'__EDITOR_CONTENT__'", 'content', $setContent); // JCE
		$setContent = str_replace('"__EDITOR_CONTENT__"', 'content', $setContent); // Code mirror
		$setContent = str_replace("__EDITOR_CONTENT__", 'content', $setContent); // TinyMCE


		// various items needed for display
		JHtml::_('behavior.tooltip');
		JHtml::_('behavior.multiselect');
		JHtml::_('behavior.modal');
		JHtml::_('behavior.formvalidation');
		JHtml::_('behavior.keepalive');

		// Setup per language data
		$this->languages = JLanguageHelper::getLanguages('lang_code');
		$this->languagesList = array();
		$allLanguages = JosettaHelper::getJosettaLanguage();
		$editorsJs = '';
		foreach ($allLanguages as $languageCode => $languageName)
		{
			if ($languageCode != JosettaHelper::getSiteDefaultLanguage() && JosettaHelper_Acl::authorize('josetta.canTranslate.' . $languageCode))
			{
				// add data for this language
				$this->languagesList[$languageCode] = $languageName;

				// store the target language code
				$this->jsTargetLanguage[$languageCode] = str_replace('-', '_', $languageCode);

				// load translate model
				$model = ShlMvcModel_Base::getInstance('translate', 'JosettaModel');
				$model->setState('jlang', $languageCode);

				// Initialiase Josetta variables.
				$this->josettaForm[$languageCode] = $model->getJosettaForm($languageCode);
				if (count($errors = $this->get('Errors')))
				{
					JError::raiseError(500, implode("\n", $errors));
					return false;
				}

				// add javascript to collect editor(s) content, if any
				$editorJs = '';
				$fields = $this->josettaForm[$languageCode]->getFieldSet();
				foreach ($fields as $field)
				{
					if (strtolower($field->type) == 'editor')
					{ // there's at least one editor field
						$fieldSavecode = $field->save();

						// fix for TinyMCE in Joomla 3
						if (version_compare(JVERSION, '3.4', 'ge') && strpos($fieldSavecode, 'tinyMCE.get') !== false && strpos($fieldSavecode, '.save(') === false)
						{
							$fieldSavecode .= 'tinyMCE.get("' . $field->id . '").save();';
						}

						$editorJs .= "\n" . $fieldSavecode;
					}
				}
				if (!empty($editorJs))
				{
					$editorsJs .= "
				Josetta.saveEditorContent['" . $this->jsTargetLanguage[$languageCode] . "'] = function() {
					" . $editorJs . "
				};";
				}
				else
				{
					$editorsJs .= "
				Josetta.saveEditorContent['" . $this->jsTargetLanguage[$languageCode] . "'] = function() {
				};";
				}
				// Get the to-translate item unique id
				$this->translatedItemId[$languageCode] = $model->getAssociatedItemId($this->originalItem->id, $languageCode);

				// flag to show/hide the dissociate button
				// need to be associated to show it
				$this->showDissociateButton[$languageCode] = !empty($this->translatedItemId[$languageCode]);

				// get the current value for drop down on completion
				$completionValue[$languageCode] = $model
					->getCompletionValue($this->translatedItemId[$languageCode], $this->state->get('type'), $languageCode);

				// Completion dropdown list
				$this->completionSelectList[$languageCode] = JHTML::_('select.genericlist', $defaultValues,
					$this->jsTargetLanguage[$languageCode] . '_completion', 'onchange="Josetta.itemChanged(this);"', 'value', 'name',
					$completionValue);

				// compute the name of the database field that contains the publication status
				$this->publishingFieldName[$languageCode] = $this->jsTargetLanguage[$languageCode] . '_'
					. (string) $definitions->reference->published->column;

				// and get the current publication status
				$this->published[$languageCode] = $this->josettaForm[$languageCode]->getValue($this->publishingRealFieldName);

				//Get the Language ID and title
				$this->translate_language_title[$languageCode] = JText::sprintf('COM_JOSETTA_TRANSLATE_TO', (string) $definitions->name,
					JosettaHelper::getLanguageTitle($languageCode));

				// compute the full url for the Use existing content popup
				$this->useExistingContentUrl[$languageCode] = 'index.php?option=com_josetta&view=translator&tmpl=component&layout=modal&type='
					. $this->josetta_type . '&filter_lang=' . $languageCode;
				// append any filter for this particular type of content
				$filters = JosettaHelper::getUseExistingFilters($this->josetta_type, $this->originalItem);
				// if there are more than one
				if (!empty($filters))
				{
					foreach ($filters as $filter)
					{
						$this->useExistingContentUrl[$languageCode] .= '&' . $filter->name . '=' . $filter->value;
					}
				}

				// finally route it
				$this->useExistingContentUrl[$languageCode] = JRoute::_($this->useExistingContentUrl[$languageCode]);

			}
		}

		// various javascript vars, to pass data to live page
		$js = "
			Josetta.suggestUrl = '" . JRoute::_('index.php?option=com_josetta&task=translate.suggest&format=raw', false)
			. "';
	Josetta.itemType = '" . $this->josetta_type . "';
    Josetta.srcLanguage = '" . $this->originalItem->language . "';
    Josetta.originalItemId = " . $this->originalItem->id . ";
    Josetta.josettaItemid = " . JosettaHelper::getJosettaItemid() . ";
    Josetta.formToken = '" . JSession::getFormToken() . "';
    Josetta.getEditorContent = function( instance) {
    var content = " . $getContent . "
    return content;
    }
    Josetta.setEditorContent = function( instance, content) {
    " . $setContent . "
    return content;
    }
    ";

		// pass the translation target language list to js
		// and initialize modified state of form
		$saved = array();
		$jsLanguageList = array();
		$dirty = $model->getState('dirty', array());
		foreach ($this->languagesList as $languageCode => $languageName)
		{
			$jsLanguageList[] = array('code' => $this->jsTargetLanguage[$languageCode], 'name' => $languageName);
			$saved[$this->jsTargetLanguage[$languageCode]] = empty($dirty[$languageCode]);
		}

		// pass the translator view url to js
		$js .= "\nJosetta.translatorViewUrl = '"
			. JRoute::_('index.php?option=com_josetta&view=translator&Itemid=' . JosettaHelper::getJosettaItemid(), false) . '\';';

		// init modified state
		$js .= "\nJosetta.saved = " . json_encode($saved);

		$js .= "\nJosetta.languages = " . json_encode($jsLanguageList) . ';';

		$js .= "\nJosetta.init();";

		$js .= "\nfunction JosettaUseExistingContent(id,language) {
				Josetta.useExistingContent(id,language);
				}";

		$js .= $editorsJs;

		// insert into view
		$document->addScriptDeclaration($js);

		$this->addTranslatedStrings();

		parent::display($tpl);

	}

	/**
	 * Initiliase Joomla javascript JText
	 */
	private function addTranslatedStrings()
	{
		JText::script('COM_JOSETTA_CONFIRM_TRANSLATE_CANCEL');
		JText::script('COM_JOSETTA_CONFIRM_DISSOCIATE');
		JText::script('COM_JOSETTA_TRANSLATE_INVALID');
		JText::script('COM_JOSETTA_ERROR_SERVER_NOT_RESPONDING');
		JText::script('COM_JOSETTA_CONFIRM_LOOSE_TRANSLATION');
	}

}
