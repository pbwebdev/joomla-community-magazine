<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of newsfeeds.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_newsfeeds
 * @since		1.6
 */
class JosettaViewNewsfeeds extends ShlMvcView_Base {
  protected $items;
  protected $pagination;
  protected $state;

  /**
   * Display the view
   */
  public function display($tpl = null)
  {
    $this->items		= $this->get('Items');
    $this->pagination	= $this->get('Pagination');
    $this->state		= $this->get('State');

    // Check for errors.
    if (count($errors = $this->get('Errors'))) {
      JError::raiseError(500, implode("\n", $errors));
      return false;
    }

    // load com_contact strings
    JFactory::getLanguage()->load( 'com_newsfeeds', JPATH_ADMINISTRATOR);

    // backend list styles
    $document = JFactory::getDocument();
    $document->addStyleSheet( JURI::root(true).'/media/system/css/adminlist.css');
    $document->addStyleSheet(JURI::root(true).'/media/com_josetta/css/josetta.css');

    parent::display($tpl);
  }

}
