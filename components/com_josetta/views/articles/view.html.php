<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of articles.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_content
 * @since		1.6
 */
class JosettaViewArticles extends ShlMvcView_Base {
  
  protected $items;
  protected $pagination;
  protected $state;

  /**
   * Display the view
   *
   * @return	void
   */
  public function display($tpl = null)
  {
    $this->items		= $this->get('Items');
    $this->pagination	= $this->get('Pagination');
    $this->state		= $this->get('State');
    $this->authors		= $this->get('Authors');

    // Check for errors.
    if (count($errors = $this->get('Errors'))) {
      JError::raiseError(500, implode("\n", $errors));
      return false;
    }

    // Levels filter.
    $options	= array();
    $options[]	= JHtml::_('select.option', '1', JText::_('J1'));
    $options[]	= JHtml::_('select.option', '2', JText::_('J2'));
    $options[]	= JHtml::_('select.option', '3', JText::_('J3'));
    $options[]	= JHtml::_('select.option', '4', JText::_('J4'));
    $options[]	= JHtml::_('select.option', '5', JText::_('J5'));
    $options[]	= JHtml::_('select.option', '6', JText::_('J6'));
    $options[]	= JHtml::_('select.option', '7', JText::_('J7'));
    $options[]	= JHtml::_('select.option', '8', JText::_('J8'));
    $options[]	= JHtml::_('select.option', '9', JText::_('J9'));
    $options[]	= JHtml::_('select.option', '10', JText::_('J10'));

    $this->assign('f_levels', $options);

    // load com_contact strings
    JFactory::getLanguage()->load( 'com_content', JPATH_ADMINISTRATOR);

    // backend list styles
    $document = JFactory::getDocument();
    $document->addStyleSheet( JURI::root(true).'/media/system/css/adminlist.css');
    $document->addStyleSheet(JURI::root(true).'/media/com_josetta/css/josetta.css');

    parent::display($tpl);
  }

}
