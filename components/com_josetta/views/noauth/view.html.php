<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to translate Josetta interface.
 *
 * @package		Josetta
 * @subpackage	com_josetta
 * @since		1.6
 */

class JosettaViewNoauth extends ShlMvcView_Base {

  public function display($tpl = null)
  {
    $document = JFactory::getDocument();

    // add css and js
    $document->addStyleSheet( JURI::root(true).'/templates/system/css/general.css');
    $document->addStyleSheet( JURI::root(true).'/media/com_josetta/css/josetta.css');

    parent::display($tpl);
  }
}