<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

defined('_JEXEC') or die;

/**
 * Josetta component helper.
 *
 * @package		Joomla.Site
 * @subpackage	com_josetta
 */

abstract class JosettaHelper_Acl
{

	protected static $_permissions = null;

	/**
	 * Build an in memory array of permission, based on currently logged in
	 * user and installed languages
	 */
	public static function loadPermissions()
	{
		return true;
	}

	/**
	 * Check if a particular action is allowed for a user
	 * or the currently logged in user if none supplied
	 * @param string $action
	 * @param string $user
	 */
	public static function authorize($action, $user = null)
	{
		return true;
	}
}

