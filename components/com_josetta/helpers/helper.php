<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 */

defined('_JEXEC') or die;

/**
 * Josetta component helper.
 *
 * @package		Joomla.Site
 * @subpackage	com_josetta
 */

abstract class JosettaHelper
{
	const TRANSLATION_STATUS_ONLY_UNCOMPLETED = 1;
	const TRANSLATION_STATUS_ONLY_COMPLETED = 2;
	const TRANSLATION_STATUS_ONLY_STARTED = 3;
	/**
	 * Cached array of the category items.
	 *
	 * @var    array
	 * @since  11.1
	 */
	protected static $_categoriesPerLanguage = array();

	protected static $_menus = null;

	protected static $_version = '1.3.3.449';

	protected static $_permissions = null;

	protected static $_joomlaAssociationContexts = array(
			'2' => array('com_menus.item'),
			'3' => array('com_menus.item', 'com_contact.item',
					'com_content.item', 'com_newsfeeds.item',
					'com_weblinks.item', 'com_categories.item'));

	/**
	 * Return hardcoded version string
	 */
	public static function getVersion()
	{
		return self::$_version;
	}

	/**
	 * Check if menus or items associations are enabled
	 * at application level, compatible with both J2 and J3
	 *
	 * @param object Joomla! application object
	 */
	public static function isAssociationEnabled($app)
	{
		if (version_compare(JVERSION, '3', 'ge'))
		{
			$property = 'item_associations';
		}
		else
		{
			$property = 'menu_associations';
		}

		$enabled = !empty($app->$property);
		return $enabled;
	}

	public static function getLanguageTitle($languageCode)
	{
		jimport('joomla.language.helper');
		$languages = JLanguageHelper::getLanguages('lang_code');

		$languageName = empty($languages[$languageCode]) ? ''
				: $languages[$languageCode]->title;

		return $languageName;
	}

	/**
	 * Fetches the list of available languages,
	 * removing the default one
	 *
	 * @return	array
	 *
	 */
	public static function getJosettaLanguage($includeDefault = false)
	{
		$languagesList = array();
		$languages = JLanguageHelper::getLanguages();
		$defaultLanguage = JosettaHelper::getSiteDefaultLanguage();

		foreach ($languages as $language)
		{
			// we collect all languages except the default one
			if ($includeDefault || $defaultLanguage != $language->lang_code)
			{
				//load the array for language
				$languagesList[$language->lang_code] = $language->title;
			}
		}
		return $languagesList;
	}

	/**
	 *
	 * Joomla does not seem to report the default language properly
	 * as of J! 2.5.1; It always report 'en-GB' as the 'default'
	 * property does not seem to be updated with the user set
	 * default language
	 * So this will read it directly from the languages table
	 */
	public static function getSiteDefaultLanguage($clientId = 0)
	{
		static $_defaultLanguage = null;

		if (is_null($_defaultLanguage))
		{
			jimport('joomla.application.component.helper');
			$params = JComponentHelper::getParams('com_languages');
			jimport('joomla.application.helper');
			$client = JApplicationHelper::getClientInfo((int) $clientId);
			$_defaultLanguage = $params->get($client->name, 'en-GB');
		}

		return $_defaultLanguage;
	}

	/**
	 * Check the access level to authenticate josetta User and group.
	 * We only check user is a member of the Josetta user group
	 *
	 */
	public static function checkViewAccess($view)
	{
		// allow access to the not authorized view!
		if ($view == 'noauth')
		{
			return;

		}

		$app = JFactory::getApplication();

		// if not logged in, we redirect to the login page
		$user = JFactory::getUser();
		if ($user->guest)
		{
			$returnUrl = 'index.php?option=com_josetta&view=translator&Itemid='
					. self::getJosettaItemid();
			$returnUrl = base64_encode($returnUrl);
			$targetUrl = JRoute::_(
					'index.php?option=com_users&view=login&return='
							. $returnUrl, false);
			$app->redirect($targetUrl);
		}

		// is user allowed to access Josetta
		$authorized = JosettaHelper_Acl::authorize('josetta.canAccess');

		// do not allow direct access to modal views (articles, contacts, newsfeeds)
		if ($authorized
				&& in_array($view, array('articles', 'contacts', 'newsfeeds')))
		{
			$layout = $app->input->getCmd('layout');
			$authorized = $layout == 'modal';
		}

		// if not a member of the required group, we redirect to the Not authorized view
		if (!$authorized)
		{
			$targetUrl = JRoute::_(
					'index.php?option=com_josetta&view=noauth&Itemid='
							. self::getJosettaItemid(), false);
			$app->redirect($targetUrl);
		}
	}

	/**
	 * Identify the Itemid used to display Josetta pages
	 */
	public static function getJosettaItemid()
	{
		static $_menuItemid = null;

		if (is_null($_menuItemid))
		{
			try
			{
				$_menuItemid = (int) ShlDbHelper::selectResult('#__menu', 'id',
						array('menutype' => 'josetta'));
				if (empty($_menuItemid))
				{
					// user moved or deleted our menu item
					// fetch installed Josetta extension id, as menu item needs that
					$componentId = (int) ShlDbHelper::selectResult(
							'#__extensions', 'extension_id',
							array('type' => 'component',
									'element' => 'com_josetta'));
					if (!empty($componentId))
					{
						// we know com_josetta component id, let's try find one or more menu items for it
						$menuItems = ShlDbHelper::selectObjectList('#__menu',
								'*',
								array('type' => 'component',
										'component_id' => $componentId,
										'published' => 1, 'client_id' => 0));
						if (!empty($menuItems))
						{
							// we have one or more, try to pick one
							foreach ($menuItems as $menuItem)
							{
								// first one to the exact link wins
								if ($menuItem->link
										== 'index.php?option=com_josetta&view=translator')
								{
									$_menuItemid = $menuItem->id;
									break;
								}

							}
						}
					}
				}
			}
			catch (Exception $e)
			{
				$_menuItemid = 0;
				ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__,
						__METHOD__, __LINE__, $e->getMessage());
			}
		}

		return $_menuItemid;
	}

	/**
	 * Method to get josetta Defination by calling onJosettaGetDefinitions method of plugin.
	 *
	 *@param context type
	 *
	 * @return	 JXMLElement Object
	 */
	public static function getJosettaDefinitions($context)
	{
		static $_definitions = array();

		if (empty($_definitions[$context]))
		{
			//helper method to load josetta plugin
			self::loadPlugins();
			$dispatcher = JDispatcher::getInstance();

			//call to onJosettaGetDefinitions plugin method to load the xml
			$_definitions[$context] = $dispatcher
					->trigger('onJosettaGetDefinitions', array($context));
			if (!empty($_definitions[$context]))
			{
				$_definitions[$context] = is_array($_definitions[$context])
						? $_definitions[$context][0] : $_definitions[$context];
			}
		}

		return $_definitions[$context];
	}

	/**
	 * Method to call the type of  josetta plugin enabled .
	 *
	 *
	 * @return	array
	 * @since	1.6
	 */
	public static function getJosettaContentTypes()
	{
		//helper method to load josetta plugin
		self::loadPlugins();
		$dispatcher = JDispatcher::getInstance();

		//call plugin method onJosettaGetTypes
		$results = $dispatcher->trigger('onJosettaGetTypes');
		$getJosettaContentTypes = array();
		if (!empty($results))
		{
			foreach ($results as $result)
			{
				$getJosettaContentTypes = array_merge($getJosettaContentTypes,
						$result[0]);
			}
		}

		return $getJosettaContentTypes;
	}

	/**
	 * Returns an array of categories for the given extension.
	 *
	 * //TODO should be in plugin
	 *
	 * @param   string  $extension  The extension option e.g. com_something.
	 * @param   array   $config     An array of configuration options. By default, only
	 *                              published and unpublished categories are returned.
	 *
	 * @return  array
	 *
	 */
	public static function getCategoryOptionsPerLanguage($extension,
			$config = array('filter.published' => array(0, 1),
					'filter.languages' => array()))
	{
		$hash = md5($extension . '.' . serialize($config));

		if (!isset(self::$_categoriesPerLanguage[$hash]))
		{
			$config = (array) $config;
			$db = ShlDbHelper::getDb();
			$query = $db->getQuery(true);

			$query->select('a.id, a.title, a.level');
			$query->from('#__categories AS a');
			$query->where('a.parent_id > 0');

			// Filter on extension.
			$query->where('extension = ' . $db->quote($extension));

			// Filter on the published state
			if (isset($config['filter.published']))
			{
				if (is_numeric($config['filter.published']))
				{
					$query
							->where(
									'a.published = '
											. (int) $config['filter.published']);
				}
				elseif (is_array($config['filter.published']))
				{
					JArrayHelper::toInteger($config['filter.published']);
					$query
							->where(
									'a.published IN ('
											. implode(',',
													$config['filter.published'])
											. ')');
				}
			}

			if (isset($config['filter.languages']))
			{
				if (is_array($config['filter.languages'])
						&& !empty($config['filter.languages']))
				{
					$languages = array();
					foreach ($config['filter.languages'] as $language)
					{
						$languages[] = $db->quote($language);
					}
					$query
							->where(
									'a.language IN ('
											. implode(',', $languages) . ')');
				}
			}

			$query->order('a.lft');
			try
			{
				$db->setQuery($query);
				$items = $db->shlLoadObjectList();
			}
			catch (Exception $e)
			{
				$items = array();
				ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__,
						__METHOD__, __LINE__, $e->getMessage());
			}
			// Assemble the list options.
			self::$_categoriesPerLanguage[$hash] = array();

			foreach ($items as &$item)
			{
				$repeat = ($item->level - 1 >= 0) ? $item->level - 1 : 0;
				$item->title = str_repeat('- ', $repeat) . $item->title;
				self::$_categoriesPerLanguage[$hash][] = JHtml::_(
						'select.option', $item->id, $item->title);
			}
		}

		return self::$_categoriesPerLanguage[$hash];
	}

	public static function getItemSubtitle($context, $item)
	{

		if (empty($context) || empty($item))
		{
			return '';
		}

		//helper method to load josetta plugin
		self::loadPlugins();
		$dispatcher = JDispatcher::getInstance();

		//call to onJosettaGetDefinitions plugin method to load the xml
		$subTitle = $dispatcher
				->trigger('onJosettaGetSubtitle', array($context, $item));
		$subTitle = empty($subTitle) ? ''
				: (is_array($subTitle) ? $subTitle[0] : $subTitle);

		return $subTitle;
	}

	/**
	 * Get html for a filter to be displayed on the main translator page
	 * when such filter is not one of Josetta built in filters
	 *
	 * @param string $context
	 * @param string $filterType
	 * @param string $filterName
	 * @param mixed $current
	 */
	public static function get3rdPartyFilter($context, $filterType, $filterName,
			$current)
	{
		if (empty($context))
		{
			return array();
		}

		// helper method to load josetta plugin
		self::loadPlugins();
		$dispatcher = JDispatcher::getInstance();

		// trigger event
		$filter = $dispatcher
				->trigger('onJosettaGet3rdPartyFilter',
						array($context, $filterType, $filterName, $current));
		$filter = empty($filter) ? array()
				: (is_array($filter) ? $filter[0] : $filter);

		return $filter;
	}

	/**
	 * Get html for filters to be displayed on the "Use existing content"
	 * modal windows
	 *
	 * @param string $context
	 * @param object $item
	 */
	public static function getUseExistingFilters($context, $item)
	{
		if (empty($context) || empty($item))
		{
			return array();
		}

		//helper method to load josetta plugin
		self::loadPlugins();
		$dispatcher = JDispatcher::getInstance();

		//call to onJosettaGetDefinitions plugin method to load the xml
		$filters = $dispatcher
				->trigger('onJosettaGetUseExistingFilters',
						array($context, $item));
		$filters = empty($filters) ? array()
				: (is_array($filters) ? $filters[0] : $filters);

		return $filters;
	}

	/**
	 * Get details of a category, identified by its id
	 *
	 * //TODO should be in plugin
	 *
	 * @param integer $id
	 */
	public static function getCategoryDetails($id)
	{
		$category = null;
		if (!empty($id))
		{
			try
			{
				$category = ShlDbHelper::selectObject('#__categories', '*',
						array('id' => $id));

			}
			catch (Exception $e)
			{
				ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__,
						__METHOD__, __LINE__, $e->getMessage());
			}
		}

		return $category;
	}

	/**
	 * Returns an array of available menu
	 *
	 * @param   array   $config     An array of configuration options. By default, only
	 *                              published and unpublished menu are returned.
	 *
	 * @return  array
	 *
	 */
	public static function getMenuList()
	{
		if (is_null(self::$_menus))
		{
			try
			{
				self::$_menus = ShlDbHelper::selectObjectList('#__menu_types',
						array('menutype', 'title'), array(), array(),
						array('title'));

			}
			catch (Exception $e)
			{
				ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__,
						__METHOD__, __LINE__, $e->getMessage());
			}
			self::$_menus = empty(self::$_menus) ? array() : self::$_menus;
		}

		return self::$_menus;
	}

	public static function getSiteModuleListOptions()
	{
		// Initialise variables.
		$options = array();

		// read list of modules
		try
		{
			$modules = ShlDbHelper::selectObjectList('#__modules',
					array('module'), array('client_id' => 0), array(),
					array('module'));

			// create html options
			foreach ($modules as $module)
			{
				$options[$module->module] = JHtml::_('select.option',
						$module->module, $module->module);
			}
		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__,
					__METHOD__, __LINE__, $e->getMessage());
		}

		return $options;
	}

	public static function getTranslationStats()
	{
		$stats = new stdClass();
		$stats->total = 0;
		$stats->error = '';

		try
		{
			// read associations records, grouping them by type. We only count the number of translations, not records
			// so we substract 1 from each grouping, so as to get rid of the original language record
			$stats->associationsRecords = ShlDbHelper::quoteQuery(
					'select sum(?? - 1) as count, t2.?? from (select count( t.??) as counter, t.??, t.?? from ?? as t group by t.??) as t2 group by t2.??',
					array('counter', 'context', 'key', 'key', 'context',
							'#__josetta_associations', 'key', 'context'))
					->loadObjectList('context');

			// add up to get a total
			foreach ($stats->associationsRecords as $record)
			{
				$stats->total += $record->count;
			}
		}
		catch (Exception $e)
		{
			ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__,
					__METHOD__, __LINE__, $e->getMessage());
			$stats->error = JText::_('COM_JOSETTA_UNABLE_TO_CREATE_STATS');
		}

		return $stats;
	}

	/**
	 * Returns
	 * @param unknown_type $userId
	 */
	public static function getUserName($userId)
	{
		$userId = (int) $userId;

		if (empty($userId))
		{
			return '-';
		}

		// we have a non empty user, id let's load user data
		$user = JUser::getInstance($userId);
		return empty($user) ? '-' : $user->name;

	}

	public static function getInputArray($input, $arrayName)
	{
		$superGlobal = '_' . strtoupper($arrayName);

		if (isset($GLOBALS[$superGlobal]))
		{
			$keys = array();
			foreach (array_keys($GLOBALS[$superGlobal]) as $key)
			{
				$keys[$key] = 'raw';
			}
			return $input->getArray($keys);
		}

		return null;
	}

	/**
	 * Check the action of josetta User and group.
	 *
	 */
	public static function getActions()
	{
		$user = JFactory::getUser();
		$result = new JObject;

		$assetName = 'com_josetta';

		$actions = array('core.admin', 'core.manage', 'core.create',
				'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete');

		foreach ($actions as $action)
		{
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}

	public static function enqueueMessages($errors, $type = 'error')
	{
		if (empty($errors))
		{
			return;
		}

		if (!is_array($errors))
		{
			$errors = array($errors);
		}

		foreach ($errors as $error)
		{
			JFactory::getApplication()->enqueueMessage($error, $type);
		}
	}

	/**
	 * Method to import josetta plugin into the framework
	 *
	 * @param	string	load the josetta plugin.
	 *
	 * @return	status of plugin
	 *
	 */
	public static function loadPlugins($pluginGroups = array())
	{
		$pluginGroups = empty($pluginGroups) ? array('josetta', 'josetta_ext')
				: $pluginGroups;
		$pluginGroups = is_array($pluginGroups) ? $pluginGroups
				: array($pluginGroups);
		// required joomla library
		jimport('joomla.plugin.helper.php');
		// import the plugin files
		$status = true;
		foreach ($pluginGroups as $group)
		{
			$newStatus = JPluginHelper::importPlugin($group);
			if (!$newStatus && $group == 'josetta')
			{
				// we care only about core plugins, as there may not be any extension plugins
				$status = false;
				ShlSystem_log::logError('Unable to load core Josetta plugins');
				ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__,
						__METHOD__, __LINE__,
						'Unable to load core Josetta plugins');
			}
		}
		return $status;
	}

	/**
	 *
	 * Method to build the list of available extension for joomla category
	 *
	 * @return array a list of options to be used to build a select list
	 *
	 */
	public static function buildComponentList()
	{
		static $_extensionList = null;

		if (is_null($_extensionList))
		{
			try
			{
				$extensions = ShlDbHelper::quoteQuery(
						'select distinct ?? from ?? where ?? != ?',
						array('extension', '#__categories', 'level'), array(0))
						->shlLoadObjectList();
				foreach ($extensions as $extension)
				{
					//list of extension in array
					$extensionList[$extension->extension] = $extension
							->extension;
				}
			}
			catch (Exception $e)
			{
				$_extensionList = array();
				ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__,
						__METHOD__, __LINE__, $e->getMessage());
			}
		}

		return $extensionList;

	}
	/**
	 *
	 * Method to format the grid of data which contain user language and date
	 *
	 *@param        string data value of the field
	 *@param        string $type the
	 *
	 *@return       html format of data
	 *
	 */
	public static function formatGridData($data, $type)
	{
		switch ($type)
		{
			case 'level':
				if ($data[1] > 0)
				{
					$data = str_repeat(
							'<span class="josetta_levels_display">|&mdash;</span>',
							$data[1] - 1) . ($data[0]);
				}
				else
				{
					$data = $data[0];
				}
				break;

			case 'user':
			//get the name of user from id
				$user = self::_getUSerDetails($data);
				$data = empty($user) ? '-' : $user->name;
				break;

			case 'language':
				if ($data == '*')
				{
					$data = JText::alt('JALL', 'language');
				}
				else
				{
					//name of language on the basis of language code
					jimport('joomla.language.helper');
					$languages = JLanguageHelper::getLanguages('lang_code');
					$data = ($languages[$data]->title);
				}
				break;

			case 'flag':
				if ($data == '*')
				{
					$data = JText::alt('JALL', 'language');
				}
				else
				{
					//name of language on the basis of language code
					jimport('joomla.language.helper');
					$languages = JLanguageHelper::getLanguages('lang_code');
					$data = '<span class="josettaLanguageBadge">'
							. strtoupper($languages[$data]->sef) . '</span>';
				}
				break;

			case 'date':
			//return date format
				if ($data == '0000-00-00 00:00:00')
				{
					$data = '-';
				}
				else
				{
					$data = JHtml::_('date', $data, 'Y-m-d H:i:s');
				}
				break;

			default:
			// give a chance to 3rd party plugins to add their own formatting
				$thirdPartyData = self::_get3rdPartyFormatGridData($data, $type);

				// and merge them with built in ones
				if (!empty($thirdParty))
				{
					$data = $thirdPartyData;
				}
				break;
		}
		return $data;
	}

	protected static function _getUSerDetails($id)
	{
		static $_users = array();

		if (empty($_users[$id]))
		{
			try
			{
				$user = ShlDbHelper::selectObject('#__users', '*',
						array('id' => $id));
				return $user;

			}
			catch (Exception $e)
			{
				ShlSystem_Log::error('josetta', '%s::%s::%d: %s', __CLASS__,
						__METHOD__, __LINE__, $e->getMessage());
				return null;
			}
		}

		return $_users[$id];
	}

	/**
	 * Get a formatted representation of the translator view
	 * display field from the 3rd party plugins
	 * Basic fields types formatting is built in, but
	 * plugins can (will) have their own fiels type, and so
	 * they should provide formatting for them
	 *
	 * @param string $data the raw data to display
	 * @param string $type the data type, ie language, date, etc
	 * @param string the formatted, ready to display, string
	 */
	protected static function _get3rdPartyFormatGridData($data, $type)
	{
		if (empty($data))
		{
			return '';
		}

		// helper method to load josetta plugin
		self::loadPlugins();
		$dispatcher = JDispatcher::getInstance();

		// trigger event
		$text = $dispatcher
				->trigger('onJosettaGet3rdPartyFormatGridData',
						array($data, $type));
		$text = empty($text) ? '' : (is_array($text) ? $text[0] : $text);

		return $text;
	}

	/**
	 * format the original field of josetta
	 *
	 * @param	form		JForm object.
	 * @param	field		JForm field.
	 *
	 *@return	string html representation of the field, ready to display
	 */
	public static function formatTranslationField($form, $field)
	{
		$html = '';

		// adjust output according to field type
		switch (strtolower($field->type))
		{
			case 'editor':
				$sha1 = '<input type="hidden" name="jta_sha1_' . $field->id
						. '" value="" />';
				$html .= '<div class="jseditor">' . $sha1 . $field->input
						. '</div>';
				break;
			default:
			// give a chance to 3rd party plugins to add their own formatting
				$thirdPartyHtml = self::_get3rdPartyFormatTranslationField(
						$form, $field);

				// and merge them with built in ones
				if (!empty($thirdPartyHtml))
				{
					$html = $thirdPartyHtml;
				}
				else
				{
					$html = $field->input;
				}
				break;
		}

		return $html;
	}

	/**
	 * Get a formatted representation of a translate view translation field
	 * from third party plugins
	 * Basic types fields display is builtin, but plugins can/will add
	 * their own fields, for which they should provide formatting
	 *
	 * @param object $form the Joomla! form object to which the field belongs
	 * @param object $field the Joomla! field object
	 * @param string the formatted, ready to display, string
	 */
	protected static function _get3rdPartyFormatTranslationField($form, $field)
	{
		if (empty($form) || empty($field))
		{
			return '';
		}

		// helper method to load josetta plugin
		self::loadPlugins();
		$dispatcher = JDispatcher::getInstance();

		// trigger event
		$html = $dispatcher
				->trigger('onJosettaGet3rdPartyFormatTranslationField',
						array($form, $field));
		$html = empty($html) ? '' : (is_array($html) ? $html[0] : $html);

		return $html;
	}

	/**
	 * format the original field of josetta
	 *
	 *@param	mixed value of the original field
	 *@param	field	JForm field.
	 *
	 *@return	string
	 */
	public static function formatOriginalField($originalItem,
			$originalFieldTitle, $field)
	{
		$html = '';
		// adjust output according to field type
		switch (strtolower($field->type))
		{

			case 'languagecategoryparent':
			case 'languagecategory';
			case 'category':
			// elemnt id can be stored in 2 different locations, depending on plugin
				$elementId = empty($originalItem->request)
						|| !isset($originalItem->request['id']) ? null
						: $originalItem->request['id'];
				$elementId = is_null($elementId)
						? $originalItem->$originalFieldTitle : $elementId;
				if (is_array($elementId))
				{
					// mmultiple categories selected
					$size = $field->element->attributes()->size;
					$size = empty($size) ? 10 : $size;
					$html .= '<select name="josetta_dummy" id="josetta_dummy" class="inputbox" size="'
							. $size
							. '" multiple="multiple" disabled="disabled">'
							. "\n";
					$categoriesOptionsHtml = JHtml::_('category.options',
							$field->element->attributes()->extension);
					$html .= JHtml::_('select.options', $categoriesOptionsHtml,
							'value', 'text', $elementId) . "\n";
					$html .= "</select>\n";
				}
				else
				{
					// just one category
					if (empty($elementId))
					{
						$html = JText::_('ROOT');
					}
					else
					{
						$categoryDetails = self::getCategoryDetails($elementId);
						$html = empty($categoryDetails) ? $elementId
								: $categoryDetails->title;
						if ($html == 'ROOT')
						{
							$html = JText::_('ROOT');
						}
					}
				}
				break;

			case 'languagemenuparent':
			case 'menuordering':
			case 'menuitem':
				$html = '';
				$elementId = $originalItem->$originalFieldTitle;
				if ($elementId == 1)
				{
					$html = JText::_('COM_MENUS_ITEM_ROOT');
				}
				else if (!empty($elementId))
				{
					$app = JFactory::getApplication();
					$menu = $app->getMenu();
					$menuItem = $menu->getItem($elementId);
					$html = empty($menuItem->title) ? '' : $menuItem->title;
				}
				break;

			case 'bannerclient':
				$html = '';
				$elementId = $originalItem->$originalFieldTitle;
				if (!empty($elementId))
				{
					try
					{
						$html = ShlDbHelper::selectResult('#__banner_clients',
								'name', array('id' => $elementId));
					}
					catch (Exception $e)
					{
						ShlSystem_Log::error('josetta', '%s::%s::%d: %s',
								__CLASS__, __METHOD__, __LINE__,
								$e->getMessage());
						$html = '';
					}
				}
				break;

			case 'josettalist':
				$html = JText::_(trim($field->element->option));
				break;

			case 'menu':
				$menuList = JosettaHelper::getMenuList();
				$html = $originalItem->$originalFieldTitle;
				foreach ($menuList as $menu)
				{
					if ($menu->menutype == $originalItem->menutype)
					{
						$html = $menu->title;
					}
				}
				break;

			case 'modal_article':
				$elementId = empty($originalItem->request)
						|| empty($originalItem->request['id']) ? null
						: $originalItem->request['id'];
				$elementId = is_null($elementId)
						? $originalItem->$originalFieldTitle : $elementId;
				$html = ShlDbHelper::selectResult('#__content', 'title',
						array('id' => $elementId));
				break;

			case 'modal_contacts':
				$elementId = empty($originalItem->request)
						|| empty($originalItem->request['id']) ? null
						: $originalItem->request['id'];
				$elementId = is_null($elementId)
						? $originalItem->$originalFieldTitle : $elementId;
				$html = ShlDbHelper::selectResult('#__contact_details', 'name',
						array('id' => $elementId));
				break;

			case 'modal_newsfeeds':
				$elementId = empty($originalItem->request)
						|| empty($originalItem->request['id']) ? null
						: $originalItem->request['id'];
				$elementId = is_null($elementId)
						? $originalItem->$originalFieldTitle : $elementId;
				$html = ShlDbHelper::selectResult('#__newsfeeds', 'name',
						array('id' => $elementId));
				break;

			case 'requesttext':
				$html = $originalItem->request[$originalFieldTitle];
				break;

			case 'editor':
				$html = self::preprocessHtmlOriginalField(
						$originalItem->$originalFieldTitle);
				break;

			case 'text':
			case 'textarea':
			case 'josettatext':
				$html = $originalItem->$originalFieldTitle;
				break;

			default:
			// give a chance to 3rd party plugins to add their own formatting
				$thirdPartyHtml = self::_get3rdPartyFormatOriginalField(
						$originalItem, $originalFieldTitle, $field);

				// and merge them with built in ones
				if (!empty($thirdPartyHtml))
				{
					$html = $thirdPartyHtml;
				}
				else
				{
					$html = $originalItem->$originalFieldTitle;
				}
				break;
		}

		return $html;
	}

	/**
	 * Get a formatted representation of a translate view original value field
	 * from third party plugins
	 * Basic types fields display is builtin, but plugins can/will add
	 * their own fields, for which they should provide formatting
	 *
	 * @param object $originalItem the actual data of the original item
	 * @param string $originalFieldTitle the field title
	 * @param object $field the Joomla! field object
	 * @param string the formatted, ready to display, string
	 */
	protected static function _get3rdPartyFormatOriginalField($originalItem,
			$originalFieldTitle, $field)
	{
		if (empty($originalItem) || empty($field))
		{
			return '';
		}

		// helper method to load josetta plugin
		self::loadPlugins();
		$dispatcher = JDispatcher::getInstance();

		// trigger event
		$html = $dispatcher
				->trigger('onJosettaGet3rdPartyFormatOriginalField',
						array($originalItem, $originalFieldTitle, $field));
		$html = empty($html) ? '' : (is_array($html) ? $html[0] : $html);

		return $html;
	}

	/**
	 * Create Copy and suggest buttons
	 *
	 * @param	form		JForm object.
	 * @param	field		JForm field.
	 * @param string $language the translation target language
	 *
	 * @return	HTML button
	 */
	public static function createButton($form, $field, $language)
	{
		$field->title = str_replace($language . '_josetta_form_', '',
				$field->id);

		$html = '';
		$content = '';
		switch (strtolower($field->type))
		{

			case 'editor':
			//call javascript method copyjosettaeditor to copy data from hidden span to textarea
				$content = ' <a  class="josetta_button" onclick="Josetta.copyEditor(\''
						. $field->title . '\', \'' . $language . '\');">'
						. JText::_('COM_JOSETTA_TRANSLATE_COPY') . '</a>';
				if (self::canSuggestTranslation())
				{
					$content .= self::getSuggestButton($field, $language);
				}
				break;
			case 'josettalist':
			case 'list':
				$content = '&nbsp;';
				break;
			case 'languagecategory':
			case 'languagecategoryparent':
			case 'category':
			case 'modal_article':
				$content = '&nbsp;';
				break;
			case 'menu':
			case 'languagemenuparent':
			case 'menuordering':
				$content = self::getCopyButton($field, $language);
				break;
			default:
			// give a chance to 3rd party plugins to add their own formatting
				$thirdPartyButtons = self::_get3rdPartycreateButton($form,
						$field, $language);

				// and merge them with built in ones
				if (!empty($thirdPartyButtons))
				{
					$content .= $thirdPartyButtons;
				}
				else
				{
					// no third party plugins took over, use built in behavior
					// always add a copy button
					$content = self::getCopyButton($field, $language);
					// and optionnally add a suggest translation button
					if (self::canSuggestTranslation())
					{
						$content .= self::getSuggestButton($field, $language);
					}
				}
				break;
		}
		$html .= '<div class="josetta_button_container">';
		$html .= $content;
		$html .= '</div>';

		return $html;
	}

	/**
	 * Get html for buttons to be offered to users, to carry over
	 * translation: normally a copy button, and optionnally a suggest
	 * translation button
	 * Basic types fields display is builtin, but plugins can/will add
	 * their own fields, for which they should provide buttons
	 *
	 * @param object $form the Joomla! form object to which the field belongs
	 * @param object $field the Joomla! field object
	 * @param string the formatted, ready to display, string
	 */
	protected static function _get3rdPartycreateButton($form, $field, $language)
	{
		if (empty($form) || empty($field))
		{
			return '';
		}

		// helper method to load josetta plugin
		self::loadPlugins();
		$dispatcher = JDispatcher::getInstance();

		// trigger event
		$html = $dispatcher
				->trigger('onJosettaGet3rdPartycreateButton',
						array($form, $field, $language));
		$html = empty($html) ? '' : (is_array($html) ? $html[0] : $html);

		return $html;
	}

	public static function getCopyButton($field, $language)
	{
		$content = '<a title="' . JText::_('COM_JOSETTA_TRANSLATE_COPY_HELP')
				. '" class="josetta_button hasTip" onclick="Josetta.copyValue( \''
				. $field->title . '\', \'' . $language . '\');">'
				. JText::_('COM_JOSETTA_TRANSLATE_COPY') . '</a>';
		return $content;
	}

	public static function getSuggestButton($field, $language)
	{
		$content = ShlMvcLayout_Helper::render(
				'com_josetta.translate.suggest_button_'
						. JosettaadminHelper_General::getVersionType(),
				array('field' => $field, 'language' => $language),
				JOSETTA_LAYOUTS_PATH);
		return $content;
	}

	public static function canSuggestTranslation()
	{
		// get params:
		$josettaParams = JComponentHelper::getParams('com_josetta');

		// check if a service is enabled
		$translationService = $josettaParams->get('translation_service_type');
		return !empty($translationService);
	}

	/**
	 * Clean up raw html to be displayed as a reference
	 * for the translator. Essentially remove images
	 * replacing them with an icon and the textual original link
	 *
	 * @param	string $html raw original html content
	 *
	 * @return	string  clean html
	 */
	public static function preprocessHtmlOriginalField($html)
	{
		$patterns = array();
		$replacements = array();

		// replace images with icon
		$html = preg_replace_callback(
				'#<img\s*src\s*=\s*[\"|\'](.*)[\"|\'](.*)\/>#iUs',
				'JosettaHelper::_imageTagReplacerCallback', $html);

		return $html;
	}

	protected static function _imageTagReplacerCallback($matches)
	{
		if (count($matches) < 2)
		{
			// could not find a link, don't change anything
			return $matches[0];
		}

		// we have a link, we can replace the image
		$replacement = '';
		$toolTip = JText::_('COM_JOSETTA_IMAGE') . '::';

		$tags = array();
		// search for file name
		$tags['file'] = '<strong>' . JText::_('COM_JOSETTA_FILE')
				. '</strong>: ' . $matches[1];

		// do we have more than just the src tag?
		if (!empty($matches[2]))
		{

			$bits = array();
			if (preg_match('#title\s*=\s*[\"|\'](.*)[\"|\']#iUs', $matches[2],
					$bits))
			{
				$tags['title'] = '<strong>' . JText::_('COM_JOSETTA_TITLE_TAG')
						. '</strong>: ' . $bits[1];
			}

			$bits = array();
			if (preg_match('#alt\s*=\s*[\"|\'](.*)[\"|\']#iUs', $matches[2],
					$bits))
			{
				$tags['alt'] = '<strong>' . JText::_('COM_JOSETTA_ALT_TAG')
						. '</strong>: ' . $bits[1];
			}
		}

		$tags['raw'] = '<strong>' . JText::_('COM_JOSETTA_RAW_IMAGE')
				. '</strong>: '
				. htmlspecialchars(trim($matches[0]), ENT_COMPAT, 'UTF-8');
		$toolTip .= implode('<br />', $tags);
		$replacement = '<img src="' . JURI::base(true)
				. '/media/com_josetta/images/icon-josetta-16x16.png'
				. '" class="hasTip" title="' . $toolTip . '">';

		return $replacement;
	}

	/**
	 * tooltip for ascoaition
	 *
	 * @param	int $id     Translation context item id
	 *
	 * return html tooltip for grid
	 */
	public static function getAssociationsTooltipData($id, $context)
	{
		// Get an instance of the Translate model
		$model = ShlMvcModel_Base::getInstance('translate', 'JosettaModel',
				array('ignore_request' => true));

		// Get the associations
		$associatedItems = $model->getAssociatedItemsList($id, $context);
		$languages = JLanguageHelper::getLanguages();
		$defaultLanguage = JosettaHelper::getSiteDefaultLanguage();

		// metadata
		$definitions = JosettaHelper::getJosettaDefinitions($context);
		$publishFieldName = (string) $definitions->reference->published->column;
		$lastModifiedFieldName = isset($definitions->reference->last_modified)
				? (string) $definitions->reference->last_modified->column : '';
		if (!empty($lastModifiedFieldName))
		{
			// this type of content has a last_modified field
			// we fetch the original item last_modified value, so as
			// to signal if a translation may need to be updated
			$originalLastModified = '';
			foreach ($languages as $language)
			{
				//if the language code is the current language skip the process
				if (!empty($associatedItems[$language->lang_code]->id)
						&& $language->lang_code == $defaultLanguage)
				{
					$originalLastModified = $associatedItems[$language
							->lang_code]->$lastModifiedFieldName;
				}
			}
		}

		// Construct html
		$tooltipData = new StdClass();
		$tooltipData->translationsCount = 0;
		$tooltipData->translatedLanguages = array();
		$tooltipStrings = '';
		jimport('joomla.string.string');
		foreach ($languages as $language)
		{
			//if the language code is the current language skip the process
			if (JosettaHelper_Acl::authorize(
					'josetta.canTranslate.' . $language->lang_code)
					&& !empty($associatedItems[$language->lang_code]->id)
					&& $language->lang_code != $defaultLanguage)
			{
				$tooltipData->translationsCount++;
				$tooltipData->translatedLanguages[] = $language->lang_code;

				// completion degree
				$completionString = empty(
						$associatedItems[$language->lang_code]->completion)
						? JText::_(
								'COM_JOSETTA_TRANSLATION_COMPLETION_JUST_STARTED')
						: $associatedItems[$language->lang_code]->completion
								. '%';

				// publication status
				$publishedString = empty(
						$associatedItems[$language->lang_code]
								->$publishFieldName) ? JText::_('JUNPUBLISHED')
						: JText::_('JPUBLISHED');

				// last modified date
				$thisLanguageLastModified = empty($lastModifiedFieldName) ? ''
						: $associatedItems[$language->lang_code]
								->$lastModifiedFieldName;
				$lastModifiedString = empty($thisLanguageLastModified)
						|| $thisLanguageLastModified == '0000-00-00 00:00:00'
						? ''
						: '<br /><small>('
								. JText::_('COM_JOSETTA_LAST_MODIFIED') . ': '
								. $thisLanguageLastModified . ')</small>';
				$needUpdate = !(empty($thisLanguageLastModified)
						|| $thisLanguageLastModified == '0000-00-00 00:00:00')
						&& ($originalLastModified > $thisLanguageLastModified);
				$color = $needUpdate ? 'red' : 'green';

				// build up string for this language
				$tooltipStrings[] = ($needUpdate ? '!&nbsp;' : '')
						. '<strong><font color=\'' . $color . '\'>'
						. $language->title . '</font></strong>: '
						. $completionString . ' ('
						. JString::strtolower($publishedString) . ')'
						. $lastModifiedString;
			}
		}

		$tooltipData->tooltip = empty($tooltipData->translationsCount)
				? JText::_('COM_JOSETTA_NO_AVAILABLE_TRANSLATIONS')
						. '::&nbsp;'
				: JText::_('COM_JOSETTA_AVAILABLE_TRANSLATIONS') . '::'
						. implode('<br />', $tooltipStrings);

		return $tooltipData;
	}

	/**
	 * Check if a given association context string is handled by current Joomla version
	 *
	 * @param string $context the context to be checked
	 * @param boolean if true, the context follows Josetta convention and must first be converted to Joomla format
	 * @return boolean true if parameter matches an existing Joomla assocation context
	 */
	public static function hasJoomlaAssociation($context,
			$isJosettaContext = false)
	{
		if (version_compare(JVERSION, '3', 'ge'))
		{
			$prefix = '3';
		}
		else
		{
			$prefix = '2';
		}
		$context = $isJosettaContext ? self::context2Joomla($context) : $context;
		$hasJoomlaAssociation = in_array($context,
				self::$_joomlaAssociationContexts[$prefix]);
		return $hasJoomlaAssociation;
	}

	public static function context2Joomla($context)
	{
		switch ($context)
		{
			case 'com_menus_item':
			case 'com_contact_item':
			case 'com_content_item': /* case 'module_item': */
			case 'com_newsfeeds_item':
			case 'com_weblinks_item':
				$context = str_replace('_item', '.item', $context);
				break;
			case 'categories_item':
				$context = 'com_categories.item';
				break;
		}
		return $context;
	}

	public static function joomla2Context($context)
	{
		switch ($context)
		{
			case 'com_menus.item':
			case 'com_contact.item':
			case 'com_content.item': /* case 'module.item': */
			case 'com_newsfeeds.item':
			case 'com_weblinks.item':
				$context = str_replace('.item', '_item', $context);
				break;
			case 'com_categories.item':
				$context = 'categories_item';
				break;
		}
		return $context;
	}
}
