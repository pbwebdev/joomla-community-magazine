<?php
/**
 * Josetta - native multilingual manager for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     josetta
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     2.4.3.723
 * @date		2015-12-23
 *
 * build 2.4.3.723
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

// check shLib is available
if (!defined('SHLIB_VERSION'))
{
	$app = JFactory::getApplication();
	$app->JComponentTitle = '<div class="pagetitle"><h2>Josetta translations manager</h2></div>';
	$app
		->enqueuemessage('Josetta requires the shLib system plugin to be enabled, but you appear to have disabled it. Please enable it again!',
			'error');
	return;
}

// autoload our stuff
$path = JPATH_ROOT . '/administrator/components/com_josetta';
ShlSystem_Autoloader::registerPrefix('Josettaadmin', $path, $isPackage = false);
$path = JPATH_ROOT . '/components/com_josetta';
ShlSystem_Autoloader::registerPrefix('Josetta', $path, $isPackage = false);

$app = JFactory::getApplication();

// make sure we're displaying using our template
$app->setTemplate('atomic_josetta');

// Access check.
JosettaHelper_Acl::loadPermissions();
JosettaHelper::checkViewAccess($app->input->getCmd('view'));

// Execute the task.
$controller = ShlMvcController_Base::getInstance('Josetta');
$task = $app->input->getCmd('task');
$controller->execute($task);
$controller->redirect();
