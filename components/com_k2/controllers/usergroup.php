<?php
/**
 * @version             1..0.4 2011-08-05
 * @package             K2 - Users view
 * @author              Olivier Nolbert http://www.jiliko.net
 * @copyright           Copyright (c) 2009 - 2011 jiliko.net.
 * @license             GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

require_once(JPATH_COMPONENT.DS.'helpers'.DS.'users.php');

$doc =& JFactory::getDocument();
$doc->addStyleSheet(JURI::base().'components'.DS.'com_k2'.DS.'css'.DS.'k2_usersview.css');

class K2ControllerUsergroup extends K2Controller {

	function display() {

		$format=JRequest::getWord('format','html');
		$document =& JFactory::getDocument();
		$viewType = $document->getType();
		$view = &$this->getView('usergroup', $viewType);
		$model=&$this->getModel('users');
		$view->setModel($model);
        $user = &JFactory::getUser();
        if ($user->guest){
            $cache = true;
        }
        else {
            $cache = false;
        }
        parent::display($cache);
	}

}
?>
